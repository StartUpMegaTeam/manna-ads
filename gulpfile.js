const gulp = require('gulp');

const requireDir = require('require-dir');
const tasks = requireDir('./task');

exports.style = tasks.style;
exports.script = tasks.script;
exports.watch = tasks.watch;