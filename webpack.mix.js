const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.js('resources/js/app.js', 'public/js');
/* Combine JS */
mix.combine([
    'public/assets/plugins/jquery/1.11.1/jquery.min.js',
    'public/assets/plugins/jqueryui/1.9.2/jquery-ui.min.js',
    /* 'public/assets/plugins/popper.js/1.14.7/popper.min.js', */
    /* 'public/assets/bootstrap/js/bootstrap.min.js', */
    /* Bundled JS files (bootstrap.bundle.js and minified bootstrap.bundle.min.js) include Popper, but not jQuery. */
    'public/assets/bootstrap/js/bootstrap.bundle.min.js',
    'public/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.min.js',
    'public/assets/plugins/select2/js/select2.full.min.js',
    'public/assets/plugins/SocialShare/SocialShare.min.js',
    'public/assets/js/jquery.parallax-1.1.js',
    'public/assets/js/hideMaxListItem-min.js',
    'public/assets/plugins/jquery-nice-select/js/jquery.nice-select.min.js',
    'public/assets/plugins/jquery.nicescroll/dist/jquery.nicescroll.min.js',
    'public/assets/plugins/owlcarousel/owl.carousel.js',
    'public/assets/js/ajaxSetup.js',
    'public/assets/js/script.js',
    'public/assets/plugins/autocomplete/jquery.mockjax.js',
    'public/assets/plugins/autocomplete/jquery.autocomplete.min.js',
    'public/assets/js/app/autocomplete.cities.js',
    'public/assets/plugins/bootstrap-waitingfor/bootstrap-waitingfor.min.js',
    'public/assets/js/form-validation.js',
    'public/assets/js/app/show.phone.js',
    'public/assets/js/app/make.favorite.js',
    'public/js/jscroll/jquery.jscroll.min.js',

    'resources/js/app.js',
], 'public/js/app.js', true);

/* Disable Compilation Notification */
/* mix.disableNotifications(); */

/* Cache Busting */
mix.version();
