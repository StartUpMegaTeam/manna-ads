<div class="modal fade" id="quickLogin" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog col-md-6 col-xl-8 ">
        <div class="modal-content modal__content">

            <div class="modal__header">
                <h4 class="modal__title"> {{ t('log_in') }} </h4>

                <img class="modal__button-close" data-bs-dismiss="modal" src="{{asset('icon/login/close.svg')}}" alt="#">
            </div>

            <form role="form" method="POST" action="{{ \App\Helpers\UrlGen::login() }}">
                {!! csrf_field() !!}
                <input type="hidden" name="language_code" value="{{ config('app.locale') }}">
                <div class="modal__body">

                    @if (isset($errors) && $errors->any() && old('quickLoginForm')=='1')
                        <div class="alert alert-danger mb-2">
                            <ul class="modal__errors-list align-items-center">
                                <li>{{t('wrong_login_or_password')}}</li>
                            </ul>
                        </div>
                    @endif

                    @includeFirst([config('larapen.core.customizedViewPath') . 'auth.login.inc.social', 'auth.login.inc.social'], ['socialCol' => 12])
                    <?php $mtAuth = !socialLoginIsEnabled() ? '' : ''; ?>

                    <?php
                    $loginValue = (session()->has('login')) ? session('login') : old('login');
                    $loginField = getLoginField($loginValue);
                    if ($loginField == 'phone') {
                        $loginValue = $loginValue;
                    }
                    ?>


                        {{-- login --}}
                        <?php $loginError = (isset($errors) && $errors->has('login')) ? ' is-invalid' : ''; ?>
                        <div class="row">
                            <div class="col">
                                <div class="input{{ $mtAuth }} {{ $loginError }}">
                                    <label for="login" class="input__label">{{ t('login') }}</label>
                                    <div class="input-group">
                                        <input id="mLogin" name="login" type="text" placeholder="{{ t('login') }}"
                                               class="input-text{{ $loginError }}" value="{{ $loginValue }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- password --}}
                        <?php $passwordError = (isset($errors) && $errors->has('password')) ? ' is-invalid' : ''; ?>
                        <div class="row mb-2 mt-3">
                            <div class="col">
                                <div class="input {{ $passwordError }}">
                                    <label for="password" class="input__label">{{ t('password') }}</label>
                                    <div class="input-password show-pwd-group">
                                        <input id="mPassword" name="password" type="password"
                                               class="input-text{{ $passwordError }}"
                                               placeholder="{{ t('password') }}" autocomplete="off">
                                        <span class="icon-append show-pwd">
								<button type="button" class="eyeOfPwd js-password-hidden">
										<img class="input-password__icon" src="{{asset('icon/login/hidden.svg')}}"
                                             alt="#">
								</button>
							</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- remember --}}
                    <?php $rememberError = (isset($errors) && $errors->has('remember')) ? ' is-invalid' : ''; ?>
                    <div class="d-flex align-center justify-content-between mb-4">
                        <div class="input checkbox input-checkbox">
                            <input type="checkbox" value="1" name="remember" id="mRemember"
                                   class="checkbox__input{{ $rememberError }}">
                            <label class="checkbox__label" for="mRemember"
                                   style="font-weight: normal;">
                                <p class="login-lost-your"> {{ t('keep_me_logged_in') }}</p>
                            </label>
                        </div>
                        <div class=" float-end">
                            <a class="login-reset-password"
                               href="{{ url('password/reset') }}"> {{ t('lost_your_password') }} </a>
                        </div>
                    </div>

                    @include('layouts.inc.tools.captcha', ['label' => true])

                    <input type="hidden" name="quickLoginForm" value="1">

                    <div class="d-flex gap-3 col-12">
                        <button type="submit" class="col btn btn--success btn--height">{{ t('log_in') }}</button>
                        <button class="col btn btn--cancel btn--height"
                                data-bs-dismiss="modal">{{ t('Cancel') }}</button>
                    </div>

                    <div class="row login-box-btm text-center mb-0">
                        <div class="col">
                            <p>
                                {{ t('do_not_have_an_account') }}
                                <a href="{{ \App\Helpers\UrlGen::register() }}">{{ t('sign_up') }}</a>
                            </p>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col">
                            @component('components.account.social-login')@endcomponent
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
