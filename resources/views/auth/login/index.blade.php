{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="main-container">
        <div class="container">
            <div class="row  mb-3">

                @if (session()->has('flash_notification'))
                    <div class="col-12">
                        @include('flash::message')
                    </div>
                @endif

                @includeFirst([config('larapen.core.customizedViewPath') . 'auth.login.inc.social', 'auth.login.inc.social'], ['boxedCol' => 8])
                <?php $mtAuth = !socialLoginIsEnabled() ? ' mt-2' : ' mt-1'; ?>

                <div class="col-12 login-box{{ $mtAuth }}">

                    <form class="d-flex justify-content-center" id="loginForm" role="form" method="POST"
                          action="{{ url()->current() }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="country" value="{{ config('country.code') }}">
                        <div class="col-xl-5 col-md-6 col-10">

                            @if (isset($errors) && $errors->any())
                                <div class="col-12 mt-3">
                                    <div class="alert-danger">
                                        <ul class="alert-danger__list">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif

                            <div class="panel-intro">
                                <div class="d-flex justify-content-center">
                                    <h1>{{MetaTag::get('h1') ?? ''}}</h1>
                                </div>
                            </div>
                            <div class="card-body px-4">
                                <?php
                                $loginValue = (session()->has('login')) ? session('login') : old('login');
                                $loginField = getLoginField($loginValue);
                                if ($loginField == 'phone') {
                                    $loginValue = $loginValue;
                                }
                                ?>
                                {{-- login --}}
                                <?php $loginError = (isset($errors) && $errors->has('login')) ? ' is-invalid' : ''; ?>
                                <div class="row">
                                    <div class="col">

                                        <div class="mb-3 input {{ $loginError }}">
                                            <label for="login"
                                                   class="input__label">{{ t('login') }}</label>
                                            <div class="input-group">
                                                <input id="login" name="login" type="text"
                                                       placeholder="{{ t('login') }}"
                                                       class="input-text{{ $loginError }}" value="{{ $loginValue }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- password --}}
                                <?php $passwordError = (isset($errors) && $errors->has('password')) ? ' is-invalid' : ''; ?>
                                <div class="row mb-1">
                                    <div class="col">
                                        <div class="mb-1 input {{ $passwordError }}">
                                            <label for="password" class="input__label">{{ t('password') }}</label>
                                            <div class="input-password show-pwd-group">
                                                <input id="password" name="password" type="password"
                                                       class="input-text{{ $passwordError }}"
                                                       placeholder="{{ t('password') }}"
                                                       autocomplete="off">

                                                <span class="icon-append show-pwd">
											<button type="button" class="eyeOfPwd js-password-hidden">
												<img src="{{asset('icon/login/hidden.svg')}}" alt="#">
											</button>
										</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-center justify-content-between mb-4">
                                    <div class="input checkbox input-checkbox">
                                        <input class="checkbox__input" type="checkbox" value="1" name="remember"
                                               id="remember">
                                        <label class="checkbox__label" for="remember"
                                               style="font-weight: normal;">
                                            <p class="login-lost-your">{{ t('keep_me_logged_in') }}</p>
                                        </label>
                                    </div>
                                    <div class=" float-end">
                                        <a class="login-reset-password"
                                           href="{{ url('password/reset') }}"> {{ t('lost_your_password') }} </a>
                                    </div>
                                    {{--									<div style=" clear:both"></div>--}}
                                </div>

                                @include('layouts.inc.tools.captcha', ['noLabel' => true])

                                {{-- Submit --}}
                                <div class="d-flex gap-2 gap-md-3 col-12">
                                    <button id="loginBtn" class="col btn btn--success"> {{ t('log_in') }} </button>
                                </div>
                                <div class="row mb-2">
                                    <div class="col">
                                        <div class="login-box-btm text-center">
                                            <p>
                                                {{ t('do_not_have_an_account') }}
                                                <a href="{{ \App\Helpers\UrlGen::register() }}">{{ t('sign_up') }}</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        @component('components.account.social-login')@endcomponent
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                @component('auth.inc.deshbord')@endcomponent
            </div>
        </div>
    </div>
@endsection

@section('after_scripts')

    <script>
        $(document).ready(function () {
            $("#loginBtn").click(function () {
                $("#loginForm").submit();
                return false;
            });
        });
    </script>
@endsection
