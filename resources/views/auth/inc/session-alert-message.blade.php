<?php
$sessionAlertMessage = ['email', 'phone', 'login'];
?>

@foreach($sessionAlertMessage as $name)
    @if (session()->has($name))
        @if (session()->has($name))
            <div class="col-12">
                <div class="alert alert-danger alert-dismissible">
                    <p class="mb-0">{{ session($name) }}</p>
                    <button type="button" class="btn-close m-0 p-0" data-bs-dismiss="alert"
                            aria-label="{{ t('Close') }}"></button>
                </div>
            </div>
        @endif
    @endif
@endforeach

