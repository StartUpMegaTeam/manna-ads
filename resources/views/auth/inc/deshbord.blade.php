<div class="login-box login-benefits col-12">
    <div class="col login-benefits__block ">
        <div class="login-benefits__title mb-2">
            <img src="{{asset('icon/login/ready.svg')}}" alt="#">
            <h3>{{ t('post_a_classified_ad') }}</h3>
        </div>
        <p class="login-benefits__descriptions">
            {{ t('do_you_have_something_text', ['appName' => config('app.name')]) }}
        </p>
    </div>
    <div class="col login-benefits__block ">
        <div class="login-benefits__title mb-2">
            <img src="{{asset('icon/login/pencil.svg')}}" alt="#">
            <h3>{{ t('create_and_manage_items') }}</h3>
        </div>
        <p class="login-benefits__descriptions">
            {{ t('become_a_best_seller_or_buyer_text') }}
        </p>
    </div>
    <div class="col login-benefits__block ">
        <div class="login-benefits__title mb-2">
            <img src="{{asset('icon/login/heart.svg')}}" alt="#">
            <h3>{{ t('create_your_favorite_ads_list') }}</h3>
        </div>
        <p class="login-benefits__descriptions">
            {{ t('create_your_favorite_ads_list_text') }}
        </p>
    </div>
</div>
