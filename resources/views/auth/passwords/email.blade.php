{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @if (!(isset($paddingTopExists) and $paddingTopExists))
        <div class="p-0 mt-lg-4 mt-md-3 mt-3"></div>
    @endif
    <div class="main-container">
        <div class="container">
            <div class="row mb-4">

                @if (session()->has('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible d-flex align-items-center justify-content-between">
                            <p class="m-0 p-0">{{ session('status') }}</p>
                            <button type="button" class="btn-close m-0 p-0 position-relative" data-bs-dismiss="alert"
                                    aria-label="{{ t('Close') }}"></button>
                        </div>
                    </div>
                @endif

                @component('auth.inc.session-alert-message')@endcomponent

                @if (session()->has('flash_notification'))
                    <div class="col-12">
                        @include('flash::message')
                    </div>
                @endif

                <div class="col-12 d-flex justify-content-center login-box mt-2">
                    <div class="col-xl-4 col-md-6 col-10">

                        @if (isset($errors) && $errors->any())
                                <div class="alert-danger d-flex justify-content-center m-auto mt-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <ul class="alert-danger__list">
                                                @foreach ($errors->all() as $error)
                                                    @if($errors->has('login'))
                                                        <li>{{ t('invalid_email') }}</li>
                                                    @else
                                                        <li>{{$error}}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="panel-intro col-xl-12">
                                <div class="d-flex justify-content-center">
                                    <h1 class="register-title ">{{MetaTag::get('h1') ?? ''}}</h1>
                                </div>
                            </div>

                            <div class="">
                                <form class="" id="pwdForm" role="form" method="POST"
                                      action="{{ url('password/email') }}">
                                    {!! csrf_field() !!}

                                    {{-- login --}}
                                    <?php $loginError = (isset($errors) and $errors->has('login')) ? ' is-invalid' : ''; ?>
                                    <div class="mb-3 input">
                                        <label for="login"
                                               class="input__label">{{ t('email') }}</label>
                                        <input id="login"
                                               name="login"
                                               type="text"
                                               placeholder="{{ t('email') }}"
                                               class="input-text{{ $loginError }}"
                                               value="{{ old('login') }}"
                                        >
                                    </div>

                                    @include('layouts.inc.tools.captcha', ['noLabel' => true])

                                    {{-- Submit --}}
                                    <div class="login-button col mb-2">
                                        <button id="pwdBtn" type="submit"
                                                class="btn btn--success btn--height">{{ t('Repost') }}</button>
                                    </div>
                                </form>
                            </div>

                            <div class=" mb-5">
                                <a class="login-back" href="{{ \App\Helpers\UrlGen::login() }}"><img
                                            src="{{asset('icon/account/back.svg')}}"
                                            alt="#"> {{ t('back_to_the_log_in_page') }} </a>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row mb-4">
                @component('auth.inc.deshbord')@endcomponent
            </div>
        </div>
    </div>
@endsection

@section('after_scripts')
    <script>
        $(document).ready(function () {
            $("#pwdBtn").click(function () {
                $("#pwdForm").submit();
                return false;
            });
        });
    </script>
@endsection