{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}


@extends('layouts.master')

@section('content')
    @if (!(isset($paddingTopExists) and $paddingTopExists))
        <div class="p-0 mt-lg-4 mt-md-3 mt-3"></div>
    @endif
    <div class="main-container">
        <div class="container">
            <div class="row mb-4">

                @if (session()->has('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible d-flex align-items-center justify-content-between">
                            <p class="m-0 p-0">{{ session('status') }}</p>
                            <button type="button" class="btn-close m-0 p-0 position-relative" data-bs-dismiss="alert"
                                    aria-label="{{ t('Close') }}"></button>
                        </div>
                    </div>
                @endif

                @component('auth.inc.session-alert-message')@endcomponent

                @if (session()->has('flash_notification'))
                    <div class="col-12">
                        @include('flash::message')
                    </div>
                @endif

                <div class="col-12 d-flex justify-content-center login-box mt-2 height-40">
                    <div class="col-xl-5 col-md-8 col-10">

                        @if (isset($errors) && $errors->any())
                            <div class="alert-danger d-flex justify-content-center m-auto mt-3">
                                <div class="row">
                                    <div class="col-12">
                                        <ul class="alert-danger__list">
                                            @foreach ($errors->all() as $error)
                                                @if($errors->has('login'))
                                                    <li>{{ t('invalid_email_or_phone_number') }}</li>
                                                @else
                                                    <li>{{$error}}</li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="panel-intro col-xl-12">
                            <div class="d-flex justify-content-center">
                                <h2 class="register-title">{{ t('recover_password') }}</h2>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col d-flex gap-2 password-text-success"><img
                                        src="{{asset('icon/check-mark.svg')}}" alt="#">Инструкция по сбросу пароля была
                                отправлена на ваш email.
                            </div>
                        </div>

                            <div class="mb-5">
                                <a class="login-back" href="{{ URL::previous() }}"><img
                                        src="{{asset('icon/account/back.svg')}}"
                                        alt="#"> {{ t('back_to_the_log_in_page') }} </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                @component('auth.inc.deshbord')@endcomponent
            </div>
        </div>
    </div>
@endsection
