{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @if (!(isset($paddingTopExists) and $paddingTopExists))
        <div class="p-0 mt-lg-4 mt-md-3 mt-3"></div>
    @endif
    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('flash::message')
                </div>

                @if (session()->has('flash_notification'))
                    <div class="col-12">
                        @include('flash::message')
                    </div>
                @endif

                <div class="col-12 d-flex justify-content-center login-box mt-2">
                    <div class="col-xl-4 col-md-6 col-10">

                        @if (isset($errors) && $errors->any())
                            <div class="col-12 mt-3">
                                <div class="alert-danger ">
                                    <ul class="alert-danger__list">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        <div class="panel-intro col-xl-12">
                            <div class="d-flex justify-content-center">
                                <h1 class="register-title ">{{MetaTag::get('h1') ?? ''}}</h1>
                            </div>
                        </div>

                        <div class="">
                            <form method="POST" id="pwdForm" action="{{ url('password/reset') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="token" value="{{ $token }}">

                                {{-- login --}}
                                <?php $loginError = (isset($errors) and $errors->has('login')) ? ' is-invalid' : ''; ?>
                                <input type="hidden" name="login" value="{{ request()->get('email') }}"
                                       autocomplete="new-password"
                                       placeholder="{{ getLoginLabel() }}" class="input-text{{ $loginError }}">


                                {{-- password --}}
                                <?php $passwordError = (isset($errors) and $errors->has('password')) ? ' is-invalid' : ''; ?>
                                <div class="mb-3 input">
                                    <label for="password" class="input__label">{{ t('password') }}</label>
                                    <div class="input-password show-pwd-group">
                                        <input type="password"
                                               name="password"
                                               placeholder="{{t('Enter a new password')}}"
                                               id="password"
                                               autocomplete="new-password"
                                               class="email input-text{{ $passwordError }}">
                                        <div class="form-text">{{ t('at_least_6_characters') }}</div>
                                        <span class="icon-append show-pwd">
														<button type="button" class="eyeOfPwd js-password-hidden">
																<img src="{{asset('icon/login/hidden.svg')}}" alt="#">
														</button>
													</span>
                                    </div>
                                </div>

                                {{-- password_confirmation --}}
                                <?php $passwordError = (isset($errors) and $errors->has('password')) ? ' is-invalid' : ''; ?>
                                <div class="mb-3 input">
                                    <label for="password_confirmation"
                                           class="input__label">{{ t('Password Confirmation') }}</label>
                                    <div class="input-password show-pwd-group">
                                        <input type="password"
                                               name="password_confirmation"
                                               placeholder="{{t('Enter password')}}"
                                               id="password_confirmation"
                                               autocomplete="new-password"
                                               class="email input-text{{ $passwordError }}">
                                        <span class="icon-append show-pwd">
                                            <button type="button"
                                                    class="eyeOfPwc js-password-confirmation-hidden">
														<img src="{{asset('icon/login/hidden.svg')}}" alt="#">
                                             </button>
                                        </span>
                                    </div>
                                </div>

                                @include('layouts.inc.tools.captcha', ['noLabel' => true])

                                {{-- Submit --}}
                                <div class="login-button col mb-3">
                                    <button id="pwdBtn" type="submit"
                                            class="btn btn--success btn--height">{{ t('submit') }}</button>
                                </div>
                            </form>
                        </div>

                            <div class=" mb-5">
                                <a class="login-back" href="{{ \App\Helpers\UrlGen::login() }}"><img
                                            src="{{asset('icon/account/back.svg')}}"
                                            alt="#"> {{ t('back_to_the_log_in_page') }} </a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after_scripts')
    <script>
        $(document).ready(function () {
            $("#pwdBtn").click(function () {
                $("#pwdForm").submit();
                return false;
            });
        });
    </script>
@endsection