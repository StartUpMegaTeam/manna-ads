{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @include('common.spacer')
    <div class="main-container">
        <div class="container">
            <div class="row mb-3">

                <div class="login-box d-flex justify-content-center height-80">
                    <div class="row modal-message-success d-flex col col-md-7 col-xl-4 px-0 ">
                        <h4 class="register-title mt-4 text-center"> {{t('Email confirmation')}} </h4>
                        <p class="text-center m-0 mb-2">
                            На ваш email {{$userEmail}} было отправлено письмо с подтверждением.
                        </p>

                        <p class="text-center m-0 mb-4">Если письмо не пришло, Вы можете отправить его повторно.
                            <span class="js-send-code-link"></span>
                        </p>

                        <div class="mb-3">
                            <a id="urlResendEmail"
                               data-parent="{{$url}}"
                               class="js-send-code-btn col px-5 btn btn--resend-email btn--blocked ">
                                {{t('Re-send')}}
                            </a>
                        </div>

                        <div class="row">
                            <div class="have-account">
                                <span class="have-account__text">{{ t('have_account') }}</span>
                                <a href="{{ url('/login') }}"
                                   class="have-account__link ">{{ t('have_account_login') }}</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                @component('auth.inc.deshbord')@endcomponent
            </div>
        </div>
    </div>

    <script src="{{ asset('js/send-code.js') }}"></script>

@endsection
<?php
if (!session()->has('emailVerificationSent') && !session()->has('phoneVerificationSent')) {
    if (session()->has('message')) {
        session()->forget('message');
    }
}
?>
