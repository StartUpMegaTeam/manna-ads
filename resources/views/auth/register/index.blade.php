{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @if (!(isset($paddingTopExists) and $paddingTopExists))
        <div class="p-0 mt-lg-5 mt-md-3 mt-3"></div>
    @endif
    <div class="main-container">
        <div class="container">
            <div class="row">

                @if (session()->has('flash_notification'))
                    <div class="col-12">
                        @include('flash::message')
                    </div>
                @endif

                <div class="login-box mb-3">

                    <div class="col d-flex justify-content-center">

                        @includeFirst([config('larapen.core.customizedViewPath') . 'auth.login.inc.social', 'auth.login.inc.social'])
                        <?php $mtAuth = !socialLoginIsEnabled() ? '' : ''; ?>
                        <div class="content pt-4 col-xl-5 col-md-6 col-12{{ $mtAuth }}">

                            @if (isset($errors) && $errors->any())

                                <div class="alert-danger d-flex justify-content-center m-auto mt-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <h5 class="alert-danger__title">{{ t('oops_an_error_has_occurred') }}</h5>
                                            <ul class="alert-danger__list">
                                                @foreach ($errors->all() as $error)
                                                    <li><img src="{{asset('icon/login/close.svg')}}">{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="d-flex justify-content-center ">
                                <h1>{{MetaTag::get('h1') ?? ''}}</h1>
                            </div>

                            <div class="m-auto col-xl-12">
                                <form class="d-flex justify-content-center" id="signupForm" class="form-horizontal"
                                      method="POST" action="{{ url()->current() }}">
                                    {!! csrf_field() !!}
                                    <fieldset>

                                        {{-- name --}}
                                        <?php $nameError = (isset($errors) and $errors->has('name')) ? ' is-invalid' : ''; ?>
                                        <div class="row mb-3 required input {{ $nameError }}">
                                            <div class="col">
                                                <label class=" input__label">{{ t('Name') }} <sup>*</sup></label>
                                                <input name="name" placeholder="{{ t('your_name') }}"
                                                       class="input-text" type="text"
                                                       value="{{ old('name') }}">
                                            </div>
                                        </div>

                                        {{-- country_code --}}
                                        @if (empty(config('country.code')))
                                                <?php $countryCodeError = (isset($errors) and $errors->has('country_code')) ? ' is-invalid' : ''; ?>
                                            <div class="row mb-3 required input">
                                                <div class="col">
                                                    <label class="input__label{{ $countryCodeError }}"
                                                           for="country_code">{{ t('your_country') }}
                                                        <sup>*</sup></label>
                                                    <select id="countryCode" name="country_code"
                                                            class="form-control large-data-selecter{{ $countryCodeError }}">
                                                        <option value="0" {{ (!old('country_code') or old('country_code')==0) ? 'selected="selected"' : '' }}>{{ t('Select') }}</option>
                                                        @foreach ($countries as $code => $item)
                                                            <option value="{{ $code }}" {{ (old('country_code', (!empty(config('ipCountry.code'))) ? config('ipCountry.code') : 0)==$code) ? 'selected="selected"' : '' }}>
                                                                {{ $item->get('name') }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @else
                                            <input id="countryCode" name="country_code" type="hidden"
                                                   value="{{ config('country.code') }}">
                                        @endif

                                        @if (isEnabledField('phone'))
                                            {{-- phone --}}
                                                <?php $phoneError = (isset($errors) and $errors->has('phone')) ? ' is-invalid' : ''; ?>
                                            <div class="row mb-3 required input {{ $phoneError }}">
                                                <div class="col">
                                                    <label class="input__label">{{ t('phone_number_local') }}
                                                        @if (!isEnabledField('email'))
                                                            <sup>*</sup>
                                                        @endif
                                                    </label>
                                                    <input name="phone"
                                                           placeholder="{{ (!isEnabledField('email')) ? t('Mobile Phone Number') : t('phone_number_example') }}"
                                                           class="js-phone-mask input-text"
                                                           type="text"
                                                           value="{{ old('phone') }}"
                                                    >
                                                </div>
                                            </div>
                                        @endif

                                        @if (isEnabledField('email'))
                                            {{-- email --}}
                                                <?php $emailError = (isset($errors) and $errors->has('email')) ? ' is-invalid' : ''; ?>
                                            <div class="row mb-3 required input {{ $emailError }}">
                                                <div class="col">
                                                    <label class="input__label" for="email">{{ t('email') }}
                                                        <sup>*</sup>
                                                    </label>
                                                    <input id="email"
                                                           name="email"
                                                           type="email"
                                                           class="input-text"
                                                           placeholder="{{ t('email_example') }}"
                                                           value="{{ old('email') }}"
                                                    >
                                                </div>
                                            </div>
                                        @endif

                                        @if (isEnabledField('username'))
                                            {{-- username --}}
                                                <?php $usernameError = (isset($errors) and $errors->has('username')) ? ' is-invalid' : ''; ?>
                                            <div class="row mb-3 required input {{ $usernameError }}">
                                                <div class="col">
                                                    <label class="input__label"
                                                           for="email">{{ t('Username') }}</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="icon-user"></i>
                                                            </span>
                                                        </div>
                                                        <input id="username"
                                                               name="username"
                                                               type="text"
                                                               class="input-text"
                                                               placeholder="{{ t('Username') }}"
                                                               value="{{ old('username') }}"
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- password --}}
                                        <?php $passwordError = (isset($errors) and $errors->has('password')) ? ' is-invalid' : ''; ?>
                                        <div class="row mb-3 required input {{ $passwordError }}">
                                            <div class="col">
                                                <label class="input__label" for="password">{{ t('password') }}
                                                    <sup>*</sup></label>
                                                <div class="input-password show-pwd-group">
                                                    <input id="password" name="password" type="password"
                                                           class="input-text"
                                                           placeholder="{{ t('Create_a_password') }}"
                                                           autocomplete="off">
                                                    <div class="form-text">{{ t('at_least_6_characters') }}</div>
                                                    <span class="icon-append show-pwd">
														<button type="button" class="eyeOfPwd js-password-hidden">
																<img src="{{asset('icon/login/hidden.svg')}}" alt="#">
														</button>
													</span>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-4 required input {{ $passwordError }}">
                                            <div class="col">
                                                <label class="input__label"
                                                       for="password">{{ t('Password Confirmation') }}
                                                    <sup>*</sup></label>
                                                <div class="input-password show-pwd-group">
                                                    <input id="password_confirmation" name="password_confirmation"
                                                           type="password" class="input-text"
                                                           placeholder="{{ t('Confirm the password') }}"
                                                           autocomplete="off">
                                                    <span class="icon-append show-pwd">
														<button type="button"
                                                                class="eyeOfPwc js-password-confirmation-hidden">
																<img src="{{asset('icon/login/hidden.svg')}}" alt="#">
														</button>
													</span>
                                                </div>

                                            </div>

                                        </div>

                                        @include('layouts.inc.tools.captcha', ['colLeft' => 'col-md-4', 'colRight' => ''])

                                        {{-- accept_terms --}}
                                        <?php $acceptTermsError = (isset($errors) and $errors->has('accept_terms')) ? ' is-invalid' : ''; ?>
                                        <div class="row mb-1 required">
                                            <div class="col">
                                                <div class="checkbox input-checkbox">
                                                    <input name="accept_terms" id="acceptTerms"
                                                           class="checkbox__input{{ $acceptTermsError }}"
                                                           value="1"
                                                           type="checkbox" {{ (old('accept_terms')=='1') ? 'checked="checked"' : '' }}
                                                    >
                                                    <label class="checkbox__label" for="acceptTerms"
                                                           style="font-weight: normal;">
                                                        <p
                                                                class="input-checkbox__label-text">
                                                            {!! t('accept_terms_label', ['attributes' => getUrlPageBySlug('terms'), 'polit' => getUrlPageBySlug('privacy')]) !!}</p>
                                                    </label>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>

                                        {{-- accept_marketing_offers --}}

                                        <?php $acceptMarketingOffersError = (isset($errors) and $errors->has('accept_marketing_offers')) ? ' is-invalid' : ''; ?>
                                        <div class="row mb-4 required">
                                            <div class="col mb-2">
                                                <div class="checkbox input-checkbox">
                                                    <input name="accept_marketing_offers" id="acceptMarketingOffers"
                                                           class="checkbox__input{{ $acceptMarketingOffersError }}"
                                                           value="1"
                                                           type="checkbox" {{ (old('accept_marketing_offers')=='1') ? 'checked="checked"' : '' }}
                                                    >
                                                    <label class="checkbox__label" for="acceptMarketingOffers"
                                                           style="font-weight: normal;">
                                                        <p class="input-checkbox__label-text">
                                                            {!! t('accept_marketing_offers_label') !!}
                                                        </p>
                                                    </label>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>

                                        {{-- Button  --}}
                                        <div class="row mb-4">
                                            <div class="login-button register-button">
                                                <button id="signupBtn"
                                                        class="col btn btn--success btn--height"> {{ t('register') }} </button>
                                            </div>
                                        </div>

                                        <div class="row mb-4">
                                            <div class="have-account">
                                                <span class="have-account__text">{{ t('have_account') }}</span>
                                                <a class="have-account__link"
                                                   href="{{ url('/login') }}">{{ t('have_account_login') }}</a>
                                            </div>
                                        </div>

                                        <div class="row mb-5">
                                            <div class="col">
                                                @component('components.account.social-login')@endcomponent
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                @component('auth.inc.deshbord')@endcomponent
            </div>
        </div>

        @endsection

        @section('after_scripts')
            <script src="{{ asset('/js/validation/clear-is-invalid.js') }}"></script>

            <script>
                $(document).ready(function () {
                    /* Submit Form */
                    clearIsInvalid();

                    $("#signupBtn").click(function () {
                        $("#signupForm").submit();
                        return false;
                    });
                });
            </script>
@endsection