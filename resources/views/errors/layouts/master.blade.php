{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
<!DOCTYPE html>
<html lang="{{ ietfLangTag(config('app.locale')) }}"{!! (config('lang.direction')=='rtl') ? ' dir="rtl"' : '' !!}>
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="robots" content="noindex,nofollow">
	<meta name="googlebot" content="noindex">
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('larapen.core.favicon')) }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset(config('larapen.core.favicon_png')) }}">
    <title>@yield('title')</title>

    @if (file_exists(public_path('manifest.json')))
        <link rel="manifest" href="/manifest.json">
    @endif

    @yield('before_styles')

    @if (config('lang.direction') == 'rtl')
        <link href="https://fonts.googleapis.com/css?family=Cairo|Changa" rel="stylesheet">
        <link href="{{ url('css/app.rtl.css') }}" rel="stylesheet">
    @else
		<link href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ url('css/app.min.css') }}" rel="stylesheet">
    @endif
    @includeFirst([config('larapen.core.customizedViewPath') . 'layouts.inc.tools.style', 'layouts.inc.tools.style'])
    <link href="{{ url('css/custom.css') . getPictureVersion() }}" rel="stylesheet">

	<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>

    @yield('after_styles')
	
	@if (config('settings.style.custom_css'))
		{!! printCss(config('settings.style.custom_css')) . "\n" !!}
	@endif
	
	@if (config('settings.other.js_code'))
		{!! printJs(config('settings.other.js_code')) . "\n" !!}
	@endif

    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

	<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

	<script src="{{ url()->asset('assets/js/pace.min.js') }}"></script>
	<script src="{{ url()->asset('assets/plugins/modernizr/modernizr-custom.js') }}"></script>
	<script src="{{ asset('js/custom-select/custom-select.js') }}"></script>
	<script src="{{ asset('js/custom-alert/custom-alert.js') }}"></script>

	<script src="{{ asset('assets/plugins/jquery/3.3.1/jquery.min.js') }}"></script>


	<script>
		paceOptions = {
			elements: true
		};
	</script>
	<script src="{{ url('assets/js/pace.min.js') }}"></script>
	<script src="{{ asset('js/custom-select/custom-select.js') }}"></script>

</head>
<body class="{{ config('settings.style.app_skin') }}">

<div class="wrapper">

	@section('header')
		<x-header :isPostDetails="true"></x-header>
	@show

	@section('search')
	@show

	@yield('content')

	@section('info')
	@show


</div>

@section('modal_message')
@show
@includeWhen(!auth()->check(), 'auth.login.inc.modal')


@yield('before_scripts')

<script>
	{{-- Init. Root Vars --}}
	var siteUrl = '{{ url('/') }}';
	var languageCode = '<?php echo config('app.locale'); ?>';
    var countryCode = '<?php echo config('country.code', 0); ?>';

    {{-- Init. Translation Vars --}}
    var langLayout = {
        'hideMaxListItems': {
            'moreText': "{{ t('View More') }}",
            'lessText': "{{ t('View Less') }}"
        }
    };
</script>
<script src="{{ url('js/app.js') }}"></script>

@yield('after_scripts')

@if (config('settings.footer.tracking_code'))
    {!! printJs(config('settings.footer.tracking_code')) . "\n" !!}
@endif
</body>
</html>