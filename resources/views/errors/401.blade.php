{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('errors.layouts.master')

@section('title', t('Unauthorized action'))

@section('header')
    <x-header :isPostDetails="true" :showSearch="false"></x-header>
@endsection


@section('content')
    <x-error-page
            errorCode="401"
            :errorMessage="t('Unauthorized action')"
            :exception="$exception"
    ></x-error-page>
@endsection
