{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('search')
    @parent
@endsection

@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="container categories-page">

        <div class="categories-page-go-home">
            <a class="categories-page-go-home__title" href="{{ url('/') }}">
                <img src="{{ asset('icon/icon-link-arrow-accent.svg') }}">
                <span>{{ t('go_home') }}</span>
            </a>
        </div>

        <h1>{{MetaTag::get('h1') ?? ''}}</h1>


        <div class="categories-page__container">
            @foreach($cats3 as $col)
                <div class="categories-page__column-3">
                    @foreach($col as $cat)
                      <x-categories-page-item
                      :cat="$cat"
                      :subCats="$subCats">
                      </x-categories-page-item>
                    @endforeach
                </div>
            @endforeach
            @foreach($cats2 as $col)
                <div class="categories-page__column-2">
                    @foreach($col as $cat)
                        <x-categories-page-item
                                :cat="$cat"
                                :subCats="$subCats">
                        </x-categories-page-item>
                    @endforeach
                </div>
            @endforeach
            @foreach($cats1 as $cat)
                <div class="categories-page__column-1">
                    <x-categories-page-item
                            :cat="$cat"
                            :subCats="$subCats">
                    </x-categories-page-item>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('before_scripts')
    @parent
    <script>
        var maxSubCats = 15;
    </script>
@endsection
