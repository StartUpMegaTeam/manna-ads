{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')


@section('content')
	@includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])

	{{-- TODO Добавить хлебные крошки --}}

	<div class="main-container inner-page">
		<div class="container">
			<div class="container">
				<div class="text-page">
					<h1 class="text-page__title">
						{{ $page->title }}
					</h1>
					<div class="text-page__content">
						{!! $page->content !!}
					</div>
					<a href="{{ url()->previous() }}" class="text-page__btn btn btn--success">
						Назад
					</a>
				</div>
			</div>
		</div>
	</div>
@endsection
