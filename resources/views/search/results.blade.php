{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('search')
    @parent
    <div class="ads-search-container">
        <div class="container">
            @if(isset($count) && $count->get('all') > 0)
                <x-search :showSaveSearch="true" :count="$count->get('all')"></x-search>
            @else
                <x-search :showSaveSearch="true"></x-search>
            @endif
        </div>
    </div>
@endsection

@section('content')
    <div class="main-container">

        <div class="breadcrumbs-container">
            @includeFirst([config('larapen.core.customizedViewPath') . 'search.inc.breadcrumbs', 'search.inc.breadcrumbs'])
        </div>


        <div class="container categiries-header">
            <div class="categiries-header__info-container">
                <h1>{{MetaTag::get('h1') ?? ''}}</h1>
                <span class="categiries-header__ads-count">
                     {{ trans_choice('global.found_posts', getPlural($count->get('all')), ['count' => $count->get('all')], config('app.locale')) }}
                </span>
            </div>


            <div class="categiries-header__action">

                <?php $currDisplay = config('settings.listing.display_mode'); ?>

                <div class="categiries-header__set-view">
                    <a class="set-grid-view"
                       href="{!! qsUrl(request()->url(), array_merge(request()->except('display'), ['display'=>'grid']), null, false) !!}">
                        @if($currDisplay == 'make-grid')
                            @component('components.actions.make-grid-view', ['color' => '#535456'])@endcomponent
                        @else
                            @component('components.actions.make-grid-view', ['color' => '#ABB4C3'])@endcomponent
                        @endif
                    </a>
                    <a class="set-compact-view"
                       href="{!! qsUrl(request()->url(), array_merge(request()->except('display'), ['display'=>'list']), null, false) !!}">
                        @if($currDisplay == 'make-list')
                            @component('components.actions.make-list-view', ['color' => '#535456'])@endcomponent
                        @else
                            @component('components.actions.make-list-view', ['color' => '#ABB4C3'])@endcomponent
                        @endif
                    </a>
                    <a class="set-list-view"
                       href="{!! qsUrl(request()->url(), array_merge(request()->except('display'), ['display'=>'compact']), null, false) !!}">

                        @if($currDisplay == 'make-compact')
                            @component('components.actions.make-compact-view', ['color' => '#535456'])@endcomponent
                        @else
                            @component('components.actions.make-compact-view', ['color' => '#ABB4C3'])@endcomponent
                        @endif
                    </a>
                </div>

                <a id="show-filters" class="categiries-header__filter">
                    <img src="{{ asset('icon/filter-icon/mobile-filter.svg') }}">
                </a>

                <div class="categiries-header__sort">
                    <x-search-filter></x-search-filter>
                </div>

                <div class="categiries-header__set-view--mobile">
                    <x-change-posts-view></x-change-posts-view>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">

                {{--                 Sidebar--}}
                @includeFirst([config('larapen.core.customizedViewPath') . 'search.inc.sidebar', 'search.inc.sidebar'])

                {{--                 Content--}}
                <div class="col-md-8 col-xl-9 mb-4">
                    <div class="scrolling-pagination">
                        @if($posts && count($posts) > 0)
                            @if (config('settings.listing.display_mode') == 'make-list')
                                <div class="search-posts--list">
                                    @includeFirst([config('larapen.core.customizedViewPath') . 'search.inc.posts.template.list', 'search.inc.posts.template.list'])
                                </div>

                            @elseif (config('settings.listing.display_mode') == 'make-compact')
                                <div class="search-posts--compact">
                                    @includeFirst([config('larapen.core.customizedViewPath') . 'search.inc.posts.template.compact', 'search.inc.posts.template.compact'])
                                </div>
                            @else
                                <div class="search-posts--grid">
                                    @includeFirst([config('larapen.core.customizedViewPath') . 'search.inc.posts.template.grid', 'search.inc.posts.template.grid'])
                                </div>
                            @endif

                            @if ($posts->hasPages())
                                <nav class="" aria-label="">
                                    {!! $posts->appends(request()->query())->links() !!}
                                </nav>
                            @endif
                        @else
                            @component('components.post-cards.not-found')@endcomponent
                        @endif
                    </div>
                </div>
            </div>
        </div>

        {{-- Show Posts Tags --}}
        @if (config('settings.listing.show_posts_tags'))
            @if (isset($tags) && !empty($tags))
                <div class="container">
                    <div class="card mb-3">
                        <div class="card-body">
                            <h2 class="card-title"><i class="icon-tag"></i> {{ t('Tags') }}:</h2>
                            @foreach($tags as $iTag)
                                <span class="d-inline-block border border-inverse bg-light rounded-1 py-1 px-2 my-1 me-1">
									<a href="{{ \App\Helpers\UrlGen::tag($iTag) }}">
										{{ $iTag }}
									</a>
								</span>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        @endif

    </div>

@endsection

@section('modal_location')
    @includeFirst([config('larapen.core.customizedViewPath') . 'layouts.inc.modal.location', 'layouts.inc.modal.location'])
@endsection

@section('after_scripts')
    <script>
        $('ul.pagination').hide();

        $('.scrolling-pagination').jscroll({
            autoTrigger: true,
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.scrolling-pagination',
            loadingHtml: '<div class="stage"> <div class="dot-typing"></div> </div>',
            callback: function () {
                $('ul.pagination').remove();
                $('.make-favorite').off('click');
                $('.make-favorite').click(function () {
                    savePost(this, false);
                });
            }
        });

        $(document).ready(function () {
            $('#postType a').click(function (e) {
                e.preventDefault();
                var goToUrl = $(this).attr('href');
                redirect(goToUrl);
            });

            $('#show-filters').click(function () {
                $('.search-sidebar').toggleClass("open");
            });

            $('.search-sidebar__close-filters').click(function () {
                $('.search-sidebar').removeClass("open");
            });

            $('#orderBy').change(function () {
                var goToUrl = $(this).val();
                redirect(goToUrl);
            });
        });
    </script>
@endsection
