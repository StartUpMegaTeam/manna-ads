<div class="col-md-4 col-xl-3">
    <aside>
        <div class="search-sidebar">
            @includeFirst([config('larapen.core.customizedViewPath') . 'search.inc.sidebar.categories', 'search.inc.sidebar.categories'])

            @includeFirst([config('larapen.core.customizedViewPath') . 'search.inc.sidebar.fields', 'search.inc.sidebar.fields'])
        </div>

        <div class="promo-card">
            <span class="promo-card__title">
                {{ t('promo_title') }}
            </span>
            <span class="promo-card__text">
                {{ t('promo_text') }}
            </span>
            <x-promo-button></x-promo-button>
        </div>
    </aside>
</div>

@section('after_scripts')
    @parent
    <script>
        var baseUrl = '{{ request()->url() }}';
    </script>
@endsection