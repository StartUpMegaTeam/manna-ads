{{-- Category --}}
<div class="search-sidebar__header">
    <a class="search-sidebar__title" href="{{ url('/') }}">
        <img src="{{ asset('icon/icon-link-arrow-accent.svg') }}">
        <span>{{ t('go_home') }}</span>
    </a>

    <a class="search-sidebar__close-filters">
        <img src="{{ asset('icon/search-icon/close-filter-menu.svg') }}">
    </a>
</div>

<div class="search-sidebar__divider search-sidebar__divider--first mobile">
</div>

@if(isset($cats))
    <div class="search-sidebar__categories--scroll custom-scroll-bar">
        @include('search.inc.sidebar.categories.children-categories', ['categories' => $cats, 'mainCategory' => true])
    </div>
@endif

