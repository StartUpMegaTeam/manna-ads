<ul class="@if(!$mainCategory) ps-2 @endif categories-bar">
    @foreach($categories as $category)
        <li class="categories-bar__item" id="category{{$category['id']}}">
            @if(count($category['children']) > 0)
                <div class="categories-bar__header" id="{{$category['id']}}title">
                    <img class="search-sidebar-arrow"
                         src="{{ asset('icon/icon-select.svg') }}" alt="#">
                    <div class="categories-bar__content">
                        <a href="{{ url('category/' . (array_key_exists('parent' ,$category) ? $category['parent']['slug'] . '/' . $category['slug'] : $category['slug']) . (!is_null(request()->get('l')) ? ( '?l=' . request()->get('l')) : '')) }}"
                           class="categories-bar__title @if(isset($cat) && $cat->id == $category['id']) categories-bar__title--active @endif">
                            {{ $category['name'] }}
                        </a>
                    </div>
                </div>
                @include('search.inc.sidebar.categories.children-categories', ['categories' => $category['children'], 'mainCategory' => false])
            @else
                <div class="categories-bar__content ps-3">
                    <a class="categories-bar__title @if(isset($cat) && $cat->id == $category['id']) categories-bar__title--active @endif"
                       href="{{ url('category/' . (array_key_exists('parent' ,$category) ? $category['parent']['slug'] . '/' . $category['slug'] : $category['slug']) . (!is_null(request()->get('l')) ? ( '?l=' . request()->get('l')) : '')) }}">
                        {{ $category['name'] }}
                    </a>
                </div>
            @endif
        </li>
    @endforeach
</ul>