<?php
// Clear Filter Button
$clearFilterBtn = \App\Helpers\UrlGen::getPriceFilterClearLink($cat ?? null, $city ?? null);
?>
@if (isset($cat) and !empty($cat))
    @if (!in_array($cat->type, ['not-salable']))
        {{-- Price --}}

        <div class="search-sidebar__divider">
        </div>


        <div class="search-sidebar__price">
            <span class="search-filter__price-title">
                {{ (!in_array($cat->type, ['job-offer', 'job-search'])) ? t('price_range') : t('salary_range') }}
            </span>


        </div>


{{--        <div class="block-title has-arrow sidebar-header">--}}
{{--            <h5>--}}
{{--                <span class="fw-bold">--}}
{{--                    {{ (!in_array($cat->type, ['job-offer', 'job-search'])) ? t('price_range') : t('salary_range') }}--}}
{{--                </span> {!! $clearFilterBtn !!}--}}
{{--            </h5>--}}
{{--        </div>--}}
        <div class="block-content list-filter">
            <form role="form" class="form-inline" action="{{ request()->url() }}" method="GET">
                @foreach(request()->except(['page', 'minPrice', 'maxPrice', '_token']) as $key => $value)
                    @if (is_array($value))
                        @foreach($value as $k => $v)
                            @if (is_array($v))
                                @foreach($v as $ik => $iv)
                                    @continue(is_array($iv))
                                    <input type="hidden" name="{{ $key.'['.$k.']['.$ik.']' }}" value="{{ $iv }}">
                                @endforeach
                            @else
                                <input type="hidden" name="{{ $key.'['.$k.']' }}" value="{{ $v }}">
                            @endif
                        @endforeach
                    @else
                        <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                    @endif
                @endforeach
                <div class="filter-price">
                    <div class="filter-price__input-container">
                        <div class="input">
                            <input type="number" min="0" id="minPrice" name="minPrice" class="form-control"
                                   placeholder="{{ t('Min') }}" value="{{ request()->get('minPrice') }}">
                        </div>
                        <div class="input">
                            <input type="number" min="0" id="maxPrice" name="maxPrice" class="form-control"
                                   placeholder="{{ t('Max') }}" value="{{ request()->get('maxPrice') }}">
                        </div>
                    </div>
                    <div class="">
                        <button class="search-filter__submit" type="submit">
                            <img src="{{ asset('icon/icon-link-arrow-inactive.svg') }}">
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div style="clear:both"></div>

        <script>
            $(document).ready(function() {
                $('.search-filter__submit').prop('disabled', true);

                $('#minPrice').on('keyup input', function () {
                    if($('#minPrice').val().length > 0 && $('#maxPrice').val().length > 0) {
                    $('.search-filter__submit').addClass('active');
                        $('.search-filter__submit').prop('disabled', false);
                    } else {
                        $('.search-filter__submit').prop('disabled', true);
                        $('.search-filter__submit').removeClass('active');
                    }
                });
                $('#maxPrice').on('keyup input', function () {
                    if($('#minPrice').val().length > 0 && $('#maxPrice').val().length > 0) {
                        $('.search-filter__submit').addClass('active');
                        $('.search-filter__submit').prop('disabled', false);
                    } else {
                        $('.search-filter__submit').prop('disabled', true);
                        $('.search-filter__submit').removeClass('active');
                    }
                });
            });
        </script>
    @endif
@endif