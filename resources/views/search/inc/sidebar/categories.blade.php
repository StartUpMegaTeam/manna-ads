<?php
// Clear Filter Button
if (isset($cat)) {
    $clearFilterBtn = \App\Helpers\UrlGen::getCategoryFilterClearLink($cat ?? null, $city ?? null);
}
?>

@php
    $httpQuery = http_build_query(request()->all());
    $getParams = strlen($httpQuery) > 0 ? '?' . $httpQuery : '';
@endphp

@includeFirst([config('larapen.core.customizedViewPath') . 'search.inc.sidebar.categories.root', 'search.inc.sidebar.categories.root'])

<script>
    @if(isset($cat))
        openCategory({{$cat->id}});
    @endif

    $('.categories-bar__item').on('click', function (e) {
        e.stopPropagation();
        closeAllCategory($(this));
        $(this).toggleClass('categories-bar__item--open');
    });

    $('.categories-bar__title').on('click', function (e){
        e.stopPropagation();
    });

    function closeAllCategory(currentCategory) {
        currentCategory.closest('.categories-bar').find('.categories-bar__item').not(currentCategory).removeClass('categories-bar__item--open');
    }

    function openCategory(categoryId = null) {
        if (categoryId === null) {
            return;
        }

        let categoryItem = $('.categories-bar__item[id="category' + categoryId + '"]');
        categoryItem.addClass('categories-bar__item--open');
        categoryItem.parents('.categories-bar__item').addClass('categories-bar__item--open');
    }
</script>