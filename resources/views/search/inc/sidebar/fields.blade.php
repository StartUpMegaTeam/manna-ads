<form id="cfForm" role="form" class="form d-flex flex-column" action="{{ request()->url() }}" method="GET">

    <div class="search-sidebar__divider">
    </div>
    <div class="search-sidebar__cities">
        <x-city-filter></x-city-filter>
    </div>


    @if (isset($cat) and !empty($cat))
        @if (!in_array($cat->type, ['not-salable']))

            <div class="search-sidebar__divider">
            </div>

            <div class="search-sidebar__block custom-scroll-bar">
                <span class="search-filter__title">
                    {{ (!in_array($cat->type, ['job-offer', 'job-search'])) ? t('price_range') : t('salary_range') }}
                </span>
                <div class="search-filter">
                    <div class="input range">
                        <input type="number" form="globalSearch" min="0" id="minPrice" name="minPrice"
                               class="input range"
                               placeholder="{{ t('Min') }}" value="{{ request()->get('minPrice') }}">
                    </div>
                    <div class="input range">
                        <input type="number" form="globalSearch" min="0" id="maxPrice" name="maxPrice"
                               class="input range"
                               placeholder="{{ t('Max') }}" value="{{ request()->get('maxPrice') }}">
                    </div>
                </div>
            </div>
        @endif
    @endif

    @if (isset($customFields) and $customFields->count() > 0)
            <?php
            $disabledFieldsTypes = ['file', 'video'];
            $clearFilterBtn = '';
            $firstFieldFound = false;

            ?>

        @foreach($customFields as $field)
            @continue(in_array($field->type, $disabledFieldsTypes) or $field->use_as_filter != 1)
                <?php
                // Fields parameters
                $fieldId = 'cf.' . $field->id;
                $fieldName = 'cf[' . $field->id . ']';
                $fieldOld = 'cf.' . $field->id;

                // Get the default value
                if ($field->type === 'number_range' || $field->type === 'float_number_range') {
                    $defaultValue = (request()->filled($fieldOld)) ? request()->input($fieldOld) : [];
                } else {
                    $defaultValue = (request()->filled($fieldOld)) ? request()->input($fieldOld) : $field->default_value;
                }

                // Field Query String
                $fieldQueryString = '<input type="hidden" form="globalSearch" id="cf' . $field->id . 'QueryString" value="' . httpBuildQuery(request()->except(['page', $fieldId])) . '">';

                // Clear Filter Button
                $clearFilterBtn = \App\Helpers\UrlGen::getCustomFieldFilterClearLink($fieldOld, $cat ?? null, $city ?? null);
                ?>

            @if (in_array($field->type, ['text', 'textarea', 'url', 'number']))

                {{-- text --}}

                <div class="search-sidebar__divider">
                </div>

                <div class="search-sidebar__block">
                    <span class="search-filter__title">
                        {{ $field->name }}
                    </span>

                    <div class="input">
                        <input id="{{ $fieldId }}"
                               name="{{ $fieldName }}"
                               form="globalSearch"
                               type="{{ ($field->type == 'number') ? 'number' : 'text' }}"
                               placeholder="{{ $field->name }}"
                               class="input"
                               value="{{ strip_tags($defaultValue) }}"{!! ($field->type == 'number') ? ' autocomplete="off"' : '' !!}
                        >
                    </div>
                </div>

                {!! $fieldQueryString !!}

            @endif
            @if ($field->type == 'checkbox')

                {{-- checkbox --}}

                <div class="search-sidebar__divider">
                </div>
                <div class="search-sidebar__block search-sidebar__block--checkbox">
                    <div class="search-filter input">
                        <input name="{{ $fieldName }}"
                               value="0"
                               form="globalSearch"
                               type="hidden"
                        >
                        <div class="search-filter__input-container input-switch">
                            <label class="switch">
                                <input id="{{ $fieldId }}"
                                       name="{{ $fieldName }}"
                                       value="1"
                                       form="globalSearch"
                                       type="checkbox"
                                       class="checkbox"
                                        {{ ($defaultValue == '1') ? 'checked="checked"' : '' }}
                                >
                                <span class="slidere round"></span>
                            </label>
                            <label class="untouchable form-check-label" for="{{ $fieldId }}">
                                {{ $field->name }}
                            </label>
                        </div>
                    </div>
                </div>
                {!! $fieldQueryString !!}
                <div style="clear:both"></div>

            @endif

            @if ($field->type == 'number_range')

                <div class="search-sidebar__divider">
                </div>

                <div class="search-sidebar__block">
                    <span class="search-filter__title">
                        {{ $field->name }}
                    </span>

                    <div class="search-filter">
                        <div class="input range">
                            <input type="number" min="0" id="{{ $fieldId }}" name="{{$fieldName}}[0]"
                                   class="input range"
                                   form="globalSearch"
                                   placeholder="{{ t('Min') }}"
                                   value="{{ $defaultValue === [] ? '' : strip_tags($defaultValue[0]) }}">
                        </div>
                        <div class="input range">
                            <input type="number" min="0" id="{{ $fieldId }}" name="{{$fieldName}}[1]"
                                   class="input range"
                                   form="globalSearch"
                                   placeholder="{{ t('Max') }}"
                                   value="{{ $defaultValue === [] ? '' : strip_tags($defaultValue[1]) }}">
                        </div>
                    </div>
                </div>
            @endif

            @if ($field->type == 'float_number_range')

                <div class="search-sidebar__divider">
                </div>

                <div class="search-sidebar__block">
                    <span class="search-filter__title">
                        {{ $field->name }}
                    </span>

                    <div class="search-filter">
                        <div class="input range">
                            <input type="number" step="0.01" min="0" id="{{ $fieldId }}" name="{{$fieldName}}[0]"
                                   class="input range float-number"
                                   form="globalSearch"
                                   placeholder="{{ t('Min') }}"
                                   value="{{ $defaultValue === [] ? '' : strip_tags($defaultValue[0]) }}">
                        </div>
                        <div class="input range">
                            <input type="number" step="0.01" min="0" id="{{ $fieldId }}" name="{{$fieldName}}[1]"
                                   class="input range float-number"
                                   form="globalSearch"
                                   placeholder="{{ t('Max') }}"
                                   value="{{ $defaultValue === [] ? '' : strip_tags($defaultValue[1]) }}">
                        </div>
                    </div>
                </div>
            @endif

            @if ($field->type == 'checkbox_multiple')

                @if ($field->options->count() > 0)
                    {{-- checkbox_multiple --}}
                    <div class="search-sidebar__divider">
                    </div>

                    <div class="search-sidebar__block">
                        <span class="search-filter__title">
                            {{ $field->name }}
                        </span>

                        <div class="input">
                                <?php $cmFieldStyle = ($field->options->count() > 12) ? 'search-filter__scroll-bar' : ''; ?>
                            <div class="checkbox-container custom-scroll-bar {!! $cmFieldStyle !!}">
                                @foreach ($field->options as $option)
                                        <?php
                                        // Get the default value
                                        $defaultValue = (request()->filled($fieldOld . '.' . $option->id))
                                            ? request()->input($fieldOld . '.' . $option->id)
                                            : (
                                            (is_array($field->default_value) && isset($field->default_value[$option->id], $field->default_value[$option->id]->value))
                                                ? $field->default_value[$option->id]->value
                                                : $field->default_value
                                            );

                                        // Field Query String
                                        $fieldQueryString = '<input form="globalSearch" type="hidden" id="cf' . $field->id . $option->id . 'QueryString"
									value="' . httpBuildQuery(request()->except(['page', $fieldId . '.' . $option->id])) . '">';
                                        ?>
                                    <div class="input">
                                        <div class="checkbox input-checkbox">
                                            <input name="{{ $fieldName . '[' . $option->id . ']' }}"
                                                   value="0"
                                                   type="hidden"
                                                   form="globalSearch"
                                            >
                                            <input id="{{ $fieldId . '.' . $option->id }}"
                                                   class="checkbox__input"
                                                   form="globalSearch"
                                                   name="{{ $fieldName . '[' . $option->id . ']' }}"
                                                   value="{{ $option->id }}"
                                                   type="checkbox"
                                                    {{ ($defaultValue == $option->id) ? 'checked="checked"' : '' }}
                                            >
                                            <label class="untouchable checkbox__label checkbox__label--filter"
                                                   for="{{ $fieldId . '.' . $option->id }}">
                                                {{ $option->value }}
                                            </label>
                                        </div>
                                    </div>
                                    {!! $fieldQueryString !!}
                                @endforeach
                            </div>
                        </div>
                    </div>

                @endif

            @endif
            @if ($field->type == 'radio')

                @if ($field->options->count() > 0)
                    {{-- radio --}}

                    <div class="search-sidebar__divider">
                    </div>

                    <div class="search-sidebar__block">
                        <span class="search-filter__title">
                            {{ $field->name }}
                        </span>

                        <div class="">
                                <?php $rFieldStyle = ($field->options->count() > 12) ? 'search-filter__scroll-bar' : ''; ?>
                            <div class="input search-sidebar__radio custom-scroll-bar {!! $rFieldStyle !!}">
                                @foreach ($field->options as $option)
                                    <div class="input-switch">
                                        <label class="switch">
                                            <input id="{{ $fieldId . '.' . $option->id }}"
                                                   name="{{ $fieldName }}"
                                                   value="{{ $option->id }}"
                                                   type="radio"
                                                   form="globalSearch"
                                                   class="input-text"
                                                    {{ ($defaultValue == $option->id) ? 'checked="checked"' : '' }}
                                            >
                                            <span class="slidere round"></span>
                                        </label>
                                        <label class="untouchable form-check-label"
                                               for="{{ $fieldId . '.' . $option->id }}">
                                            {{ $option->value }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    {!! $fieldQueryString !!}
                @endif

            @endif
            @if ($field->type == 'select')

                {{-- select --}}

                <div class="search-sidebar__divider">
                </div>

                <div class="search-sidebar__block">
                        <span class="search-filter__title">
                            {{ $field->name }}
                        </span>

                    <div class="input">
                        <div class="input-select-2">
                                <?php
                                $select2Type = ($field->options->count() <= 10) ? 'selecter-' . $field->id : 'large-data-selecter-' . $field->id;
                                ?>
                            <select id="{{ $fieldId }}" form="globalSearch" name="{{ $fieldName }}"
                                    class="{{ $select2Type }}">
                                <option value=""
                                        @if (old($fieldOld) == '' or old($fieldOld) == 0)
                                            selected="selected"
                                        @endif
                                >
                                    {{ t('Select') }}
                                </option>
                                @if ($field->options->count() > 0)
                                    @foreach ($field->options as $option)
                                        <option value="{{ $option->id }}"
                                                @if ($defaultValue == $option->id)
                                                    selected="selected"
                                                @endif
                                        >
                                            {{ $option->value }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="select-dropdown-{{$select2Type}}"></div>
                        </div>
                    </div>
                </div>

                <script>
                    $(document).ready(function () {

                        $('.selecter-{{$field->id}}').select2({
                            width: '100%',
                            dropdownAutoWidth: 'true',
                            dropdownParent: $('.select-dropdown-{{$select2Type}}')
                        });

                        {{-- Searchable Select Boxes --}}
                        $('.large-data-selecter-{{$field->id}}').select2({
                            width: '100%',
                            dropdownAutoWidth: 'true',
                            dropdownParent: $('.select-dropdown-{{$select2Type}}')
                        });
                    });

                </script>

                {!! $fieldQueryString !!}

            @endif

            @if ($field->type == 'select_multi')

                @if ($field->options->count() > 0)
                    {{-- select_multi --}}
                    <div class="search-sidebar__divider">
                    </div>

                    <div class="search-sidebar__block">
                        <span class="search-filter__title">
                            {{ $field->name }}
                        </span>

                        <div class="input">
                                <?php $cmFieldStyle = ($field->options->count() > 12) ? 'search-filter__scroll-bar' : ''; ?>
                            <div class="checkbox-container custom-scroll-bar {!! $cmFieldStyle !!}">
                                @foreach ($field->options as $option)
                                        <?php
                                        // Get the default value
                                        $defaultValue = (request()->filled($fieldOld . '.' . $option->id))
                                            ? request()->input($fieldOld . '.' . $option->id)
                                            : (
                                            (is_array($field->default_value) && isset($field->default_value[$option->id], $field->default_value[$option->id]->value))
                                                ? $field->default_value[$option->id]->value
                                                : $field->default_value
                                            );

                                        // Field Query String
                                        $fieldQueryString = '<input form="globalSearch" type="hidden" id="cf' . $field->id . $option->id . 'QueryString"
									value="' . httpBuildQuery(request()->except(['page', $fieldId . '.' . $option->id])) . '">';
                                        ?>
                                    <div class="input">
                                        <div class="checkbox input-checkbox">
                                            <input name="{{ $fieldName . '[' . $option->id . ']' }}"
                                                   value="0"
                                                   form="globalSearch"
                                                   type="hidden"
                                            >
                                            <input id="{{ $fieldId . '.' . $option->id }}"
                                                   class="checkbox__input"
                                                   form="globalSearch"
                                                   name="{{ $fieldName . '[' . $option->id . ']' }}"
                                                   value="{{ $option->id }}"
                                                   type="checkbox"
                                                    {{ ($defaultValue == $option->id) ? 'checked="checked"' : '' }}
                                            >
                                            <label class="untouchable checkbox__label checkbox__label--filter"
                                                   for="{{ $fieldId . '.' . $option->id }}">
                                                {{ $option->value }}
                                            </label>
                                        </div>
                                    </div>
                                    {!! $fieldQueryString !!}
                                @endforeach
                            </div>
                        </div>
                    </div>

                @endif
            @endif

            @if (in_array($field->type, ['date', 'date_time', 'date_range']))

                <div class="search-sidebar__divider">
                </div>

                <div class="search-sidebar__block">
                        <span class="search-filter__title">
                            {{ $field->name }}
                        </span>

                    {{-- date --}}
                        <?php
                        $datePickerClass = '';
                        if (in_array($field->type, ['date', 'date_time'])) {
                            $datePickerClass = ' cf-date';
                        }
                        if ($field->type == 'date_range') {
                            $datePickerClass = ' cf-date_range';
                        }
                        ?>
                    <div id="daterange-container">

                    </div>

                    <div class="block-content list-filter">
                        <div class="input">
                            <input id="{{ $fieldId }}"
                                   name="{{ $fieldName }}"
                                   type="text"
                                   form="globalSearch"
                                   placeholder="{{ $field->name }}"
                                   class="input{{ $datePickerClass }}"
                                   value="{{ strip_tags($defaultValue) }}"
                                   autocomplete="off"
                            >
                        </div>
                    </div>
                </div>


                {!! $fieldQueryString !!}

            @endif

        @endforeach
    @endif

    <div class="search-sidebar__divider">
    </div>

    <div class="search-sidebar__block">
        <button type="submit" form="globalSearch"
                class="search-sidebar__submit btn btn--success btn--height">{{ t('go') }}</button>
    </div>

    <a class="search-sidebar__clear untouchable" href="{{ request()->url() }}">{{ t('clear_filters') }}</a>

</form>

@section('after_styles')
    <link href="{{ url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
@endsection
@section('after_scripts')
    @parent
    <script src="{{ url('assets/plugins/momentjs/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"
            type="text/javascript"></script>
    <script>

        $(document).ready(function () {
            /*
             * Custom Fields Date Picker
             * https://www.daterangepicker.com/#options
             */
            {{-- Single Date --}}
            $('#cfForm .cf-date').daterangepicker({
                autoUpdateInput: false,
                autoApply: true,
                showDropdowns: true,
                drops: "auto",
                minYear: parseInt(moment().format('YYYY')) - 100,
                maxYear: parseInt(moment().format('YYYY')) + 20,
                locale: {
                    format: '{{ t('datepicker_format') }}',
                    applyLabel: "{{ t('datepicker_applyLabel') }}",
                    cancelLabel: "{{ t('datepicker_cancelLabel') }}",
                    fromLabel: "{{ t('datepicker_fromLabel') }}",
                    toLabel: "{{ t('datepicker_toLabel') }}",
                    customRangeLabel: "{{ t('datepicker_customRangeLabel') }}",
                    weekLabel: "{{ t('datepicker_weekLabel') }}",
                    daysOfWeek: [
                        "{{ t('datepicker_monday') }}",
                        "{{ t('datepicker_tuesday') }}",
                        "{{ t('datepicker_wednesday') }}",
                        "{{ t('datepicker_thursday') }}",
                        "{{ t('datepicker_friday') }}",
                        "{{ t('datepicker_saturday') }}",
                        "{{ t('datepicker_sunday') }}",
                    ],
                    monthNames: [
                        "{{ t('January') }}",
                        "{{ t('February') }}",
                        "{{ t('March') }}",
                        "{{ t('April') }}",
                        "{{ t('May') }}",
                        "{{ t('June') }}",
                        "{{ t('July') }}",
                        "{{ t('August') }}",
                        "{{ t('September') }}",
                        "{{ t('October') }}",
                        "{{ t('November') }}",
                        "{{ t('December') }}"
                    ],
                    firstDay: 0
                },
                parentEl: '#daterange-container',
                singleDatePicker: true,
                startDate: moment().format('{{ t('datepicker_format') }}')
            });
            $('#cfForm .cf-date').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('{{ t('datepicker_format') }}'));
            });

            {{-- Date Range --}}
            $('#cfForm .cf-date_range').daterangepicker({
                autoUpdateInput: false,
                autoApply: true,
                showDropdowns: false,
                drops: "auto",
                minYear: parseInt(moment().format('YYYY')) - 100,
                maxYear: parseInt(moment().format('YYYY')) + 20,
                locale: {
                    format: '{{ t('datepicker_format') }}',
                    applyLabel: "{{ t('datepicker_applyLabel') }}",
                    cancelLabel: "{{ t('datepicker_cancelLabel') }}",
                    fromLabel: "{{ t('datepicker_fromLabel') }}",
                    toLabel: "{{ t('datepicker_toLabel') }}",
                    customRangeLabel: "{{ t('datepicker_customRangeLabel') }}",
                    weekLabel: "{{ t('datepicker_weekLabel') }}",
                    daysOfWeek: [
                        "{{ t('datepicker_monday') }}",
                        "{{ t('datepicker_tuesday') }}",
                        "{{ t('datepicker_wednesday') }}",
                        "{{ t('datepicker_thursday') }}",
                        "{{ t('datepicker_friday') }}",
                        "{{ t('datepicker_saturday') }}",
                        "{{ t('datepicker_sunday') }}",
                    ],
                    monthNames: [
                        "{{ t('January') }}",
                        "{{ t('February') }}",
                        "{{ t('March') }}",
                        "{{ t('April') }}",
                        "{{ t('May') }}",
                        "{{ t('June') }}",
                        "{{ t('July') }}",
                        "{{ t('August') }}",
                        "{{ t('September') }}",
                        "{{ t('October') }}",
                        "{{ t('November') }}",
                        "{{ t('December') }}"
                    ],
                    firstDay: 0,
                },
                parentEl: '#daterange-container',
                startDate: moment().format('{{ t('datepicker_format') }}'),
                endDate: moment().add(1, 'days').format('{{ t('datepicker_format') }}')
            });
            $('#cfForm .cf-date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('{{ t('datepicker_format') }}') + ' / ' + picker.endDate.format('{{ t('datepicker_format') }}'));
            });
        });
    </script>
@endsection