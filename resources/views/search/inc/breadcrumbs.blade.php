<div class="container">
    <nav aria-label="breadcrumb" role="navigation" class="search-breadcrumb">
        <ol class="breadcrumbs">
            <li class="breadcrumbs-item"><a href="{{ url('/') }}">{{ t('main_page') }}</a></li>
            @if (isset($bcTab) and is_array($bcTab) and count($bcTab) > 0)
                @foreach($bcTab as $key => $value)
                    @if ($value->has('position') and $key == count($bcTab)-1)
                        <li class="breadcrumbs-item active">
                            <a> {!! $value->get('name') !!}</a>
                        </li>
                    @else
                        <li class="breadcrumbs-item"><a href="{{ $value->get('url') }}">{!! $value->get('name') !!}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        </ol>
    </nav>
</div>
