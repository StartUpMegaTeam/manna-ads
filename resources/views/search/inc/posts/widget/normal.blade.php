<?php
if (!isset($cacheExpiration)) {
    $cacheExpiration = (int)config('settings.optimization.cache_expiration');
}
$hideOnMobile = '';
if (isset($widget->options, $widget->options['hide_on_mobile']) && $widget->options['hide_on_mobile'] == '1') {
    $hideOnMobile = ' hidden-sm';
}
?>
@if (isset($widget) && !empty($widget) && $widget->posts->count() > 0)
        <?php
        $isFromHome = (
        Illuminate\Support\Str::contains(
            Illuminate\Support\Facades\Route::currentRouteAction(),
            'Web\HomeController'
        )
        );
        ?>
    @if ($isFromHome)
        @includeFirst([config('larapen.core.customizedViewPath') . 'home.inc.spacer', 'home.inc.spacer'], ['hideOnMobile' => $hideOnMobile])
    @endif

    <div class="container">
        <div class="latest-posts">
            <div class="latest-posts__header">
                <span class="latest-posts__title">
                    {{ t('Home - Latest Ads') }}
                </span>
                <span class="latest-posts__count">
                    {{ $postsCount . ' ' . trans_choice('global.count_posts_lower', getPlural($postsCount), [], config('app.locale')) }}
                </span>
            </div>

                <?php $posts = App\Models\Post::whereHas('category', function (\Illuminate\Database\Eloquent\Builder $query) {
                $query->where('archived', 0)
                    ->where('archived_manually', 0)
                    ->where('active', 1)
                    ->limit(8);
            })->orderBy('created_at', 'DESC')->get(); ?>

            <div class="latest-posts__grid">
                @includeFirst([config('larapen.core.customizedViewPath') . 'search.inc.posts.template.grid', 'search.inc.posts.template.grid'])
            </div>
            @if(count($posts) >= 8)
                <x-load-more-posts></x-load-more-posts>
            @endif
        </div>
    </div>
@endif

@section('after_scripts')
    @parent
@endsection