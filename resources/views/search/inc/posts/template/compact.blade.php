<?php
if (!isset($cacheExpiration)) {
    $cacheExpiration = (int)config('settings.optimization.cache_expiration');
}
if (config('settings.listing.display_mode') == 'make-compact') {
    $colDescBox = 'col-sm-9 col-12';
    $colPriceBox = 'col-sm-3 col-12';
} else {
    $colDescBox = 'col-sm-7 col-12';
    $colPriceBox = 'col-sm-3 col-12';
}
?>
@if (isset($posts) && $posts->count() > 0)
        <?php
        if (!isset($cats)) {
            $cats = collect([]);
        }
    foreach ($posts as $key => $post):
        ?>
    @component('components.post-cards.compact',[
        'post' => $post,
        'postImg' => $post->getMainPictureUrl(),
])@endcomponent
    <?php endforeach; ?>
@endif

@section('after_scripts')
    @parent
    <script>
        {{-- Favorites Translation --}}
        var lang = {
            labelSavePostSave: "{!! t('Save ad') !!}",
            labelSavePostRemove: "{!! t('Remove favorite') !!}",
            loginToSavePost: "{!! t('Please log in to save the Ads') !!}",
            loginToSaveSearch: "{!! t('Please log in to save your search') !!}",
            confirmationSavePost: "{!! t('Post saved in favorites successfully') !!}",
            confirmationRemoveSavePost: "{!! t('Post deleted from favorites successfully') !!}",
            confirmationSaveSearch: "{!! t('Search saved successfully') !!}",
            confirmationRemoveSaveSearch: "{!! t('Search deleted successfully') !!}"
        };
    </script>
@endsection
