<?php
if (!isset($cacheExpiration)) {
    $cacheExpiration = (int)config('settings.optimization.cache_expiration');
}
?>
@if (isset($posts) && $posts->count() > 0)
        <?php
        if (!isset($cats)) {
            $cats = collect([]);
        }

    foreach ($posts as $key => $post):
        ?>
    @component('components.post-cards.list',[
        'post' => $post,
        'postImg' => $post->getMainPictureUrl(),
    ])@endcomponent
    <?php endforeach; ?>
@endif

@section('after_scripts')
    @parent
    <script>
        {{-- Favorites Translation --}}
        var lang = {
            labelSavePostSave: "{!! t('Save ad') !!}",
            labelSavePostRemove: "{!! t('Remove favorite') !!}",
            loginToSavePost: "{!! t('Please log in to save the Ads') !!}",
            loginToSaveSearch: "{!! t('Please log in to save your search') !!}",
            confirmationSavePost: "{!! t('Post saved in favorites successfully') !!}",
            confirmationRemoveSavePost: "{!! t('Post deleted from favorites successfully') !!}",
            confirmationSaveSearch: "{!! t('Search saved successfully') !!}",
            confirmationRemoveSaveSearch: "{!! t('Search deleted successfully') !!}"
        };
    </script>
@endsection
