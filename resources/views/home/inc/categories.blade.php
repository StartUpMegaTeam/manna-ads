<?php
$hideOnMobile = '';
if (isset($categoriesOptions, $categoriesOptions['hide_on_mobile']) and $categoriesOptions['hide_on_mobile'] == '1') {
    $hideOnMobile = ' hidden-sm';
}
?>
@if (isset($categoriesOptions) and isset($categoriesOptions['type_of_display']))
    @includeFirst([config('larapen.core.customizedViewPath') . 'home.inc.spacer', 'home.inc.spacer'], ['hideOnMobile' => $hideOnMobile])

            <div class="categories">
                <div class="category-slider-container">
                    <div class="slider">
                        <div class="categories__preview">
                            <div class="swiper-wrapper">
                                @if (isset($categories) and $categories->count() > 0)
                                    @foreach($categories as $key => $cat)
                                        @component('components.categories.category-card', [
                                            'cat' => $cat
                                        ])@endcomponent
                                    @endforeach
                                @endif
                            </div>
                            <div class="swiper--next">
                                <img src="icon/icon-link-arrow-accent.svg" class="untouchable" style="width: 25%;" alt="">
                            </div>
                            <div class="swiper--prev">
                                <img src="icon/icon-link-arrow-accent.svg" class="untouchable" style="width: 25%;" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endif

@section('before_scripts')
    @parent
    @if (isset($categoriesOptions) and isset($categoriesOptions['max_sub_cats']) and $categoriesOptions['max_sub_cats'] >= 0)
        <script>
            var maxSubCats = {{ (int)$categoriesOptions['max_sub_cats'] }};
        </script>
    @endif

    <script>
        let categoriesSwiper = new Swiper(".categories__preview", {
            slidesPerView: 7,
            spaceBetween: 20,
            direction: 'horizontal',
            loop: true,
            longSwipesMs: 600,
            threshold: 5,
            mousewheel: {
                invert: false,
                forceToAxis: true,
            },
            breakpoints: {
                1200: {
                    slidesPerView: 7,
                    spaceBetween: 20,
                },
                1025: {
                    slidesPerView: 6,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 6,
                    spaceBetween: 15,
                },
                580: {
                    slidesPerView: 5,
                    spaceBetween: 10,
                },
                500: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                },
                350: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                },
                0: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                }
            },
            navigation: {
                nextEl: ".swiper--next",
                prevEl: ".swiper--prev",
            },
        });
    </script>
@endsection
