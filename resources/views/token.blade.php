{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="main-container">
        <div class="container">
            <div class="row">

                @if (isset($errors) and $errors->any())
                    <div class="col-xl-12">
                        <div class="alert alert-danger">
                            <ul class="list list-check">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                @if (session('code'))
                    <div class="col-xl-12">
                        <div class="alert alert-danger">
                            <p>{{ session('code') }}</p>
                        </div>
                    </div>
                @endif

                @if (session()->has('flash_notification'))
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-12">
                                @include('flash::message')
                            </div>
                        </div>
                    </div>
                @endif

                <div class="login-box d-flex justify-content-center height-80">
                    <div class="row modal-message-success d-flex col col-md-7 col-xl-4 px-0 ">
                        <h4 class="register-title mt-4 text-center"> {{t('Email confirmation')}} </h4>

                        <p class="text-center m-0 mb-4">Если письмо не пришло, Вы можете отправить его повторно.
                            <span class="js-send-code-link"></span>
                        </p>

                        <form action="{{url()->current()}}">

                            <div class="input mb-3">
                                <input name="resend_email" type="email" placeholder="{{t('email_example')}}">
                            </div>

                            <div class="mb-3">
                                <button type="submit" id="urlResendEmail"
                                        data-parent=""
                                        class="js-send-code-btn col px-5 btn btn--resend-email btn--height">
                                    {{t('Re-send')}}
                                </button>
                            </div>

                        </form>


                        <div class="row">
                            <div class="have-account">
                                <span class="have-account__text">{{ t('have_account') }}</span>
                                <a href="{{ url('/login') }}"
                                   class="have-account__link ">{{ t('have_account_login') }}</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_scripts')
    <script>
        $(document).ready(function () {
            $("#tokenBtn").click(function () {
                $("#tokenForm").submit();
                return false;
            });
        });
    </script>
@endsection