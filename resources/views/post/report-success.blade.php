{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="main-container">
        <div class="container">
            <div class="row">

                <div class="col-md-12 login-box d-flex align-items-center justify-content-center height-80">
                    <div class="modal-message-success d-flex">
                        <img class="modal-message-success__img" src="{{ asset('/icon/success.svg') }}">
                        <span class="modal-message-success__title">
                        {!! t('message_reports_success_send') !!}
                    </span>
                        <div class="row">
                            <div class="col">
                                <a href="{{url('/')}}" class="px-5 btn btn--success ">
                                    {{ t('go_home') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after_scripts')
    <script src="{{ url('assets/js/form-validation.js') }}"></script>
@endsection

