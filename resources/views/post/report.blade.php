{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="main-container">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <div id="message_form" class="login-box">

                        @if (isset($errors) && $errors->any())
                            <div class="d-flex justify-content-center  ">
                                <div class="alert alert-danger alert-danger--text-center mt-5 col-xl-5 col-md-6 col-10 mb-0">
                                    <h5 class="text-center">{{ t('oops_an_error_has_occurred') }}</h5>
                                    <ul class="alert-danger__list">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        <div class="row panel-intro align-center justify-content-center  text-center">
                            <div class="col-xl-5 col-md-6 col-10 ">
                                <div class="post-free__title p-0">{{ $title }}</div>
                                <h4 class="post-free__title-description col mt-4">{{ t('What exactly do you think is unacceptable in this ad?') }}</h4>
                            </div>

                        </div>

                        <form id="formReport" role="form" method="POST"
                              action="{{ url('posts/' . $post->id . '/report') }}">
                            {!! csrf_field() !!}
                            <fieldset>
                                <div class="row justify-content-center">
                                    <div class="col-xl-5 col-md-6 col-10">
                                        {{-- report_type_id --}}
                                        <?php $reportTypeIdError = (isset($errors) and $errors->has('report_type_id')) ? ' is-invalid' : ''; ?>
                                        <div class="col mb-3 required input {{ $reportTypeIdError }}">
                                            <label for="report_type_id"
                                                   class="input__label">{{ t('Reason') }}
                                                <sup>*</sup></label>
                                            <div class="input-select-2">
                                                <select id="reportTypeId" name="report_type_id"
                                                        class="form-control selecter">
                                                    <option value="">{{ t('Select a reason') }}</option>
                                                    @foreach($reportTypes as $reportType)
                                                        <option value="{{ $reportType->id }}" {{ (old('report_type_id', 0)==$reportType->id) ? 'selected="selected"' : '' }}>
                                                            {{ $reportType->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <div class="report-dropdown"></div>
                                            </div>
                                        </div>

                                        {{-- email --}}
                                        @if (auth()->check() and isset(auth()->user()->email))
                                            <input type="hidden" name="email" value="{{ auth()->user()->email }}">
                                        @else
                                                <?php $emailError = (isset($errors) and $errors->has('email')) ? ' is-invalid' : ''; ?>
                                            <div class="col required input mb-3 {{ $emailError }}">
                                                <label for="email"
                                                       class="input__label {{ $emailError }}">{{ t('Your Email') }}
                                                    <sup>*</sup></label>

                                                <input id="email" name="email" type="text" maxlength="60"
                                                       class="input-text{{ $emailError }}"
                                                       value="{{ old('email') }}">

                                            </div>
                                        @endif

                                        {{-- message --}}
                                        <?php $messageError = (isset($errors) and $errors->has('message')) ? ' is-invalid' : ''; ?>
                                        <div class="col mb-5 required input {{ $messageError }}">
                                            <label for="message"
                                                   class="input__label">{{ t('Message') }}
                                                <sup>*</sup></label>
                                            <textarea id="message"
                                                      name="message"
                                                      placeholder="{{t('Describe_the_problem')}}"
                                                      class="input-text custom-scroll-bar"
                                                      rows="10"
                                                      style="height: 200px;"
                                            >{{ old('message') }}</textarea>
                                            <div class="text-muted form-text">{{ t('no_more_than_1000_characters') }}</div>
                                        </div>

                                        @include('layouts.inc.tools.captcha', ['label' => true])

                                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                                        <input type="hidden" name="abuseForm" value="1">

                                        <div class="d-flex gap-2 gap-md-3 col-12 mb-5">
                                            <button id="buttonSubmit" type="submit"
                                                    class="col btn btn--success btn--height">{{ t('Send') }}</button>
                                            <a href="{{ \App\Helpers\UrlGen::post($post) }}"
                                               class="col btn btn--cancel btn--height">{{ t('Cancel') }}</a>

                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('/js/validation/clear-is-invalid.js') }}"></script>

    <script>
        $(document).ready(function () {
            clearIsInvalid();

            $('#reportTypeId').select2({
                dropdownParent: $('.report-dropdown')
            });
        });

        $('#formReport').submit(function () {
            if ({{isset($errors)}}) {

            } else {
                $('#message_form').css('display', 'none');
                $('.modal-message-success').css('display', 'flex');
            }
        })
    </script>
@endsection

@section('after_scripts')
    <script src="{{ url('assets/js/form-validation.js') }}"></script>
@endsection

