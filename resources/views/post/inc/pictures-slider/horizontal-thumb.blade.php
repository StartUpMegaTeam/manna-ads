@php
    /** @var \App\Models\Post $post */
@endphp

<div class="row item-slider posts-image m-0 mb-xl-4">
    <div class="col owl-slider">
        <?php $titleSlug = \Illuminate\Support\Str::slug($post->title); ?>
        @if ($post->pictures->count() > 0)
            <div class="owl-carousel owl-theme" data-slider-id="1">
                @foreach($post->pictures as $key => $image)
                    {!! imgTag($image->filename, 'big', ['alt' => $titleSlug . '-big-' . $key]) !!}
                @endforeach
            </div>
            @if($post->pictures->count() > 1)
                <div class="picture-custom-navigation">
                    <div class="custom-navigation-item untouchable custom-navigation-item--prev d-none">
                        <img alt="" src="{{asset('icon/owlActions/owlArrow.svg')}}">
                    </div>
                    <div class="custom-navigation-item untouchable custom-navigation-item--next">
                        <img alt="" src="{{asset('icon/owlActions/owlArrow.svg')}}">
                    </div>
                </div>
            @endif
        @else
            <div class="owl-carousel owl-theme" data-slider-id="1">
                {!! imgTag(config('larapen.core.picture.default'), 'big', ['alt' => '']) !!}
            </div>
        @endif
    </div>
    <div class="owl-thumbs " data-slider-id="1">
        @foreach($post->pictures as $image)
            <img class="owl-thumb-item untouchable" alt="" src="{{ imgUrl($image->filename, 'small') }}">
        @endforeach
    </div>
</div>

@section('after_scripts')
    @parent
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css"/>
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>

    <link rel="stylesheet" href="{{ asset('js/owlCarousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/owlCarousel/owl.theme.default.min.css') }}">
    <script src="{{ asset('js/owlCarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/owlCarousel/owl.carousel2.thumbs.js') }}"></script>
    <script>
        $(document).ready(function () {

            var Lowl = $(".owl-carousel");
            let imagesList = $('.owl-carousel img');

            Lowl.owlCarousel({
                items: 1,
                margin: 10,
                autoHeight: true,
                thumbs: true,
                thumbsPrerendered: true,
                dots: false,
            });

            $('.owl-carousel').on('changed.owl.carousel', function (event) {

                if (event.item.index === imagesList.length - 1) {
                    $(".custom-navigation-item--next").addClass('d-none')
                } else {
                    $(".custom-navigation-item--next").removeClass('d-none')
                }

                if (event.item.index === 0) {
                    $(".custom-navigation-item--prev").addClass('d-none')
                } else {
                    $(".custom-navigation-item--prev").removeClass('d-none')
                }

            });


            $(".custom-navigation-item--next").click(function () {
                Lowl.trigger('next.owl.carousel');
            });

            $(".custom-navigation-item--prev").click(function () {
                Lowl.trigger('prev.owl.carousel');
            });

            Lowl.trigger('owl.play', false);


            if (imagesList.length > 0) {
                for (let i = 0; i < imagesList.length; i++) {
                    $(imagesList[i]).attr('data-fancybox', 'gallery');
                }

                Fancybox.bind('[data-fancybox="gallery"]', {
                    caption: function (fancybox, carousel, slide) {
                        return (
                            `${slide.index + 1} / ${carousel.slides.length} <br />` + slide.caption
                        );
                    },
                    Carousel: {
                        on: {
                            change: (that) => {
                                Lowl.trigger("to.owl.carousel", [that.page, 1])
                            },
                        },
                    },
                });
            }
        });

        /* bxSlider - Initiates Responsive Carousel */
        function bxSliderSettings() {
            var smSettings = {
                slideWidth: 65,
                minSlides: 1,
                maxSlides: 4,
                slideMargin: 5,
                adaptiveHeight: true,
                pager: false
            };
            var mdSettings = {
                slideWidth: 100,
                minSlides: 1,
                maxSlides: 4,
                slideMargin: 5,
                adaptiveHeight: true,
                pager: false
            };
            var lgSettings = {
                slideWidth: 100,
                minSlides: 3,
                maxSlides: 6,
                pager: false,
                slideMargin: 10,
                adaptiveHeight: true
            };

            if ($(window).width() <= 640) {
                return smSettings;
            } else if ($(window).width() > 640 && $(window).width() < 768) {
                return mdSettings;
            } else {
                return lgSettings;
            }
        }
    </script>
@endsection