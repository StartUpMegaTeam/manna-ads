@if (isset($errors) and $errors->any())
    <div class="col-xl-12 mb-3">
        <div class="alert-danger mt-3 m-auto">
            <h5 class="alert-danger__title">{{ t('oops_an_error_has_occurred') }}</h5>
            <ul class="alert-danger__list">
                @foreach ($errors->all() as $error)
                    <li><img src="{{asset('icon/login/close.svg')}}">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

@if (session()->has('flash_notification'))
    <div class="col-xl-12">
        <div class="row">
            <div class="col-xl-12">
                @include('flash::message')
            </div>
        </div>
    </div>
@endif