<?php
if (!isset($languageCode) or empty($languageCode)) {
    $languageCode = config('app.locale', session('langCode'));
}
?>
@if (isset($fields) and $fields->count() > 0)
    @foreach($fields as $field)
            <?php
            // Fields parameters
            $fieldId = 'cf.' . $field->id;
            $fieldName = 'cf[' . $field->id . ']';
            $fieldOld = 'cf.' . $field->id;

            // Errors & Required CSS
            $requiredClass = ($field->required == 1) ? 'required' : '';
            $errorClass = (isset($errors) && $errors->has($fieldOld)) ? ' is-invalid' : '';

            // Get the default value
            $defaultValue = (isset($oldInput) && isset($oldInput[$field->id])) ? $oldInput[$field->id] : $field->default_value;
            ?>

        @if ($field->type == 'checkbox')

            {{-- checkbox --}}
            <div class="row mb-4">
                <div class="col">
                    <div class="input{{ $requiredClass . $errorClass }}" style="margin-top: -10px;">
                        <div class="col-md-8 ">
                            <div class="input-switch ">
                                <label class="switch">
                                    <input id="{{ $fieldId }}"
                                           name="{{ $fieldName }}"
                                           value="1"
                                           type="checkbox"
                                           class="input-text"
                                            {{ ($defaultValue=='1') ? 'checked="checked"' : '' }}
                                    >
                                    <span class="slidere round"></span>
                                </label>
                                <label class="untouchable form-check-label" for="{{ $fieldId }}">
                                    {{ $field->name }}
                                </label>
                            </div>
                        </div>
                        @if($field->help !== null && $field->help != '')
                            <div class="form-text text-muted">{!! $field->help !!}</div>
                        @endif
                    </div>
                </div>
            </div>

        @elseif ($field->type == 'checkbox_multiple')

            @if ($field->options->count() > 0)
                {{-- checkbox_multiple --}}
                <div class="row mb-4">
                    <div class="col">
                        <div class="input {{ $requiredClass . $errorClass }}">
                            <label class="input__label" for="{{ $fieldId }}">
                                {{ $field->name }}
                                @if ($field->required == 1)
                                    <sup>*</sup>
                                @endif
                            </label>
                                <?php $cmFieldStyle = ($field->options->count() > 12) ? 'search-filter__scroll-bar' : ''; ?>
                            <div class="checkbox-container checkbox-container--column custom-scroll-bar"{!! $cmFieldStyle !!}>
                                @foreach ($field->options as $option)
                                        <?php
                                        // Get the default value
                                        $defaultValue = (isset($oldInput) && isset($oldInput[$field->id]) && isset($oldInput[$field->id][$option->id]))
                                            ? $oldInput[$field->id][$option->id]
                                            : (
                                            (is_array($field->default_value) && isset($field->default_value[$option->id]) && isset($field->default_value[$option->id]->id))
                                                ? $field->default_value[$option->id]->id
                                                : $field->default_value
                                            );
                                        ?>
                                    <div class="input">
                                        <div class="checkbox input-checkbox">
                                            <input id="{{ $fieldId . '.' . $option->id }}"
                                                   name="{{ $fieldName . '[' . $option->id . ']' }}"
                                                   value="{{ $option->id }}"
                                                   type="checkbox"
                                                   class="checkbox__input"
                                                    {{ ($defaultValue == $option->id) ? 'checked="checked"' : '' }}
                                            >
                                            <label class="untouchable checkbox__label checkbox__label--filter"
                                                   for="{{ $fieldId . '.' . $option->id }}">
                                                {{ $option->value }}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                                @if($field->help !== null && $field->help != '')
                                    <div class="form-text text-muted">{!! $field->help !!}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        @elseif ($field->type == 'file')

            {{-- file --}}
            <div class="row mb-4">
                <div class="col">
                    <div class="input {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="">
                            <div class="mb10 file-loading">
                                <input id="{{ $fieldId }}" name="{{ $fieldName }}" type="file"
                                       class="file picimg">
                            </div>
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">
                                    {!! $field->help !!} {{ t('file_types', ['file_types' => showValidFileTypes('file')], 'global', $languageCode) }}
                                </div>
                            @endif
                            @if (!empty($field->default_value) and $disk->exists($field->default_value))
                                <div>
                                    <a class="btn btn-default" href="{{ fileUrl($field->default_value) }}"
                                       target="_blank">
                                        <i class="icon-attach-2"></i> {{ t('Download') }}
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @elseif ($field->type == 'radio')

            @if ($field->options->count() > 0)
                {{-- radio --}}
                <div class="row mb-4">
                    <div class="col">
                        <div class="input {{ $requiredClass . $errorClass }}">
                            <label class="input__label" for="{{ $fieldId }}">
                                {{ $field->name }}
                                @if ($field->required == 1)
                                    <sup>*</sup>
                                @endif
                            </label>
                            <div class="col-md-8">
                                @foreach ($field->options as $option)

                                    <div class="input mt-2 {{ $requiredClass }}"
                                         style="margin-top: -10px;">
                                        <div class="col-md-8 ">
                                            <div class="input-switch ">
                                                <label class="switch">
                                                    <input id="{{ $fieldId . '.' . $option->id }}"
                                                           name="{{ $fieldName }}"
                                                           value="{{ $option->id }}"
                                                           type="radio"
                                                           class="checkbox"
                                                            {{ ($defaultValue == $option->id) ? 'checked="checked"' : '' }}
                                                    >
                                                    <span class="slidere round"></span>
                                                </label>
                                                <label class="untouchable cursor-pointer"
                                                       for="{{ $fieldId . '.' . $option->id }}">
                                                    {{ $option->value }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            @endif

        @elseif ($field->type == 'select')

            {{-- select --}}
            <div class="row mb-4">
                <div class="col">
                    <div class="input {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="input-select-2" style="-webkit-overflow-scrolling: touch;">
                                <?php
                                $select2Type = ($field->options->count() <= 10) ? 'selecter-field' : 'large-data-selecter-field';
                                ?>
                            <select id="{{ $fieldId }}" name="{{ $fieldName }}"
                                    class="selecter-field"
                                    data-select="{{$field->id}}">
                                <option value="{{ $field->default_value }}"
                                        @if (old($fieldOld)=='' or old($fieldOld)==$field->default_value)
                                            selected="selected"
                                        @endif
                                >
                                    {{ t('Select', [], 'global', $languageCode) }}
                                </option>
                                @if ($field->options->count() > 0)
                                    @foreach ($field->options as $option)
                                        <option value="{{ $option->id }}"
                                                @if ($defaultValue == $option->id)
                                                    selected="selected"
                                                @endif
                                        >
                                            {{ $option->value }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="select-dropdown" data-dropdown="{{$field->id}}"></div>
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @elseif ($field->type == 'select_multi')

            {{-- select_multi --}}
            <div class="row mb-3">
                <div class="col">
                    <div class="input {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="input-select-2">
                                <?php
                                $select2Type = ($field->options->count() <= 10) ? 'selecter-field' : 'large-data-selecter-field';
                                ?>
                            <select id="{{ $fieldId }}" name="{{ $fieldName }}"
                                    class=" {{ $select2Type }}"
                                    data-select="{{$field->id}}">
                                <option value="{{ $field->default_value }}"
                                        @if (old($fieldOld)=='' or old($fieldOld)==$field->default_value)
                                            selected="selected"
                                        @endif
                                >
                                    {{ t('Select', [], 'global', $languageCode) }}
                                </option>
                                @if ($field->options->count() > 0)
                                    @foreach ($field->options as $option)
                                        <option value="{{ $option->id }}"
                                                @if ($defaultValue == $option->id)
                                                    selected="selected"
                                                @endif
                                        >
                                            {{ $option->value }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="select-dropdown" data-dropdown="{{$field->id}}"></div>
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @elseif ($field->type == 'textarea')

            {{-- textarea --}}
            <div class="row mb-4">
                <div class="col">
                    <div class="input mb-3 row {{ $requiredClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="">
					<textarea class="input-text"
                              id="{{ $fieldId }}"
                              name="{{ $fieldName }}"
                              placeholder="{{ $field->name }}"
                              rows="10">{{ $defaultValue }}
                    </textarea>
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @elseif ($field->type == 'url')

            {{-- url --}}
            <div class="row mb-4">
                <div class="col">
                    <div class="input  {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="">
                            <input id="{{ $fieldId }}"
                                   name="{{ $fieldName }}"
                                   type="text"
                                   placeholder="{{ $field->name }}"
                                   class="input-text"
                                   value="{{ $defaultValue }}">
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @elseif ($field->type == 'number')

            {{-- number --}}
            <div class="row mb-4">
                <div class="col">
                    <div class="input  {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="">
                            <input id="{{ $fieldId }}"
                                   name="{{ $fieldName }}"
                                   type="number"
                                   placeholder="{{ $field->name }}"
                                   class="input-text"
                                   value="{{ $defaultValue }}">
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @elseif ($field->type == 'number_range')

            <div class="row mb-4">
                <div class="col">
                    <div class="input  {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="">
                            <input id="{{ $fieldId }}"
                                   name="{{ $fieldName }}"
                                   type="number"
                                   placeholder="{{ $field->name }}"
                                   class="input-text"
                                   value="{{ $defaultValue }}">
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @elseif ($field->type == 'float_number_range')

            <div class="row mb-4">
                <div class="col">
                    <div class="input  {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="">
                            <input id="{{ $fieldId }}"
                                   name="{{ $fieldName }}"
                                   type="number"
                                   step="0.01"
                                   placeholder="{{ $field->name }}"
                                   class="input-text float-number"
                                   value="{{ $defaultValue }}">
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @elseif ($field->type == 'date')

            {{-- date --}}
            <div class="row mb-4">
                <div class="col">
                    <div class="input {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="col-md-3">
                            <input id="{{ $fieldId }}"
                                   name="{{ $fieldName }}"
                                   type="text"
                                   placeholder="{{ $field->name }}"
                                   class="input-text cf-date"
                                   value="{{ $defaultValue }}"
                                   autocomplete="off"
                            >
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @elseif ($field->type == 'date_time')

            {{-- date_time --}}
            <div class="row mb-4">
                <div class="col">
                    <div class="input {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="col-md-3">
                            <input id="{{ $fieldId }}"
                                   name="{{ $fieldName }}"
                                   type="text"
                                   placeholder="{{ $field->name }}"
                                   class="input-text cf-date_time"
                                   value="{{ $defaultValue }}"
                                   autocomplete="off"
                            >
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @elseif ($field->type == 'date_range')

            {{-- date_range --}}
            <div class="row mb-4">
                <div class="col">
                    <div class="input {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="col-md-3">
                            <input id="{{ $fieldId }}"
                                   name="{{ $fieldName }}"
                                   type="text"
                                   placeholder="{{ $field->name }}"
                                   class="input-text cf-date_range"
                                   value="{{ $defaultValue }}"
                                   autocomplete="off"
                            >
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @elseif ($field->type == 'working_time')
                <?php
                $defaultValue = explode('-', $defaultValue);
                ?>
            {{-- working_time --}}
            <div class="row mb-4 {{ $errorClass }}">
                <label class="input__label" for="{{ $fieldId }}">
                    {{ $field->name }}
                    @if ($field->required == 1)
                        <sup>*</sup>
                    @endif
                </label>
                <div class="col">
                    <div class="row input {{ $requiredClass }}">
                        <div class="">
                            <input id="{{ $fieldId }}.0"
                                   type="time"
                                   placeholder="{{ $field->name }}"
                                   class="input-text"
                                   value="{{ count($defaultValue) == 2 ? $defaultValue[0] : '' }}">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row input {{ $requiredClass }}">
                        <div class="">
                            <input id="{{ $fieldId }}.1"
                                   type="time"
                                   placeholder="{{ $field->name }}"
                                   class="input-text"
                                   value="{{ count($defaultValue) == 2 ? $defaultValue[1] : '' }}">
                        </div>
                    </div>
                </div>
                @if($field->help !== null && $field->help != '')
                    <div class="form-text text-muted">{!! $field->help !!}</div>
                @endif
                <input type="hidden" name="{{ $fieldName }}" id="{{ $fieldId }}.2">

            </div>

            <script>
                document.getElementById('{{$fieldId}}.0').addEventListener("input", () => {
                    if (document.getElementById('{{$fieldId}}.0').value != '' && document.getElementById('{{$fieldId}}.1').value != '') {
                        document.getElementById('{{$fieldId}}.2').value =
                            document.getElementById('{{$fieldId}}.0').value + '-'
                            + document.getElementById('{{$fieldId}}.1').value;
                    }
                }, false);

                document.getElementById('{{$fieldId}}.1').addEventListener("input", () => {
                    if (document.getElementById('{{$fieldId}}.0').value != '' && document.getElementById('{{$fieldId}}.1').value != '') {
                        document.getElementById('{{$fieldId}}.2').value =
                            document.getElementById('{{$fieldId}}.0').value + '-'
                            + document.getElementById('{{$fieldId}}.1').value;
                    }
                }, false);
            </script>
        @else

            {{-- text --}}
            <div class="row mb-4">
                <div class="col">
                    <div class="row input {{ $requiredClass . $errorClass }}">
                        <label class="input__label" for="{{ $fieldId }}">
                            {{ $field->name }}
                            @if ($field->required == 1)
                                <sup>*</sup>
                            @endif
                        </label>
                        <div class="">
                            <input id="{{ $fieldId }}"
                                   name="{{ $fieldName }}"
                                   type="text"
                                   placeholder="{{ $field->name }}"
                                   class="input-text"
                                   value="{{ $defaultValue }}">
                            @if($field->help !== null && $field->help != '')
                                <div class="form-text text-muted">{!! $field->help !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endif

<script>
    let selecterField = $('.selecter-field');
    let largeDataSelecterField = $('.large-data-selecter-field');

    selecterField.each((index, item) => {
        $(document).find("[data-select='" + item.dataset.select + "']").select2({
            language: langLayout.select2,
            width: '100%',
            dropdownAutoWidth: 'true',
            dropdownParent: $(document).find("[data-dropdown='" + item.dataset.select + "']")
        });
    });

    largeDataSelecterField.each((index, item) => {
        $(document).find("[data-select='" + item.dataset.select + "']").select2({
            language: langLayout.select2,
            width: '100%',
            dropdownAutoWidth: 'true',
            dropdownParent: $(document).find("[data-dropdown='" + item.dataset.select + "']")
        });
    });

    $(function () {
        /*
         * Custom Fields Date Picker
         * https://www.daterangepicker.com/#options
         */

        {{-- Single Date --}}
        $('#cfContainer .cf-date').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            showDropdowns: true,
            minYear: parseInt(moment().format('YYYY')) - 100,
            maxYear: parseInt(moment().format('YYYY')) + 20,
            locale: {
                format: '{{ t('datepicker_format') }}',
                applyLabel: "{{ t('datepicker_applyLabel') }}",
                cancelLabel: "{{ t('datepicker_cancelLabel') }}",
                fromLabel: "{{ t('datepicker_fromLabel') }}",
                toLabel: "{{ t('datepicker_toLabel') }}",
                customRangeLabel: "{{ t('datepicker_customRangeLabel') }}",
                weekLabel: "{{ t('datepicker_weekLabel') }}",
                daysOfWeek: [
                    "{{ t('datepicker_sunday') }}",
                    "{{ t('datepicker_monday') }}",
                    "{{ t('datepicker_tuesday') }}",
                    "{{ t('datepicker_wednesday') }}",
                    "{{ t('datepicker_thursday') }}",
                    "{{ t('datepicker_friday') }}",
                    "{{ t('datepicker_saturday') }}"
                ],
                monthNames: [
                    "{{ t('January') }}",
                    "{{ t('February') }}",
                    "{{ t('March') }}",
                    "{{ t('April') }}",
                    "{{ t('May') }}",
                    "{{ t('June') }}",
                    "{{ t('July') }}",
                    "{{ t('August') }}",
                    "{{ t('September') }}",
                    "{{ t('October') }}",
                    "{{ t('November') }}",
                    "{{ t('December') }}"
                ],
                firstDay: 1
            },
            singleDatePicker: true,
            startDate: moment().format('{{ t('datepicker_format') }}')
        });
        $('#cfContainer .cf-date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('{{ t('datepicker_format') }}'));
        });

        {{-- Single Date (with Time) --}}
        $('#cfContainer .cf-date_time').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            showDropdowns: false,
            minYear: parseInt(moment().format('YYYY')) - 100,
            maxYear: parseInt(moment().format('YYYY')) + 20,
            locale: {
                format: '{{ t('datepicker_format_datetime') }}',
                applyLabel: "{{ t('datepicker_applyLabel') }}",
                cancelLabel: "{{ t('datepicker_cancelLabel') }}",
                fromLabel: "{{ t('datepicker_fromLabel') }}",
                toLabel: "{{ t('datepicker_toLabel') }}",
                customRangeLabel: "{{ t('datepicker_customRangeLabel') }}",
                weekLabel: "{{ t('datepicker_weekLabel') }}",
                daysOfWeek: [
                    "{{ t('datepicker_sunday') }}",
                    "{{ t('datepicker_monday') }}",
                    "{{ t('datepicker_tuesday') }}",
                    "{{ t('datepicker_wednesday') }}",
                    "{{ t('datepicker_thursday') }}",
                    "{{ t('datepicker_friday') }}",
                    "{{ t('datepicker_saturday') }}"
                ],
                monthNames: [
                    "{{ t('January') }}",
                    "{{ t('February') }}",
                    "{{ t('March') }}",
                    "{{ t('April') }}",
                    "{{ t('May') }}",
                    "{{ t('June') }}",
                    "{{ t('July') }}",
                    "{{ t('August') }}",
                    "{{ t('September') }}",
                    "{{ t('October') }}",
                    "{{ t('November') }}",
                    "{{ t('December') }}"
                ],
                firstDay: 1
            },
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
            startDate: moment().format('{{ t('datepicker_format_datetime') }}')
        });
        $('#cfContainer .cf-date_time').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('{{ t('datepicker_format_datetime') }}'));
        });

        {{-- Date Range --}}
        $('#cfContainer .cf-date_range').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            showDropdowns: false,
            minYear: parseInt(moment().format('YYYY')) - 100,
            maxYear: parseInt(moment().format('YYYY')) + 20,
            locale: {
                format: '{{ t('datepicker_format') }}',
                applyLabel: "{{ t('datepicker_applyLabel') }}",
                cancelLabel: "{{ t('datepicker_cancelLabel') }}",
                fromLabel: "{{ t('datepicker_fromLabel') }}",
                toLabel: "{{ t('datepicker_toLabel') }}",
                customRangeLabel: "{{ t('datepicker_customRangeLabel') }}",
                weekLabel: "{{ t('datepicker_weekLabel') }}",
                daysOfWeek: [
                    "{{ t('datepicker_sunday') }}",
                    "{{ t('datepicker_monday') }}",
                    "{{ t('datepicker_tuesday') }}",
                    "{{ t('datepicker_wednesday') }}",
                    "{{ t('datepicker_thursday') }}",
                    "{{ t('datepicker_friday') }}",
                    "{{ t('datepicker_saturday') }}"
                ],
                monthNames: [
                    "{{ t('January') }}",
                    "{{ t('February') }}",
                    "{{ t('March') }}",
                    "{{ t('April') }}",
                    "{{ t('May') }}",
                    "{{ t('June') }}",
                    "{{ t('July') }}",
                    "{{ t('August') }}",
                    "{{ t('September') }}",
                    "{{ t('October') }}",
                    "{{ t('November') }}",
                    "{{ t('December') }}"
                ],
                firstDay: 1
            },
            startDate: moment().format('{{ t('datepicker_format') }}'),
            endDate: moment().add(1, 'days').format('{{ t('datepicker_format') }}')
        });
        $('#cfContainer .cf-date_range').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('{{ t('datepicker_format') }}') + ' - ' + picker.endDate.format('{{ t('datepicker_format') }}'));
        });
    });
</script>
