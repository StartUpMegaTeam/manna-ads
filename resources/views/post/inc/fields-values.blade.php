@if (isset($customFields) && $customFields->count() > 0)
	<div class="row gx-1 gy-1 mt-4 mt-lg-5">
		<div class="col-12">
			<div class="row">
				<div class="col-12">
					<h4 class="ads-show__descriptions-title p-0 "> {{ t('Ads Details') }}</h4>
				</div>
			</div>
		</div>

		<div class="col-sm-12 col-md-8 col-xl-8">
			<div class="row gx-1 gy-1 ads-show__descriptions-details">
				@foreach($customFields as $field)
						<?php
						if (in_array($field->type, ['radio', 'select', 'select_multi'])) {
							if (is_numeric($field->default_value)) {
								$option = \App\Models\FieldOption::find($field->default_value);
								if (!empty($option)) {
									$field->default_value = $option->value;
								}
							}
						}
						if (in_array($field->type, ['checkbox'])) {
							$field->default_value = ($field->default_value == 1) ? t('Yes') : t('It is forbidden');
						}
						if ($field->type == 'video') {
							$field->default_value = \App\Helpers\VideoEmbedding::embedVideo($field->default_value);
						}
						?>
					@if ($field->type == 'file')
						<div class="col">
							<div class=" ads-show__descriptions-details-item">
								<div class=" fw-bolder ads-show__descriptions-details-title">{{ $field->name }}</div>
								<div class="col-6 text-sm-end text-end ads-show__descriptions-details-meaning">
									<a class="btn btn-default" href="{{ fileUrl($field->default_value) }}"
									   target="_blank">
										<i class="icon-attach-2"></i> {{ t('Download') }}
									</a>
								</div>
							</div>
						</div>
					@else
						@if (!is_array($field->default_value) && $field->type != 'video')
							@if ($field->type == 'url')
								<div class="col">
									<div class=" ads-show__descriptions-details-item">
										<div class=" fw-bolder ads-show__descriptions-details-title">{{ $field->name }}</div>
                                        <div class="col-6 text-sm-end text-end ads-show__descriptions-details-meaning">
                                            <a href="{{ addHttp($field->default_value) }}" target="_blank"
                                               rel="nofollow">{{ addHttp($field->default_value) }}</a>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col">
                                    <div class=" ads-show__descriptions-details-item">
                                        <div class=" fw-bolder ads-show__descriptions-details-title">{{ $field->name }}</div>
                                        <div class="col-6 text-end ads-show__descriptions-details-meaning">{{ $field->default_value }}</div>
                                    </div>
                                </div>
                            @endif
                        @elseif (!is_array($field->default_value) && $field->type == 'video')
                            <div class="col">
                                <div class=" ads-show__descriptions-details-item">
                                    <div class="col-6 fw-bolder ads-show__descriptions-details-title">{{ $field->name }}
                                        :
                                    </div>
                                    <div class="col-6 text-end embed-responsive embed-responsive-16by9">
										{!! $field->default_value !!}
									</div>
								</div>
							</div>
						@else
							@if (is_array($field->default_value) && count($field->default_value) > 0)
								<div class="col">
									<div class="ads-show__descriptions-details-item">
										<div class="col fw-bolder ads-show__descriptions-details-title">{{ $field->name }}
                                        </div>
                                        <div class="col-6 text-end ads-show__descriptions-details-meaning">
												{{ implode(', ', array_map(function ($item) {
                            						return $item->value;
                        						}, $field->default_value)) }}
										</div>
									</div>
								</div>
							@endif
						@endif
					@endif
				@endforeach
			</div>
		</div>
	</div>
@endif
