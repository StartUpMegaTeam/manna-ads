{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@php
    /** @var \App\Models\Post $post */
@endphp

@extends('layouts.master')

@section('search')
    @parent
    <div class="ads-search-container">
        <div class="container">
            @if(isset($count) && $count->get('all') > 0)
                <x-search :showSaveSearch="true" :count="$count->get('all')" :isPostDetails="true"></x-search>
            @else
                <x-search :showSaveSearch="true" :isPostDetails="true"></x-search>
            @endif
        </div>
    </div>
@endsection

@section('content')
    {!! csrf_field() !!}
    <input type="hidden" id="postId" name="post_id" value="{{ $post->id }}">

    <div class="main-container">
        @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
        <?php $paddingTopExists = true; ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('flash::message')
                </div>
            </div>
        </div>
        <?php session()->forget('flash_notification.message'); ?>

        <?php if (isset($topAdvertising) && !empty($topAdvertising)): ?>
        @includeFirst([config('larapen.core.customizedViewPath') . 'layouts.inc.advertising.top', 'layouts.inc.advertising.top'], ['paddingTopExists' => $paddingTopExists ?? false])
            <?php
            $paddingTopExists = false;
        endif;
        ?>

        @if(isset($post->postReject))
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="moderation-alert">
                        <span class="moderation-alert__title">
                            {{ 'Это объявление отклонено по причине “' . $post->postReject->title . '”' }}
                        </span>
                            <span class="moderation-alert__description">
                            {{ $post->postReject->description }}
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="container {{ (isset($topAdvertising) && !empty($topAdvertising)) ? 'mt-3' : 'mt-2' }}">
            <div class="row">
                <div class="d-flex align-items-center justify-content-between col-md-12 mt-4 mb-4">

                    <nav aria-label="breadcrumb" role="navigation" class="float-start">
                        <ol class="breadcrumbs flex-wrap">
                            <li class="breadcrumbs-item">
                                <a class="ads-show__breadcrumb-link" href="{{ url('/') }}">{{ t('main_page') }}</a>
                            </li>
                            @if (isset($catBreadcrumb) && is_array($catBreadcrumb) && count($catBreadcrumb) > 0)
                                @foreach($catBreadcrumb as $key => $value)
                                    <li class="breadcrumbs-item">
                                        <a href="{{ $value->get('url').(!is_null(request()->get('l')) ? ( '?l=' . request()->get('l')) : '') }}">
                                            {!! $value->get('name') !!}
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                            <li class="breadcrumbs-item active" aria-current="page">
                                <a>{{ \Illuminate\Support\Str::limit($post->title, 70) }}</a></li>
                        </ol>
                    </nav>

                    {{--                    <div class="float-end ads-show__backtolist d-none d-md-block ">--}}
                    {{--                        <a class="ads-show__backtolist-link" href="{{ rawurldecode(url()->previous()) }}"><img--}}
                    {{--                                    src="{{asset('icon/account/back.svg')}}"> {{ t('back_to_results') }}</a>--}}
                    {{--                    </div>--}}

                </div>
            </div>
        </div>

        <div class="container">

            <div class="row d-none d-md-block ">
                <div class="col">
                    <div class="ads-show__info">

                        <span class="ads-show__info-item"{!! (config('lang.direction')=='rtl') ? ' dir="rtl"' : '' !!}>
								<img src="{{asset('icon/ads-show/location.svg')}}" alt="#"> {{ $post->city->name }}
							</span>
                        @if (!config('settings.single.hide_dates'))
                            <span class="ads-show__info-item"{!! (config('lang.direction')=='rtl') ? ' dir="rtl"' : '' !!}>
								<img src="{{asset('icon/ads-show/created_at.svg')}}" alt="#"> {!! $post->created_at_formatted !!}
							</span>
                        @endif
                        <span class="ads-show__info-item"{!! (config('lang.direction')=='rtl') ? ' dir="rtl"' : '' !!}>
								<img src="{{asset('icon/ads-show/show.svg')}}" alt="#"> {{ \App\Helpers\Number::short($post->visits) }}
							</span>
                    </div>
                </div>
            </div>

            <div class="row ads-show__title d-none d-md-flex ">
                <div class="col ">
                    <h1 class="ads-show__title-header">{{MetaTag::get('h1') ?? ''}}</h1>
                </div>

                @if (!in_array($post->category->type, ['not-salable']))
                    {{-- Price / Salary --}}
                    <div class="col text-end">
                        <h4 class="p-0 ads-show__title-price">
                          <span>
                                @if (is_numeric($post->price) && $post->price > 0)
                                  {!! \App\Helpers\Number::money($post->price, ' ') !!}
                              @elseif(is_numeric($post->price) && $post->price == 0)
                                  {!! t('free_as_price') !!}
                              @else
                                  {!! t('free_ad') !!}
                              @endif

                               </span>
                            @if(auth()->id() !== $post->user->id)

                                <div class="ads-show__title-buttons">
                                    @if ($post->negotiable == 1)
                                        <small class="label bg-success"> {{ t('negotiable') }}</small>
                                    @endif

                                    @if (isVerifiedPost($post))
                                        <div>
                                            @if (auth()->check())
                                                <a class="make-favorite-details" id="{{ $post->id }}"
                                                   href="javascript:void(0)">
                                                    @if (isset($post->savedByLoggedUser) && $post->savedByLoggedUser->count() > 0)
                                                        <img src="{{asset('icon/ads-show/heart-active.svg')}}"
                                                             class="like-hover--active"
                                                        >
                                                    @else
                                                        <img src="{{asset('icon/ads-show/heart.svg')}}"
                                                             class="like-hover"
                                                        >
                                                    @endif
                                                </a>
                                            @else
                                                <a class="" id="{{ $post->id }}"
                                                   href="#quickLogin"
                                                   data-bs-toggle="modal"
                                                >
                                                    <img src="{{asset('icon/ads-show/heart.svg')}}"
                                                         class="like-hover"
                                                    >
                                                </a>
                                            @endif
                                        </div>
                                        <div>
                                            <a class="ads-show__title-buttons--img-effects"
                                               href="{{ url('posts/' . $post->id . '/report') }}">
                                                <img src="{{asset('icon/ads-show/report.svg')}}"
                                                >
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            @endif

                        </h4>
                    </div>
                @endif
            </div>

            <div class="row ">
                <div class="col-md-8 col-lg-9 col-thin-right">

                    <?php $picturesSlider = 'post.inc.pictures-slider.' . config('settings.single.pictures_slider', 'horizontal-thumb'); ?>
                    @if (view()->exists($picturesSlider))
                        @includeFirst([config('larapen.core.customizedViewPath') . $picturesSlider, $picturesSlider])
                    @endif

                    @if (config('plugins.reviews.installed'))
                        @if (view()->exists('reviews::ratings-single'))
                            @include('reviews::ratings-single')
                        @endif
                    @endif

                    <!--/.items-details-wrapper-->
                </div>
                <!--/.page-content-->

                <div class="col-md-4 col-lg-3 reg-sidebar reg-sidebar--md">
                    <aside>
                        <div class="card pt-4 pb-3">
                            @if (auth()->check() && auth()->id() == $post->user_id)
                                <div class="card__header-name mb-3 ">{{ t('Manage Ad') }}</div>
                            @else
                                <div class="card__header">
                                    <div class="card__header-avatar">
                                        <img src="{{ $post->user_photo_url }}" alt="{{ $post->contact_name }}">
                                    </div>
                                    <div class="card__header-content">
                                        <h5 class="card__header-title p-0">{{ t('Posted by') }}</h5>
                                        <span class="card__header-name">
											@if (isset($user) && !empty($user))
                                                <a href="{{ \App\Helpers\UrlGen::user($user) . '?l=0' }}">
													{{ $user->name }}
												</a>
                                            @else
                                                {{ $post->contact_name }}
                                            @endif
										</span>

                                        @if (config('plugins.reviews.installed'))
                                            @if (view()->exists('reviews::ratings-user'))
                                                @include('reviews::ratings-user')
                                            @endif
                                        @endif

                                    </div>
                                </div>
                            @endif

                            <div class="card__content pb-4">
                                @if (auth()->check())
                                    @if (auth()->user()->id == $post->user_id)
                                        <a class="btn btn--success-background"
                                           href="{{ \App\Helpers\UrlGen::editPost($post) }}">
                                            <img src="{{asset('icon/btn/edit.svg')}}">
                                            {{ t('Edit') }}
                                        </a>
                                        @if($post->archived != 1)
                                            <a class="btn btn--success-background"
                                               href="{{ url('account/my-posts/'.$post->id.'/offline') }}">
                                                <img src="{{asset('icon/btn/hidden.svg')}}">
                                                {{ t('Hide') }}
                                            </a>
                                        @else
                                            <a class="btn btn--success-background"
                                               href="{{ url('account/archived/'.$post->id.'/repost') }}">
                                                <img src="{{asset('icon/btn/show.svg')}}">
                                                {{ t('Show') }}
                                            </a>
                                        @endif
                                        <a class="btn btn--delete-background"
                                           href="{{ url('account/my-posts/'.$post->id.'/delete') }}">
                                            <img src="{{asset('icon/btn/delete.svg')}}">
                                            {{ t('Delete') }}
                                        </a>
                                        @if (config('settings.single.publication_form_type') == '1')
                                            @if (isset($countPackages) && isset($countPaymentMethods) && $countPackages > 0 && $countPaymentMethods > 0)
                                                <a href="{{ url('posts/' . $post->id . '/payment') }}"
                                                   class="btn btn--success-background">
                                                    <img src="{{asset('icon/btn/write.svg')}}">
                                                    {{ t('Make It Premium') }}
                                                </a>
                                            @endif
                                        @endif
                                    @else
                                        {!! genPhoneNumberBtn($post, true) !!}
                                        {!! genEmailContactBtn($post, true) !!}
                                    @endif
                                @else
                                    {!! genPhoneNumberBtn($post, true) !!}
                                    {!! genEmailContactBtn($post, true) !!}
                                @endif
                            </div>
                        </div>

                        @if (config('settings.single.show_post_on_googlemap'))
                            <div class="card sidebar-card">
                                <div class="card-header">{{ t('location_map') }}</div>
                                <div class="card-content">
                                    <div class="card-body text-start p-0">
                                        <div class="posts-googlemaps">
                                            <iframe id="googleMaps" width="100%" height="250" frameborder="0"
                                                    scrolling="no" marginheight="0" marginwidth="0" src=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="card reg-sidebar__card pt-4 mt-4">
                            <h3>{{ t('Safety Tips for Buyers') }}</h3>
                            <div class="card-contact">
                                <div class="text-start">
                                    <ul class="reg-sidebar__card-list border-bottom-0 p-0">
                                        <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                                                 alt="#">
                                            <span>{{ t('Meet seller at a public place') }}</span>
                                        </li>
                                        <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                                                 alt="#">
                                            <span>{{ t('Check the item before you buy') }}</span>
                                        </li>
                                        <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                                                 alt="#">
                                            <span>{{ t('Pay only after collecting the item') }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </aside>
                </div>

                <div class="col d-block d-md-none ">

                    <div class="d-flex mb-4 flex-wrap gap-2 ads-show__info">

                            <span class="col-12 ads-show__info-item"{!! (config('lang.direction')=='rtl') ? ' dir="rtl"' : '' !!}>
								<img src="{{asset('icon/ads-show/location.svg')}}" alt="#"> {{ $post->city->name }}
							</span>
                        <div class="col-12 d-flex justify-content-between">
                            @if (!config('settings.single.hide_dates'))
                                <span class="ads-show__info-item"{!! (config('lang.direction')=='rtl') ? ' dir="rtl"' : '' !!}>
								<img src="{{asset('icon/ads-show/created_at.svg')}}" alt="#"> {!! $post->created_at_formatted !!}
							  </span>
                            @endif
                            <span class="ads-show__info-item"{!! (config('lang.direction')=='rtl') ? ' dir="rtl"' : '' !!}>
								<img src="{{asset('icon/ads-show/show.svg')}}" alt="#"> {{ \App\Helpers\Number::short($post->visits) }}
							  </span>
                        </div>
                    </div>
                </div>

                <div class="ads-show__title d-block d-md-none ">
                    <div class="col-12 d-flex justify-content-between align-items-center ">
                        <h1 class="ads-show__title-header p-0">
                            <span>
                                {{ $post->title }}
                            </span>

                            @if (config('settings.single.show_post_types'))
                                @if (isset($post->postType) && !empty($post->postType))
                                    <small class="label label-default adlistingtype">{{ $post->postType->name }}</small>
                                @endif
                            @endif
                            @if ($post->featured==1 && !empty($post->latestPayment))
                                @if (isset($post->latestPayment->package) && !empty($post->latestPayment->package))
                                    <i class="icon-ok-circled"
                                       style="color: {{ $post->latestPayment->package->ribbon }};"
                                       data-bs-placement="bottom"
                                       data-bs-toggle="tooltip"
                                       title="{{ $post->latestPayment->package->short_name }}"
                                    ></i>
                                @endif
                            @endif
                        </h1>

                        <div class="ads-show__title-buttons ads-show__title-buttons--gap">

                            @if (auth()->check())
                                @if (auth()->user()->id == $post->user_id)
                                    <a class=""
                                       href="{{ \App\Helpers\UrlGen::editPost($post) }}">
                                        <img src="{{asset('icon/btn/edit.svg')}}">
                                    </a>
                                    @if($post->archived != 1)
                                        <a class=""
                                           href="{{ url('account/my-posts/'.$post->id.'/offline') }}">
                                            <img src="{{asset('icon/btn/hidden.svg')}}">
                                        </a>
                                    @else
                                        <a class=""
                                           href="{{ url('account/archived/'.$post->id.'/repost') }}">
                                            <img src="{{asset('icon/btn/show.svg')}}">
                                        </a>
                                    @endif
                                    <a class="ads-show__title-buttons--delete"
                                       href="{{ url('account/my-posts/'.$post->id.'/delete') }}">
                                        <img src="{{asset('icon/btn/delete.svg')}}">
                                    </a>
                                @else
                                    @if ($post->negotiable == 1)
                                        <small class="label bg-success"> {{ t('negotiable') }}</small>
                                    @endif

                                    @if (isVerifiedPost($post))
                                        <div>
                                            @if (auth()->check())
                                                <a class="make-favorite-details" id="{{ $post->id }}"
                                                   href="javascript:void(0)">
                                                    @if (isset($post->savedByLoggedUser) && $post->savedByLoggedUser->count() > 0)
                                                        <img src="{{asset('icon/ads-show/heart-active.svg')}}"
                                                             class="like-hover--active"
                                                        >
                                                    @else
                                                        <img src="{{asset('icon/ads-show/heart.svg')}}"
                                                             class="like-hover"
                                                        >
                                                    @endif
                                                </a>
                                            @else
                                                <a class="" id="{{ $post->id }}"
                                                   href="#quickLogin"
                                                   data-bs-toggle="modal"
                                                >
                                                    <img src="{{asset('icon/ads-show/heart.svg')}}"
                                                         class="like-hover"
                                                    >
                                                </a>
                                            @endif
                                        </div>
                                        <div>
                                            <a href="{{ url('posts/' . $post->id . '/report') }}">
                                                <img src="{{asset('icon/ads-show/circled.svg')}}"
                                                >
                                            </a>
                                        </div>
                                    @endif
                                @endif
                            @endif
                        </div>
                    </div>

                    @if (!in_array($post->category->type, ['not-salable']))
                        {{-- Price / Salary --}}
                        <div class="col ">
                            <h4 class="p-0 ads-show__title-price text-start justify-content-start">
                                <span class="p-0">
                                    @if (is_numeric($post->price) && $post->price > 0)
                                        {!! \App\Helpers\Number::money($post->price, ' ') !!}
                                    @elseif(is_numeric($post->price) && $post->price == 0)
                                        {!! t('free_as_price') !!}
                                    @else
                                        {!! t('free_ad') !!}
                                    @endif
                                 </span>
                            </h4>
                        </div>
                    @endif
                    @if (auth()->check())
                        @if (auth()->user()->id == $post->user_id)
                            <div class="row gap-2 d-md-none m-0">
                                @if (config('settings.single.publication_form_type') == '1')
                                    @if (isset($countPackages) && isset($countPaymentMethods) && $countPackages > 0 && $countPaymentMethods > 0)
                                        <a href="{{ url('posts/' . $post->id . '/payment') }}"
                                           class="btn btn--success-background">
                                            <img src="{{asset('icon/btn/write.svg')}}">
                                            {{ t('Make It Premium') }}
                                        </a>
                                    @endif
                                @endif
                                @else
                                    <div class="col mt-4">
                                        @if($post->phone && $post->phone_hidden == 0)
                                            <div class="col">
                                                {!! genPhoneNumberBtn($post, true) !!}
                                            </div>
                                        @endif
                                        <div class="col mt-2">
                                            {!! genEmailContactBtn($post, true) !!}
                                        </div>
                                    </div>
                                @endif
                                @else
                                    <div class="col mt-4">
                                        @if($post->phone && $post->phone_hidden == 0)
                                            <div class="col">
                                                {!! genPhoneNumberBtn($post, true) !!}
                                            </div>
                                        @endif
                                        <div class="col mt-2">
                                            {!! genEmailContactBtn($post, true) !!}
                                        </div>
                                    </div>
                                @endif
                            </div>
                </div>

                <div class="ads-show__descriptions">
                    {{-- Tab panes --}}
                    <div class="tab-content mb-3" id="itemsDetailsTabsContent">
                        <div class="tab-pane show active" id="item-details" role="tabpanel"
                             aria-labelledby="item-details-tab">
                            <div class="row pb-3">
                                <div class="items-details-info col-md-12 col-sm-12 col-12 ">

                                    <div class="row mt-4 mt-lg-5">
                                        {{-- Location --}}
                                        <div class="col-md-6 col-sm-6 col-6">
                                            <h4 class="ads-show__descriptions-title   p-0">  {{ t('location') }}</h4>

                                            <a class="ads-show__descriptions-content ads-show__descriptions-content--city"
                                               href="{!! '/search?l=' . $post->city->id !!}">
                                                <img src="{{asset('icon/ads-show/location.svg')}}"
                                                     alt="#"> {{ $post->city->name }}
                                            </a>
                                        </div>
                                    </div>

                                    {{-- Description --}}
                                    <div class="row mt-4 mt-lg-5">
                                        <div class="col-12 ">
                                            <h4 class="ads-show__descriptions-title p-0"> {{ t('describe_what_makes_your_ad_unique') }}</h4>

                                            <div class="p-0 m-0 ads-show__descriptions-content">{!! nl2br($post->description) !!}</div>
                                        </div>
                                    </div>

                                    {{-- Custom Fields --}}
                                    @includeFirst([config('larapen.core.customizedViewPath') . 'post.inc.fields-values', 'post.inc.fields-values'])

                                    {{-- Tags --}}
                                    @if (!empty($post->tags))
                                        <div class="row mt-4 mt-lg-5 ads-show-tugs">
                                            <div class="col-12">
                                                <h4 class="ads-show__descriptions-title p-0">
                                                    <img src="{{asset('icon/ads-show/tugs.svg')}}" alt="#">
                                                    {{ t('Tags') }}
                                                </h4>

                                                <div class=" ads-show-tugs__chips">
                                                    @foreach($post->tags as $iTag)
                                                        <a class="ads-show-tugs__item"

                                                           href="{{ \App\Helpers\UrlGen::tag($iTag).(!is_null(request()->get('l')) ? ( '?l=' . request()->get('l')) : '') }}">
                                                            {{ $iTag }}
                                                        </a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        @if (config('plugins.reviews.installed'))
                            @if (view()->exists('reviews::comments'))
                                @include('reviews::comments')
                            @endif
                        @endif
                    </div>
                    <!-- /.tab content -->

                </div>

                <div class="col-md-4 col-lg-3 reg-sidebar d-block d-md-none ">
                    <aside>
                        @if (auth()->check())
                            @if (auth()->user()->id != $post->user_id)

                                <div class="card pt-4 pb-3">
                                    <div class="card__header">
                                        <div class="card__header-avatar">
                                            <img src="{{ $post->user_photo_url }}" alt="{{ $post->contact_name }}">
                                        </div>
                                        <div class="card__header-content">
                                            <h5 class="card__header-title p-0">{{ t('Posted by') }}</h5>
                                            <span class="card__header-name">
                   @if (isset($user) && !empty($user))
                                                    <a href="{{ \App\Helpers\UrlGen::user($user) . '?l=0' }}">
                           {{ $user->name }}
                       </a>
                                                @else
                                                    {{ $post->contact_name }}
                                                @endif
               </span>

                                            @if (config('plugins.reviews.installed'))
                                                @if (view()->exists('reviews::ratings-user'))
                                                    @include('reviews::ratings-user')
                                                @endif
                                            @endif

                                        </div>
                                    </div>
                                    <div class="card__content">
                                        @if (auth()->check())
                                            @if (auth()->user()->id == $post->user_id)
                                                @if (config('settings.single.publication_form_type') == '1')
                                                    @if (isset($countPackages) && isset($countPaymentMethods) && $countPackages > 0 && $countPaymentMethods > 0)
                                                        <a href="{{ url('posts/' . $post->id . '/payment') }}"
                                                           class="btn btn--success-background">
                                                            <img src="{{asset('icon/btn/write.svg')}}">
                                                            {{ t('Make It Premium') }}
                                                        </a>
                                                    @endif
                                                @endif
                                            @else
                                                {!! genPhoneNumberBtn($post, true) !!}
                                                {!! genEmailContactBtn($post, true) !!}
                                            @endif
                                        @else
                                            {!! genPhoneNumberBtn($post, true) !!}
                                            {!! genEmailContactBtn($post, true) !!}
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endif

                        @if (config('settings.single.show_post_on_googlemap'))
                            <div class="card sidebar-card">
                                <div class="card-header">{{ t('location_map') }}</div>
                                <div class="card-content">
                                    <div class="card-body text-start p-0">
                                        <div class="posts-googlemaps">
                                            <iframe id="googleMaps" width="100%" height="250" frameborder="0"
                                                    scrolling="no" marginheight="0" marginwidth="0" src=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="card reg-sidebar__card reg-sidebar__card--background pt-4 mt-4">
                            <h3>{{ t('Safety Tips for Buyers') }}</h3>
                            <div class="card-contact">
                                <div class="text-start">
                                    <ul class="reg-sidebar__card-list border-bottom-0 p-0">
                                        <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                                                 alt="#">
                                            <span>{{ t('Meet seller at a public place') }}</span>
                                        </li>
                                        <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                                                 alt="#">
                                            <span>{{ t('Check the item before you buy') }}</span>
                                        </li>
                                        <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                                                 alt="#">
                                            <span>{{ t('Pay only after collecting the item') }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </aside>
                </div>
            </div>

            @includeFirst([config('larapen.core.customizedViewPath') . 'layouts.inc.advertising.bottom', 'layouts.inc.advertising.bottom'], ['firstSection' => false])

            @if (isVerifiedPost($post))
                @includeFirst([config('larapen.core.customizedViewPath') . 'layouts.inc.tools.facebook-comments', 'layouts.inc.tools.facebook-comments'], ['firstSection' => false])
            @endif

        </div>
        @endsection
        <?php
        if (!session()->has('emailVerificationSent') && !session()->has('phoneVerificationSent')) {
            if (session()->has('message')) {
                session()->forget('message');
            }
        }
        ?>

        @section('modal_message')
            @if (config('settings.single.show_security_tips') == '1')
                @includeFirst([config('larapen.core.customizedViewPath') . 'post.inc.security-tips', 'post.inc.security-tips'])
            @endif
            @if (auth()->check() || config('settings.single.guests_can_contact_ads_authors')=='1')
                @includeFirst([config('larapen.core.customizedViewPath') . 'account.messenger.modal.create', 'account.messenger.modal.create'])
            @endif
        @endsection

        @section('after_styles')
            {{-- bxSlider CSS file --}}
            @if (config('lang.direction') == 'rtl')
                <link href="{{ url('assets/plugins/bxslider/jquery.bxslider.rtl.css') }}" rel="stylesheet"/>
            @else
                <link href="{{ url('assets/plugins/bxslider/jquery.bxslider.css') }}" rel="stylesheet"/>
            @endif
        @endsection

        @section('before_scripts')
            <script>
                var showSecurityTips = '{{ config('settings.single.show_security_tips', '0') }}';
            </script>
        @endsection

        @section('after_scripts')
            @if (config('services.googlemaps.key'))
                <script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.googlemaps.key') }}"
                        type="text/javascript"></script>
            @endif

            {{-- bxSlider Javascript file --}}
            <script src="{{ url('assets/plugins/bxslider/jquery.bxslider.min.js') }}"></script>

                @if (env('APP_ENV') != 'local')
                    <script>
                        $('#btnCall').click(function () {
                            ym(91551648, 'reachGoal', 'clicktel');
                        });
                    </script>
                    @if(request()->session()->get('onboard') !== null)
                        <script>
                            ym(91551648, 'reachGoal', 'onboard');
                        </script>
                    @endif
                @endif

            <script>
                {{-- Favorites Translation --}}
                var lang = {
                    labelSavePostSave: "{!! t('Save ad') !!}",
                    labelSavePostRemove: "{!! t('Remove favorite') !!}",
                    loginToSavePost: "{!! t('Please log in to save the Ads') !!}",
                    loginToSaveSearch: "{!! t('Please log in to save your search') !!}",
                    confirmationSavePost: "{!! t('Post saved in favorites successfully') !!}",
                    confirmationRemoveSavePost: "{!! t('Post deleted from favorites successfully') !!}",
                    confirmationSaveSearch: "{!! t('Search saved successfully') !!}",
                    confirmationRemoveSaveSearch: "{!! t('Search deleted successfully') !!}"
                };

                $(document).ready(function () {
                    {{-- Tooltip --}}
                    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[rel="tooltip"]'));
                    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                        return new bootstrap.Tooltip(tooltipTriggerEl)
                    });

                    @if (config('settings.single.show_post_on_googlemap'))
                    {{-- Google Maps --}}
                    getGoogleMaps(
                        '{{ config('services.googlemaps.key') }}',
                        '{{ (isset($post->city) && !empty($post->city)) ? addslashes($post->city->name) . ',' . config('country.name') : config('country.name') }}',
                        '{{ config('app.locale') }}'
                    );
                    @endif

                    {{-- Keep the current tab active with Twitter Bootstrap after a page reload --}}
                    $('button[data-bs-toggle="tab"]').on('shown.bs.tab', function (e) {
                        /* save the latest tab; use cookies if you like 'em better: */
                        /* localStorage.setItem('lastTab', $(this).attr('href')); */
                        localStorage.setItem('lastTab', $(this).attr('data-bs-target'));
                    });
                    {{-- Go to the latest tab, if it exists: --}}
                    let lastTab = localStorage.getItem('lastTab');
                    if (lastTab) {
                        let triggerEl;

                        /* triggerEl = document.querySelector('a[href="' + lastTab + '"]'); */
                        triggerEl = document.querySelector('button[data-bs-target="' + lastTab + '"]');

                        let tab = new bootstrap.Tab(triggerEl);
                        tab.show();
                    }
                });
            </script>
@endsection