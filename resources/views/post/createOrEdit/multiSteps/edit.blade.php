{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('wizard')
    @includeFirst([config('larapen.core.customizedViewPath') . 'post.createOrEdit.multiSteps.inc.wizard', 'post.createOrEdit.multiSteps.inc.wizard'])
@endsection

<?php
// Category
if ($post->category) {
    if (empty($post->category->parent_id)) {
        $postCatParentId = $post->category->id;
    } else {
        $postCatParentId = $post->category->parent_id;
    }
} else {
    $postCatParentId = 0;
}
?>
@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="main-container">
        <div class="container">
            <div class="row">

                @includeFirst([config('larapen.core.customizedViewPath') . 'post.inc.notification', 'post.inc.notification'])


                <div class="col-md-9 col-xl-9 post-free-container">
                    <h2 class="post-free__title">
                        <a href="{{ \App\Helpers\UrlGen::post($post) }}"
                        >{!! \Illuminate\Support\Str::limit($post->title, 45) !!}</a>
                    </h2>

                    <div class="row">
                        <div class="col-12">

                            <form class="form-horizontal" id="postForm" method="POST"
                                  action="{{ url()->current() }}" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <input name="_method" type="hidden" value="PUT">
                                <input type="hidden" name="post_id" value="{{ $post->id }}">
                                <fieldset>

                                    {{-- category_id --}}
                                    <?php $categoryIdError = (isset($errors) and $errors->has('category_id')) ? ' is-invalid' : ''; ?>
                                    <div class="row mb-3 required">
                                        <div class="col  ">

                                            <div id="categorySelect" class="col input {{ $categoryIdError }}">
                                                <label class="input__label">{{ t('category') }}
                                                    <sup>*</sup></label>
                                                <div class="col">
                                                    <div id="catsContainer"
                                                         class="input-select">
                                                        @if($post->category)
                                                            @if (!empty($post->category->parent))
                                                                <a href="#browseCategories" data-bs-toggle="modal"
                                                                   id="categoryLinkTitle"
                                                                   class="cat-link"
                                                                   data-id="0">
                                                                    @component('post.createOrEdit.inc.category.postEditCategory', ['parent' => $post->category])@endcomponent
                                                                </a>
                                                            @endif
                                                        @else
                                                            {{ t('select_a_category') }}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" name="category_id" id="categoryId"
                                                   value="{{ old('category_id', $post->category->id) }}">
                                            <input type="hidden" name="category_type" id="categoryType"
                                                   value="{{ old('category_type', $post->category->type) }}">
                                        </div>
                                    </div>

                                    @if (config('settings.single.show_post_types'))
                                        {{-- post_type_id --}}
                                        @if (isset($postTypes))
                                                <?php $postTypeIdError = (isset($errors) and $errors->has('post_type_id')) ? ' is-invalid' : ''; ?>
                                            <div id="postTypeBloc" class="row mb-3 required input">
                                                <label class="col-md-3 input__label{{ $postTypeIdError }}">{{ t('type') }}
                                                    <sup>*</sup></label>
                                                <div class="col-md-8">
                                                    @foreach ($postTypes as $postType)
                                                        <div class="form-check form-check-inline">
                                                            <input name="post_type_id"
                                                                   id="postTypeId-{{ $postType->id }}"
                                                                   value="{{ $postType->id }}"
                                                                   type="radio"
                                                                   class="form-check-input{{ $postTypeIdError }}"
                                                                    {{ (old('post_type_id', $post->post_type_id)==$postType->id) ? ' checked="checked"' : '' }}
                                                            >
                                                            <label class="form-check-label mb-0"
                                                                   for="postTypeId-{{ $postType->id }}">
                                                                {{ $postType->name }}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                    <div class="form-text text-muted">{{ t('post_type_hint') }}</div>
                                                </div>
                                            </div>
                                        @endif
                                    @endif

                                    {{-- title --}}
                                    <?php $titleError = (isset($errors) and $errors->has('title')) ? ' is-invalid' : ''; ?>
                                    <div class="row mb-3 required input">
                                        <div class="col {{ $titleError }}">
                                            <label class="input__label"
                                                   for="title">{{ t('title') }} <sup>*</sup></label>
                                            <div class="col">
                                                <input id="title" name="title" placeholder="{{ t('ad_title') }}"
                                                       class="input-text"
                                                       type="text" value="{{ old('title', $post->title) }}">
                                                <div class="form-text">{{ t('a_great_title_needs_at_least_60_characters') }}</div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- description --}}
                                    <?php $descriptionError = (isset($errors) and $errors->has('description')) ? ' is-invalid' : ''; ?>
                                    <div class="row mb-3 required input">
                                        <div class="input {{ $descriptionError }}">
                                            <?php
                                            $descriptionErrorLabel = '';
                                            $descriptionColClass = 'col-md-8';
                                            if (config('settings.single.wysiwyg_editor') != 'none') {
                                                $descriptionColClass = 'col-md-12';
                                                $descriptionErrorLabel = $descriptionError;
                                            } else {
                                                $post->description = strip_tags($post->description);
                                            }
                                            ?>
                                            <label class="input__label{{ $descriptionErrorLabel }}"
                                                   for="description">
                                                {{ t('Description') }} <sup>*</sup>
                                            </label>
                                            <div class="{{ $descriptionColClass }}">
												<textarea
                                                        class="input-text custom-scroll-bar"
                                                        name="description"
                                                        placeholder="{{ t('placeholder_description') }}"
                                                        rows="15"
                                                        style="height: 300px"
                                                >{{ old('description', $post->description) }}</textarea>
                                                <div class="form-text text-muted">{{ t('describe_what_makes_your_ad_unique') }}</div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- cfContainer --}}
                                    <div id="cfContainer"></div>

                                    <div class="row mb-4">
                                        {{-- ads type--}}
                                        <div class="col-md-4" id="selectTypeAds">
                                            <div class="input mb-3">
                                                <label class="input__label"
                                                       for="ads_type">{{ t('ads_type') }}</label>
                                                <div class="input-select-2">
                                                    <select id="adsType" name="ads_type"
                                                            class="ad-type-selecter">
                                                        @foreach(\App\Enums\AdsTypesEnum::adsTypes() as $type)
                                                            <option value="{{ array_search($type, \App\Enums\AdsTypesEnum::adsTypes()) }}"
                                                                    {{ $post->ads_type && $post->ads_type == array_search($type, \App\Enums\AdsTypesEnum::adsTypes()) ? 'selected="selected"' : '' }}>
                                                                {{ $type }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="ad-type-dropdown"></div>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- price --}}
                                        <div class="col-md-4">
                                            <?php $priceError = (isset($errors) and $errors->has('price')) ? ' is-invalid' : ''; ?>
                                            <div id="priceBloc" class="row mb-3 required input {{ $priceError }} ">
                                                <label class="input__label"
                                                       for="price">{{ t('price') }}<sup>*</sup></label>
                                                <div class="col">
                                                    <?php
                                                    $price = \App\Helpers\Number::format(old('price', $post->price), 2, '.', '');
                                                    ?>
                                                    <input id="price"
                                                           name="price"
                                                           class="input-text"
                                                           placeholder="{{ t('ei_price') }}"
                                                           type="number"
                                                           min="0"
                                                           step="{{ getInputNumberStep((int)config('currency.decimal_places', 2)) }}"
                                                           value="{!! $price !!}"
                                                    >
                                                </div>
                                            </div>
                                        </div>

                                        <div class=" p-l-15 col-md-8 col-xl-9">
                                            {{-- country_code --}}
                                            <input id="countryCode" name="country_code" type="hidden"
                                                   value="{{ !empty($post->country_code) ? $post->country_code : config('country.code') }}">

                                            @if (config('country.admin_field_active') == 1 and in_array(config('country.admin_type'), ['1', '2']))
                                                {{-- admin_code --}}
                                                    <?php $adminCodeError = (isset($errors) and $errors->has('admin_code')) ? ' is-invalid' : ''; ?>
                                                <div id="locationBox" class="row mb-3 required">
                                                    <label class="col-md-3 col-form-label{{ $adminCodeError }}"
                                                           for="admin_code">
                                                        {{ t('location') }} <sup>*</sup>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select id="adminCode" name="admin_code"
                                                                class="form-control large-data-selecter{{ $adminCodeError }}">
                                                            <option value="0" {{ (!old('admin_code') or old('admin_code')==0) ? 'selected="selected"' : '' }}>
                                                                {{ t('select_your_location') }}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- city_id --}}
                                            <?php $cityIdError = (isset($errors) and $errors->has('city_id')) ? ' is-invalid' : ''; ?>
                                            <div id="cityBox" class="row mb-3 required input {{ $cityIdError }}">
                                                <label class="input__label" for="city_id">
                                                    {{ t('city') }} <sup>*</sup>
                                                </label>
                                                <div class="input-select-2">
                                                    <select id="citySelect" name="city_id"
                                                            class="form-control large-data-selecter">
                                                        <option value="0" {{ (!old('city_id') or old('city_id')==0) ? 'selected="selected"' : '' }}>
                                                            {{ t('select_a_city') }}
                                                        </option>
                                                        @foreach($cityList as $key => $cityName)
                                                            <option value="{{ $key }}"
                                                                    @if($post->city_id == $key) selected="selected"@endif>
                                                                {{ $cityName }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="create-city-select-dropdown"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- tags --}}
                                    <?php $tagsError = (isset($errors) and $errors->has('tags.*')) ? ' is-invalid' : ''; ?>
                                    <div class="row mb-3">
                                        <label class="input__label{{ $tagsError }}"
                                               for="tags">{{ t('Tags') }}</label>
                                        <div class="col input-tugs">
                                            <select id="tags" name="tags[]" class="tags-selecter"
                                                    multiple="multiple">
                                                <?php $tags = old('tags', $post->tags); ?>
                                                @if (!empty($tags))
                                                    @foreach($tags as $iTag)
                                                        <option selected="selected">{{ $iTag }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <div class="form-text text-muted">
                                                {!! t('tags_hint', [
                                                        'limit' => (int)config('settings.single.tags_limit', 15),
                                                        'min'   => (int)config('settings.single.tags_min_length', 2),
                                                        'max'   => (int)config('settings.single.tags_max_length', 30)
                                                    ]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    {{-- is_permanent --}}
                                    @if (config('settings.single.permanent_posts_enabled') == '3')
                                        <input type="hidden" name="is_permanent" id="isPermanent"
                                               value="{{ old('is_permanent', $post->is_permanent) }}">
                                    @else
                                            <?php $isPermanentError = (isset($errors) and $errors->has('is_permanent')) ? ' is-invalid' : ''; ?>
                                        <div id="isPermanentBox" class="row mb-3 required hide">
                                            <label class="col-md-3 col-form-label"></label>
                                            <div class="col-md-8">
                                                <div class="form-check">
                                                    <input name="is_permanent" id="isPermanent"
                                                           class="form-check-input mt-1{{ $isPermanentError }}"
                                                           value="1"
                                                           type="checkbox" {{ (old('is_permanent', $post->is_permanent)=='1') ? 'checked="checked"' : '' }}
                                                    >
                                                    <label class="form-check-label mt-0" for="is_permanent">
                                                        {!! t('is_permanent_label') !!}
                                                    </label>
                                                </div>
                                                <div class="form-text text-muted">{{ t('is_permanent_hint') }}</div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                    @endif


                                    <div class=" post-free__title-salesman">
                                        <strong>{{ t('seller_information') }}</strong>
                                    </div>


                                    <div class="row">
                                        {{-- contact_name --}}
                                        <?php $contactNameError = (isset($errors) and $errors->has('contact_name')) ? ' is-invalid' : ''; ?>
                                        <div class="mb-3 input col-md-4 col-xl-3 {{ $contactNameError }}">
                                            <label class="input__label"
                                                   for="contact_name">
                                                {{ t('your_name') }} <sup>*</sup>
                                            </label>
                                            <div class="">
                                                <input id="contact_name"
                                                       name="contact_name"
                                                       placeholder="{{ t('your_name') }}"
                                                       class="input-text"
                                                       type="text"
                                                       value="{{ old('contact_name', $post->contact_name) }}">
                                            </div>
                                        </div>

                                        {{-- phone --}}
                                        <?php $phoneError = (isset($errors) and $errors->has('phone')) ? ' is-invalid' : ''; ?>
                                        <div class="mb-3 input input-phone col-md-8 col-xl-5 {{ $phoneError }}">
                                            <label class="input__label"
                                                   for="phone">{{ t('phone_number') }}
                                                @if (!isEnabledField('email'))
                                                    <sup>*</sup>
                                                @endif
                                            </label>
                                            <div class="">
                                                <div class="input-phone--flex-no-wrap ">
                                                    <input id="phone" name="phone"
                                                           placeholder="{{ t('phone_number') }}"
                                                           class="js-phone-mask input-text post-free__salesman-input-input"
                                                           type="text"
                                                           value="{{ old('phone', $post->phone) }}">
                                                    <span class="post-free__salesman-input-phone">
														<input class="input-phone--hidden"
                                                               name="phone_hidden" id="phoneHidden" type="checkbox"
                                                               value="1" {{ (old('phone_hidden', $post->phone_hidden)=='1') ? 'checked="checked"' : '' }}>
                                                        <label class="post-free__salesman-label-phone untouchable js-phone-hidden"
                                                               for="phoneHidden">{{ t('hide') }}</label>
													</span>
                                                </div>
                                                <div id="phone-support-text"
                                                     class="form-text">{{t('You can hide the number from other users')}}</div>
                                            </div>
                                        </div>

                                        {{-- email --}}
                                        <?php $emailError = (isset($errors) and $errors->has('email')) ? ' is-invalid' : ''; ?>
                                        <div class=" mb-3 input col-xl-4 {{ $emailError }}">
                                            <label class="input__label"
                                                   for="email">{{ t('email') }}
                                                @if (!isEnabledField('phone') or !auth()->check())
                                                    <sup>*</sup>
                                                @endif
                                            </label>
                                            <div class="">
                                                <div class="input-group ">
                                                    <input id="email" name="email"
                                                           class="input-text"
                                                           placeholder="{{ t('email') }}"
                                                           type="text"
                                                           value="{{ old('email', $post->email) }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- Button --}}
                                    <div class="row mb-3 pt-5 ">
                                        <div class="d-flex gap-4 col-md-9 col-xl-7 text-center">
                                            <button id="nextStepBtn"
                                                    class="col btn btn--success btn--height">{{ t('Further') }}</button>
                                            <a href="{{ \App\Helpers\UrlGen::post($post) }}"
                                               class="col d-flex align-center justify-content-center btn btn--cancel btn--height">
                                                {{ t('Back') }}
                                            </a>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>

                        </div>
                    </div>
                </div>

                <!-- /.page-content -->

                <div class="col-md-3 reg-sidebar">
                    @includeFirst([config('larapen.core.customizedViewPath') . 'post.createOrEdit.inc.right-sidebar', 'post.createOrEdit.inc.right-sidebar'])
                </div>

            </div>
        </div>
    </div>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="{{ asset('/js/validation/clear-is-invalid.js') }}"></script>

    <script>
        $('#phone').focus(function () {
            $('#phone-support-text').show();
        });
        $('#phone').focusout(function () {
            $('#phone-support-text').hide();
        });
    </script>

    <script>
        $('.js-phone-hidden').click(function () {
            if ($('#phoneHidden').is(':checked')) {
                $('.js-phone-hidden').html('{{ t('hide') }}')
            } else {
                $('.js-phone-hidden').html('{{ t('show') }}')
            }
        });
    </script>

    <script>
        $('#priceBloc').hide();

        let postFree = {{ $post->ads_type }} === {{ \App\Enums\AdsTypesEnum::FREE }};

        $(document).ready(function () {
            clearIsInvalid();

            $('.ad-type-selecter').select2({
                language: langLayout.select2,
                width: '100%',
                dropdownAutoWidth: 'true',
                dropdownParent: $('.ad-type-dropdown'),
            });

            $('#citySelect').select2({
                language: langLayout.select2,
                width: '100%',
                dropdownAutoWidth: 'true',
                dropdownParent: $('.create-city-select-dropdown')
            });

            $('#adsType').on('select2:select', function (e) {
                let data = e.params.data;
                if (parseInt(data.id) == '{{ \App\Enums\AdsTypesEnum::FREE }}') {
                    $('#priceBloc').hide();
                } else {
                    $('#priceBloc').show();
                }
            });
        });
    </script>

    @includeFirst([config('larapen.core.customizedViewPath') . 'post.createOrEdit.inc.category-modal', 'post.createOrEdit.inc.category-modal'])
@endsection

@section('after_styles')
@endsection

@section('after_scripts')
@endsection

@includeFirst([config('larapen.core.customizedViewPath') . 'post.createOrEdit.inc.form-assets', 'post.createOrEdit.inc.form-assets'])
