@if (isset($wizardMenu) && !empty($wizardMenu))
    <div class="step-wizard">
        <div class="step-wizard__container">
            <ul class="step-wizard__container-nav">
                @foreach($wizardMenu as $menu)
                    @continue(!$menu['condition'])
                    <li class="{{ $menu['class'] }}">
                        @if (!empty($menu['url']))
                            <img class="step-wizard__icon " src="{{ $menu['icon'] }}" alt="#">
                            <a href="{{ $menu['url'] }}">{{ $menu['name'] }}</a>
                            <img class="step-wizard__step "
                                 src="{{asset('icon/creating-advertisement/further.svg')}}" alt="#">
                        @else
                            <img class="step-wizard__icon" src="{{ $menu['icon'] }}" alt="#">
                            <p>{{ $menu['name'] }}</p>
                            <img class="step-wizard__step" src="{{asset('icon/creating-advertisement/further.svg')}}"
                                 alt="#">
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

@section('after_styles')
    @parent
    @if (config('lang.direction') == 'rtl')
        <link href="{{ url('assets/css/rtl/wizard.css') }}" rel="stylesheet">
    @else
        <link href="{{ url('assets/css/wizard.css') }}" rel="stylesheet">
    @endif
@endsection
@section('after_scripts')
    @parent
@endsection