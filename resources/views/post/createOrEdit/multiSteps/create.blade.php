{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}

@extends('layouts.master')

@section('content')
    <div class="main-container">
        @includeFirst([config('larapen.core.customizedViewPath') . 'post.createOrEdit.multiSteps.inc.wizard', 'post.createOrEdit.multiSteps.inc.wizard'])
        @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])

        <div class="container">
            <div class="row ">

                @includeFirst([config('larapen.core.customizedViewPath') . 'post.inc.notification', 'post.inc.notification'])

                <?php $postInput = $postInput ?? []; ?>
                <div class="col-md-9 col-xl-9 post-free-container">
                    <h1>{{MetaTag::get('h1') ?? ''}}</h1>

                    <div class="row ">
                        <div class="col-xl-12">

                            <form class="form-horizontal" id="postForm" method="POST"
                                  action="{{ request()->fullUrl() }}" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <fieldset class="">

                                    {{-- category_id --}}
                                    <?php $categoryIdError = (isset($errors) && $errors->has('category_id')) ? ' is-invalid' : ''; ?>
                                    <div class="row">
                                        <div class=" mb-4 col  ">
                                            <div id="categorySelect" class="input {{ $categoryIdError }}">
                                                <label class="input__label">{{ t('category') }}
                                                    <sup>*</sup></label>
                                                <div class="">
                                                    <div class="input-select">
                                                        <a href="#browseCategories" data-bs-toggle="modal"
                                                           id="categoryLinkTitle"
                                                           class="cat-link"
                                                           data-id="0">
                                                            <h3 class="input-select--text-color">{{ t('select_a_category') }}</h3>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="category_id" id="categoryId"
                                                   value="{{ old('category_id', data_get($postInput, 'category_id', 0)) }}">
                                            <input type="hidden" name="category_type" id="categoryType"
                                                   value="{{ old('category_type', data_get($postInput, 'category_type')) }}">
                                        </div>
                                    </div>
                                    @if (config('settings.single.show_post_types'))
                                        {{-- post_type_id --}}
                                        @if (isset($postTypes))
                                                <?php $postTypeIdError = (isset($errors) && $errors->has('post_type_id')) ? ' is-invalid' : ''; ?>
                                            <div id="postTypeBloc" class="row mb-3 required">
                                                <label class="col-form-label">{{ t('type') }} <sup>*</sup></label>
                                                <div class="col-md-8 {{ $postTypeIdError }}">
                                                    @foreach ($postTypes as $postType)
                                                        <div class="form-check form-check-inline pt-2">
                                                            <input name="post_type_id"
                                                                   id="postTypeId-{{ $postType->id }}"
                                                                   value="{{ $postType->id }}"
                                                                   type="radio"
                                                                   class="form-check-input"
                                                                    {{ (old('post_type_id', data_get($postInput, 'post_type_id'))==$postType->id) ? ' checked="checked"' : '' }}
                                                            >
                                                            <label class="form-check-label mb-0"
                                                                   for="postTypeId-{{ $postType->id }}">
                                                                {{ $postType->name }}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                    <div class="form-text text-muted">{{ t('post_type_hint') }}</div>
                                                </div>
                                            </div>
                                        @endif
                                    @endif

                                    {{-- title --}}
                                    <?php $titleError = (isset($errors) && $errors->has('title')) ? ' is-invalid' : ''; ?>
                                    <div class="row">
                                        <div class=" mb-4 col">
                                            <div class="input {{ $titleError }}">
                                                <label class="input__label"
                                                       for="title">{{ t('title') }} <sup>*</sup> </label>
                                                <div class="">
                                                    <input id="title" name="title" placeholder="{{ t('ad_title') }}"
                                                           class="input-text"
                                                           type="text"
                                                           value="{{ old('title', data_get($postInput, 'title')) }}">
                                                    <div class="form-text">{{ t('a_great_title_needs_at_least_60_characters') }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- description --}}
                                    <?php $descriptionError = (isset($errors) && $errors->has('description')) ? ' is-invalid' : ''; ?>
                                    <div class="row">
                                        <div class=" mb-4 ">
                                            <div class="input {{ $descriptionError }}">
                                                <?php
                                                $descriptionErrorLabel = '';
                                                $descriptionColClass = 'col-md-8';
                                                if (config('settings.single.wysiwyg_editor') != 'none') {
                                                    $descriptionColClass = 'col-md-12';
                                                    $descriptionErrorLabel = $descriptionError;
                                                }
                                                ?>
                                                <label class="input__label"
                                                       for="description">
                                                    {{ t('Description') }} <sup>*</sup>
                                                </label>
												<textarea class="input-text custom-scroll-bar {{$descriptionColClass}}"
                                                          name="description"
                                                          placeholder="{{ t('placeholder_description') }}"
                                                          rows="15"
                                                          style="height: 300px"
                                                >{{ old('description', data_get($postInput, 'description')) }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- cfContainer --}}
                                    <div id="cfContainer" class="post-free__cfContainer"></div>

                                    <div class="row mb-4">

                                        {{-- ads type--}}
                                        <div class="col-md-4" id="selectTypeAds">
                                            <div class="input mb-4">
                                                <label class="input__label"
                                                       for="ads_type">{{ t('ads_type') }}</label>
                                                <div class="input-select-2">
                                                    <select id="adsType" name="ads_type"
                                                            class="ad-type-selecter">
                                                        @foreach(\App\Enums\AdsTypesEnum::adsTypes() as $type)
                                                            <option value="{{ array_search($type, \App\Enums\AdsTypesEnum::adsTypes()) }}"
                                                                    {{ old('ads_type') == array_search($type, \App\Enums\AdsTypesEnum::adsTypes()) ? 'selected="selected"' : '' }}>
                                                                {{ $type }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="ad-type-dropdown"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            {{-- price --}}
                                            <?php $priceError = (isset($errors) && $errors->has('price')) ? ' is-invalid' : ''; ?>
                                            <div class="input {{ $priceError }} mb-4" id="priceBloc">
                                                <label class="input__label"
                                                       for="price">{{ t('price') }} <sup>*</sup></label>
                                                <div class="">
                                                    <div class="input-group">
                                                        <?php
                                                        $price = \App\Helpers\Number::format(old('price',
                                                            data_get($postInput, 'price')), 2, '.', '');
                                                        ?>
                                                        <input id="price"
                                                               name="price"
                                                               class="input-text"
                                                               placeholder="{{ t('ei_price') }}"
                                                               type="number"
                                                               min="0"
                                                               step="{{ getInputNumberStep((int)config('currency.decimal_places', 2)) }}"
                                                               value="{!! $price !!}"
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- country_code --}}
                                        <?php $countryCodeError = (isset($errors) && $errors->has('country_code')) ? ' is-invalid' : ''; ?>
                                        @if (empty(config('country.code')))
                                            <div class="row mb-3 required">
                                                <label class="{{ $countryCodeError }}"
                                                       for="country_code">
                                                    {{ t('your_country') }}
                                                </label>
                                                <div class="col-md-8">
                                                    <select id="countryCode" name="country_code"
                                                            class="form-control large-data-selecter{{ $countryCodeError }}">
                                                        <option value="0" {{ (!old('country_code') || old('country_code')==0) ? 'selected="selected"' : '' }}>
                                                            {{ t('select_a_country') }}
                                                        </option>
                                                        @foreach ($countries as $item)
                                                            <option value="{{ $item->get('code') }}" {{
																	(
																		old(
																			'country_code',
																			(!empty(config('ipCountry.code'))) ? config('ipCountry.code') : 0
																		) == $item->get('code')
																	)
																	? 'selected="selected"' : ''
																}}
                                                            >
                                                                {{ $item->get('name') }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @else
                                            <input id="countryCode" name="country_code" type="hidden"
                                                   value="{{ config('country.code') }}">
                                        @endif

                                        <div class=" p-l-15 col-md-8 col-xl-9">
                                            @if (config('country.admin_field_active') == 1 && in_array(config('country.admin_type'), ['1', '2']))
                                                {{-- admin_code --}}
                                                    <?php $adminCodeError = (isset($errors) && $errors->has('admin_code')) ? ' is-invalid' : ''; ?>
                                                <div id="locationBox" class="row mb-3 input ">
                                                    <label class="col-form-label{{ $adminCodeError }}"
                                                           for="admin_code">{{ t('location') }} <sup>*</sup></label>
                                                    <div class="input-select-2">
                                                        <select id="adminCode" name="admin_code"
                                                                class=" large-data-selecter{{ $adminCodeError }}">
                                                            <option value="0" {{ (!old('admin_code') || old('admin_code')==0) ? 'selected="selected"' : '' }}>
                                                                {{ t('select_your_location') }}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- city_id --}}
                                            <?php $cityIdError = (isset($errors) && $errors->has('city_id')) ? ' is-invalid' : ''; ?>
                                            <div class="input {{$cityIdError}} ">
                                                <label class="input__label"
                                                       for="city_id">{{ t('city') }} <sup>*</sup> </label>
                                                <div class="input-select-2">
                                                    <select id="citySelect" name="city_id"
                                                            class="create-city-select">
                                                        <option value="0" {{ (!old('city_id') || old('city_id')==0) ? 'selected="selected"' : '' }}>
                                                            {{ t('select_a_city') }}
                                                        </option>
                                                        @foreach($cityList as $key => $cityName)
                                                            <?php
                                                                if(auth()->check() && $userCity = auth()->user()->city_id) {
                                                                    $oldCity = $userCity;
                                                                } else {
                                                                    $oldCity = old('city_id');
                                                                }
                                                                ?>
                                                            <option value="{{ $key }}"
                                                                    @if($oldCity == $key) selected="selected"@endif>
                                                                {{ $cityName }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="create-city-select-dropdown"></div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- tags --}}
                                    <?php $tagsError = (isset($errors) && $errors->has('tags.*')) ? ' is-invalid' : ''; ?>
                                    <div class="input {{ $tagsError }}">
                                        <label class="input__label"
                                               for="tags">{{ t('Tags') }}</label>
                                        <div class="input-tugs">
                                            <select id="tags" name="tags[]" class=" tags-selecter"
                                                    multiple="multiple">
                                                <?php $tags = old('tags', data_get($postInput, 'tags')); ?>
                                                @if (!empty($tags))
                                                    @foreach($tags as $iTag)
                                                        <option selected="selected">{{ $iTag }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    {{-- is_permanent --}}
                                    @if (config('settings.single.permanent_posts_enabled') == '3')
                                        <input type="hidden" name="is_permanent" id="isPermanent" value="0">
                                    @else
                                            <?php $isPermanentError = (isset($errors) && $errors->has('is_permanent')) ? ' is-invalid' : ''; ?>
                                        <div id="isPermanentBox" class="row mb-3 required hide">
                                            <label class="col-form-label"></label>
                                            <div class="col-md-8">
                                                <div class="form-check">
                                                    <input name="is_permanent" id="isPermanent"
                                                           class="form-check-input mt-1{{ $isPermanentError }}"
                                                           value="1"
                                                           type="checkbox" {{ (old('is_permanent', data_get($postInput, 'is_permanent'))=='1') ? 'checked="checked"' : '' }}
                                                    >
                                                    <label class="form-check-label mt-0" for="is_permanent">
                                                        {!! t('is_permanent_label') !!}
                                                    </label>
                                                </div>
                                                <div class="form-text text-muted">{{ t('is_permanent_hint') }}</div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class=" post-free__title-salesman">
                                        <strong>{{ t('seller_information') }}</strong>
                                    </div>

                                    <div class="row">

                                        {{-- contact_name --}}
                                        <?php $contactNameError = (isset($errors) && $errors->has('contact_name')) ? ' is-invalid' : ''; ?>
                                        @if (auth()->check())
                                            <input id="contact_name" name="contact_name" type="hidden"
                                                   value="{{ auth()->user()->name }}">
                                        @else
                                            <div class="mb-3 input col-md-4 col-xl-3 {{ $contactNameError }}">
                                                <label class="input__label"
                                                       for="contact_name">
                                                    {{ t('your_name') }}
                                                </label>
                                                <div class="">
                                                    <input id="contact_name"
                                                           name="contact_name"
                                                           placeholder="{{ t('your_name') }}"
                                                           class="input-text"
                                                           type="text"
                                                           value="{{ old('contact_name', data_get($postInput, 'contact_name')) }}"
                                                    >
                                                </div>
                                            </div>
                                        @endif

                                        <?php
                                        if (auth()->check()) {
                                            $formPhone = (auth()->user()->country_code == config('country.code')) ? auth()->user()->phone : '';
                                        } else {
                                            $formPhone = data_get($postInput, 'phone');
                                        }
                                        ?>
                                        {{-- phone --}}
                                        <?php $phoneError = (isset($errors) and $errors->has('phone')) ? ' is-invalid' : ''; ?>
                                        <div class="mb-4 input input-phone col-md-8 col-xl-5 {{ $phoneError }}">
                                            <label class="input__label"
                                                   for="phone">{{ t('phone_number') }}
                                            </label>
                                            <div class="">
                                                <div class="input-phone--flex-no-wrap ">
                                                    <input id="phone" name="phone"
                                                           placeholder="{{ t('phone_number_example') }}"
                                                           class="js-phone-mask input-text post-free__salesman-input-input"
                                                           type="text"
                                                           autocomplete="new-password"
                                                           value="{{old('phone',\Illuminate\Support\Facades\Auth::user()->phone)}}"
                                                    >
                                                    <span class="post-free__salesman-input-phone">
														<input class="input-phone--hidden"
                                                               name="phone_hidden" id="phoneHidden" type="checkbox"
                                                               value="1" {{ (old('phone_hidden', data_get($postInput, 'phone_hidden'))=='1') ? 'checked="checked"' : '' }}>
														<label class="post-free__salesman-label-phone untouchable js-phone-hidden"
                                                               for="phoneHidden">{{ t('hide') }}</label>
													</span>
                                                </div>
                                                <div id="phone-support-text"
                                                     class="form-text">{{t('You can hide the number from other users')}}</div>
                                            </div>

                                        </div>

                                        {{-- email --}}
                                        <?php $emailError = (isset($errors) && $errors->has('email')) ? ' is-invalid' : ''; ?>
                                        <div class=" mb-4 input col-xl-4 {{ $emailError }}">
                                            <label class="input__label"
                                                   for="email">{{ t('email') }}
                                                @if (!isEnabledField('phone') || !auth()->check())
                                                    <sup>*</sup>
                                                @endif
                                            </label>
                                            <div class="">
                                                <div class="input-group ">
                                                    <input id="email" name="email"
                                                           class="input-text"
                                                           placeholder="{{ t('email_example') }}"
                                                           type="text"
                                                           value="{{ old(
																		'email',
																		(
																			(auth()->check() && isset(auth()->user()->email))
																			? auth()->user()->email
																			: data_get($postInput, 'email')
																		)
																	) }}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if (!auth()->check())
                                        @if (in_array(config('settings.single.auto_registration'), [1, 2]))
                                            {{-- auto_registration --}}
                                            @if (config('settings.single.auto_registration') == 1)
                                                    <?php $autoRegistrationError = (isset($errors) && $errors->has('auto_registration')) ? ' is-invalid' : ''; ?>

                                                <div class="row input">
                                                    <label class=""></label>
                                                    <div class="col-md-8">
                                                        <div class="checkbox">
                                                            <input name="auto_registration"
                                                                   id="auto_registration"
                                                                   class="form-check-input{{ $autoRegistrationError }}"
                                                                   value="1"
                                                                   type="checkbox"
                                                                   checked="checked"
                                                            >
                                                            <label for="auto_registration"
                                                                   class="checkbox__label"></label>
                                                        </div>

                                                        <label class="form-check-label" for="auto_registration">
                                                            {!! t('I want to register by submitting this ad') !!}
                                                        </label>
                                                    </div>
                                                    <div class="form-text text-muted">{{ t('You will receive your authentication information by email') }}</div>
                                                    <div style="clear:both"
                                                         class="post-free__input-checkbox--label"></div>
                                                </div>
                                            @else
                                                <input type="hidden" name="auto_registration" id="auto_registration"
                                                       value="1">
                                            @endif
                                        @endif
                                    @endif

                                    @include('layouts.inc.tools.captcha', ['colLeft' => 'col-md-3', 'colRight' => 'col-md-8'])

                                    @if (!auth()->check())
                                        {{-- accept_terms --}}
                                            <?php $acceptTermsError = (isset($errors) and $errors->has('accept_terms')) ? ' is-invalid' : ''; ?>

                                        <div class="input">
                                            <label class=""></label>
                                            <div class="{{ $acceptTermsError }}">
                                                <div class="checkbox input-checkbox">
                                                    <input name="accept_terms" id="acceptTerms"
                                                           class=" checkbox__input"
                                                           value="1"
                                                           type="checkbox" {{ (old('accept_terms', data_get($postInput, 'accept_terms'))=='1') ? 'checked="checked"' : '' }}
                                                    >
                                                    <label for="acceptTerms"
                                                           style="font-weight: normal;"
                                                           class="checkbox__label"><p>
                                                            {!! t('accept_terms_label', ['attributes' => getUrlPageByType('terms')]) !!}</p>
                                                    </label>
                                                </div>
                                                <div style="clear:both"
                                                     class="post-free__input-checkbox--label"></div>
                                            </div>
                                        </div>

                                        {{-- accept_marketing_offers --}}
                                            <?php $acceptMarketingOffersError = (isset($errors) && $errors->has('accept_marketing_offers')) ? ' is-invalid' : ''; ?>
                                        <div class="row input">
                                            <label class=""></label>
                                            <div class="">
                                                <div class="checkbox input-checkbox {{ $acceptMarketingOffersError }}">
                                                    <input name="accept_marketing_offers"
                                                           id="acceptMarketingOffers"
                                                           class="checkbox__input"
                                                           value="1"
                                                           type="checkbox"
                                                            {{ (
                                                                old(
                                                                    'accept_marketing_offers',
                                                                    data_get($postInput, 'accept_marketing_offers')
                                                                ) == '1'
                                                            ) ? 'checked="checked"' : '' }}
                                                    >
                                                    <label class="checkbox__label" for="acceptMarketingOffers"
                                                           style="font-weight: normal;">
                                                        {!! t('accept_marketing_offers_label') !!}</label>
                                                </div>
                                                <div style="clear:both"
                                                     class="post-free__input-checkbox--label"></div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- Button --}}
                                    <div class="row mb-4 pt-5 ">
                                        <div class="d-flex gap-4 col-md-9 col-xl-7 text-center">
                                            <button id="nextStepBtn"
                                                    class="col btn btn--success btn--height">{{ t('Further') }}</button>
                                            <a href="{{url('/')}}"
                                               class="col d-flex align-center justify-content-center btn btn--cancel btn--height">
                                                {{ t('No') }}
                                            </a>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.page-content -->

                <div class="reg-sidebar col">
                    @includeFirst([config('larapen.core.customizedViewPath') . 'post.createOrEdit.inc.right-sidebar', 'post.createOrEdit.inc.right-sidebar'])
                </div>

            </div>
        </div>
    </div>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="{{ asset('/js/validation/clear-is-invalid.js') }}"></script>

    <script>
        $('.js-phone-hidden').click(function () {
            if ($('#phoneHidden').is(':checked')) {
                $('.js-phone-hidden').html('{{ t('hide') }}')
            } else {
                $('.js-phone-hidden').html('{{ t('show') }}')
            }
        });


        $('#phone').focus(function () {
            $('#phone-support-text').show();
        });
        $('#phone').focusout(function () {
            $('#phone-support-text').hide();
        });

        $(document).ready(function () {
            clearIsInvalid();

            $('.ad-type-selecter').select2({
                language: langLayout.select2,
                width: '100%',
                dropdownAutoWidth: 'true',
                dropdownParent: $('.ad-type-dropdown'),
            });

            $('#citySelect').select2({
                language: langLayout.select2,
                width: '100%',
                dropdownAutoWidth: 'true',
                dropdownParent: $('.create-city-select-dropdown')
            });

            if ({{ old('ads_type') ? old('ads_type') : 0 }} === {{ \App\Enums\AdsTypesEnum::FREE }}) {
                $('#priceBloc').hide();
            }

            $('#adsType').on('select2:select', function (e) {
                let data = e.params.data;
                if (parseInt(data.id) == '{{ \App\Enums\AdsTypesEnum::FREE }}') {
                    $('#priceBloc').hide();
                } else {
                    $('#priceBloc').show();
                }
            });
        });
    </script>
    @includeFirst([config('larapen.core.customizedViewPath') . 'post.createOrEdit.inc.category-modal', 'post.createOrEdit.inc.category-modal'])
@endsection

@section('after_styles')
@endsection

@section('after_scripts')
@endsection

@includeFirst([config('larapen.core.customizedViewPath') . 'post.createOrEdit.inc.form-assets', 'post.createOrEdit.inc.form-assets'])

