@if (isset($parent) and !empty($parent))
    @if (!empty($parent->parent))
        @component('post.createOrEdit.inc.category.postEditCategory', ['parent' => $parent->parent])@endcomponent
    @endif
    @if (isset($parent->children) and $parent->children->count() > 0)
        <h3 class="input-category-selected"> {{ $parent->name }} </h3>
    @else
        {{ $parent->name }}
        [ <a href="#browseCategories"
             data-bs-toggle="modal"
             class="cat-link"
             data-id="{{ (isset($parent->parent) and !empty($parent->parent)) ? $parent->parent->id : 0 }}"
        ><i class="far fa-edit"></i> {{ t('Edit') }}</a> ]
    @endif
@endif
@if(isset($category))
    <h3 class="input-category-selected"> {{ $category->name }} </h3>
@endif