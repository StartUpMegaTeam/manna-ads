@if (isset($hasChildren) and !$hasChildren)

    @if (isset($category) and !empty($category))
        <div class="post-edit-category">

                    <a href="#browseCategories" data-bs-toggle="modal"
                       id="categoryLinkTitle"
                       class="cat-link"
                       data-id="0">
                        @if (!empty($category->parent))
                        @component('post.createOrEdit.inc.category.postEditCategory', ['parent' => $category->parent])@endcomponent
                        @elseif(!empty($category))
                            @component('post.createOrEdit.inc.category.postEditCategory', ['category' => $category])@endcomponent
                        @endif
                    </a>

            @if (isset($category->children) and $category->children->count() > 0)
                    <h3 class="input-category-selected"> {{ $parent->name }} </h3>
            @endif
        </div>
    @else
        <a href="#browseCategories" data-bs-toggle="modal" class="cat-link" data-id="0">
            {{ t('select_a_category') }}
        </a>
    @endif

@else
    <div class="post-category-modal__header">
        <h4 class="modal-title post-category-modal__header-title" id="categoriesModalLabel">
            @if (isset($category) and !empty($category))
                <div class="d-none d-md-block">
                    {{ t('select_a_sub_category') }}
                </div>

                <div class="d-block d-md-none width-calc">
                    <nav aria-label="breadcrumb" role="navigation" class="float-start px-3">
                        <ol class="breadcrumbs breadcrumbs--scrollbar width-content">

                            <li class="breadcrumbs-item d-flex align-items-center">
                                <img class="post-category-modal__arrow" src="{{asset('icon/account/back.svg')}}"
                                     alt="#">
                                <a class="ads-show__breadcrumb-link cat-link" data-id="{{ $category->parent_id }}"
                                   href="#">{{ t('header_categories') }}</a>
                            </li>

                            @include('post.createOrEdit.inc.category.parent-categories', ['item' => $category])

                        </ol>
                    </nav>
                </div>

            @else
                {{ t('select_a_category') }}
            @endif
        </h4>

        <button type="button" class="post-category-modal__close" data-bs-dismiss="modal">
            <span aria-hidden="true"></span>
            <span class="sr-only">{{ t('Close') }}</span>
        </button>
    </div>

    <div class="post-category-modal__body d-flex flex-column">

        @if (isset($category) and !empty($category))
            <div class="d-none d-md-block">
                <nav aria-label="breadcrumb" role="navigation" class="float-start px-4 mb-2">
                    <ol class="breadcrumbs flex-wrap">
                        <li class="breadcrumbs-item">
                            <a class="ads-show__breadcrumb-link cat-link" data-id="{{ $category->parent_id }}"
                               href="#">{{ t('header_categories') }}</a>
                        </li>

                        @include('post.createOrEdit.inc.category.parent-categories', ['item' => $category])

                    </ol>
                </nav>
            </div>
        @endif

        @if (isset($categoriesOptions) and isset($categoriesOptions['type_of_display']))
            <div class="col-xl-12 layout-section">
                <div class="modal-category__inner">

                    @if (isset($category) and !empty($category))
                        @if (isset($categories) and $categories->count() > 0)
                            <div class="d-block d-md-none post-category-modal__header pt-0">
                                <div class=" post-category-modal__header-title">
                                    {{ t('select_a_sub_category') }}
                                </div>
                            </div>
                            <div class="modal-category__box-list custom-scroll-bar">
                                @foreach($categories as $key => $cat)
                                    <div class="modal-category__box-list-item">
                                        <a href="#" class="cat-link" data-id="{{ $cat->id }}">{{ $cat->name }}</a>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    @elseif ($categoriesOptions['type_of_display'] == 'c_picture_icon')
                        @if (isset($categories) and $categories->count() > 0)
                            <div class="modal-category__box ">
                                @foreach($categories as $key => $cat)
                                    <a href="#" class="cat-link" data-id="{{ $cat->id }}">
                                        <div class="modal-category__item">
                                            <span class="modal-category__item-title">{{ $cat->name }}</span>

                                            <img src="{{ imgUrl($cat->picture, 'cat') }}" class="lazyload"
                                                 alt="{{ $cat->name }}">
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                        @endif

                    @elseif (in_array($categoriesOptions['type_of_display'], ['cc_normal_list', 'cc_normal_list_s']))

                        <div style="clear: both;"></div>
                            <?php $styled = ($categoriesOptions['type_of_display'] == 'cc_normal_list_s') ? ' styled' : ''; ?>

                        @if (isset($categories) and $categories->count() > 0)
                            <div class="col-xl-12">
                                <div class="list-categories-children{{ $styled }}">
                                    <div class="row">
                                        @foreach ($categories as $key => $cols)
                                            <div class="col-md-4 col-sm-4 {{ (count($categories) == $key+1) ? 'last-column' : '' }}">
                                                @foreach ($cols as $iCat)

                                                        <?php
                                                        $randomId = '-' . substr(uniqid(rand(), true), 5, 5);
                                                        ?>

                                                    <div class="cat-list">
                                                        <h3 class="cat-title rounded">
                                                            @if (isset($categoriesOptions['show_icon']) and $categoriesOptions['show_icon'] == 1)
                                                                <i class="{{ $iCat->icon_class ?? 'icon-ok' }}"></i>
                                                                &nbsp;
                                                            @endif
                                                            <a href="#" class="cat-link" data-id="{{ $iCat->id }}">
                                                                {{ $iCat->name }}
                                                            </a>
                                                            <span class="btn-cat-collapsed collapsed"
                                                                  data-bs-toggle="collapse"
                                                                  data-bs-target=".cat-id-{{ $iCat->id . $randomId }}"
                                                                  aria-expanded="false"
                                                            >
																<span class="icon-down-open-big"></span>
															</span>
                                                        </h3>
                                                        <ul class="cat-collapse collapse show cat-id-{{ $iCat->id . $randomId }} long-list-home">
                                                            @if (isset($subCategories) and $subCategories->has($iCat->id))
                                                                @foreach ($subCategories->get($iCat->id) as $iSubCat)
                                                                    <li>
                                                                        <a href="#" class="cat-link"
                                                                           data-id="{{ $iSubCat->id }}">
                                                                            {{ $iSubCat->name }}
                                                                        </a>
                                                                    </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        @endif

                    @else

                            <?php
                            $listTab = [
                                'c_circle_list' => 'list-circle',
                                'c_check_list' => 'list-check',
                                'c_border_list' => 'list-border',
                            ];
                            $catListClass = (isset($listTab[$categoriesOptions['type_of_display']])) ? 'list ' . $listTab[$categoriesOptions['type_of_display']] : 'list';
                            ?>
                        @if (isset($categories) and $categories->count() > 0)
                            <div class="col-xl-12">
                                <div class="list-categories">
                                    <div class="row">
                                        @foreach ($categories as $key => $items)
                                            <ul class="cat-list {{ $catListClass }} col-md-4 {{ (count($categories) == $key+1) ? 'cat-list-border' : '' }}">
                                                @foreach ($items as $k => $cat)
                                                    <li>
                                                        @if (isset($categoriesOptions['show_icon']) and $categoriesOptions['show_icon'] == 1)
                                                            <i class="{{ $cat->icon_class ?? 'icon-ok' }}"></i>&nbsp;
                                                        @endif
                                                        <a href="#" class="cat-link" data-id="{{ $cat->id }}">
                                                            {{ $cat->name }}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif

                    @endif

                </div>
            </div>
        @endif
    </div>

@endif

@section('before_scripts')
    @parent
    @if (isset($categoriesOptions) and isset($categoriesOptions['max_sub_cats']) and $categoriesOptions['max_sub_cats'] >= 0)
        <script>
            var maxSubCats = {{ (int)$categoriesOptions['max_sub_cats'] }};
        </script>
    @endif
@endsection
