@if (is_null($item->parent_id))

    <li class="breadcrumbs-item" aria-current="page">
        <a href="#" class="cat-link" data-id="{{ $item->id }}">{{ $item->name }}</a>
    </li>

@elseif(!is_null($item->parent_id) && $item->parent_id !='')

    @include('post.createOrEdit.inc.category.parent-categories', ['item' => \App\Models\Category::where('id',$item->parent_id)->first()])

    <li class="breadcrumbs-item" aria-current="page" data-id="{{ $item->id }}">
        <a href="#" class="cat-link" data-id="{{ $item->id }}">{{ $item->name }}</a>
    </li>
@endif
