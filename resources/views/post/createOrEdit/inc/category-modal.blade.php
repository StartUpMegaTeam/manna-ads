{{-- Modal Select Category --}}
<div class="modal fade" id="browseCategories" tabindex="-1" aria-labelledby="categoriesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg post-category-modal">
        <div class="modal-content modal-category-select">

                <div class="col-xl-12" id="selectCats"></div>
        </div>
    </div>
</div>
<!-- /.modal -->

@section('after_scripts')
    @parent
    <script>
        /* Modal Default Admin1 Code */
        @if (isset($city) and !empty($city))
        var modalDefaultAdminCode = '{{ $city->subadmin1_code }}';
        @elseif (isset($admin) and !empty($admin))
        var modalDefaultAdminCode = '{{ $admin->code }}';
        @else
        var modalDefaultAdminCode = 0;
        @endif
    </script>
    <?php /*<script src="{{ url('assets/js/app/load.cities.js') . vTime() }}"></script>*/ ?>
@endsection