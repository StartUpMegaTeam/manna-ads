<div class=" text-center">

    <div class="reg-sidebar-inner reg-sidebar__card">
        <h3>{{ t('how_to_sell_quickly') }}    </h3>
        <div class="reg-sidebar__card-content">
            <div class="text-start">
                <ul class="reg-sidebar__card-list">
                    <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                             alt="#">
                        <span>{{ t('sell_quickly_advice_1') }}</span>
                    </li>
                    <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                             alt="#">
                        <span>{{ t('sell_quickly_advice_2') }}</span>
                    </li>
                    <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                             alt="#">
                        <span>{{ t('sell_quickly_advice_3') }}</span>
                    </li>
                    <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                             alt="#">
                        <span>{{ t('sell_quickly_advice_4') }}</span>
                    </li>
                    <li><img src="{{asset('icon/creating-advertisement/check.svg')}}"
                             alt="#">
                        <span>{{ t('sell_quickly_advice_5') }}</span>
                    </li>
                </ul>
            </div>
            @if (request()->segment(1) == 'create' or request()->segment(2) == 'create')
                {{-- Create Form --}}
                <div class="reg-sidebar__promo">
                    <h3>{{ t('post_free_ads') }}</h3>
                    <p>
                        {{ t('do_you_have_something_text', ['appName' => config('app.name')]) }}
                    </p>
                </div>
            @endif
        </div>
    </div>

    @if (request()->segment(1) == 'create' or request()->segment(2) == 'create')
    @else
        {{-- Edit Form --}}
        @if (config('settings.single.publication_form_type') == '2')
            {{-- Single Step Form --}}
            @if (auth()->check())
                @if (auth()->user()->id == $post->user_id)
                    <div class="reg-sidebar-inner card sidebar-card panel-contact-seller">
                        <div class="card__header-title">{{ t('author_actions') }}</div>
                        <div class="card-content user-info">
                            <div class="card-body text-center">
                                <a href="{{ \App\Helpers\UrlGen::post($post) }}" class="btn btn-default btn-block">
                                    <i class="icon-right-hand"></i> {{ t('Return to the Ad') }}
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
            @endif

        @else
            {{-- Multi Steps Form --}}
            {{--            @if (auth()->check())--}}
            {{--                @if (auth()->user()->id == $post->user_id)--}}
            {{--                    <div class="card sidebar-card panel-contact-seller mt-3">--}}
            {{--                        <div class="card__header-name">{{ t('author_actions') }}</div>--}}
            {{--                        <div class="card-content user-info">--}}
            {{--                            <div class="card-body text-center row gap-2">--}}
            {{--                                <a href="{{ \App\Helpers\UrlGen::post($post) }}" class="btn btn--success-background">--}}
            {{--                                    <img src="{{asset('icon/btn/write.svg')}}"> {{ t('Return to the Ad') }}--}}
            {{--                                </a>--}}
            {{--                                <a href="{{ url('posts/' . $post->id . '/photos') }}"--}}
            {{--                                   class="btn btn--success-background">--}}
            {{--                                    <img src="{{asset('icon/btn/write.svg')}}"> {{ t('Update Photos') }}--}}
            {{--                                </a>--}}
            {{--                                @if (isset($countPackages) and isset($countPaymentMethods) and $countPackages > 0 and $countPaymentMethods > 0)--}}
            {{--                                    <a href="{{ url('posts/' . $post->id . '/payment') }}"--}}
            {{--                                       class="btn btn-success btn-block">--}}
            {{--                                        <i class="icon-ok-circled2"></i> {{ t('Make It Premium') }}--}}
            {{--                                    </a>--}}
            {{--                                @endif--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                @endif--}}
            {{--            @endif--}}

        @endif
    @endif

</div>