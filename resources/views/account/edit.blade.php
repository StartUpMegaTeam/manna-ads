{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xl-3 page-sidebar">
                    @includeFirst([config('larapen.core.customizedViewPath') . 'account.inc.sidebar', 'account.inc.sidebar'])
                </div>
                <!--/.page-sidebar-->

                <div class="col-md-8 col-xl-9 card-default">

                    @include('flash::message')

                    @if (isset($errors) && $errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <h5><strong>{{ t('oops_an_error_has_occurred') }}</strong></h5>
                            <ul class="alert-danger__list">
                                @foreach ($errors->all() as $error)
                                    <li><img src="{{asset('icon/login/close.svg')}}">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div id="avatarUploadError" class="center-block" style="width:100%; display:none"></div>
                    <div id="avatarUploadSuccess" class="alert alert-success fade show" style="display:none;"></div>

                    <div class="profile-panel-box d-block mb-3">


                        <div class="edit-profile__header">

                            <div class="d-flex gap-2">
                                <div class="edit-profile__avatar">
                                    <img id="userImg" class="edit-profile__avatar-img" src="{{ $user->photo_url }}"
                                         alt="user">
                                    <label for="photoField" class="edit-profile__add-avatar"><img
                                                src="{{ asset('icon/account/pencil.svg') }}"></label>
                                    <input type="file" class="edit-profile__file-input" id="photoField" name="photo"
                                           accept=".jpg, .jpeg, .png .gif">

                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col edit-profile__name-and-date mb-2 ">
                                            <h3 class="edit-profile__user-name p-0" title="{{ $user->name }}">
                                                {{ \Illuminate\Support\Str::limit($user->name, 25) }}
                                            </h3>
                                            <div class="edit-profile__last-login">
                                                <img src="{{asset('icon/account/created_at.svg')}}"
                                                     alt="#">
                                                {{ \App\Helpers\Date::format($user->last_login_at, 'datetime') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row d-none d-md-block">
                                        <div class="col">
                                            <div class="edit-profile__stats-block">

                                                <div class="edit-profile__stats-item">
                                                    <img src="{{asset('icon/account/write.svg')}}">
                                                    <span> {{ isset($countThreads) ? \App\Helpers\Number::short($countThreads) : 0 }}</span>
                                                    <p>{{ trans_choice('global.count_mails', getPlural($countThreads), [], config('app.locale')) }}</p>
                                                </div>

                                                <div class="edit-profile__stats-item">
                                                    <img src="{{asset('icon/account/description.svg')}}">
                                                    <span>{{ \App\Helpers\Number::short($countPosts) }}</span>
                                                    <p>{{ trans_choice('global.count_posts', getPlural($countPosts), [], config('app.locale')) }}</p>
                                                </div>

                                                <div class="edit-profile__stats-item">
                                                    <img src="{{asset('icon/account/visit.svg')}}">
                                                    <?php $totalPostsVisits = (isset($countPostsVisits) and $countPostsVisits->total_visits) ? $countPostsVisits->total_visits : 0 ?>
                                                    <span> {{ \App\Helpers\Number::short($totalPostsVisits) }}</span>
                                                    <p>{{ trans_choice('global.count_visits', getPlural($totalPostsVisits), [], config('app.locale')) }}</p>
                                                </div>

                                                <div class="edit-profile__stats-item">
                                                    <img src="{{asset('icon/account/heart.svg')}}">
                                                    <span> {{ \App\Helpers\Number::short($countFavoritePosts) }}</span>
                                                    <p>В избранном</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col d-block d-md-none">
                                <div class="col">
                                    <div class="edit-profile__stats-block">

                                        <div class="edit-profile__stats-item">
                                            <img src="{{asset('icon/account/write.svg')}}">
                                            <span> {{ isset($countThreads) ? \App\Helpers\Number::short($countThreads) : 0 }}</span>
                                            <p>{{ trans_choice('global.count_mails', getPlural($countThreads), [], config('app.locale')) }}</p>
                                        </div>

                                        <div class="edit-profile__stats-item">
                                            <img src="{{asset('icon/account/description.svg')}}">
                                            <span>{{ \App\Helpers\Number::short($countPosts) }}</span>
                                            <p>{{ trans_choice('global.count_posts', getPlural($countPosts), [], config('app.locale')) }}</p>
                                        </div>

                                        <div class="edit-profile__stats-item">
                                            <img src="{{asset('icon/account/visit.svg')}}">
                                            <?php $totalPostsVisits = (isset($countPostsVisits) and $countPostsVisits->total_visits) ? $countPostsVisits->total_visits : 0 ?>
                                            <span> {{ \App\Helpers\Number::short($totalPostsVisits) }}</span>
                                            <p>{{ trans_choice('global.count_visits', getPlural($totalPostsVisits), [], config('app.locale')) }}</p>
                                        </div>

                                        <div class="edit-profile__stats-item">
                                            <img src="{{asset('icon/account/heart.svg')}}">
                                            <span> {{ \App\Helpers\Number::short($countFavoritePosts) }}</span>
                                            <p>{{ trans_choice('global.count_favorites', getPlural($countFavoritePosts), [], config('app.locale')) }} </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- USER --}}
                        <div class="row edit-profile__block">
                            <h4 class="edit-profile__block-title">
                                {{ t('Information') }}
                            </h4>

                            <div class="col-xl-6" id="userPanel">

                                <form name="details"
                                      class="form-horizontal"
                                      role="form"
                                      method="POST"
                                      action="{{ url('account') }}"
                                      enctype="multipart/form-data"
                                >
                                    {!! csrf_field() !!}
                                    <input name="_method" type="hidden" value="PUT">
                                    <input name="panel" type="hidden" value="user">

                                    {{-- gender_id --}}
                                    <?php $genderIdError = (isset($errors) && $errors->has('gender_id')) ? ' is-invalid' : ''; ?>
                                    <div class="row mb-3 required">
                                        <div class="col">
                                            <label class="input__label">{{ t('gender') }} <sup>*</sup></label>
                                            <div class="col">
                                                <div class="d-flex gap-3">
                                                    @if ($genders->count() > 0)
                                                        @foreach ($genders as $gender)
                                                            <div class="input-radio">
                                                                <input name="gender_id"
                                                                       id="gender_id-{{ $gender->id }}"
                                                                       value="{{ $gender->id }}"
                                                                       class="{{ $genderIdError }}"
                                                                       type="radio" {{ (old('gender_id', $user->gender_id)==$gender->id) ? 'checked="checked"' : '' }}
                                                                >
                                                                <label class=""
                                                                       for="gender_id-{{ $gender->id }}">
                                                                    {{ $gender->name }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- name --}}
                                    <?php $nameError = (isset($errors) && $errors->has('name')) ? ' is-invalid' : ''; ?>
                                    <div class="row mb-3 required input">
                                        <div class="col">
                                            <label class="input__label">{{ t('Name') }}
                                                <sup>*</sup></label>

                                            <input name="name" type="text" class="input-text{{ $nameError }}"
                                                   placeholder="{{ t('Name') }}" value="{{ old('name', $user->name) }}">
                                        </div>
                                    </div>

                                    {{-- phone --}}
                                    <?php $phoneError = (isset($errors) && $errors->has('phone')) ? ' is-invalid' : ''; ?>
                                    <div class="row mb-3 required input">
                                        <div class="col">
                                            <label for="phone" class="input__label">{{ t('phone') }}</label>
                                            <input id="phone" name="phone" type="text"
                                                   class="input-text js-phone-mask {{ $phoneError }}"
                                                   placeholder="{{ (!isEnabledField('email')) ? t('Mobile Phone Number') : t('phone_number') }}"
                                                   value="{{ old('phone', $user->phone) }}">
                                        </div>
                                    </div>

                                    {{-- email --}}
                                    <?php $emailError = (isset($errors) && $errors->has('email')) ? ' is-invalid' : ''; ?>
                                    <div class="row mb-3 required input">
                                        <div class="col">
                                            <label class="input__label">{{ t('email') }}
                                                <sup>*</sup>
                                            </label>

                                            <input id="email"
                                                   name="email"
                                                   type="email"
                                                   class="input-text{{ $emailError }}"
                                                   placeholder="{{ t('email') }}"
                                                   value="{{ old('email', $user->email) }}"
                                            >
                                        </div>
                                    </div>

                                    <div id="cityBox" class="row mb-3 required input">
                                        <div class="input-select-2">
                                            <label class="input__label" for="city_id">
                                                Регион для поиска
                                            </label>
                                            <select id="citySelect" name="city_id"
                                                    class="form-control large-data-selecter">
                                                <option value="0" {{ (!old('city_id') or old('city_id')==0) ? 'selected="selected"' : '' }}>
                                                    {{ t('select_a_city') }}
                                                </option>
                                                @foreach($cityList as $key => $city)
                                                    <option value="{{ $key }}"
                                                            @if($user->city_id == $key) selected="selected"@endif>
                                                        {{ $city }}</option>
                                                @endforeach
                                            </select>
                                            <div class="account-city-select-dropdown"></div>
                                        </div>
                                    </div>

                                    <script>
                                        $(document).ready(function () {
                                            $('#citySelect').select2({
                                                dropdownParent: $('.account-city-select-dropdown')
                                            });
                                        });
                                    </script>

                                    <input name="country_code" type="hidden" value="{{ $user->country_code }}">


                                    <div class="row mb-4">
                                        <div class="offset-md-3 col-md-9"></div>
                                    </div>

                                    {{-- button --}}
                                    <div class="row">
                                        <div class="col-6 col-lg-7">
                                            <button type="submit"
                                                    class="btn btn--success btn--height">{{ t('Update') }}</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    {{-- SETTINGS --}}
                    <div class="profile-panel-box d-block">
                        <div class="edit-profile__block">
                            <h4 class="edit-profile__block-title">
                                {{ t('change_password') }}
                            </h4>

                            <div class="col-xl-6"
                                 id="settingsPanel">
                                <form name="settings"
                                      role="form"
                                      method="POST"
                                      action="{{ url('account/settings') }}"
                                      enctype="multipart/form-data"
                                >
                                    {!! csrf_field() !!}
                                    <input name="_method" type="hidden" value="PUT">
                                    <input name="panel" type="hidden" value="settings">

                                    <input name="gender_id" type="hidden" value="{{ $user->gender_id }}">
                                    <input name="name" type="hidden" value="{{ $user->name }}">
                                    <input name="phone" type="hidden" value="{{ $user->phone }}">
                                    <input name="email" type="hidden" value="{{ $user->email }}">

                                    @if (config('settings.single.activation_facebook_comments') && config('services.facebook.client_id'))
                                        {{-- disable_comments --}}
                                        <div class="row mb-3">
                                            <label class="col-md-3 col-form-label"></label>
                                            <div class="col-md-9">
                                                <div class="form-check pt-2">
                                                    <input id="disable_comments"
                                                           name="disable_comments"
                                                           class="form-check-input"
                                                           value="1"
                                                           type="checkbox" {{ ($user->disable_comments==1) ? 'checked' : '' }}
                                                    >
                                                    <label class="form-check-label" for="disable_comments"
                                                           style="font-weight: normal;">
                                                        {{ t('Disable comments on my ads') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- password --}}
                                    <?php $passwordError = (isset($errors) && $errors->has('password')) ? ' is-invalid' : ''; ?>
                                    <div class="row mb-3 input">
                                        <div class="col">
                                            <label class="input__label">{{ t('New Password') }}</label>
                                            <div class="input-password show-pwd-group">
                                                <input id="password" name="password" type="password"
                                                       class="input-text{{ $passwordError }}"
                                                       autocomplete="new-password"
                                                       placeholder="{{ t('Create_a_password') }}">
                                                <span class="icon-append show-pwd">
														<button type="button" class="eyeOfPwd js-password-hidden">
																<img src="{{asset('icon/login/hidden.svg')}}" alt="#">
														</button>
													</span>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- password_confirmation --}}
                                    <?php $passwordError = (isset($errors) && $errors->has('password')) ? ' is-invalid' : ''; ?>
                                    <div class="row mb-3 input">
                                        <label class="input__label">{{ t('Password Confirmation') }}</label>
                                        <div class="col">
                                            <div class="input-password show-pwd-group">
                                                <input id="password_confirmation" name="password_confirmation"
                                                       type="password"
                                                       autocomplete="new-password"
                                                       class="input-text{{ $passwordError }}"
                                                       placeholder="{{ t('Confirm the password') }}">
                                                <span class="icon-append show-pwd">
                                                     <button type="button"
                                                             class="eyeOfPwc js-password-confirmation-hidden">
														     <img src="{{asset('icon/login/hidden.svg')}}" alt="#">
                                                     </button>
                                               </span>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- button --}}
                                    <div class="row mt-4">
                                        <div class="col-6 col-lg-7">
                                            <button type="submit"
                                                    class="btn btn--success btn--height">{{ t('Update') }}</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!--/.page-content-->
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!-- /.main-container -->

    <script src="{{ asset('js/castom-modal/profile-avatar.js')}}"></script>

@endsection

@section('after_styles')
    <link href="{{ url('assets/plugins/bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet">
    @if (config('lang.direction') == 'rtl')
        <link href="{{ url('assets/plugins/bootstrap-fileinput/css/fileinput-rtl.min.css') }}" rel="stylesheet">
    @endif
    <style>
        .krajee-default.file-preview-frame:hover:not(.file-preview-error) {
            box-shadow: 0 0 5px 0 #666666;
        }

        .file-loading:before {
            content: " {{ t('Loading') }}...";
        }
    </style>
    <style>
        /* Avatar Upload */
        .photo-field {
            display: inline-block;
            vertical-align: middle;
        }

        .photo-field .krajee-default.file-preview-frame,
        .photo-field .krajee-default.file-preview-frame:hover {
            margin: 0;
            padding: 0;
            border: none;
            box-shadow: none;
            text-align: center;
        }

        .photo-field .file-input {
            display: table-cell;
            width: 150px;
        }

        .photo-field .krajee-default.file-preview-frame .kv-file-content {
            width: 150px;
            height: 160px;
        }

        .kv-reqd {
            color: red;
            font-family: monospace;
            font-weight: normal;
        }

        .file-preview {
            padding: 2px;
        }

        .file-drop-zone {
            margin: 2px;
            min-height: 100px;
        }

        .file-drop-zone .file-preview-thumbnails {
            cursor: pointer;
        }

        .krajee-default.file-preview-frame .file-thumbnail-footer {
            height: 30px;
        }

        /* Allow clickable uploaded photos (Not possible) */
        .file-drop-zone {
            padding: 20px;
        }

        .file-drop-zone .kv-file-content {
            padding: 0
        }
    </style>
@endsection

@includeFirst([config('larapen.core.customizedViewPath') . 'post.createOrEdit.inc.form-assets', 'post.createOrEdit.inc.form-assets'])

@section('after_scripts')
    <script src="{{ url('assets/plugins/bootstrap-fileinput/js/plugins/sortable.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ url('assets/plugins/bootstrap-fileinput/js/fileinput.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/plugins/bootstrap-fileinput/themes/fas/theme.js') }}" type="text/javascript"></script>
    <script src="{{ url('js/fileinput/locales/' . config('app.locale') . '.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/Cookies.js') }}"></script>
    <script>
        var uploadExtraData = {
            _token: '{{ csrf_token() }}',
            _method: 'PUT',
            name: '{{ $user->name }}',
            phone: '{{ $user->phone }}',
            email: '{{ $user->email }}',
        };
        var initialPreviewConfigExtra = uploadExtraData;
        initialPreviewConfigExtra.remove_photo = 1;

        var photoInfo = '<h6 class="text-muted pb-0">{{ t('Click to select') }}</h6>';
        var footerPreview = '<div class="file-thumbnail-footer pt-2">\n' +
            '    {actions}\n' +
            '</div>';

        let addAvatar = true;

        $('.edit-profile__file-input').change(function () {
            let formData = new FormData();
            let files = $('.edit-profile__file-input').prop('files');
            formData.append('photo', files[0]);
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('_method', 'PUT');

            $('.edit-profile__file-input').val(null);

            $.ajax({
                url: '{{ 'account/photo' }}',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.initialPreview) {
                        $('#userImg').attr("src", data.initialPreview[0]);
                        alert('{{ t('success_update_avatar') }}', 'success');
                    } else {
                        alert(data.error, 'error');
                    }
                },
                error: function (data) {
                    alert(data.responseJSON.error, 'error');
                }
            });
        });

    </script>
@endsection
