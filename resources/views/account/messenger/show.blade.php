{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}

<?php
/** @var  $thread \App\Models\Thread */
?>

@extends('layouts.master')

@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="main-container">
        <div class="container">
            <div class="row">

                @if (session()->has('flash_notification'))
                    <div class="row">
                        <div class="col-xl-12">
                            @include('flash::message')
                        </div>
                    </div>
                @endif

                <div class="col-md-4 col-xl-3 page-sidebar">
                    @includeFirst([config('larapen.core.customizedViewPath') . 'account.inc.sidebar', 'account.inc.sidebar'])
                </div>
                <!--/.page-sidebar-->

                <div class="col-md-8 col-xl-9 page-content mobile-full-screen">
                    <div class="profile__inner">
                        <div class="chat">
                            <div class="chat__header chat-header">
                                <a href="{{request()->headers->get('referer')}}" class="chat-header__back">
                                    <img src="{{asset('icon/account/back.svg')}}" alt="">
                                </a>
                                {!! imgTag($thread->post->getMainPictureUrl(), 'medium', ['class' => 'chat-header__img', 'alt' => $thread->post->title]) !!}
                                <div class="chat-header__info chat-info">
                                    <div class="chat-info__top">
                                        <div class="chat-info__username">
                                            @if($thread->interlocutor())
                                                {{ \Illuminate\Support\Str::limit($thread->interlocutor()->name, 14) }}
                                            @else
                                                {{ t('user_deleted') }}
                                            @endif
                                        </div>
                                        <div class="chat-info__last-online">
                                            @if($thread->interlocutor())
                                            {{ isUserOnline($thread->interlocutor()) ? t('online') : t('was online at ').\Carbon\Carbon::parse($thread->interlocutor()->last_activity)->format('H:i') }}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="chat-info__bottom">
                                        <a href="{{ \App\Helpers\UrlGen::post($thread->post) }}" class="chat-info__post-name">
                                            {{ $thread->subject }}
                                        </a>
                                        <a href="{{ \App\Helpers\UrlGen::post($thread->post) }}" class="chat-info__price">
                                            @if($thread->post->price == null || $thread->post->price == 0)
                                                Бесплатно
                                            @else
                                                {{ \App\Helpers\Number::money($thread->post->price, ' ') }}
                                            @endif
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div id="messageChatHistory" class="chat__body chat-message">
                                <div id="linksMessages" class="text-center">
                                    {!! $linksRender !!}
                                </div>

                                @include('account.messenger.messages.messages')
                            </div>
                            <?php $updateUrl = url('account/messages/' . $thread->id); ?>
                            <form id="chatForm" class="chat__footer chat-footer" role="form" method="POST"
                                  action="{{ $updateUrl }}"
                                  enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <input name="_method" type="hidden" value="PUT">
                                <div id="addFileBtn" class="chat-footer__attach">
                                    <img src="{{ asset('icon\icon-attach.svg') }}" alt="">
                                </div>
                                <div class="chat-footer__input">
                                        <textarea id="body"
                                                  name="body"
                                                  placeholder="{{ t('Type a message') }}"></textarea>
                                </div>
                                <div class="hidden">
                                    <input id="addFile" name="filename" type="file"
                                           data-default-img-url="{{url("icon/account/default-file.svg")}}">
                                </div>
                                <button id="sendChat" class="chat-footer__send-btn chat-footer__send-btn--sending" type="submit">
                                    <img src="{{ asset('icon\icon-send.svg') }}" alt="">
                                </button>
                            </form>
                            <div class="chat__files" id="attachedFiles">
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.page-content-->
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!-- /.main-container -->
@endsection

@section('after_scripts')
    @parent
    <script src="{{ url('assets/js/app/messenger.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/app/messenger-chat.js') }}" type="text/javascript"></script>

    <script src="{{ url('assets/plugins/bootstrap-fileinput/js/plugins/sortable.min.js') }}"
            type="text/javascript"></script>
@endsection