<?php
/** @var  $thread \App\Models\Thread */
?>

<div data-url="{{ url('account/messages/' . $thread->id) }}" class="messages__item messages-block">
    <div class="messages-block__checkbox">
        <div class="input checkbox input-checkbox">
            <input class="checkbox__input" type="checkbox" name="entries[]" value="{{ $thread->id }}"
                   id="isSelect{{ $thread->id }}">
            <label class="checkbox__label" for="isSelect{{ $thread->id }}"
                   style="">
            </label>
        </div>
    </div>
    <div class="messages-block__img-inner">
        {!! imgTag($thread->post->getMainPictureUrl(), 'medium', ['class' => 'messages-block__img', 'alt' => $thread->post->title]) !!}
    </div>
    <div class="messages-block__body">
        <div class="messages-block__header">
            <div class="messages-block__info">
                <div class="messages-block__username @if($thread->interlocutor()) {{ isUserOnline($thread->interlocutor()) ? ' messages-block__username--online' : '' }} @endif">
                    @if($thread->interlocutor())
                        {{ \Illuminate\Support\Str::limit($thread->interlocutor()->name, 14) }}
                    @else
                        {{ t('user_deleted') }}
                    @endif
                </div>
                <div class="messages-block__date">
                    {{ $thread->created_at_formatted }}
                </div>
            </div>
            <div class="messages-block__action list-box-action">
                @if ($thread->isImportant())
                    <a href="{{ url('account/messages/' . $thread->id . '/actions?type=markAsNotImportant') }}"
                       data-bs-toggle="tooltip"
                       data-bs-placement="top"
                       class="messages-block__action-icon markAsNotImportant action-icon__important--active"
                       title="{{ t('Mark as not important') }}">
                    </a>
                @else
                    <a href="{{ url('account/messages/' . $thread->id . '/actions?type=markAsImportant') }}"
                       data-bs-toggle="tooltip"
                       data-bs-placement="top"
                       class="messages-block__action-icon markAsImportant action-icon__important"
                       title="{{ t('Mark as important') }}">
                    </a>
                @endif
                <a href="{{ url('account/messages/' . $thread->id . '/delete') }}"
                   data-bs-toggle="tooltip"
                   data-bs-placement="top"
                   class="messages-block__action-icon action-icon__delete"
                   title="{{ t('Delete') }}">
                </a>
            </div>
        </div>
        <div class="messages-block__post-info">
            <div class="messages-block__post-name">
                {{ $thread->subject }}
            </div>
            <div class="messages-block__post-sum">
                @if($thread->post->price == null || $thread->post->price == 0)
                    Бесплатно
                @else
                    {{ \App\Helpers\Number::money($thread->post->price, ' ') }}
                @endif
            </div>
        </div>

        <div class="messages-block__text">
            <div class="messages-block__message">
                @if($thread->latest_message)
                    {{ $thread->latest_message->body }}
                @endif
            </div>
            @if ($thread->unred_message_count)
                <div class="messages-block__count">
                    <span>{{ $thread->unred_message_count }}</span>
                </div>
            @endif
        </div>
    </div>
</div>
