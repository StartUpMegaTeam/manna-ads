<div class="messages__header">
    <div class="messages__nav messages-nav">
        <a href="{{ url('account/messages') }}"
           class="messages-nav__item {{ (!request()->has('filter') || request()->get('filter')=='') ? ' messages-nav__item--active' : '' }} ">
            {{ t('inbox') }}
        </a>
        <a href="{{ url('account/messages?filter=unread') }}"
           class="messages-nav__item {{ (request()->get('filter')=='unread') ? ' messages-nav__item--active' : '' }}">
            {{ t('unread') }}
        </a>
        <a href="{{ url('account/messages?filter=started') }}"
           class="messages-nav__item {{ (request()->get('filter')=='started') ? ' messages-nav__item--active' : '' }}">
            {{ t('started') }}
        </a>
        <a href="{{ url('account/messages?filter=important') }}"
           class="messages-nav__item {{ (request()->get('filter')=='important') ? ' messages-nav__item--active' : '' }}">
            {{ t('important') }}
        </a>
    </div>
    <div class="messages__alert">
        <div id="successMsg" class="alert alert-success hide" role="alert"></div>
        <div id="errorMsg" class="alert alert-danger hide" role="alert"></div>
    </div>

    @if (isset($threads) && $threads->count() > 0)
        <div class="messages__action-btns messages-btns">
            <div class="messages-btns__item message-select">
                <button class="message-select__btn" id="selectModeBtn">
                    {{t('Select')}}
                </button>
                <div class="message-select__select-block">
                    <button class="message-select__select" id="actionSelect">
                        {{t('Mark All')}} <img src="{{asset('icon/account/back.svg')}}" alt="">
                    </button>
                    <div class="message-select__dropdown">
                        <ul class="message-select__options" id="groupedAction">
                            <li class="message-select__option">
                                <a href="{{ url('account/messages/actions?type=markAsImportant') }}">
                                    <img src="{{asset('icon/icon-star-blue.svg')}}" alt="">
                                    {{ t('important') }}
                                </a>
                            </li>
                            <li class="message-select__option">
                                <a href="{{ url('account/messages/actions?type=markAsNotImportant') }}">
                                    <img src="{{asset('icon/icon-star.svg')}}" alt="">
                                    {{ t('not important') }}
                                </a>
                            </li>
                            <li class="     message-select__option">
                                <a href="{{ url('account/messages/delete') }}">
                                    <img src="{{asset('icon/icon-tush-red.svg')}}" alt="">
                                    {{ t('Delete') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <a class="messages-btns__delete-all deleteAllMessages"
               href="{{ url('account/messages/delete') }}">
                <img src="{{ asset('icon/post-actions/delete.svg') }}" class="untouchable">
                <span> {{ t('delete_all') }} </span>
            </a>
        </div>
    @endif
</div>