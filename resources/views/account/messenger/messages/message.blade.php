<div class="chat-message__item message @if($message->user) {{auth()->id() == $message->user->id ? "message--me" : "message--user"}} @endif">
    @if($message->user)
        <a href="{{ \App\Helpers\UrlGen::user($message->user) }}" class="message__profile-img">
            <img src="{{ url($message->user->photo_url) }}" alt="{{ $message->user->name }}">
        </a>
    @else
        <a href="" class="message__profile-img">
            <img src="{{ url('images/user.svg') }}" alt="{{ t('user_deleted') }}">
        </a>
    @endif
    <div class="message__body">
        @if (!empty($message->body))
            <div class="message__text">
                {!! createAutoLink(nlToBr($message->body)) !!}
            </div>
        @endif
        @if (!empty($message->filename) && $disk->exists($message->filename))
            <div class="message__file">
                @if (preg_match("/\b(\.jpg|\.JPG|\.jpeg|\.JPEG|\.png|\.PNG|\.gif|\.GIF)\b/", $message->filename, $output_array))
                    <a href="{{ fileUrl($message->filename) }}" target="_blank">
                        <img src="{{ fileUrl($message->filename) }}" alt="{{ basename($message->filename) }}">
                    </a>
                @else
                    <a class="message__document message-document"
                       href="{{ fileUrl($message->filename) }}"
                       target="_blank"
                       title="{{ basename($message->filename) }}">
                        <img src="{{url('icon/icon-attach.svg')}}" alt="">
                        <div class="message-document__name">{{ \Illuminate\Support\Str::limit(basename($message->filename), 20) }}</div>
                    </a>
                @endif
            </div>
        @endif
    </div>
    <div class="message__time">
        {{ \Carbon\Carbon::parse($message->created_at)->format('H:i')}}
    </div>
    @if($message->user)
            <?php $recipient = $message->recipients()->first(); ?>
        <div class="message__read-marker">
            <img src="{{ !(!empty($recipient) && !$message->thread->isUnread($recipient->user_id)) ? url("icon/unread.svg") : url("icon/read.svg")}}"
                 alt="">
        </div>
    @endif
</div>