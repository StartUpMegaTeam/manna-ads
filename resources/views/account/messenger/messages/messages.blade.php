@if (isset($messages))
    @if ($messages->count() > 0)
        @foreach($messages as $message)
            @include('account.messenger.messages.message', ['message' => $message])
        @endforeach
    @endif
@endif

<div class="chat-message__item message message--me message--sending d-none">
    <a href="{{ \App\Helpers\UrlGen::user($user) }}" class="message__profile-img">
        <img src="{{ url(auth()->user()->photo_url) }}" alt="{{ auth()->user()->name }}">
    </a>
    <div class="message__body">
        <div class="message__text">
            {!! t('message_sending') !!}
        </div>
    </div>
</div>