@if ($paginator->hasPages())
    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
        <span class="text-muted">
            <a class="load-old-messages mb-3" href="{{ $paginator->nextPageUrl() }}" rel="next">
                {{ t('Load old messages') }}
            </a>
        </span>
    @endif
@endif
