<div class="modal fade" id="contactUser" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog col-md-6 col-xl-8 ">
        <div class="modal-content modal__content">

            <div class="modal__header">
                <h4 class="modal__title">{{ t('Message_to_the_seller') }}</h4>

                <button type="button" class="modal__button-close" data-bs-dismiss="modal">
                    <span aria-hidden="true"><img src="{{asset('icon/login/close.svg')}}" alt="#"></span>
                    <span class="sr-only">{{ t('Close') }}</span>
                </button>
            </div>

            {!! csrf_field() !!}
            <div class="modal__body">
                <div id="message_form">

                    <div class="alert alert-danger mb-2 validation-error-alert">
                        <ul class="modal__errors-list">

                        </ul>
                    </div>

                    @if (isset($errors) && $errors->any() && old('messageForm')=='1')
                        <div class="alert alert-danger mb-2">
                            <ul class="modal__errors-list">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (auth()->check())
                        <input type="hidden" name="from_name" value="{{ auth()->user()->name }}">
                        @if (!empty(auth()->user()->email))
                            <input type="hidden" id="from_email" name="from_email" value="{{ auth()->user()->email }}">
                        @else
                            {{-- from_email --}}
                                <?php $fromEmailError = (isset($errors) and $errors->has('from_email')) ? ' is-invalid' : ''; ?>
                            <div class="mb-3 required input">
                                <label for="from_email" class="input__label">{{ t('E-mail') }}
                                    @if (!isEnabledField('phone'))
                                        <sup>*</sup>
                                    @endif
                                </label>
                                <div class="input-group">
                                    <input id="from_email"
                                           name="from_email"
                                           type="text"
                                           class="input-text{{ $fromEmailError }}"
                                           placeholder="{{ t('eg_email') }}"
                                           value="{{ old('from_email', auth()->user()->email) }}"
                                    >
                                </div>
                            </div>
                        @endif
                    @else
                        {{-- from_email --}}
                            <?php $fromEmailError = (isset($errors) and $errors->has('from_email')) ? ' is-invalid' : ''; ?>
                        <div class="mb-3 required input">
                            <label for="from_email" class="input__label">{{ t('E-mail') }}
                                @if (!isEnabledField('phone'))
                                    <sup>*</sup>
                                @endif
                            </label>

                            <input id="from_email"
                                   name="from_email"
                                   type="text"
                                   class="input-text{{ $fromEmailError }}"
                                   placeholder="{{ t('eg_email') }}"
                                   value="{{ old('from_email') }}"
                            >
                        </div>
                    @endif

                    {{-- body --}}
                    <?php $bodyError = (isset($errors) and $errors->has('body')) ? ' is-invalid' : ''; ?>
                    <div class="mb-3 required input">
                        <label for="body" class="input__label">
                            {{ t('message_text') }} <span class="text-count"></span>
                        </label>
                        <textarea id="body"
                                  name="body"
                                  rows="5"
                                  class="input-text required custom-scroll-bar{{ $bodyError }}"
                                  style="height: 150px;"
                                  placeholder="{{ t('your_message_here') }}"
                        >{{ old('body') }}</textarea>
                        <div class="form-text">{{ t('no_more_than_1000_characters') }}</div>
                    </div>

                    {{-- TODO Расскомментировать, для реализации отправки резюме.--}}
                    {{--                    <?php--}}
                    {{--                    $cat = (isset($post->category) && !empty($post->category)) ? $post->category : null;--}}
                    {{--                    $catType = isset($cat->parent, $cat->parent->type) ? $cat->parent->type : null;--}}
                    {{--                    $catType = (isset($cat->type) && !empty($cat->type)) ? $cat->type : $catType;--}}
                    {{--                    ?>--}}
                    {{--                    @if (in_array($catType, ['job-offer']))--}}
                    {{--                        --}}{{-- filename --}}
                    {{--                            <?php $filenameError = (isset($errors) and $errors->has('filename')) ? ' is-invalid' : ''; ?>--}}
                    {{--                        <div class="mb-3 required" {!! (config('lang.direction')=='rtl') ? 'dir="rtl"' : '' !!}>--}}
                    {{--                            <label for="filename" class="control-label{{ $filenameError }}">{{ t('Resume') }} </label>--}}
                    {{--                            <input id="filename" name="filename" type="file" class="file{{ $filenameError }}">--}}
                    {{--                            <div class="form-text text-muted">--}}
                    {{--                                {{ t('file_types', ['file_types' => showValidFileTypes('file')]) }}--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <input type="hidden" name="catType" value="{{ $catType }}">--}}
                    {{--                    @endif--}}

                    @include('layouts.inc.tools.captcha', ['label' => true])

                    <input type="hidden" name="country_code" value="{{ config('country.code') }}">
                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                    <input type="hidden" name="messageForm" value="1">


                    <div class="d-flex gap-3 col-12">
                        <button type="submit" id="send_message" class="col btn btn--success">{{ t('Send') }}</button>
                        <button class="col btn btn--cancel" data-bs-dismiss="modal">{{ t('Cancel') }}</button>
                    </div>
                </div>
                <div class="modal-message-success">
                    <img class="modal-message-success__img" src="{{ asset('/icon/success.svg') }}">
                    <span class="modal-message-success__title">
                        {!! t('message_success_send') !!}
                    </span>
                    <a class="modal-message-success__btn btn btn--success">
                        {{ t('go_to_thread') }}
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
@section('after_styles')
    @parent
    <link href="{{ url('assets/plugins/bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet">
    @if (config('lang.direction') == 'rtl')
        <link href="{{ url('assets/plugins/bootstrap-fileinput/css/fileinput-rtl.min.css') }}" rel="stylesheet">
    @endif
    <style>
        .krajee-default.file-preview-frame:hover:not(.file-preview-error) {
            box-shadow: 0 0 5px 0 #666666;
        }
    </style>
@endsection

@section('after_scripts')
    @parent

    <script src="{{ url('assets/plugins/bootstrap-fileinput/js/plugins/sortable.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ url('assets/plugins/bootstrap-fileinput/js/fileinput.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/plugins/bootstrap-fileinput/themes/fas/theme.js') }}" type="text/javascript"></script>
    <script src="{{ url('js/fileinput/locales/' . config('app.locale') . '.js') }}" type="text/javascript"></script>

    <script>

        $('#send_message').click(message);

        $('.input-text').keyup(function (e) {
            if (e.which === 13) {
                message();
            }
        });

        function message() {
            $('#body').removeClass('is-invalid');
            $('.validation-error-alert').css('display', 'none');
            $('.modal__errors-list').empty();
            let loader = $('.modal__body');
            loader.customLoaderStart();
            $.ajax({
                url: '/account/messages/posts/{{ $post->id }}',
                method: 'post',
                dataType: 'json',
                data: {
                    from_name: $('#from_name').val(),
                    from_email: $('#from_email').val(),
                    from_phone: $('#from_phone').val(),
                    body: $('#body').val(),
                    post_id: '{{ $post->id }}',
                },
                success: function (data) {
                    ym(91551648,'reachGoal','onmess');
                    loader.customLoaderStop();
                    $('#message_form').css('display', 'none');
                    $('.modal-message-success').css('display', 'flex');
                    $('.modal-message-success__btn').attr('href', '/account/messages/' + data.message);
                    window.setTimeout(function () {
                        $('#message_form').css('display', 'block');
                        $('.modal-message-success').css('display', 'none');
                        $('#from_phone').val('');
                        $('#body').val('');
                        $('#body').removeClass('is-invalid');
                        $('.validation-error-alert').css('display', 'none');
                        $('.modal__errors-list').empty();
                    }, 5000);
                },
                error: function (data) {
                    loader.customLoaderStop();
                    showErrors(data.responseJSON.errors);
                }
            });
        }

        function showErrors(errors = null) {
            $('.validation-error-alert').css('display', 'block');
            errors = Object.entries(errors);
            if(errors != null) {
                errors.forEach(function (item) {
                    $('#' + item[0]).addClass('is-invalid');
                    item[1].forEach(function (errorText) {
                        $('.modal__errors-list').append('<li>' + errorText + '</li>');
                    });
                })
            }
        }

        /* Initialize with defaults (Resume) */
        $('#filename').fileinput(
            {
                theme: 'fas',
                language: '{{ config('app.locale') }}',
                @if (config('lang.direction') == 'rtl')
                rtl: true,
                @endif
                showPreview: false,
                allowedFileExtensions: {!! getUploadFileTypes('file', true) !!},
                showUpload: false,
                showRemove: false,
                maxFileSize: {{ (int)config('settings.upload.max_file_size', 1000) }}
            });

        $(document).ready(function () {
            @if ($errors->any())
            @if ($errors->any() && old('messageForm')=='1')
            {{-- Re-open the modal if error occured --}}
            let quickLogin = new bootstrap.Modal(document.getElementById('contactUser'), {});
            quickLogin.show();
            @endif
            @endif
        });
    </script>
@endsection
