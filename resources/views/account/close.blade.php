{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="main-container">
        <div class="container">
            <div class="row">

                @if (session()->has('flash_notification'))
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-12">
                                @include('flash::message')
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-md-4 col-xl-3 page-sidebar">
                    @includeFirst([config('larapen.core.customizedViewPath') . 'account.inc.sidebar', 'account.inc.sidebar'])
                </div>
                <!--/.page-sidebar-->

                <div class="col-md-8 col-xl-9 page-content">
                    <div class="profile-inner-box">
                        <h2 class="profile-inner-box-title"> {{ t('Close account') }} </h2>
                        <p class="mb-0">{{ t('you_are_sure_you_want_to_close_your_account') }}</p>
                        <p>{{ t('all_data_will_be_lost') }}</p>
                        @if ($user->can(\App\Models\Permission::getStaffPermissions()))
                            <div class="alert alert-danger" role="alert">
                                {{ t('Admin users can not be deleted by this way') }}
                            </div>
                        @else
                            <form role="form" method="POST" action="{{ url('account/close') }}">
                                {!! csrf_field() !!}

                                <div class="profile-button-group">
                                    <div class="d-flex gap-3 col-md-10 col-xl-6">

                                        <input class="input-hidden"
                                               type="text"
                                               name="close_account_confirmation"
                                               id="closeAccountConfirmation1"
                                               value="1"
                                        >
                                        <label class="col-7" for="closeAccountConfirmation1">
                                            <button type="submit"
                                                    class="btn btn--cancel">
                                                <p class="profile-text--desctop">{{ t('Delete my account') }}</p>
                                                <p class="profile-text--mobile">{{ t('Delete my account mobile') }}</p>
                                            </button>
                                        </label>

                                        <div class="col">
                                            <a href="{{url('/')}}"
                                               class="btn btn--cancel">
                                                {{ t('No') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif

                    </div>
                    <!--/.inner-box-->
                </div>
                    <!--/.page-content-->

            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!-- /.main-container -->
@endsection
