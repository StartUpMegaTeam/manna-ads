{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="main-container">
        <div class="container">
            <div class="row">

                @if (session()->has('flash_notification'))
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-12">
                                @include('flash::message')
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-md-3 page-sidebar">
                    @includeFirst([config('larapen.core.customizedViewPath') . 'account.inc.sidebar', 'account.inc.sidebar'])
                </div>
                <!--/.page-sidebar-->

                <div class="col-md-9 page-content">

                    <div class="account-posts">

                        <div class="account-go-home">
                            <a class="account-go-home__title" href="{{ url('/') }}">
                                <img src="{{ asset('icon/icon-link-arrow-accent.svg') }}">
                                <span>{{ t('go_home') }}</span>
                            </a>
                        </div>

                        <div class="account-posts__header">
                            <div class="account-posts__title">
                                @if ($pagePath=='my-posts')
                                    {{ t('my_ads') }}
                                @elseif ($pagePath=='archived')
                                    {{ t('archived_ads') }}
                                @elseif ($pagePath=='favourite')
                                    {{ t('favourite_ads') }}
                                @elseif ($pagePath=='pending-approval')
                                    {{ t('pending_approval') }}
                                @else
                                    {{ t('posts') }}
                                @endif
                            </div>

                            <div class="account-posts__count">

                                @if(isset($posts_count) && $posts_count > 0)
                                    {{  $posts_count . ' ' . trans_choice('global.count_posts_lower', getPlural($posts->count()), [], config('app.locale')) }}
                                @endif
                            </div>
                        </div>

                        <div class="scrolling-pagination">
                            <div class="account-posts__container">
                                @if(isset($posts) && $posts->count() > 0)
                                    @foreach ($posts as $key => $post)
                                        @if($pagePath=='favourite')
                                                <?php $post = $post->post ?>
                                        @endif
                                        @component('components.post-cards.list', [
                                                'post' => $post,
                                                'postImg' => $post->getMainPictureUrl(),
                                                'pagePath' => $pagePath,
                                            ])@endcomponent
                                    @endforeach
                                        @if ($posts->hasPages())
                                            <nav class="" aria-label="">
                                                {!! $posts->appends(request()->query())->links() !!}
                                            </nav>
                                        @endif
                                @else
                                    <x-not-found-page :fromPage="$pagePath"></x-not-found-page>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_styles')
    <style>
        .action-td p {
            margin-bottom: 5px;
        }
    </style>
@endsection

@section('after_scripts')
    <script>
        $('ul.pagination').hide();

        $('.scrolling-pagination').jscroll({
            autoTrigger: true,
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.scrolling-pagination',
            loadingHtml: '<div class="stage"> <div class="dot-typing"></div> </div>',
            callback: function () {
                $('ul.pagination').remove();
            }
        });
    </script>


    <script src="{{ url('assets/js/footable.js?v=2-0-1') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/footable.filter.js?v=2-0-1') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('#addManageTable').footable().bind('footable_filtering', function (e) {
                var selected = $('.filter-status').find(':selected').text();
                if (selected && selected.length > 0) {
                    e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                    e.clear = !e.filter;
                }
            });

            $('.clear-filter').click(function (e) {
                e.preventDefault();
                $('.filter-status').val('');
                $('table.demo').trigger('footable_clear_filter');
            });

            $('.from-check-all').click(function () {
                checkAll(this);
            });

            $('a.delete-action, button.delete-action, a.confirm-action').click(function (e) {
                e.preventDefault(); /* prevents the submit or reload */
                var confirmation = confirm("{{ t('confirm_this_action') }}");

                if (confirmation) {
                    if ($(this).is('a')) {
                        var url = $(this).attr('href');
                        if (url !== 'undefined') {
                            redirect(url);
                        }
                    } else {
                        $('form[name=listForm]').submit();
                    }
                }

                return false;
            });
        });
    </script>
    {{-- include custom script for ads table [select all checkbox]  --}}
    <script>
        function checkAll(bx) {
            if (bx.type !== 'checkbox') {
                bx = document.getElementById('checkAll');
                bx.checked = !bx.checked;
            }

            var chkinput = document.getElementsByTagName('input');
            for (var i = 0; i < chkinput.length; i++) {
                if (chkinput[i].type == 'checkbox') {
                    chkinput[i].checked = bx.checked;
                }
            }
        }
    </script>
@endsection
