<aside>
    <div class="profile-panel-box ">
        <div class="profile-panel-sidebar">

            <div class="profile-collapse-box pb-3 mb-4">
                <div class="profile-collapse-box__title ">
                    <a href="/" class="profile-collapse-box__to-main">
                        <img src="{{asset('icon/account/back.svg')}}">
                        {{ t('all_categories') }}&nbsp;
                    </a>
                </div>
            </div>
            <!-- /.collapse-box  -->

            <div class="profile-collapse-box pb-3 mb-4">
                <div class="profile-collapse-box__title ">
                    <h5 class="profile-collapse-box__title-text mb-3">
                        <img src="{{asset('icon/account/profile.svg')}}">
                        {{ t('My Account') }}&nbsp;
                    </h5>

                    <a href="#MyClassified" data-bs-toggle="collapse" aria-expanded="true" class="float-end mb-4">
                        <img src="{{asset('icon/account/back.svg')}}">
                    </a>
                </div>
                <div class="profile-collapse-box__list collapse show" id="MyClassified">
                    <ul>
                        <li>
                            <a {!! ($pagePath=='') ? 'class="active"' : '' !!} href="{{ url('account') }}">
                                {{ t('Personal Home') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.collapse-box  -->

            <div class="profile-collapse-box pb-3 mb-4">
                <div class="profile-collapse-box__title ">
                    <h5 class="profile-collapse-box__title-text mb-3">
                        <img src="{{asset('icon/login/ready.svg')}}">
                        {{ t('my_ads') }}

                    </h5>
                    <a href="#MyAds" data-bs-toggle="collapse" aria-expanded="true" class="float-end">
                        <img src="{{asset('icon/account/back.svg')}}">
                    </a>
                </div>

                <div class="profile-collapse-box__list collapse show" id="MyAds">
                    <ul>
                        <li>
                            <a{!! ($pagePath=='my-posts') ? ' class="active"' : '' !!} href="{{ url('account/my-posts') }}
                            ">
                            {{ t('my_ads') }}&nbsp;
                            <span class="profile-collapse-box__list-count">
								{{ isset($countMyPosts) ?  \App\Helpers\Number::short($countMyPosts)== 0 ? '' : \App\Helpers\Number::short($countMyPosts) : '' }}
							</span>
                            </a>
                        </li>
                        <li>
                            <a{!! ($pagePath=='favourite') ? ' class="active"' : '' !!} href="{{ url('account/favourite') }}
                            ">
                            {{ t('favourite_ads') }}&nbsp;
                            <span class=" profile-collapse-box__list-count">
								{{ isset($countFavouritePosts) ? \App\Helpers\Number::short($countFavouritePosts) == 0 ? '' : \App\Helpers\Number::short($countFavouritePosts) : '' }}
							</span>
                            </a>
                        </li>
                        <li>
                            <a{!! ($pagePath=='saved-search') ? ' class="active"' : '' !!} href="{{ url('account/saved-search') }}
                            ">
                            {{ t('Saved searches') }}&nbsp;
                            <span class=" profile-collapse-box__list-count">
								{{ isset($countSavedSearch) ? \App\Helpers\Number::short($countSavedSearch) == 0? '' : \App\Helpers\Number::short($countSavedSearch) : '' }}
							</span>
                            </a>
                        </li>
                        <li>
                            <a{!! ($pagePath=='archived') ? ' class="active"' : '' !!} href="{{ url('account/archived') }}
                            ">
                            {{ t('archived_ads') }}&nbsp;
                            <span class=" profile-collapse-box__list-count">
								{{ isset($countArchivedPosts) ? \App\Helpers\Number::short($countArchivedPosts) == 0? '' : \App\Helpers\Number::short($countArchivedPosts) : '' }}
							</span>
                            </a>
                        </li>
                        <li>
                            <a {!! ($pagePath=='messenger') ? 'class="active" ' : '' !!} href="{{ url('account/messages') }}">
                                {{ t('messenger') }}&nbsp;
                                <span class=" {{ \App\Helpers\Number::short($countThreadsWithNewMessage) != 0? 'profile-collapse-box__list-count profile-collapse-box__list-count--messenger': ''}} ">
								{{ isset($countThreadsWithNewMessage) ? \App\Helpers\Number::short($countThreadsWithNewMessage) == 0? '': \App\Helpers\Number::short($countThreadsWithNewMessage) : '' }}
								</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.collapse-box  -->

            <div class="profile-collapse-box pb-3">
                <div class="profile-collapse-box__title">
                    <h5 class="profile-collapse-box__title-text mb-3">
                        <img src="{{asset('icon/account/pencil.svg')}}">
                        {{ t('Terminate Account') }}

                    </h5>
                    <a href="#TerminateAccount" data-bs-toggle="collapse" aria-expanded="true" class="float-end">
                        <img src="{{asset('icon/account/back.svg')}}">
                    </a>
                </div>
                <div class="profile-collapse-box__list collapse show" id="TerminateAccount">
                    <ul>
                        <li>
                            <a {!! ($pagePath=='close') ? 'class="active"' : '' !!} href="{{ url('account/close') }}">
                                </i> {{ t('Close account') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.collapse-box  -->

        </div>
    </div>
    <!-- /.inner-box  -->
</aside>