{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
@extends('layouts.master')

@section('content')
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.spacer', 'common.spacer'])
    <div class="main-container">
        <div class="container">
            <div class="row">

                @if (session()->has('flash_notification'))
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-12">
                                @include('flash::message')
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-md-3 page-sidebar">
                    @includeFirst([config('larapen.core.customizedViewPath') . 'account.inc.sidebar', 'account.inc.sidebar'])
                </div>
                <!--/.page-sidebar-->

                <div class="col-md-9 page-content">

                    <div class="saved-search">

                        <div class="account-go-home">
                            <a class="account-go-home__title" href="{{ url('/') }}">
                                <img src="{{ asset('icon/icon-link-arrow-accent.svg') }}">
                                <span>{{ t('go_home') }}</span>
                            </a>
                        </div>

                        <div class="saved-search__title">
                            {{ t('Saved searches') }}
                        </div>

                        <div class="saved-search__subtitle">
                            @if (isset($savedSearch) && $savedSearch->getCollection()->count() > 0)
                                {{ t('Please select a saved search to show the result') }}
                            @endif
                        </div>

                        <div class="saved-search__item-container">
                            @if (!isset($savedSearch) || $savedSearch->getCollection()->count() <= 0)
                                <div class="col-md-12">
                                    <x-not-found-page fromPage="saved-search"></x-not-found-page>
                                </div>
                            @else
                                <ul>
                                    @foreach ($savedSearch->items() as $search)
                                        <li class="saved-search__item saved-search-item">
                                            <a class="saved-search-item__title"
                                               href="{{ url('/search/') . '?'.$search->query }}"
                                               class="">
                                                <span> {{ \Illuminate\Support\Str::limit(strtoupper($search->keyword), 20) }} </span>
                                            </a>
                                            <div class="saved-search-item__count-and-delete">
                                                    <div class="saved-search-item__count" id="{{ $search->id }}">{{ $search->count }}
                                                        <span>
                                                            {{ ' ' . trans_choice('global.count_posts_lower', getPlural($search->count), [], config('app.locale')) }}
                                                        </span>
                                                    </div>
                                                <span class="saved-search-item__delete">
                                                    <a href="{{ url('account/saved-search/'.$search->id.'/delete') }}"> <img src="{{ asset('icon/search-icon/close-filter-menu.svg') }}"> </a>
                                                </span>
                                            </div>

                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_scripts')
    {{-- include footable   --}}
    <script src="{{ url('assets/js/footable.js?v=2-0-1') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/footable.filter.js?v=2-0-1') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('#addManageTable').footable().bind('footable_filtering', function (e) {
                var selected = $('.filter-status').find(':selected').text();
                if (selected && selected.length > 0) {
                    e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                    e.clear = !e.filter;
                }
            });

            $('.clear-filter').click(function (e) {
                e.preventDefault();
                $('.filter-status').val('');
                $('table.demo').trigger('footable_clear_filter');
            });

        });
    </script>
    {{-- include custom script for ads table [select all checkbox]  --}}
    <script>
        function checkAll(bx) {
            var chkinput = document.getElementsByTagName('input');
            for (var i = 0; i < chkinput.length; i++) {
                if (chkinput[i].type == 'checkbox') {
                    chkinput[i].checked = bx.checked;
                }
            }
        }
    </script>
@endsection
