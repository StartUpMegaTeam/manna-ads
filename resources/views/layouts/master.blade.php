{{--
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
--}}
<?php
$plugins = array_keys((array)config('plugins'));
$publicDisk = \Storage::disk(config('filesystems.default'));
?>
        <!DOCTYPE html>
<html lang="{{ ietfLangTag(config('app.locale')) }}"{!! (config('lang.direction')=='rtl') ? ' dir="rtl"' : '' !!}>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @includeFirst([config('larapen.core.customizedViewPath') . 'common.meta-robots', 'common.meta-robots'])
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-title" content="{{ config('settings.app.app_name') }}">
    <meta name="robots" content="index, follow"/>
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ $publicDisk->url('app/default/ico/apple-touch-icon-144-precomposed.png') . getPictureVersion()
	}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="{{ $publicDisk->url('app/default/ico/apple-touch-icon-114-precomposed.png') . getPictureVersion() }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="{{ $publicDisk->url('app/default/ico/apple-touch-icon-72-precomposed.png') . getPictureVersion() }}">
    <link rel="apple-touch-icon-precomposed"
          href="{{ $publicDisk->url('app/default/ico/apple-touch-icon-57-precomposed.png') . getPictureVersion() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('larapen.core.favicon')) }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset(config('larapen.core.favicon_png')) }}">

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"/>

    <link href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/app.min.css') }}" rel="stylesheet">

    @if (config('plugins.detectadsblocker.installed'))
        <link href="{{ url('assets/detectadsblocker/css/style.css') . getPictureVersion() }}" rel="stylesheet">
    @endif

    <title>{!! MetaTag::get('title') !!}</title>
    {!! MetaTag::tag('description') !!}{!! MetaTag::tag('keywords') !!}
    <link rel="canonical" href="{{ request()->fullUrl() }}"/>
    {{-- Specify a default target for all hyperlinks and forms on the page --}}
    <base target="_top"/>
    @if (isset($post))
        @if (isVerifiedPost($post))
            @if (config('services.facebook.client_id'))
                <meta property="fb:app_id" content="{{ config('services.facebook.client_id') }}"/>
            @endif
            {!! $og->renderTags() !!}
            {!! MetaTag::twitterCard() !!}
        @endif
    @else
        @if (config('services.facebook.client_id'))
            <meta property="fb:app_id" content="{{ config('services.facebook.client_id') }}"/>
        @endif
        {!! $og->renderTags() !!}
        {!! MetaTag::twitterCard() !!}
    @endif
    @include('feed::links')
    {!! seoSiteVerification() !!}

    @if (file_exists(public_path('manifest.json')))
        <link rel="manifest" href="/manifest.json">
        @endif

    @stack('before_styles_stack')
    @yield('before_styles')

    @includeFirst([config('larapen.core.customizedViewPath') . 'layouts.inc.tools.style', 'layouts.inc.tools.style'])

    @stack('after_styles_stack')
    @yield('after_styles')
	
	@if (isset($plugins) and !empty($plugins))
		@foreach($plugins as $plugin)
			@yield($plugin . '_styles')
		@endforeach
	@endif
    
    @if (config('settings.style.custom_css'))
		{!! printCss(config('settings.style.custom_css')) . "\n" !!}
    @endif
	
	@if (config('settings.other.js_code'))
		{!! printJs(config('settings.other.js_code')) . "\n" !!}
	@endif

	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script>
            paceOptions = {
                elements: true
            };
        </script>
        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

        <script src="{{ asset('assets/plugins/jquery/3.3.1/jquery.min.js') }}"></script>

        <script src="{{ url()->asset('assets/js/pace.min.js') }}"></script>
        <script src="{{ url()->asset('assets/plugins/modernizr/modernizr-custom.js') }}"></script>
        <script src="{{ asset('js/custom-select/custom-select.js') }}"></script>
        <script src="{{ asset('js/custom-alert/custom-alert.js') }}"></script>

        <script>history.scrollRestoration = "manual"</script>

        @yield('captcha_head')
        @section('recaptcha_head')
            @if (
                config('settings.security.captcha') == 'recaptcha'
                && config('recaptcha.site_key')
                && config('recaptcha.secret_key')
            )
                <style>
                    .is-invalid .g-recaptcha iframe,
                    .has-error .g-recaptcha iframe {
                        border: 1px solid #f85359;
                    }
                </style>
                @if (config('recaptcha.version') == 'v3')
                    <script type="text/javascript">
                        function myCustomValidation(token) {
                            /* read HTTP status */
                            /* console.log(token); */

                            if ($('#gRecaptchaResponse').length) {
                                $('#gRecaptchaResponse').val(token);
                            }
                        }
                    </script>
                    {!! recaptchaApiV3JsScriptTag([
                        'action' 		    => request()->path(),
                        'custom_validation' => 'myCustomValidation'
                    ]) !!}
                @else
                    {!! recaptchaApiJsScriptTag() !!}
                @endif
            @endif
        @show
</head>

<body class="{{ config('app.skin') }} @if(\App\Helpers\AppBackground::coloredBackground()) body--colored @endif">
<div class="wrapper">

    @section('header')
        @if(isset($count) && $count->get('all') > 0)
            <x-header :count="$count->get('all')"></x-header>
        @elseif(isset($post))
            <x-header :isPostDetails="true"></x-header>
        @elseif(str_starts_with(request()->path(), 'page/') || str_starts_with(request()->path(), 'account'))
            <x-header :isPostDetails="true"></x-header>
        @else
            <x-header></x-header>
        @endif
    @show

    @section('search')
    @show

    @section('wizard')
    @show

    @if (isset($siteCountryInfo))
        <div class="p-0 mt-lg-4 mt-md-3 mt-3"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-between alert alert-warning mb-3">

                        <div class="alert-info__title">{!! $siteCountryInfo !!} </div>
                        <button type="button" class="alert-info__close" data-bs-dismiss="alert"
                                aria-label="{{ t('Close') }}"><img src="{{asset('icon/login/close.svg')}}"></button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @yield('content')

    @section('info')
    @show

    @includeFirst([config('larapen.core.customizedViewPath') . 'layouts.inc.advertising.auto', 'layouts.inc.advertising.auto'])

</div>

@section('footer')
    <x-footer></x-footer>
@show

@section('modal_location')
@show
@section('modal_abuse')
@show
@section('modal_message')
@show

@component('components.alerts.js-alert', ['modificator' => 'success'])@endcomponent
@component('components.alerts.js-alert', ['modificator' => 'info'])@endcomponent
@component('components.alerts.js-alert', ['modificator' => 'error'])@endcomponent

@component('components.alerts.confirm')@endcomponent

<x-cookies-bar></x-cookies-bar>

@includeWhen(!auth()->check(), 'auth.login.inc.modal')
@includeFirst([config('larapen.core.customizedViewPath') . 'layouts.inc.modal.change-country', 'layouts.inc.modal.change-country'])
@includeFirst([config('larapen.core.customizedViewPath') . 'layouts.inc.modal.error', 'layouts.inc.modal.error'])
@include('cookieConsent::index')

@if (config('plugins.detectadsblocker.installed'))
    @if (view()->exists('detectadsblocker::modal'))
        @include('detectadsblocker::modal')
    @endif
@endif

<script>
    {{-- Init. Root Vars --}}
    var siteUrl = '{{ url('/') }}';
    var languageCode = '<?php echo config('app.locale'); ?>';
    var countryCode = '<?php echo config('country.code', 0); ?>';
    var timerNewMessagesChecking = <?php echo (int)config('settings.other.timer_new_messages_checking', 0); ?>;
    var isLogged = <?php echo (auth()->check()) ? 'true' : 'false'; ?>;
    var isLoggedAdmin = <?php echo (auth()->check() && auth()->user()->can(\App\Models\Permission::getStaffPermissions())) ? 'true' : 'false'; ?>;

    {{-- Init. Translation Vars --}}
    var langLayout = {
        'hideMaxListItems': {
            'moreText': "{{ t('View More') }}",
            'lessText': "{{ t('View Less') }}"
        },
        'select2': {
            errorLoading: function () {
                return "{!! t('The results could not be loaded') !!}"
            },
            inputTooLong: function (e) {
                var t = e.input.length - e.maximum, n = {!! t('Please delete X character') !!};
                return t != 1 && (n += 's'), n
            },
            inputTooShort: function (e) {
                var t = e.minimum - e.input.length, n = {!! t('Please enter X or more characters') !!};
                return n
            },
            loadingMore: function () {
                return "{!! t('Loading more results') !!}"
            },
            maximumSelected: function (e) {
                var t = {!! t('You can only select N item') !!};
                return e.maximum != 1 && (t += 's'), t
            },
            noResults: function () {
                return "{!! t('No results found') !!}"
            },
            searching: function () {
                return "{!! t('Searching') !!}"
            }
        }
    };
    var fakeLocationsResults = "{{ config('settings.listing.fake_locations_results', 0) }}";
    var stateOrRegionKeyword = "{{ t('area') }}";
    var errorText = {
        errorFound: "{{ t('error_found') }}"
    };

    {{-- Prevent the page to load in IFRAME by redirecting it to the top-level window --}}
        try {
        if (window.top.location !== window.location) {
            window.top.location.replace(siteUrl);
        }
    } catch (e) {
        console.error(e);
    }
</script>

@stack('before_scripts_stack')
@yield('before_scripts')

<script src="{{ url('js/app.js') }}"></script>
@if (config('settings.optimization.lazy_loading_activation') == 1)
    <script src="{{ url()->asset('assets/plugins/lazysizes/lazysizes.min.js') }}" async=""></script>
@endif
@if (file_exists(public_path() . '/assets/plugins/select2/js/i18n/'.config('app.locale').'.js'))
    <script src="{{ url()->asset('assets/plugins/select2/js/i18n/'.config('app.locale').'.js') }}"></script>
@endif
@if (config('plugins.detectadsblocker.installed'))
    <script src="{{ url('assets/detectadsblocker/js/script.js') . getPictureVersion() }}"></script>
@endif
<script>
    $(document).ready(function () {
        {{-- Select Boxes --}}
        $('.selecter').select2({
            language: langLayout.select2,
            width: '100%',
            dropdownAutoWidth: 'true',
            minimumResultsForSearch: Infinity,
            dropdownParent: $('.field-select-dropdown')
        });

        {{-- Searchable Select Boxes --}}
        $('.large-data-selecter').select2({
            language: langLayout.select2,
            width: '100%',
            dropdownAutoWidth: 'true',
            dropdownParent: $('.field-select-dropdown')
        });

        {{-- Social Share --}}
        $('.share').ShareLink({
            title: '{{ addslashes(MetaTag::get('title')) }}',
            text: '{!! addslashes(MetaTag::get('title')) !!}',
            url: '{!! request()->fullUrl() !!}',
            width: 640,
            height: 480
        });

        {{-- Modal Login --}}
        @if (isset($errors) && $errors->any())
        @if ($errors->any() && old('quickLoginForm')=='1')
        {{-- Re-open the modal if error occured --}}
        let quickLogin = new bootstrap.Modal(document.getElementById('quickLogin'), {});
        quickLogin.show();
        @endif
        @endif
    });
</script>

@if (isset($plugins) && !empty($plugins))
    @foreach($plugins as $plugin)
        @yield($plugin . '_scripts')
    @endforeach
@endif

@if (config('settings.footer.tracking_code'))
    {!! printJs(config('settings.footer.tracking_code')) . "\n" !!}
@endif

<script src="{{asset('js/jquery-mask/jquery.mask.min.js')}}"></script>
<script src="{{asset('js/jquery-mask/jquery-script.js')}}"></script>
<script src="{{asset('js/custom-loader/custom-loader.min.js')}}"></script>
</body>
@if (env('APP_ENV') != 'local')
<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date(); for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }} k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(91551648, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/91551648" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
    <script>
        $('#signupBtn').click(function () {
            ym(91551648, 'reachGoal', '#signupBtn');
        });
    </script>
    <!-- /Yandex.Metrika counter -->
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-GN470Z77WG"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-GN470Z77WG');
    </script>
@endif

@stack('after_scripts_stack')
@yield('after_scripts')
@yield('captcha_footer')
</html>