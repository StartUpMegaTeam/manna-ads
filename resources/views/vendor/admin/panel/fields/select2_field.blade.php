{{-- select2 --}}
<div @include('admin::panel.inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('admin::panel.fields.inc.translatable_icon')
    <?php $entity_model = $xPanel->getModel(); ?>

    <div class="row">
        <div class="col">
            <select class="{{ 'select2-field-' . $field['name'] }}" name="{{ $field['name'] }}[]" multiple="multiple"
                    style="width: 100%">
                @foreach ($field['model']::all() as $connected_entity_entry)
                    <option value="{{ $connected_entity_entry->getKey() }}"
                            @if (
                                    (
                                        old($field["name"])
                                        && in_array($connected_entity_entry->getKey(), old($field["name"]))
                                    )
                                    || (
                                        isset($field['value'])
                                        && in_array(
                                            $connected_entity_entry->getKey(),
                                            $field['value']->pluck(
                                                $connected_entity_entry->getKeyName(),
                                                $connected_entity_entry->getKeyName())->toArray()
                                                )
                                        )
                                )
                                selected = "selected"
                            @endif
                    >{!! $connected_entity_entry->{$field['attribute']} !!}</option>
                @endforeach
            </select>
        </div>
    </div>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <br>
        <div class="form-text">{!! $field['hint'] !!}</div>
    @endif
</div>

<script>
    $(document).ready(function () {
        $('.select2-field-{{ $field['name'] }}').select2({
            placeholder: "{{ $field['placeholder'] }}",
        });
    });
</script>