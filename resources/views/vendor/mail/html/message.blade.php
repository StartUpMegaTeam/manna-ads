@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
    @component('mail::subcopy')
        {{ $subcopy }}
    @endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
    @component('mail::footer')
        © {{ config('app.name') }}. @lang('mail.All rights reserved.')<br>
        @lang('mail.If you have any questions or suggestions, please send them to us at')
        {{config('app.mail')}}
    @endcomponent
@endslot
@endcomponent
