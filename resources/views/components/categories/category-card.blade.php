<div class=" categories__card swiper-slide">
    <a href="{{ \App\Helpers\UrlGen::category($cat) }}" class="card-item">
        <div class="card-item__title">{{ $cat->name }}</div>
        <img src="{{ imgUrl($cat->picture, 'cat', (bool)config('settings.optimization.webp_format')) }}"
             class="lazyload card-item__img img-fluid untouchable"
             alt="{{ $cat->name }}">
    </a>
</div>
