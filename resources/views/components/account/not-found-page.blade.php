<div class="profile-not-found">
    <img class="profile-not-found__img" src="{{ $imageUrl }}">
    <div class="profile-not-found__title">
        {!! $titleText !!}
    </div>

    <a class="profile-not-found__link" href="{{ $linkUrl }}">
        {!! $linkText !!}
    </a>
</div>