<div class="auth-icons">
    @if(config('services.vkontakte.client_id') && config('services.vkontakte.client_secret'))
        <a href="{{ url('/auth/vkontakte') }}" id="vk-auth">
            <img alt="VK" src="{{ asset('/icon/auth/VK.svg') }}"/>
        </a>
    @endif
    @if(config('services.odnoklassniki.client_id') && config('services.odnoklassniki.client_public') && config('services.odnoklassniki.client_secret'))
        <a href="{{ url('/auth/odnoklassniki') }}" id="vk-auth">
            <img alt="OK" src="{{ asset('/icon/auth/OK.svg') }}"/>
        </a>
    @endif
    @if(config('services.google.client_id') && config('services.google.client_secret'))
        <a href="{{ url('/auth/google') }}" id="vk-auth">
            <img alt="Google" src="{{ asset('/icon/auth/google.svg') }}"/>
        </a>
    @endif
</div>