<div class="select" id="orderBy">

    <div class="search-order-filter--desktop">
        <button type="button" class="select__toggle filter" name="orderBy" value="{{ $oldValue }}"
                data-select="toggle" data-index="@if($key !== false){{ $key }}@else '-1' @endif">
            {{ $label }}
        </button>
    </div>

    <div class="search-order-filter--mobile">
        <a data-select="toggle" class="sort-mobile-icon" data-index="@if($key !== false){{ $key }}@else '-1' @endif">
            <img src="{{ asset('icon/filter-icon/swap-accent.svg') }}">
        </a>
    </div>

    <div class="select__dropdown filter">
        <ul class="select__options">
            @foreach($options as $key => $option)
                <li class="select__option @if($oldValue === $option['value']) select__option_selected @endif" data-select="option" data-value="{{ $option['value'] }}"
                    data-index="{{ $key }}"> {{ $option['label'] }}
                </li>
            @endforeach
        </ul>
    </div>
    <input type="hidden" id="filter-hidden" name="orderBy" value="{{ $oldValue }}">
</div>

<script>
    const selectOrderFilter = new CustomSelect('#orderBy', {redirect: true, filterParam: 'orderBy'});

</script>