<form id="globalSearch" name="search" action="{{ $searchUrl }}" method="GET" autocomplete="off">
    @if($showComponent)
        <div class="header-search @if($isHomePage == true) header-search--home is-home-page @endif">
            <div class="left-menu @if(!$isHomePage) left-menu--border @endif">
                <img class="left-menu__button-open" src="{{ asset('icon/left-menu-icon.svg') }}">

                <div class="left-menu__container">
                    <div class="left-account-menu">
                        @if (auth()->check())
                            <div class="left-account-menu__info">
                                <div class="profile-avatar">
                                    @if(auth()->user()->photo)
                                        <img src="{{ auth()->user()->photo_url }}">
                                    @else
                                        <img src="{{ asset('icon/header-profile.svg') }}" width="22" height="22">
                                    @endif
                                </div>
                                <span class="profile-name" title="{{ auth()->user()->name }}">
                                {{ \Illuminate\Support\Str::limit(auth()->user()->name, 15) }}
                            </span>
                            </div>
                            <div>
                                <ul>
                                    <li class="left-account-menu__item menu-block-items">
                                        <a href="{{ url('') }}">
                                            {{ t('main_page') }}
                                        </a>
                                    </li>
                                    <li class="left-account-menu__item menu-block-last-item">
                                        <a href="{{ \App\Helpers\UrlGen::sitemap() }}">
                                            {{ t('header_categories') }}
                                        </a>
                                    </li>
                                    <li class="left-account-menu__divider"></li>
                                    <li class="left-account-menu__item menu-block-last-item">
                                        <a href="{{ url('account') }}">
                                            {{ t('Personal Home') }}
                                        </a>
                                    </li>
                                    <li class="left-account-menu__divider"></li>
                                    <li class="left-account-menu__item menu-block-items">
                                        <a href="{{ url('account/my-posts') }}" class="left-account-menu--counter">
                                            <span>{{ t('my_ads') }}</span>
                                            <span class="menu-counter">{{ isset($countMyPosts) ? \App\Helpers\Number::short($countMyPosts) == 0 ? '' : \App\Helpers\Number::short($countMyPosts) : '' }}</span>
                                        </a>
                                    </li>
                                    <li class="left-account-menu__item menu-block-items">
                                        <a href="{{ url('account/favourite') }}" class="left-account-menu--counter">
                                            <span>{{ t('favourite_ads') }}</span>
                                            <span class="menu-counter">{{ isset($savedPosts) ? \App\Helpers\Number::short($savedPosts) == 0 ? '' : \App\Helpers\Number::short($savedPosts) : '' }}</span>
                                        </a>
                                    </li>
                                    <li class="left-account-menu__item menu-block-items">
                                        <a href="{{ url('account/saved-search') }}" class="left-account-menu--counter">
                                            <span>{{ t('Saved searches') }}</span>
                                            <span class="menu-counter">{{ isset($savedSearch) ? \App\Helpers\Number::short($savedSearch) == 0 ? '' : \App\Helpers\Number::short($savedSearch) : '' }}</span>
                                        </a>
                                    </li>
                                    <li class="left-account-menu__item menu-block-items">
                                        <a href="{{ url('account/archived') }}" class="left-account-menu--counter">
                                            <span>{{ t('archived_ads') }}</span>
                                            <span class="menu-counter">{{ isset($archivedPosts) ? \App\Helpers\Number::short($archivedPosts) == 0 ? '' : \App\Helpers\Number::short($archivedPosts) : '' }}</span>
                                        </a></li>
                                    <li class="left-account-menu__item menu-block-last-item">
                                        <a href="{{ url('account/messages') }}" class="left-account-menu--counter">
                                            <span>{{ t('messenger') }}</span>
                                            @if($notificationCount != 0)
                                                <span class="menu-counter menu-counter--background">{{ $notificationCount }}</span>
                                            @endif
                                        </a>
                                    </li>
                                    <li class="left-account-menu__divider"></li>
                                    <li class="left-account-menu__item menu-block-item-settings">
                                        <a href="{{ url('account') }}">
                                            {{ t('Settings') }}
                                        </a>
                                    </li>
                                    <li class="left-account-menu__item menu-block-last-item">
                                        <a href="{{ url('account/close') }}">
                                            {{ t('Close account') }}
                                        </a>
                                    </li>
                                    <li class="left-account-menu__divider"></li>
                                    <li class="left-account-menu__item menu-block-last-item">
                                        @if (app('impersonate')->isImpersonating())
                                            <a href="{{ route('impersonate.leave') }}"> {{ t('Leave') }}</a>
                                        @else
                                            <a href="{{ \App\Helpers\UrlGen::logout() }}">{{ t('log_out') }}</a>
                                        @endif
                                    </li>
                                </ul>

                            </div>
                        @else
                            <div class="left-account-menu__not-auth">
                                <ul>
                                    <li class="left-account-menu__item menu-block-items">
                                        <a href="{{ url('') }}">
                                            {{ t('main_page') }}
                                        </a>
                                    </li>
                                    <li class="left-account-menu__item menu-block-last-item">
                                        <a href="{{ \App\Helpers\UrlGen::sitemap() }}">
                                            {{ t('header_categories') }}
                                        </a>
                                    </li>
                                    <li class="left-account-menu__divider"></li>
                                    <li class="left-account-menu__item menu-block-items">
                                        <a href="{{ \App\Helpers\UrlGen::register() }}">
                                            {{ t('register') }}</a>
                                    </li>
                                    <li class="left-account-menu__item menu-block-last-item left-menu-login">
                                        <a href="#quickLogin" data-bs-toggle="modal">
                                            {{ t('have_account_login') }}</a>
                                    </li>
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="left-menu__background">
                        <img class="left-menu__button-close" src="{{ asset('icon/close.svg') }}">
                    </div>
                </div>
            </div>

            @if($showSearch)
                <div class="header-search__input  @if(!$isHomePage) header-search__input--border @endif">
                    <input class="header-search__query" placeholder="{{ t('what') }}" type="text"
                           value="{{ request()->get('q') }}">

                    @if(!$isHomePage && request()->filled('q') && request()->get('q') != '' && $count > 0)
                        <a count="{{ $count }}" class="header-search__save-search" id="mobileSaveSearch"
                           name="{!! qsUrl(request()->url(), request()->except(['_token', 'location']), null, false) !!}">
                            <img src="{{ asset('icon/header/star.svg') }}">
                        </a>
                    @endif
                    <button class="header-search__submit" type="submit">
                        <img src="{{ asset('icon/icon-search.svg') }}" alt="">
                    </button>
                </div>

                <div class="header-search__city city-select-2 @if(!$isHomePage) city-select-2--border @endif">
                    <img class="ads-search__city-icon untouchable" src="{{ asset('icon/icon-city.svg') }}" alt="">
                    <select class="js-mobile-city-select d-none">
                        <option value="0" selected="{{ $oldValue == null || $oldValue == 0 ? 'selected' : '' }}">Все регионы</option>
                        @foreach($cityList as $key => $city)
                            <option value="{{ $key }}"
                                    @if(($oldValue !== null && (int)$oldValue === $key)) selected="selected"@endif> {{ $city }}</option>
                        @endforeach
                    </select>
                    <div class="mobile-city-select-dropdown"></div>
                </div>

                <input type="hidden" id="query-hidden" name="q" value="{{ request()->get('q') }}">
                <input type="hidden" id="location-hidden" name="l"
                       value="{{ $oldValue }}">
            @endif
        </div>
    @endif
</form>

<script>
    $(document).ready(function() {
        let jsMobileCitySelect = $('.js-mobile-city-select');
        jsMobileCitySelect.select2({
            dropdownCssClass: 'city-select-2',
            dropdownParent: $('.mobile-city-select-dropdown')
        });

        jsMobileCitySelect.val({{$oldValue}});
        jsMobileCitySelect.trigger('change');

        jsMobileCitySelect.on('select2:select', function (e) {
            let data = e.params.data;
            $('#location-hidden').val(data.id);

            let jsSidebarCitySelect = $('.js-sidebar-city-select');
            jsSidebarCitySelect.val(data.id);
            jsSidebarCitySelect.trigger('change');
            let jsCitySelect = $('.js-city-select');
            jsCitySelect.val(data.id);
            jsCitySelect.trigger('change');

            $('#globalSearch').submit();
        });

        let mobileQuery = $('.header-search__query');
        mobileQuery.on('propertychange input', function (e) {
            var valueChanged = false;

            if (e.type=='propertychange') {
                valueChanged = e.originalEvent.propertyName=='value';
            } else {
                valueChanged = true;
            }
            if (valueChanged) {
                let query = $('.header-search__query');
                $('#query-hidden').val(query.val());
            }
        });
    });

    $('.ads-search > input')
        .focus(function () {
            $('.ads-search').addClass('focused');
        })
        .blur(function () {
            $('.ads-search').removeClass('focused');
        });
</script>