<div class="categories-page__item categories-page-item" id="{{'accordion' . $randomId}}">
    <div class="categories-page-item__maincat">
        <a class="categories-page-item__link"
           href="{{ \App\Helpers\UrlGen::category($cat) }}">
{{--            <img class="categories-page-item__maincat-icon"--}}
{{--                 src="{{ asset('icon/sitemap/cat-icon.svg') }}">--}}
            <span class="categories-page-item__title">{{ $cat->name }}</span>
        </a>
        <img class="categories-page-item__collapse"
             data-bs-toggle="collapse"
             data-bs-target="{{'#collapse' . $randomId}}"
             aria-expanded="true"
             aria-controls="{{'collapse' . $randomId}}"
             src="{{ asset('icon/sitemap/collapse.svg') }}">
    </div>

    <div class="categories-page-item__subcats collapse show"
         id="{{'collapse' . $randomId}}"
         class="accordion-collapse collapse show"
         data-bs-parent="{{'#accordion' . $randomId}}">
        @if (isset($subCats) and $subCats->has($cat->id))
            @foreach($subCats->get($cat->id) as $subCat)
                <a class="categories-page-item__subcat-title"
                   href="{{ \App\Helpers\UrlGen::category($subCat) }}">
                    {{ $subCat->name }}
                </a>
            @endforeach
        @endif
    </div>
</div>