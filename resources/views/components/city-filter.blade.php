<div class="search-sidebar__city city-select-2">
    <img class="search-sidebar__city-icon untouchable" src="{{ asset('icon/icon-city.svg') }}" alt="">
    <select class="js-sidebar-city-select d-none">
        <option value="0" selected="{{ ($oldValue == null || $oldValue == 0) ? 'selected' : '' }}">Все регионы</option>
        @foreach($options as $key => $city)
            <option value="{{ $key }}"
                    @if(($oldValue !== null && (int)$oldValue === $key)) selected="selected"@endif> {{ $city }}</option>
        @endforeach
    </select>
    <div class="sidebar-city-select-dropdown"></div>
</div>

<script>
    $(document).ready(function() {
        let jsSidebarCitySelect = $('.js-sidebar-city-select');
        jsSidebarCitySelect.select2({
            dropdownCssClass: 'sidebar-city-select-2',
            dropdownParent: $('.sidebar-city-select-dropdown')
        });

        jsSidebarCitySelect.val({{$oldValue}});
        jsSidebarCitySelect.trigger('change');

        jsSidebarCitySelect.on('select2:select', function (e) {
            let data = e.params.data;
            $('#location-hidden').val(data.id);

            let jsCitySelect = $('.js-city-select');
            jsCitySelect.val(data.id);
            jsCitySelect.trigger('change');
            let jsMobileCitySelect = $('.js-mobile-city-select');
            jsMobileCitySelect.val(data.id);
            jsMobileCitySelect.trigger('change');

            $('#globalSearch').submit();
        });
    });

</script>