<?php
$addListingUrl = \App\Helpers\UrlGen::addPost();
$addListingAttr = '';
if (!auth()->check()) {
    if (config('settings.single.guests_can_post_ads') != '1') {
        $addListingUrl = '#quickLogin';
        $addListingAttr = ' data-bs-toggle="modal"';
    }
}
if (config('settings.single.pricing_page_enabled') == '1') {
    $addListingUrl = \App\Helpers\UrlGen::pricing();
    $addListingAttr = '';
}
?>

<div class="buttons-container @if($hideFooterOnMobile) hide-on-mobile @endif">
    <div class="bottom-buttons  @if($showAddPostBtn === false) bottom-buttons--only-top-btn @endif">
        @if($showAddPostBtn === true)
        <div class="bottom-buttons__add-new-post">
            @component('components.buttons.s-large-new-post', [
                'addListingUrl' => $addListingUrl,
                'addListingAttr' => $addListingAttr,
            ])@endcomponent
        </div>
        @endif

        <div class="bottom-buttons__back-to-top untouchable">
            <img src="{{ asset('icon/back-to-top.svg') }}">
        </div>
    </div>
</div>

<footer class="main-footer app-footer-fixed @if($hideFooterOnMobile) hide-on-mobile @endif">
    <div class="footer-data">
        <div class="container">
            <div class="footer-categories">
                @if (isset($pages) and $pages->count() > 0)
                    @foreach($pages as $page)
                        <a class="footer-categories__item" {!! getUrlPageBySlug($page->slug) !!}>
                            {{ $page->name }}
                        </a>
                    @endforeach
                @endif
            </div>
            <?php
            $mtPay = '';
            $mtCopy = ' mt-4 pt-2';
            ?>
            <div class="col-xl-12">
                <div class="copy-info text-center{{ $mtCopy }}">
                    © {{ date('Y') }} {{ config('settings.app.app_name') }}. {{ t('all_rights_reserved') }}.
                    @if (!config('settings.footer.hide_powered_by'))
                        @if (config('settings.footer.powered_by_info'))
                            {{ t('Powered by') }} {!! config('settings.footer.powered_by_info') !!}
                        @else
                            {{ t('Powered by') }} <a href="https://laraclassifier.com" title="LaraClassifier">LaraClassifier</a>
                            .
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</footer>

<script>
    $(document).ready(function ($) {
        $('.bottom-buttons__back-to-top').fadeOut(0);
        let speed = 550;
        $('.bottom-buttons__back-to-top').click(function (e) {

            window.scrollTo({
                top: 0,
                behavior: 'smooth',
            });
        });

        function show_scrollTop() {
            ($(window).scrollTop() > 100) ? $('.bottom-buttons__back-to-top').fadeIn(200) : $('.bottom-buttons__back-to-top').fadeOut(200);
        }

        $(window).scroll(function () {
            show_scrollTop();
            checkOffset();
        });
        show_scrollTop();

        function checkOffset() {
            if ($('.bottom-buttons').offset().top + $('.bottom-buttons').height()
                >= $('.main-footer').offset().top - 28) {
                $('.bottom-buttons').css('position', 'relative');
                $('.bottom-buttons').css('bottom', '28px');
            }

            if ($(document).scrollTop() + window.innerHeight < $('.main-footer').offset().top) {
                $('.bottom-buttons').css('position', 'fixed');
                $('.bottom-buttons').css('bottom', '28px');
            }
        }
    });
</script>