<div id="location-modal" class="modal modal-cover fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="location-modal">

                <div class="location-modal__header">
                    <span class="location-modal__title">
                        Ваше местоположение
                    </span>
                    <img class="location-modal__close" src="{{ asset('/icon/alert/close.svg') }}"/>
                </div>

                <div id="cityBox" class="row mb-4 required input">
                    <div class="input-select-2">
                        <select id="userLocation" name="city_id"
                                class="form-control large-data-selecter">
                            <option value="0" {{ (!old('city_id') or old('city_id')==0) ? 'selected="selected"' : '' }}>
                                {{ t('select_a_city') }}
                            </option>
                            @foreach($cityList as $key => $cityName)
                                <option value="{{ $key }}"
                                        @if(auth()->user() && auth()->user()->city_id == $key) selected="selected"@endif>
                                    {{ $cityName }}</option>
                            @endforeach
                        </select>
                        <div class="create-city-select-dropdown"></div>
                    </div>
                </div>

                <a class="btn btn--success btn--height" id="setUserCity">
                    Это мое местоположение
                </a>
            </div>
        </div>
    </div>
</div>

@includeFirst([config('larapen.core.customizedViewPath') . 'post.createOrEdit.inc.form-assets', 'post.createOrEdit.inc.form-assets'])

<script>
    $(document).ready(function () {
        $('#userLocation').select2({
            width: '100%',
            dropdownAutoWidth: 'true',
            dropdownParent: $('.create-city-select-dropdown')
        });
    });

    navigator.geolocation.getCurrentPosition(function (position) {
        $.ajax({
            url: '/api/cities/userLocation',
            method: 'post',
            dataType: 'json',
            headers: {
                'X-AppApiToken': '{{ env('APP_API_TOKEN') }}',
            },
            data: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                distance: 500,
            },
            success: function (data) {
                if (Object.keys(data).length !== 0) {
                    let userLocation = $('#userLocation');
                    userLocation.val(data.id);
                    userLocation.trigger('change');

                    setTimeout(() => { $('#location-modal').modal('show'); }, 2000);
                } else {
                    $('#location-modal').modal('show');
                }
            },
            error: function (data) {
            }
        });
    });

    $('#setUserCity').click(function () {

        let cityId = $('#userLocation').select2("val");

        if ({{(!auth()->check()) ? 1 : 0}}) {
            $('#location-modal').modal('hide');
            let expiry = new Date();
            expiry.setTime(expiry.getTime() + (31 * 24 * 60 * 60 * 1000));
            document.cookie = "dontShowCityModal=" + true + "; expires=" + expiry.toGMTString();
            document.cookie = "userLocation=" + cityId + "; expires=" + expiry.toGMTString();

            let jsMobileCitySelect = $('.js-mobile-city-select');
            jsMobileCitySelect.val(cityId);
            jsMobileCitySelect.trigger('change');
            let jsCitySelect = $('.js-city-select');
            jsCitySelect.val(cityId);
            jsCitySelect.trigger('change');

            return;
        }

        if (cityId != null) {
            $.ajax({
                url: '/api/cities/setUserLocation',
                method: 'post',
                dataType: 'json',
                headers: {
                    'X-AppApiToken': '{{ env('APP_API_TOKEN') }}',
                },
                data: {
                    city_id: cityId,
                    user_id: {{ auth()->id() !== null ? auth()->id() : 0 }},
                },
                success: function () {
                    let expiry = new Date();
                    expiry.setTime(expiry.getTime() + (31 * 24 * 60 * 60 * 1000));
                    document.cookie = "dontShowCityModal=" + true + "; expires=" + expiry.toGMTString();
                    document.cookie = "userLocation=" + cityId + "; expires=" + expiry.toGMTString();

                    let jsMobileCitySelect = $('.js-mobile-city-select');
                    jsMobileCitySelect.val(cityId);
                    jsMobileCitySelect.trigger('change');
                    let jsCitySelect = $('.js-city-select');
                    jsCitySelect.val(cityId);
                    jsCitySelect.trigger('change');

                    $('#location-modal').modal('hide');
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });

    $('.location-modal__close').click(function () {
        let expiry = new Date();
        expiry.setTime(expiry.getTime() + (31 * 24 * 60 * 60 * 1000));
        document.cookie = "dontShowCityModal=" + true + "; expires=" + expiry.toGMTString();

        $('#location-modal').modal('hide');
    });
</script>
