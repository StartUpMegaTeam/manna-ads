<div class="cookies-container">
    <div class="cookies-container__text">
        {!! t('cookies_text', ['url' => $cookiesUrl]) !!}
    </div>
    <div class="cookies-container__accept btn btn--success btn--height">
        {{ t('cookies_accept') }}
    </div>
</div>

<script>
    $('.cookies-container__accept').click(function () {
        expiry = new Date();
        expiry.setTime(expiry.getTime()+(31*24*60*60*1000));
        document.cookie = "isAcceptCookie=true; expires=" + expiry.toGMTString();
        $('.cookies-container').hide();
    });
</script>