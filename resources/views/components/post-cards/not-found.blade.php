<div class="post-not-found col-sm-12 col-md-12 col-xl-7">
    <div class="post-not-found__text">
        {{ t('not_found_posts_text_1') }}
    </div>
    <div class="post-not-found__text">
        {{ t('not_found_posts_text_2') }}
    </div>
    <a href="/search" class="post-not-found__link">
        {{ t('not_found_posts_link') }}
    </a>
</div>