<?php

use App\Models\Post;

/** @var Post $post */
?>

<div class="post-compact-item">
    @if ($post->featured == 1)
        @if (isset($post->latestPayment, $post->latestPayment->package) && !empty($post->latestPayment->package))
            @if ($post->latestPayment->package->ribbon != '')
                <div class="cornerRibbons {{ $post->latestPayment->package->ribbon }}">
                    <a href="#"> {{ $post->latestPayment->package->short_name }}</a>
                </div>
            @endif
        @endif
    @endif

    <div class="post-compact-item__container">
        <a href="{{ \App\Helpers\UrlGen::post($post) . (!is_null(request()->get('l')) ? ( '?l=' . request()->get('l')) : '') }}"
           class="post-compact-item__title"
           target="_blank">{{ \Illuminate\Support\Str::limit($post->title, 70) }} </a>

        <div class="post-compact-item__loc-date-view">
            <a href="{!! '/search?l=' . $post->city->id !!}"
               class="post-compact-item__loc">
                <img src="{{ asset('icon/post-icon/card-location.svg') }}" class="untouchable">
                <span>{{ $post->city->name }}</span>
            </a>

            <div class="post-compact-item__date-view">
                    <span class="post-compact-item__date">
                        <img src="{{ asset('icon/post-icon/icon-clock.svg') }}" class="untouchable">
                        <span>{!! $post->created_at_formatted !!}</span>
                    </span>
                <span class="post-compact-item__view">
                        <img src="{{ asset('icon/post-icon/eye.svg') }}" class="untouchable">
                        <span>{{  \App\Helpers\Number::short($post->visits)  }}</span>
                    </span>
            </div>
        </div>

        <div class="post-compact-item__price">
            @if (isset($post->category->type))
                @if (!in_array($post->category->type, ['not-salable']))
                    @if (is_numeric($post->price) && $post->price > 0)
                        {!! \App\Helpers\Number::money($post->price, ' ') !!}
                    @elseif(is_numeric($post->price) && $post->price == 0)
                        {!! t('free_as_price') !!}
                    @else
                        {!! t('free_ad') !!}
                    @endif
                @endif
            @else
                {!! t('free_ad') !!}
            @endif
        </div>
    </div>

        @if(auth()->check())
            @if (isset($post->savedByLoggedUser) && $post->savedByLoggedUser->count() > 0)
                <a class="post-compact-item__like make-favorite" id="{{ $post->id }}">
                    <img src="{{ asset('icon/post-icon/icon-like-active.svg') }}"
                         class="untouchable like-hover--active ">
                </a>
            @else
                <a class="post-compact-item__like inactive make-favorite"
                   id="{{ $post->id }}">
                    <img src="{{ asset('icon/post-icon/icon-like.svg') }}"
                         class="untouchable like-hover ">
                </a>
            @endif
        @else
            <a class="post-compact-item__like inactive"
               href="#quickLogin"
               data-bs-toggle="modal"
            >
                <img src="{{ asset('icon/post-icon/icon-like.svg') }}" class="untouchable like-hover">
            </a>
        @endif
</div>