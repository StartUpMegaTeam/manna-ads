<?php

use App\Enums\PostStatusEnum;
use App\Models\Post;

/** @var Post $post */
?>

<div class="post-list-item">
    @if ($post->featured == 1)
        @if (isset($post->latestPayment, $post->latestPayment->package) && !empty($post->latestPayment->package))
            @if ($post->latestPayment->package->ribbon != '')
                <div class="cornerRibbons {{ $post->latestPayment->package->ribbon }}">
                    <a href="#"> {{ $post->latestPayment->package->short_name }}</a>
                </div>
            @endif
        @endif
    @endif
    @if($post->status != PostStatusEnum::PUBLISHED && $post->status != null)
        <div class="post-list-item__mask">
        </div>
    @endif
    <div class="post-list-item__container">
        <a href="{{ \App\Helpers\UrlGen::post($post) . (!is_null(request()->get('l')) ? ( '?l=' . request()->get('l')) : '') }}"
           target="_blank" class="post-list-item__image-container">
            {!! imgTag($postImg, 'medium', ['class' => 'post-list-item__photo', 'alt' => $post->title]) !!}
        </a>
        <div class="post-list-item__information-container">
            <div class="post-list-item__info">
                <a href="{{ \App\Helpers\UrlGen::post($post) . (!is_null(request()->get('l')) ? ( '?l=' . request()->get('l')) : '') }}"
                   target="_blank" class="post-list-item__title">
                    {{ \Illuminate\Support\Str::limit($post->title, 40) }}
                </a>
                <div class="post-list-item__loc-date-view">
                    <a href="{!! '/search?l=' . $post->city->id !!}"
                       class="post-list-item__loc">
                        <img src="{{ asset('icon/post-icon/card-location.svg') }}" class="untouchable">
                        <span>{{ $post->city->name }}</span>
                    </a>

                    <div class="post-list-item__date-view">
                        <span class="post-list-item__date">
                        <img src="{{ asset('icon/post-icon/icon-clock.svg') }}" class="untouchable">
                        <span>{!! $post->created_at_formatted !!}</span>
                    </span>
                        <span class="post-list-item__view">
                        <img src="{{ asset('icon/post-icon/eye.svg') }}" class="untouchable">
                        <span>{{  \App\Helpers\Number::short($post->visits)  }}</span>
                    </span>
                    </div>
                </div>
                <div class="post-list-item__description">
                    <p>
                        {!! $post->description !!}
                    </p>
                </div>
            </div>
            <span class="post-list-item__price">
                @if (isset($post->category->type))
                    @if (!in_array($post->category->type, ['not-salable']))
                        @if (is_numeric($post->price) && $post->price > 0)
                            {!! \App\Helpers\Number::money($post->price, ' ') !!}
                        @elseif(is_numeric($post->price) && $post->price == 0)
                            {!! t('free_as_price') !!}
                        @else
                            {!! t('free_ad') !!}
                        @endif
                    @endif
                @else
                    {!! t('free_ad') !!}
                @endif
            </span>
        </div>

    </div>

    @if(isset($pagePath))
        @if($pagePath == 'my-posts')
            <div class="post-list-item__buttons-container">
                @if($post->status != PostStatusEnum::PUBLISHED)
                    <a href="{{ \App\Helpers\UrlGen::post($post) . (!is_null(request()->get('l')) ? ( '?l=' . request()->get('l')) : '') }}"
                       target="_blank"
                       class="post-list-item__moderation @if($post->status == PostStatusEnum::REJECTED) post-list-item__moderation--reject @endif">
                        {{ PostStatusEnum::STATUS_TITLE[$post->status] }}
                    </a>
                @endif
                <div class="post-list-item__buttons">
                    {{--                    редактировать--}}
                    <a title="{{t('Edit post')}}" href="{{ \App\Helpers\UrlGen::editPost($post) }}">
                        <img src="{{ asset('icon/post-actions/edit.svg') }}"
                             class="untouchable">
                    </a>
                    {{--                    в архив--}}
                    @if($post->status == PostStatusEnum::PUBLISHED)
                        <a title="{{t('Hide post')}}" href="{{ url('account/'.$pagePath.'/'.$post->id.'/offline') }}">
                            <img src="{{ asset('icon/post-actions/archive.svg') }}"
                                 class="untouchable">
                        </a>
                    @endif
                    {{--                    удалить--}}
                    <a title="{{t('Delete post')}}" href="{{ url('account/'.$pagePath.'/'.$post->id.'/delete') }}">
                        <img src="{{ asset('icon/post-actions/delete.svg') }}" class="untouchable">
                    </a>
                </div>
            </div>

        @elseif($pagePath == 'archived')
            <div class="post-list-item__buttons-container">
                <div class="post-list-item__buttons">
                    {{--                    восстановить--}}
                    <a title="{{t('Restore post')}}" href="{{ url('account/'.$pagePath.'/'.$post->id.'/repost') }}">
                        <img src="{{ asset('icon/post-actions/restore.svg') }}"
                             class="untouchable">
                    </a>
                    {{--                    удалить--}}
                    <a title="{{t('Delete post')}}" href="{{ url('account/'.$pagePath.'/'.$post->id.'/delete') }}">
                        <img src="{{ asset('icon/post-actions/delete.svg') }}" class="untouchable">
                    </a>
                </div>
            </div>

        @elseif($pagePath == 'favourite')
            <div class="post-list-item__buttons-container">

                <div class="post-list-item__buttons">
                    {{--                    удалить--}}
                    <a title="{{t('Delete post')}}" href="{{ url('account/'.$pagePath.'/'.$post->id.'/delete') }}">
                        <img src="{{ asset('icon/post-actions/delete.svg') }}" class="untouchable">
                    </a>
                </div>
            </div>

        @elseif($pagePath == 'pending-approval')
        @endif
    @else
        <div class="post-list-item__buttons-container">

            @if(auth()->check())
                @if (isset($post->savedByLoggedUser) && $post->savedByLoggedUser->count() > 0)
                    <a class="post-list-item__like make-favorite" id="{{ $post->id }}">
                        <img src="{{ asset('icon/post-icon/icon-like-active.svg') }}"
                             class="untouchable like-hover--active">
                    </a>
                @else
                    <a class="post-list-item__like inactive make-favorite" id="{{ $post->id }}">
                        <img src="{{ asset('icon/post-icon/icon-like.svg') }}" class="untouchable like-hover">
                    </a>
                @endif
            @else
                <a class="post-list-item__like inactive"
                   href="#quickLogin"
                   data-bs-toggle="modal"
                >
                    <img src="{{ asset('icon/post-icon/icon-like.svg') }}" class="untouchable like-hover">
                </a>
            @endif
        </div>
    @endif
</div>