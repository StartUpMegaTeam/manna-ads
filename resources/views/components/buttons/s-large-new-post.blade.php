<a class="button__new-post btn btn--success"

   @if(auth()->check())
       href="{{ $addListingUrl }}"
   @else
       href="#quickLogin"
   data-bs-toggle="modal"
        @endif
        {!! $addListingAttr !!}>
    <img class="untouchable" src="{{ asset('icon/icon-plus.svg') }}"
         style="margin-right: 2px; width: 12px; height: 12px;">
    {{ t('Add Listing') }}
</a>