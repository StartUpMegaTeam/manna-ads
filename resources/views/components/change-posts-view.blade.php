<a class="set-view" href="{!! $link !!}">
    @switch($currDisplay)
        @case('make-list')
            @component('components.actions.make-list-view-mobile', ['color' => '#348EFF'])@endcomponent
            @break
        @case('make-compact')
            @component('components.actions.make-compact-view-mobile', ['color' => '#348EFF'])@endcomponent
            @break
        @case('make-grid')
            @component('components.actions.make-grid-view-mobile', ['color' => '#348EFF'])@endcomponent
            @break
    @endswitch
</a>