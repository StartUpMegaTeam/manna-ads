<div class="error-page">

    <div class="error-page__search container hide-on-mobile">
        <x-search :showSaveSearch="false"></x-search>
    </div>

    <img class="error-page__img" src="{{ asset('images/rain.svg') }}">

    <span class="error-page__code">{{ $errorCode }}</span>

    <span class="error-page__title container">
        {{ $errorMessage }}
    </span>

    <a href="{{ url('/') }}" class="error-page__button">
        {{ t('error_page_btn_text') }}
    </a>

</div>