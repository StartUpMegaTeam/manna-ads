<form id="searchForm" name="search" action="{{ $searchUrl }}" method="GET" autocomplete="off">
    <div class="ads-search @if($showSaveSearch == true) is-search-page @endif">
        <div class="ads-search__city city-select-2">
            <img class="ads-search__city-icon untouchable" src="{{ asset('icon/icon-city.svg') }}" alt="">
            <select class="js-city-select d-none" form="globalSearch">
                <option value="0" selected="{{ $oldValue == null || $oldValue == 0 ? 'selected' : '' }}">Все регионы</option>
            @foreach($cityList as $key => $city)
                    <option value="{{ $key }}" @if(($oldValue !== null && (int)$oldValue === $key)) selected="selected"@endif> {{ $city }}</option>
                @endforeach
            </select>
            <div class="city-select-dropdown"></div>
        </div>
        <input class="ads-search__query" placeholder="{{ t('what') }}" type="text"
               value="{{ request()->get('q') }}">

        @if($showSaveSearch && request()->filled('q') && request()->get('q') != ''  && $count > 0)
            <a count="{{ $count }}" class="ads-search__save-search" id="saveSearch"
               name="{!! qsUrl(request()->url(), request()->except(['_token', 'location']), null, false) !!}">
                <img src="{{ asset('icon/search-icon/save-search.svg') }}">
                <span>{{ t('Save Search') }}</span>
            </a>
        @endif
        <button class="ads-search__submit" form="globalSearch" type="submit">
            <img src="{{ asset('icon/icon-search.svg') }}" alt="">
        </button>
    </div>
</form>

<script>
    $(document).ready(function() {
        let jsCitySelect = $('.js-city-select');
        jsCitySelect.select2({
            dropdownCssClass: 'city-select-2',
            dropdownParent: $('.city-select-dropdown')
        });

        jsCitySelect.val({{$oldValue}});
        jsCitySelect.trigger('change');

        jsCitySelect.on('select2:select', function (e) {
            let data = e.params.data;
            $('#location-hidden').val(data.id);

            let jsSidebarCitySelect = $('.js-sidebar-city-select');
            jsSidebarCitySelect.val(data.id);
            jsSidebarCitySelect.trigger('change');
            let jsMobileCitySelect = $('.js-mobile-city-select');
            jsMobileCitySelect.val(data.id);
            jsMobileCitySelect.trigger('change');

            $('#globalSearch').submit();
        });

        let query = $('.ads-search__query');
        query.on('propertychange input', function (e) {
            var valueChanged = false;

            if (e.type=='propertychange') {
                valueChanged = e.originalEvent.propertyName=='value';
            } else {
                valueChanged = true;
            }
            if (valueChanged) {
                let query = $('.ads-search__query');
                $('#query-hidden').val(query.val());
            }
        });
        $('#searchForm').on('submit', function (e) {
            e.preventDefault();

            $('#globalSearch').submit();
        });

    });

    $('.ads-search > input')
        .focus(function () {
            $('.ads-search').addClass('focused');
        })
        .blur(function () {
            $('.ads-search').removeClass('focused');
        });
</script>