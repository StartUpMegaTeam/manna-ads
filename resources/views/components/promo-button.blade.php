<a class="promo-card__button btn btn--promo btn--height" href="{{ $addListingUrl }}"{!! $addListingAttr !!}>
    {{ t('Add Listing') }}
</a>