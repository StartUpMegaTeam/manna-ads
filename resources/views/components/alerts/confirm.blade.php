<div class="modal fade" id="ConfirmAlert" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal__content">
            <div class="alert-js-confirm">
                <div class="alert-js-confirm__content">
                    <span class="alert-js-confirm__title">
                        {{ t('modal_confirm_title') }}
                    </span>

                    <div class="alert-js-confirm__buttons">
                        <a class="alert-js-confirm__success btn btn--success btn--height"
                           data-bs-dismiss="modal">
                            {{ t('yes') }}
                        </a>

                        <a class="alert-js-confirm__cancel btn btn--cancel btn--height"
                           data-bs-dismiss="modal">
                            {{ t('No') }}
                        </a>
                    </div>
                </div>
                <img class="alert-js-confirm__close" data-bs-dismiss="modal" aria-hidden="true"
                     src="{{ asset('/icon/alert/close.svg') }}">
            </div>
        </div>
    </div>
</div>
