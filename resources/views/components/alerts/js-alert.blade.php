<div class="toast hide alert-js alert-js--{{$modificator}} col-md-6 col-xl-8"
     id="{{$modificator}}Alert"
     role="alert"
     aria-live="assertive"
     aria-atomic="true">
    <div class="alert-js__content">
        <div class="alert-js__icon">
            <img src="{{ asset('/icon/alert/' . $modificator . '.svg') }}">
        </div>

        <span class="alert-js__message"></span>
    </div>

    <div class="alert-js__close" data-bs-dismiss="toast" aria-label="Close">
        <img aria-hidden="true" src="{{ asset('/icon/alert/close.svg') }}">
    </div>
</div>
