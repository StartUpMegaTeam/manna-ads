<div class="container">
    <div class="latest-posts">
        <div class="latest-posts__header">
                   <h1>{{MetaTag::get('h1') ?? ''}}</h1>
            <span class="latest-posts__count">
                    {{ $postsCount . ' ' . trans_choice('global.count_posts_lower', getPlural($postsCount), [], config('app.locale')) }}
                </span>
        </div>
        <div class="scrolling-pagination">
            <div class="latest-posts__grid">
                @includeFirst([config('larapen.core.customizedViewPath') . 'search.inc.posts.template.grid', 'search.inc.posts.template.grid'])
            </div>
            @if ($posts->hasPages())
                <nav class="" aria-label="">
                    {!! $posts->appends(request()->query())->links() !!}
                </nav>
            @endif
        </div>
    </div>
</div>
@section('after_scripts')
    <script>
        $('ul.pagination').hide();

        $('.scrolling-pagination').jscroll({
            autoTrigger: true,
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.scrolling-pagination',
            loadingHtml: '<div class="stage"> <div class="dot-typing"></div> </div>',
            callback: function () {
                $('ul.pagination').remove();
                $('.make-favorite').off('click');
                $('.make-favorite').click(function () {
                    savePost(this, false);
                });
            },
        });
    </script>
@endsection