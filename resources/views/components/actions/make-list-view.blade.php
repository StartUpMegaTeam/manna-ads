<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M2.50033 11.332H8.33366C8.51775 11.332 8.66699 11.4813 8.66699 11.6654V17.4987C8.66699 17.6828 8.51775 17.832 8.33366 17.832H2.50033C2.31623 17.832 2.16699 17.6828 2.16699 17.4987V11.6654C2.16699 11.4813 2.31623 11.332 2.50033 11.332Z" stroke="{{$color}}"/>
    <path d="M2.50033 2.16797H8.33366C8.51775 2.16797 8.66699 2.31721 8.66699 2.5013V8.33464C8.66699 8.51873 8.51775 8.66797 8.33366 8.66797H2.50033C2.31623 8.66797 2.16699 8.51873 2.16699 8.33464V2.5013C2.16699 2.31721 2.31623 2.16797 2.50033 2.16797Z" stroke="{{$color}}"/>
    <path d="M11 3H18" stroke="{{$color}}" stroke-linecap="round"/>
    <path d="M11 12H18" stroke="{{$color}}" stroke-linecap="round"/>
    <path d="M11 5H15" stroke="{{$color}}" stroke-linecap="round"/>
    <path d="M11 14H15" stroke="{{$color}}" stroke-linecap="round"/>
</svg>