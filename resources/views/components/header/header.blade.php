<div class="header">
    <div class="header-navigation" role="navigation">
        <div class="container">
            <div class="header-navbar">
                <div class="navbar-identity">
                    {{-- Logo --}}
                    <a href="{{ url('/') }}" class="navbar-brand logo logo-title untouchable d-flex flex-column">
                        <img src="{{ asset('images/site/logo.svg') }}"
                             alt="{{ strtolower(config('settings.app.app_name')) }}" class="main-logo"
                             data-bs-placement="bottom"
                             data-bs-toggle="tooltip"/>
                    </a>
                </div>

                <div class="" id="navbarsDefault">
                    <ul class="navbar__container">
                        @if (!auth()->check())
                            <li class="navbar__item">
                                <a href="{{ \App\Helpers\UrlGen::sitemap() }}" class="header__link">
                                    <img src="{{ asset('icon/categories.svg') }}" class="header-icons__main-icon">
                                    {{ t('header_categories') }}</a>
                            </li>
                            <li class="navbar__item">
                                <a href="{{ \App\Helpers\UrlGen::register() }}" class="header__link">
                                    <img src="{{ asset('icon/icon-register.svg') }}" class="header-icons__main-icon">
                                    {{ t('register') }}</a>
                            </li>
                            <li class="navbar__item">
                                <a href="#quickLogin" class="header__link" data-bs-toggle="modal">
                                    <img src="{{ asset('icon/icon-login.svg') }}" class="header-icons__login">
                                    {{ t('log_in') }}</a>
                            </li>
                        @else
                            @if($notificationCount > 0)
                                <li class="navbar__item">
                                    <a href="{{ url('account/messages') }}" class="header__link--accent">
                                        <img src="{{ asset('icon/have-notify.svg') }}" class="header-icons__notify">
                                        <span class="header__notify-count--active">{{ '+' . $notificationCount }}</span>
                                    </a>
                                </li>
                            @else
                                <li class="navbar__item">
                                    <a href="{{ url('account/messages') }}" class="header__link">
                                        <img src="{{ asset('icon/notify.svg') }}" class="header-icons__notify">
                                        <span class="header__notify-count--default">{{ $notificationCount }}</span>
                                    </a>
                                </li>
                            @endif
                            <li class="navbar__item">
                                <a href="{{ url('account/favourite') }}" class="header__link">
                                    <img src="{{ asset('icon/post-icon/icon-like-header.svg') }}"
                                         class="header-icons__like">
                                    {{ t('Saved') }}</a>
                            </li>
                            <li class="navbar__item">
                                <a href="{{ \App\Helpers\UrlGen::sitemap() }}" class="header__link">
                                    <img src="{{ asset('icon/categories.svg') }}" class="header-icons__main-icon">
                                    {{ t('header_categories') }}</a>
                            </li>
                            <li class="navbar__item dropdown no-arrow">
                                <a href="#" class="dropdown-toggle header__link" data-bs-toggle="dropdown">
                                    <img src="{{ asset('icon/icon-register.svg') }}" class="header-icons__main-icon">
                                    <span title="{{ auth()->user()->name }}">{{ \Illuminate\Support\Str::limit(auth()->user()->name, 15) }}</span>
                                </a>
                                <ul id="userMenuDropdown" class="dropdown-menu account">
                                    <li class="header-menu__item">
                                        <a href="{{ url('account') }}">
                                            <span>{{ t('Personal Home') }}</span>
                                        </a>
                                    </li>
                                    <li class="header-menu__divider"></li>
                                    <li class="header-menu__item">
                                        <a href="{{ url('account/my-posts') }}" class="header-menu__item--counter">
                                            <span>{{ t('my_ads') }}</span>
                                            <span class="menu-counter">{{ isset($countMyPosts) ? \App\Helpers\Number::short($countMyPosts) == 0 ? '' : \App\Helpers\Number::short($countMyPosts) : '' }}</span>
                                        </a>
                                    </li>
                                    <li class="header-menu__item">
                                        <a href="{{ url('account/favourite') }}" class="header-menu__item--counter">
                                            <span>{{ t('favourite_ads') }}</span>
                                            <span class="menu-counter">{{ isset($savedPosts) ? \App\Helpers\Number::short($savedPosts) == 0 ? '' : \App\Helpers\Number::short($savedPosts) : '' }}</span>
                                        </a>
                                    </li>
                                    <li class="header-menu__item">
                                        <a href="{{ url('account/saved-search') }}" class="header-menu__item--counter">
                                            <span>{{ t('Saved searches') }}</span>
                                            <span class="menu-counter">{{ isset($savedSearch) ? \App\Helpers\Number::short($savedSearch) == 0 ? '' : \App\Helpers\Number::short($savedSearch) : '' }}</span>
                                        </a>
                                    </li>
                                    <li class="header-menu__item">
                                        <a href="{{ url('account/archived') }}" class="header-menu__item--counter">
                                            <span>{{ t('archived_ads') }}</span>
                                            <span class="menu-counter">{{ isset($archivedPosts) ? \App\Helpers\Number::short($archivedPosts) == 0 ? '' : \App\Helpers\Number::short($archivedPosts) : '' }}</span>
                                        </a>
                                    </li>
                                    <li class="header-menu__item">
                                        <a href="{{ url('account/messages') }}" class="header-menu__item--counter">
                                            <span>{{ t('messenger') }}</span>
                                            @if($notificationCount != 0)
                                                <span class="menu-counter menu-counter menu-counter--background">{{ $notificationCount }}</span>
                                            @endif
                                        </a>
                                    </li>
                                    <li class="header-menu__divider"></li>
                                    <li class="header-menu__item">
                                        <a href="{{ url('account') }}">
                                            <span>
                                                {{ t('Settings') }}
                                            </span>
                                        </a>
                                    </li>
                                    <li class="header-menu__item">
                                        <a href="{{ url('account/close') }}">
                                            <span>
                                                {{ t('Close account') }}
                                            </span>
                                        </a>
                                    </li>
                                    <li class="header-menu__divider"></li>
                                    <li class="header-menu__item">
                                        @if (app('impersonate')->isImpersonating())
                                            <a href="{{ route('impersonate.leave') }}">
                                                <span>
                                                    {{ t('Leave') }}
                                                </span>
                                            </a>
                                        @else
                                            <a href="{{ \App\Helpers\UrlGen::logout() }}">
                                                <span>
                                                    {{ t('log_out') }}
                                                </span>
                                            </a>
                                        @endif
                                    </li>
                                </ul>
                            </li>
                        @endif

                        @if (config('plugins.currencyexchange.installed'))
                            @include('currencyexchange::select-currency')
                        @endif

                        <?php
                        $addListingUrl = \App\Helpers\UrlGen::addPost();
                        $addListingAttr = '';
                        if (!auth()->check()) {
                            if (config('settings.single.guests_can_post_ads') != '1') {
                                $addListingUrl = '#quickLogin';
                                $addListingAttr = ' data-bs-toggle="modal"';
                            }
                        }
                        if (config('settings.single.pricing_page_enabled') == '1') {
                            $addListingUrl = \App\Helpers\UrlGen::pricing();
                            $addListingAttr = '';
                        }
                        ?>
                        <li class="header-button">
                            @component('components.buttons.s-large-new-post', [
                                'addListingUrl' => $addListingUrl,
                                'addListingAttr' => $addListingAttr,
                            ])@endcomponent
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header-category hide-on-mobile">
        <div class="category-slider-container">
            <div class="slider header-slider">
                <div class="header-category__preview">
                    <div class="swiper-wrapper">
                        @if (isset($categories) and $categories->count() > 0)
                            @foreach($categories as $category)
                                <a class="header-category__item swiper-slide @if(isset($cat) and !empty($cat) and $cat->slug === $category->slug) header-category__item--active @endif"
                                   href="{{ \App\Helpers\UrlGen::category($category) . (!is_null(request()->get('l')) ? ( '?l=' . request()->get('l')) : '') }}" style="width: auto;">
                                    {{ $category->name }}
                                </a>
                            @endforeach
                        @endif
                    </div>
                    <div class="header-swiper header-swiper--next">
                        <img src="{{ asset('icon/icon-link-arrow-accent.svg') }}" style="width: 25%;" alt="">
                    </div>
                    <div class="header-swiper header-swiper--prev">
                        <img src="{{ asset('icon/icon-link-arrow-accent.svg') }}" style="width: 25%;" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>


    @if($isPostDetails)
        <x-mobile-search :isPostDetails="$isPostDetails" :showSearch="$showSearch"></x-mobile-search>
    @elseif(isset($count) && $count > 0)
        <x-mobile-search :count="$count" :showSearch="$showSearch"></x-mobile-search>
    @else
        <x-mobile-search :showSearch="$showSearch"></x-mobile-search>
    @endif
</div>


<script>
    let headerSwiper = new Swiper(".header-category__preview", {
        slidesPerView: 'auto',
        spaceBetween: 21,
        direction: 'horizontal',
        loop: true,
        longSwipesMs: 600,
        threshold: 5,
        mousewheel: {
            invert: false,
            forceToAxis: true,
        },
        navigation: {
            nextEl: ".header-swiper--next",
            prevEl: ".header-swiper--prev",
        },
    });


    $('.left-menu__button-open').click(function () {
        $('.left-menu__container').toggleClass("open");
    });

    $('.left-menu__background').click(function () {
        $('.left-menu__container').removeClass("open");
    });

    $('.left-menu-login').click(function () {
        $('.left-menu__container').removeClass("open");
    });

</script>
