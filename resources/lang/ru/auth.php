<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Неверный email или пароль.',
    'throttle' => 'Слишком много попыток входа. Повторите попытку через :seconds секунд.',

    'login' => 'Авторизоваться',
    'register' => 'Зарегистрироваться',
    'account_info' => 'Информация об аккаунте',
    'change_info' => 'Изменить информацию',
    'change_password' => 'Изменить пароль',
    'logout' => 'Выйти',
    'remember_me' => 'Запомнить меня',
    'information_updated' => 'Ваша информация обновлена.',
    'name' => 'Имя',
    'email' => 'Email',
    'password' => 'Пароль',
    'confirm_password' => 'Подтверждение пароля',
    'old_password' => 'Старый пароль',
    'new_password' => 'Новый пароль',
    'cancel' => 'Отмена',
    'reset_password' => 'Сброс пароля',
    'send_password_reset_link' => 'Отправить ссылку для сброса пароля',
    'forgot_password_question' => 'Забыли свой пароль?',
    'wrong_password' => 'Старый пароль, который вы ввели, неверен.',
    'password_updated' => 'Ваш пароль был изменен.',
    'registration_closed' => 'Регистрация завершена.',

];
