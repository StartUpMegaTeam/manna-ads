<?php

return [
    'Авдеевка' => [
        'lat' => '48.1451465',
        'lon' => '37.7448676',
    ],
    'Амвросиевка' => [
        'lat' => '47.7967118',
        'lon' => '38.4572884',
    ],
    'Бахмут' => [
        'lat' => '48.6040205',
        'lon' => '37.975118',
    ],
    'Белицкое' => [
        'lat' => '48.4065445',
        'lon' => '37.1764703',
    ],
    'Белозёрское' => [
        'lat' => '48.5368029',
        'lon' => '37.0498298',
    ],
    'Волноваха' => [
        'lat' => '47.5979615',
        'lon' => '37.4484079',
    ],
    'Горловка' => [
        'lat' => '48.3337001',
        'lon' => '38.021837',
    ],
    'Горняк' => [
        'lat' => '48.055247',
        'lon' => '37.3619787',
    ],
    'Дебальцево' => [
        'lat' => '48.3416867',
        'lon' => '38.3893414',
    ],
    'Доброполье' => [
        'lat' => '48.4687896',
        'lon' => '37.0652921',
    ],
    'Докучаевск' => [
        'lat' => '47.7520044',
        'lon' => '37.6432404',
    ],
    'Донецк' => [
        'lat' => '47.9902617',
        'lon' => '37.6210962',
    ],
    'Дружковка' => [
        'lat' => '48.6204109',
        'lon' => '37.4925489',
    ],
    'Енакиево' => [
        'lat' => '48.2312088',
        'lon' => '38.1701539',
    ],
    'Ждановка' => [
        'lat' => '48.1375618',
        'lon' => '38.234288',
    ],
    'Зализное' => [
        'lat' => '48.3683529',
        'lon' => '37.8758041',
    ],
    'Зугрэс' => [
        'lat' => '48.0179669',
        'lon' => '38.2472021',
    ],
    'Иловайск' => [
        'lat' => '47.9276029',
        'lon' => '38.185911',
    ],
    'Комсомольское' => [
        'lat' => '47.6652986',
        'lon' => '38.0649223',
    ],
    'Константиновка' => [
        'lat' => '48.5267445',
        'lon' => '37.6691039',
    ],
    'Краматорск' => [
        'lat' => '48.7295454',
        'lon' => '37.4842306',
    ],
    'Красногоровка' => [
        'lat' => '48.0095401',
        'lon' => '37.4502924',
    ],
    'Кировское' => [
        'lat' => '48.1463959',
        'lon' => '38.3427976',
    ],
    'Курахово' => [
        'lat' => '47.9904455',
        'lon' => '37.2421775',
    ],
    'Лиман' => [
        'lat' => '48.9852655',
        'lon' => '37.7758785',
    ],
    'Макеевка' => [
        'lat' => '48.0456253',
        'lon' => '37.8929091',
    ],
    'Мариуполь' => [
        'lat' => '47.1226904',
        'lon' => '37.5117516',
    ],
    'Марьинка' => [
        'lat' => '47.9445102',
        'lon' => '37.49453',
    ],
    'Мирноград' => [
        'lat' => '48.2910243',
        'lon' => '37.2328755',
    ],
    'Моспино' => [
        'lat' => '47.8783012',
        'lon' => '38.0294509',
    ],
    'Николаевка' => [
        'lat' => '47.6519611',
        'lon' => '37.6606686',
    ],
    'Новоазовск' => [
        'lat' => '47.1159157',
        'lon' => '38.0634855',
    ],
    'Новогродовка' => [
        'lat' => '48.2105763',
        'lon' => '37.312366',
    ],
    'Покровск' => [
        'lat' => '48.2809468',
        'lon' => '37.1442529',
    ],
    'Родинское' => [
        'lat' => '48.3531204',
        'lon' => '37.2012653',
    ],
    'Светлодарск' => [
        'lat' => '48.4352534',
        'lon' => '38.2119378',
    ],
    'Святогорск' => [
        'lat' => '49.0409037',
        'lon' => '37.5610372',
    ],
    'Северск' => [
        'lat' => '48.8868667',
        'lon' => '38.062117',
    ],
    'Селидово' => [
        'lat' => '48.1480343',
        'lon' => '37.281147',
    ],
    'Славянск' => [
        'lat' => '48.8540735',
        'lon' => '37.5442301',
    ],
    'Снежное' => [
        'lat' => '48.03942',
        'lon' => '38.7153244',
    ],
    'Соледар' => [
        'lat' => '48.6969718',
        'lon' => '38.0362475',
    ],
    'Торез' => [
        'lat' => '48.033527',
        'lon' => '38.5863675',
    ],
    'Торецк' => [
        'lat' => '48.3901396',
        'lon' => '37.8291091',
    ],
    'Углегорск' => [
        'lat' => '48.312461',
        'lon' => '38.2636481',
    ],
    'Угледар' => [
        'lat' => '47.7790991',
        'lon' => '37.243881',
    ],
    'Украинск' => [
        'lat' => '48.097427',
        'lon' => '37.3545754',
    ],
    'Харцызск' => [
        'lat' => '48.0428293',
        'lon' => '38.1248555',
    ],
    'Часов Яр' => [
        'lat' => '48.5754895',
        'lon' => '37.8253985',
    ],
    'Шахтёрск' => [
        'lat' => '48.0625408',
        'lon' => '38.3828782',
    ],
    'Юнокоммунаровск' => [
        'lat' => '48.2281118',
        'lon' => '38.256757',
    ],
    'Ясиноватая' => [
        'lat' => '48.1276782',
        'lon' => '37.8450541',
    ],
    'Александровск' => [
        'lat' => '48.5773405',
        'lon' => '39.1799749',
    ],
    'Алмазная' => [
        'lat' => '48.5216655',
        'lon' => '38.5620031',
    ],
    'Алчевск' => [
        'lat' => '48.4780873',
        'lon' => '38.7684695',
    ],
    'Антрацит' => [
        'lat' => '48.1298654',
        'lon' => '39.0691894',
    ],
    'Артёмовск' => [
        'lat' => '48.6040205',
        'lon' => '37.975118',
    ],
    'Брянка' => [
        'lat' => '48.4847426',
        'lon' => '38.6426024',
    ],
    'Вахрушево' => [
        'lat' => '48.1604709',
        'lon' => '38.763046',
    ],
    'Горское' => [
        'lat' => '48.7516956',
        'lon' => '38.477446',
    ],
    'Зимогорье' => [
        'lat' => '48.5915811',
        'lon' => '38.883383',
    ],
    'Золотое' => [
        'lat' => '48.6927978',
        'lon' => '38.4843635',
    ],
    'Зоринск' => [
        'lat' => '48.4143411',
        'lon' => '38.6011291',
    ],
    'Ирмино' => [
        'lat' => '48.5969726',
        'lon' => '38.565294',
    ],
    'Кировск' => [
        'lat' => '48.1463959',
        'lon' => '38.3427976',
    ],
    'Краснодон' => [
        'lat' => '48.2993038',
        'lon' => '39.7147131',
    ],
    'Красный Луч' => [
        'lat' => '48.1272999',
        'lon' => '38.8841585',
    ],
    'Кременная' => [
        'lat' => '49.0499949',
        'lon' => '38.1860069',
    ],
    'Лисичанск' => [
        'lat' => '48.9011881',
        'lon' => '38.3820175',
    ],
    'Луганск' => [
        'lat' => '48.5801563',
        'lon' => '39.2867567',
    ],
    'Лутугино' => [
        'lat' => '48.3990576',
        'lon' => '39.1879372',
    ],
    'Миусинск' => [
        'lat' => '48.0746913',
        'lon' => '38.8661955',
    ],
    'Молодогвардейск' => [
        'lat' => '48.3540992',
        'lon' => '39.6315272',
    ],
    'Новодружеск' => [
        'lat' => '48.9714078',
        'lon' => '38.3209146',
    ],
    'Первомайск' => [
        'lat' => '48.045132',
        'lon' => '30.8168862',
    ],
    'Перевальск' => [
        'lat' => '48.4357076',
        'lon' => '38.782763',
    ],
    'Петровское' => [
        'lat' => '49.2459214',
        'lon' => '37.9106231',
    ],
    'Попасная' => [
        'lat' => '48.6316024',
        'lon' => '38.325297',
    ],
    'Приволье' => [
        'lat' => '49.0011589',
        'lon' => '38.2779614',
    ],
    'Ровеньки' => [
        'lat' => '48.067845',
        'lon' => '39.30758',
    ],
    'Рубежное' => [
        'lat' => '49.012986',
        'lon' => '38.3484146',
    ],
    'Сватово' => [
        'lat' => '49.4093305',
        'lon' => '38.1269488',
    ],
    'Свердловск' => [
        'lat' => '48.058406',
        'lon' => '39.6232005',
    ],
    'Северодонецк' => [
        'lat' => '48.9563525',
        'lon' => '38.4465319',
    ],
    'Старобельск' => [
        'lat' => '49.2775917',
        'lon' => '38.9065651',
    ],
    'Стаханов' => [
        'lat' => '48.5681885',
        'lon' => '38.623637',
    ],
    'Суходольск' => [
        'lat' => '48.3535942',
        'lon' => '39.7041204',
    ],
    'Счастье' => [
        'lat' => '48.7381373',
        'lon' => '39.2223897',
    ],
    'Червонопартизанск' => [
        'lat' => '48.0713958',
        'lon' => '39.780459',
    ],
    'Бердянск' => [
        'lat' => '47.1065883',
        'lon' => '37.4334602',
    ],
    'Васильевка' => [
        'lat' => '48.1767404',
        'lon' => '37.8542983',
    ],
    'Вольнянск' => [
        'lat' => '47.9435559',
        'lon' => '35.4163785',
    ],
    'Гуляйполе' => [
        'lat' => '47.6606489',
        'lon' => '36.246707',
    ],
    'Днепрорудное' => [
        'lat' => '47.3794542',
        'lon' => '34.979729',
    ],
    'Запорожье' => [
        'lat' => '47.8563738',
        'lon' => '35.0349292',
    ],
    'Каменка-Днепровская' => [
        'lat' => '47.4640567',
        'lon' => '34.3953789',
    ],
    'Мелитополь' => [
        'lat' => '46.8434098',
        'lon' => '35.32118',
    ],
    'Молочанск' => [
        'lat' => '47.2040678',
        'lon' => '35.580159',
    ],
    'Орехов' => [
        'lat' => '47.5658441',
        'lon' => '35.758187',
    ],
    'Пологи' => [
        'lat' => '47.4716706',
        'lon' => '36.1933747',
    ],
    'Приморск' => [
        'lat' => '46.7181545',
        'lon' => '36.3227189',
    ],
    'Токмак' => [
        'lat' => '47.2490245',
        'lon' => '35.6678154',
    ],
    'Энергодар' => [
        'lat' => '47.5006186',
        'lon' => '34.5895054',
    ],
    'Балаклея' => [
        'lat' => '49.459189',
        'lon' => '36.8174269',
    ],
    'Барвенково' => [
        'lat' => '48.9099851',
        'lon' => '36.9880895',
    ],
    'Богодухов' => [
        'lat' => '50.1617377',
        'lon' => '35.4904414',
    ],
    'Валки' => [
        'lat' => '49.8381339',
        'lon' => '35.5996825',
    ],
    'Волчанск' => [
        'lat' => '50.2850435',
        'lon' => '36.9135149',
    ],
    'Дергачи' => [
        'lat' => '50.1117798',
        'lon' => '36.0636759',
    ],
    'Змиёв' => [
        'lat' => '49.6876972',
        'lon' => '36.2990649',
    ],
    'Изюм' => [
        'lat' => '49.1958118',
        'lon' => '37.2452204',
    ],
    'Красноград' => [
        'lat' => '49.3717316',
        'lon' => '35.439051',
    ],
    'Купянск' => [
        'lat' => '49.7016112',
        'lon' => '37.5781688',
    ],
    'Лозовая' => [
        'lat' => '48.8945051',
        'lon' => '36.2807283',
    ],
    'Люботин' => [
        'lat' => '49.948361',
        'lon' => '35.8944419',
    ],
    'Мерефа' => [
        'lat' => '49.8198556',
        'lon' => '36.0245715',
    ],
    'Первомайский' => [
        'lat' => '49.3729722',
        'lon' => '36.1810189',
    ],
    'Южный (Пивденное)' => [
        'lat' => '49.8775588',
        'lon' => '36.046694',
    ],
    'Харьков' => [
        'lat' => '49.9946805',
        'lon' => '36.2156139',
    ],
    'Чугуев' => [
        'lat' => '49.8353139',
        'lon' => '36.6635221',
    ],
    'Алёшки' => [
        'lat' => '46.6169237',
        'lon' => '32.6932201',
    ],
    'Берислав' => [
        'lat' => '46.8414205',
        'lon' => '33.411829',
    ],
    'Геническ' => [
        'lat' => '46.1769314',
        'lon' => '34.7812829',
    ],
    'Голая Пристань' => [
        'lat' => '46.5226977',
        'lon' => '32.5113355',
    ],
    'Каховка' => [
        'lat' => '46.7977619',
        'lon' => '33.4613949',
    ],
    'Новая Каховка' => [
        'lat' => '46.7506705',
        'lon' => '33.3437381',
    ],
    'Скадовск' => [
        'lat' => '46.1194141',
        'lon' => '32.8930663',
    ],
    'Таврийск' => [
        'lat' => '46.7563669',
        'lon' => '33.4072672',
    ],
    'Херсон' => [
        'lat' => '46.649626',
        'lon' => '32.5726765',
    ],
];