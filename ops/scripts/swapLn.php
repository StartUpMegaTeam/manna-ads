<?php

function checkLn($path)
{
    if (!file_exists($path)) {
        echo 'Link does not exist! - ' . $path . PHP_EOL;
        die();
    }
    if (!is_link($path)) {
        echo 'Not a symbolic link! - ' . $path . PHP_EOL;
        die();
    }
}

function checkTarget($path)
{
    if (!file_exists($path)) {
        echo 'Target file does not exist! - ' . $path . PHP_EOL;
        die();
    }
}

function msg(string $msg = '')
{
    echo $msg . PHP_EOL;
}

// Prepare
$ln1 = $argv[1] ?? '';
$ln2 = $argv[2] ?? '';

checkLn($ln1);
checkLn($ln2);

$ln1path = readlink($ln1);
$ln2path = readlink($ln2);

checkTarget($ln1path);
checkTarget($ln2path);

// Start
msg('Changing links: ' . $ln1 . '  =>  ' . $ln2);
msg('Links paths: ' . $ln1path . '  =>  ' . $ln2path);
msg('=== Start links swap! ===');


// Remove ln1
if (unlink($ln1)) {
    msg('First symlink deleted successfully.');
} else {
    msg('Can\'t delete first symlink! Tried path: ' . $ln1);
    die();
}

// Create ln1 to ln2 path
if (symlink($ln2path, $ln1)) {
    msg('New first link created.');
} else {
    msg('Can\'t create new first link! Tried path: ' . $ln2path . ' With link name: ' . $ln1);
    die();
}

// Remove ln2
if (unlink($ln2)) {
    msg('Second symlink deleted successfully.');
} else {
    msg('Can\'t delete second symlink! Tried path: ' . $ln1);
    die();
}

// Create ln2 to ln1 path
if (symlink($ln1path, $ln2)) {
    msg('New second symlink created successfully.');
} else {
    msg('Can\'t create new second link! Tried path: ' . $ln1path . ' With link name: ' . $ln2);
    die();
}

// Complete
msg('=== Links swapped! ===');

?>