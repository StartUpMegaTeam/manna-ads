function clearIsInvalid() {
    let elements = $('.is-invalid');

    elements.each((index, item) => {
        let element = $(item);

        let input = element.find(':input');

        input.each((index, el) => {
            $(el).focus(function () {
                element.removeClass('is-invalid');
            });
        });

        let select2 = element.find('select');

        select2.each((index, el) => {
            $(el).on('select2:open', function () {
                element.removeClass('is-invalid');
            });
        });
    });
}


