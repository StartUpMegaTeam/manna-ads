const classSendCodeCounter = 'js-send-code-counter';
const classSendCodeLink = 'js-send-code-link';
const classSendCodeBtn = 'js-send-code-btn';
const urlResendEmail = document.querySelector('#urlResendEmail');
// not const coz script will create new counter
let sendCodeCounter = null;
let urlResendEmaillink = urlResendEmail.dataset.parent;
// if send code already activated
let isSendCodeBlocked = false;

// send code link and attaching event on click
const linkSendCodeAgain = document.querySelector('.' + classSendCodeBtn);

console.log(linkSendCodeAgain);

const waitTime = 30; // in seconds
const delayTimer = 1000; // 1 second in ms

const resendTextBefore = '('
const resendTextAfter = ')'
const sendAgainText = 'Отправить повторно';

function getInnerHtmlSend() {
    let first = document.createElement('span');
    first.innerText = resendTextBefore;

    let middle = document.createElement('span');
    middle.innerText = waitTime.toString();
    middle.classList.add(classSendCodeCounter)

    let last = document.createElement('span');
    last.innerText = resendTextAfter;

    return [first, middle, last];
}

function getInnerHtmlSendAgain() {
    let elem = document.createElement('span');
    elem.innerText = sendAgainText;
    return elem;
}

function sendCode(sendRequest) {
    // Starting the counter from 30 to 0
    // 1. Check if counter is already started
    // 2. Set isBlocked variable to true
    // 3. Make a html with counter
    // 4. Start the timer
    // 5. When timer stops - it will replace html without counter

    if (isSendCodeBlocked) {
        return;
    }

    isSendCodeBlocked = true;
    linkSendCodeAgain.innerHTML = '';
    linkSendCodeAgain.append(...getInnerHtmlSend());
    sendCodeCounter = document.querySelector('.' + classSendCodeCounter);

    if (sendRequest) {
    } else {
        sendCodeTimer(waitTime);
    }
}

function sendCodeTimer(time) {
    setTimeout(function () {
        if (time === 0) {
            linkSendCodeAgain.innerHTML = '';
            linkSendCodeAgain.append(getInnerHtmlSendAgain());
            isSendCodeBlocked = false;
            linkSendCodeAgain.classList.remove('btn--blocked');
            urlResendEmail.href = urlResendEmaillink;
            return;
        }
        sendCodeCounter.innerText = time;
        time -= 1;
        sendCodeTimer(time);
    }, delayTimer)
}

sendCode(false);
