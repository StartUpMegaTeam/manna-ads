$('.js-avatar-add').on('click', function (e) {
    e.preventDefault();
    $('body').addClass('modal-open');
    $('.js-add-avatar-popup').addClass('edit-profile-popup--active');
});

$('.js-avatar-back').on('click', function (e) {
    e.preventDefault();
    $('body').removeClass('modal-open');
    $('.js-add-avatar-popup').removeClass('edit-profile-popup--active');
});
