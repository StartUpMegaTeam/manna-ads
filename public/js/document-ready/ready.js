$(document).ready(function () {

    /* Get and Bind administrative divisions */
    getAdminDivisions(countryCode, adminType, selectedAdminCode);
    $('#countryCode').on('click, change', function () {
        countryCode = $(this).val();
        getAdminDivisions(countryCode, adminType, 0);
    });

    /* Get and Bind the selected city */
    if (adminType == 0) {

        if (userCityDB != 0) {
            getSelectedCity('RU', userCityDB);

            if($('#cityId').val() === 0) {
                getSelectedCity('RU', userCityDB);
            }
        } else {
            getSelectedCity(countryCode, cityId);
        }
    }

    /* Get AJAX's URL */
    let url = function () {
        /* Get the current country code */
        var selectedCountryCode = $('#countryCode').val();
        if (typeof selectedCountryCode !== "undefined") {
            countryCode = selectedCountryCode;
        }

        /* Get the current admin code */
        var selectedAdminCode = $('#adminCode').val();
        if (typeof selectedAdminCode === "undefined") {
            selectedAdminCode = 0;
        }

        return siteUrl + '/ajax/countries/' + strToLower(countryCode) + '/admins/' + adminType + '/' + strToLower(selectedAdminCode) + '/cities';
    };

    /* Get and Bind cities */
    $('#cityId').select2({
        language: select2Language,
        width: '100%',
        dropdownAutoWidth: 'true',
        dropdownParent: $('.create-city-select-dropdown'),
        ajax: {
            url: url,
            dataType: 'json',
            delay: 50,
            data: function (params) {
                var query = {
                    languageCode: languageCode,
                    q: params.term, /* search term */
                    page: params.page
                };

                return query;
            },
            processResults: function (data, params) {
                /*
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                */
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 10) < data.totalEntries
                    }
                };
            },
            error: function (jqXHR, status, error) {
                showErrorModal(jqXHR, error);

                return { results: [] }; /* Return dataset to load after error */
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, /* let our custom formatter work */
        minimumInputLength: 2,
        templateResult: function (data) {
            return data.text;
        },
        templateSelection: function (data, container) {
            return data.text;
        }
    });

});
