window.alert = (message, type = 'success') => {
    if (type == 'success') {
        let myToastEl = document.getElementById('successAlert');
        $('#successAlert .alert-js__message').html(message);
        new bootstrap.Toast(myToastEl).show();
    }
    if (type == 'error') {
        let myToastEl = document.getElementById('errorAlert');
        $('#errorAlert .alert-js__message').html(message);
        new bootstrap.Toast(myToastEl).show();
    }
    if (type == 'info') {
        let myToastEl = document.getElementById('infoAlert');
        $('#infoAlert .alert-js__message').html(message);
        new bootstrap.Toast(myToastEl).show();
    }
};

window.confirm = (message) => {
    $('#ConfirmAlert .alert-js-confirm__title').html(message);
    var PromiseConfirm = $('#ConfirmAlert').modal({
        keyboard: false,
        backdrop: 'static'
    }).modal('show');
    let confirm = false;
    $('#ConfirmAlert .alert-js-confirm__success').on('click', e => {
        confirm = true;
    });
    return new Promise(function (resolve, reject) {
        PromiseConfirm.on('hidden.bs.modal', (e) => {
            resolve(confirm);
        });
    });
};