/*
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

/* Prevent errors, If these variables are missing. */
if (typeof adminType === 'undefined') {
	var adminType = 0;
}
if (typeof selectedAdminCode === 'undefined') {
	var selectedAdminCode = 0;
}
if (typeof cityId === 'undefined') {
	var cityId = 0;
}
var select2Language = languageCode;
if (typeof langLayout !== 'undefined' && typeof langLayout.select2 !== 'undefined') {
	select2Language = langLayout.select2;
}

/**
 * Get and Bind Administrative Divisions
 *
 * @param countryCode
 * @param adminType
 * @param selectedAdminCode
 * @returns {*}
 */
function getAdminDivisions(countryCode, adminType, selectedAdminCode) {
	if (countryCode === 0 || countryCode === '') return false;
	
	let url = siteUrl + '/ajax/countries/' + strToLower(countryCode) + '/admins/' + adminType + '?languageCode=' + languageCode;
	
	$.ajax({
		method: 'GET',
		url: url
	}).done(function (obj) {
		/* Init. */
		$('#adminCode').empty().append('<option value="0">' + lang.select.admin + '</option>').val('0').trigger('change');
		$('#cityId').empty().append('<option value="0">' + lang.select.city + '</option>').val('0').trigger('change');
		
		/* Bind data into Select list */
		if (typeof obj.error !== 'undefined') {
			$('#adminCode').find('option').remove().end().append('<option value="0"> ' + obj.error.message + ' </option>');
			$('#adminCode').addClass('is-invalid');
			return false;
		} else {
			$('#adminCode').removeClass('is-invalid');
		}
		
		if (typeof obj.data === 'undefined') {
			return false;
		}
		$.each(obj.data, function (key, item) {
			if (selectedAdminCode == item.code) {
				$('#adminCode').append('<option value="' + item.code + '" selected="selected">' + item.name + '</option>');
			} else {
				$('#adminCode').append('<option value="' + item.code + '">' + item.name + '</option>');
			}
		});
		
		/* Get and Bind the selected city */
		getSelectedCity(countryCode, cityId);
	});
	
	return selectedAdminCode;
}

/**
 * Get and Bind (Selected) City by ID
 *
 * @param countryCode
 * @param cityId
 * @returns {number}
 */
function getSelectedCity(countryCode, cityId) {
	/* Clear by administrative divisions selection */
	$('#adminCode').on('click, change', function () {
		$('#cityId').empty().append('<option value="0">' + lang.select.city + '</option>').val('0').trigger('change');
	});
	
	let url = siteUrl + '/ajax/countries/' + strToLower(countryCode) + '/cities/' + cityId + '?languageCode=' + languageCode;
	$.ajax({
		method: 'GET',
		url: url
	}).done(function (data) {
		$('#cityId').empty().append('<option value="' + data.id + '">' + data.text + '</option>').val(data.id).trigger('change');
	}).fail(function () {
		$('#cityId').empty().append('<option value="0">' + lang.select.city + '</option>').val('0').trigger('change');
	});
	
	return 0;
}
