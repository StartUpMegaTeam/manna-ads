/*
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

if (typeof updateChatErrorMessage === 'undefined') {
    var updateChatErrorMessage = 'The chat update could not be done.';
}

var autoScrollEnabled = true;

$(function () {
    var chatTextField = $('#body');
    var chatFileFiled = $('#addFile');
    var attachedFiles = $("#attachedFiles");

    setInterval(scrollChatHistoryToBottom, 2000);
    $('#messageChatHistory').scroll(function () {
        autoScrollEnabled = false;
    });
    chatTextField.focus();

    /* Auto-Refresh Chat Messages (1000 ms * X mn) */
    if (typeof timerNewMessagesChecking !== 'undefined') {
        if (timerNewMessagesChecking > 0) {
            var showNewMsgTimer = setInterval(function () {
                getMessages(window.location.href, true);
            }, timerNewMessagesChecking);
        }
    }

    /* AJAX data loading & pagination */
    $(document).on('click', '#linksMessages a', function (e) {
        e.preventDefault();

        /* Stop New Messages Auto-Display */
        if (typeof showNewMsgTimer !== 'undefined') {
            clearInterval(showNewMsgTimer);
        }

        var url = $(this).attr('href');
        getMessages(url, false, false);

        return false;
    });

    $(document).on('click', '#addFileBtn', function (e) {
        e.preventDefault();
        chatFileFiled.trigger('click');
    });

    $(document).on('change', '#addFile', function (e) {
        $('#attachedFiles').empty();

        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        const [file] = this.files;
        if (file) {
            var imgUrl = this.getAttribute("data-default-img-url");

            if (validImageTypes.includes(file['type'])) {
                imgUrl = URL.createObjectURL(file);
            }

            var newItem = $('<div class="attach-files">' +
                '<div class="attach-files__item">\n' +
                '<div class="attach-files__img">\n' +
                '<img src="' + imgUrl + '">' +
                '<button id="deleteAttachedFile" class="attach-files__delete"></button>\n' +
                '</div>\n' +
                '</div>' +
                '</div>');

            $('#attachedFiles').append(newItem);
            $(document).on('click', '#deleteAttachedFile', function (e) {
                clearFileInput(chatFileFiled);
            });
        }
    });

    /* Submit New Chat Message */
    $('#chatForm').on('submit', function (e) {
        e.preventDefault();

        if (chatTextField.val().length == 0 && chatFileFiled.val().length == 0) {
            return false;
        }

        $('.message--sending').removeClass('d-none');
        $('#sendChat').toggleClass('chat-footer__send-btn--sending');

        autoScrollEnabled = true;
        scrollChatHistoryToBottom();
        autoScrollEnabled = false;

        updateChat(this);

        /* Fast chat fields clearing */
        chatTextField.val('');
        clearFileInput(chatFileFiled);

        return false;
    });

    /* Watch textarea for release of key press */
    chatTextField.keyup(function (e) {
        var text = $(this).val();
        var length = text.trim().length;

        if (length == 0) {
            $('#sendChat').addClass('chat-footer__send-btn--sending');
        } else {
            $('#sendChat').removeClass('chat-footer__send-btn--sending');
        }

        if (!e.shiftKey && e.keyCode === 13) {

            /* Send */
            if (length > 1) {
                ym(91551648,'reachGoal','onmess');

                $('#chatForm').submit();
            }
        }
    });
});

/* Function of AJAX data loading & pagination */
function getMessages(url, firstLoading = false, canBeAutoScroll = true) {
    $.ajax({
        url: url
    }).done(function (data) {
        $('#successMsg').empty().hide();
        $('#errorMsg').empty().hide();

        if (typeof data.messages === 'undefined' || typeof data.links === 'undefined') {
            return false;
        }

        if (firstLoading === true) {
            $('#messageChatHistory').empty().html('<div id="linksMessages" class="text-center"></div>');
        }

        $('#linksMessages').html(data.links).after(data.messages);

        autoScrollEnabled = canBeAutoScroll;
    }).fail(function () {
        alert(loadingErrorMessage, 'error');
    });
}

function updateChat(formElement) {
    var formUrl = $(formElement).attr('action');
    var formData = new FormData(formElement);

    $.ajax({
        method: 'POST',
        url: formUrl,
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {

        getMessages(formUrl, true);

        autoScrollEnabled = true;
        scrollChatHistoryToBottom();
        autoScrollEnabled = false;

        /* Chat fields clearing */
        $('#body').focus();
        clearFileInput($('#addFile'));

    }).fail(function (data) {
        $('#successMsg').empty().hide();
        let errors = '';

        if (data.responseJSON) {
            var appended = false;

            if (data.responseJSON.errors) {
                appended = true;
                $.each(data.responseJSON.errors, function (key, value) {
                    $.each(value, function (key, value) {
                        if (value != '' && value != 'undefined') {
                            errors += value + '\n';
                        }
                    });
                });
            }

            if (appended) {
                alert(errors, 'error');
            }
        }

        $('.message--sending').addClass('d-none');
    });
}

/* Auto-Scroll to Messages History to Bottom */
function scrollChatHistoryToBottom() {
    if (autoScrollEnabled) {
        /* Pure JS version */
        /*
        var el = document.getElementById('messageChatHistory');
        el.scrollTop = el.scrollHeight;
        */

        /* jQuery version */
        var el = $('#messageChatHistory');
        /* el.scrollTop(el[0].scrollHeight);           /* Without animation */
        el.animate({scrollTop: el[0].scrollHeight}); /* With animation */
    }
}

/* Clear the File Input */
function clearFileInput(input) {
    input.val(null);
    $('#attachedFiles').empty();
}
