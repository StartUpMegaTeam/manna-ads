/*
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

$(document).ready(function () {

    /* Save the Post */
    $('.make-favorite').click(function () {
        savePost(this, false);
    });

    $('.make-favorite-details').click(function () {
        savePost(this, true);
    });

    /* Save the Search */
    $('#saveSearch').click(function () {
        saveSearch(this);
    });

    $('#mobileSaveSearch').click(function () {
        saveSearch(this);
    });
});

/**
 * Save Ad
 * @param elmt
 * @returns {boolean}
 */
function savePost(elmt, isDetails) {
    var postId = $(elmt).attr('id');
    var url = siteUrl + '/ajax/save/post';
    $.ajax({
        method: 'POST',
        url: url, data: {'postId': postId, '_token': $('input[name=_token]').val()}
    }).done(function (data) {
        if (typeof data.logged == "undefined") {
            return false;
        }/* Guest Users - Need to Log In */
        if (data.logged == 0) {
            $('#quickLogin').modal();
            return false;
        }
        if (data.status == 1) {
            ym(91551648,'reachGoal','favourites');

            if (isDetails) {
                elmt.getElementsByTagName('img')[0].src = '/icon/ads-show/heart-active.svg';
            } else {
                elmt.getElementsByTagName('img')[0].src = '/icon/post-icon/icon-like-active.svg';
            }
            elmt.getElementsByTagName('img')[0].classList.add("like-hover--active");
            elmt.getElementsByTagName('img')[0].classList.remove("like-hover");
        } else {
            if (isDetails) {
                elmt.getElementsByTagName('img')[0].src = '/icon/ads-show/heart.svg';
            } else {
                elmt.getElementsByTagName('img')[0].src = '/icon/post-icon/icon-like.svg';
            }
            elmt.getElementsByTagName('img')[0].classList.add("like-hover");
            elmt.getElementsByTagName('img')[0].classList.remove("like-hover--active");
        }

        return false;
    });

    return false;
}

/**
 * Save Search
 * @param elmt
 * @returns {boolean}
 */
function saveSearch(elmt) {
    var searchUrl = $(elmt).attr('name');
    var countPosts = $(elmt).attr('count');

    let url = siteUrl + '/ajax/save/search';

    $.ajax({
        method: 'POST',
        url: url,
        data: {
            'url': searchUrl,
            'countPosts': countPosts,
            '_token': $('input[name=_token]').val()
        }
    }).done(function (data) {
        if (typeof data.logged == "undefined") {
            return false;
        }

        /* Guest Users - Need to Log In */
        if (data.logged == 0) {
            $('#quickLogin').modal();
            return false;
        }

        /* Logged Users - Notification */
        if (data.status == 1) {
            alert(lang.confirmationSaveSearch, 'success');
        } else {
            alert(lang.confirmationRemoveSaveSearch, 'success');
        }

        return false;
    });

    return false;
}