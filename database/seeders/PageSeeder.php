<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$entries = [
			[
				'parent_id'            => null,
				'type'                 => 'terms',
				'name'                 => ['en' => 'Terms'],
				'slug'                 => 'terms',
				'title'                => ['en' => 'Terms & Conditions'],
				'picture'              => null,
				'content'              => [],
				'external_link'        => null,
				'lft'                  => '6',
				'rgt'                  => '7',
				'depth'                => '1',
				'name_color'           => null,
				'title_color'          => null,
				'target_blank'         => '0',
				'excluded_from_footer' => '0',
				'active'               => '1',
				'created_at'           => now()->format('Y-m-d H:i:s'),
				'updated_at'           => now()->format('Y-m-d H:i:s'),
			],
			[
				'parent_id'            => null,
				'type'                 => 'privacy',
				'name'                 => ['en' => 'Privacy'],
				'slug'                 => 'privacy',
				'title'                => ['en' => 'Privacy'],
				'picture'              => null,
				'content'              => [],
				'external_link'        => null,
				'lft'                  => '8',
				'rgt'                  => '9',
				'depth'                => '1',
				'name_color'           => null,
				'title_color'          => null,
				'target_blank'         => '0',
				'excluded_from_footer' => '0',
				'active'               => '1',
				'created_at'           => now()->format('Y-m-d H:i:s'),
				'updated_at'           => now()->format('Y-m-d H:i:s'),
			],
			[
				'parent_id'            => null,
				'type'                 => 'standard',
				'name'                 => ['en' => 'Anti-Scam'],
				'slug'                 => 'anti-scam',
				'title'                => ['en' => 'Anti-Scam'],
				'picture'              => null,
				'content'              => [],
				'external_link'        => null,
				'lft'                  => '4',
				'rgt'                  => '5',
				'depth'                => '1',
				'name_color'           => null,
				'title_color'          => null,
				'target_blank'         => '0',
				'excluded_from_footer' => '0',
				'active'               => '1',
				'created_at'           => now()->format('Y-m-d H:i:s'),
				'updated_at'           => now()->format('Y-m-d H:i:s'),
			],
			[
				'parent_id'            => null,
				'type'                 => 'standard',
				'name'                 => ['en' => 'FAQ'],
				'slug'                 => 'faq',
				'title'                => ['en' => 'Frequently Asked Questions'],
				'picture'              => null,
				'content'              => [],
				'external_link'        => null,
				'lft'                  => '2',
				'rgt'                  => '3',
				'depth'                => '1',
				'name_color'           => null,
				'title_color'          => null,
				'target_blank'         => '0',
				'excluded_from_footer' => '0',
				'active'               => '1',
				'created_at'           => now()->format('Y-m-d H:i:s'),
				'updated_at'           => now()->format('Y-m-d H:i:s'),
			],
		];
		
		$tableName = (new Page())->getTable();
		foreach ($entries as $entry) {
			$entry = arrayTranslationsToJson($entry);
			$entryId = DB::table($tableName)->insertGetId($entry);
		}
	}
}
