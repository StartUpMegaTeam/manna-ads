<?php

namespace Database\Seeders;

use App\Models\MetaTag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MetaTagSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$entries = [
			[
				'page'        => 'home',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'search',
				'title'       => null,
				'description' => null,
				'keywords'    => null,
				'active'      => '1',
			],
			[
				'page'        => 'searchCategory',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'searchLocation',
				'title'       => null,
				'description' => null,
				'keywords'    => null,
				'active'      => '1',
			],
			[
				'page'        => 'searchProfile',
				'title'       => null,
				'description' => null,
				'keywords'    => null,
				'active'      => '1',
			],
			[
				'page'        => 'searchTag',
				'title'       => null,
				'description' => null,
				'keywords'    => null,
				'active'      => '1',
			],
			[
				'page'        => 'adDetails',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'register',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'login',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'create',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'countries',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'contact',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'sitemap',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'password',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'pricing',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
			[
				'page'        => 'staticPage',
				'title'       => [],
				'description' => [],
				'keywords'    => [],
				'active'      => '1',
			],
		];
		
		$tableName = (new MetaTag())->getTable();
		foreach ($entries as $entry) {
			$entry = arrayTranslationsToJson($entry);
			$entryId = DB::table($tableName)->insertGetId($entry);
		}
	}
}
