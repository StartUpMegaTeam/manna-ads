<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entries = [
            [
                'code' => 'RU',
                'iso3' => 'RUS',
                'iso_numeric' => '643',
                'fips' => 'RS',
                'name' => ['en' => 'Russia'],
                'capital' => 'Moscow',
                'area' => '17100000',
                'population' => '140702000',
                'continent_code' => 'EU',
                'tld' => '.ru',
                'currency_code' => 'RUB',
                'phone' => '7',
                'postal_code_format' => '######',
                'postal_code_regex' => '^(d{6})$',
                'languages' => 'ru,tt,xal,cau,ady,kv,ce,tyv,cv,udm,tut,mns,bua,myv',
                'neighbours' => 'GE,CN,BY,UA,KZ,LV,PL,EE,LT,FI,MN,NO,AZ,KP',
                'equivalent_fips_code' => '',
                'background_image' => null,
                'admin_type' => '0',
                'admin_field_active' => '0',
                'active' => true,
                'created_at' => now()->format('Y-m-d H:i:s'),
                'updated_at' => now()->format('Y-m-d H:i:s'),
            ],
        ];

        $tableName = (new Country())->getTable();
        foreach ($entries as $entry) {
            $entry = arrayTranslationsToJson($entry);
            DB::table($tableName)->insert($entry);
        }

        Country::autoTranslation(true);
    }
}
