<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $entries = [
            [
                'code' => 'RUB',
                'name' => 'Russia Ruble',
                'symbol' => '₽',
                'html_entities' => '&#8381;',
                'in_left' => '0',
                'decimal_places' => '2',
                'decimal_separator' => '.',
                'thousand_separator' => ',',
                'created_at' => now()->format('Y-m-d H:i:s'),
                'updated_at' => now()->format('Y-m-d H:i:s'),
            ],
        ];

        $tableName = (new Currency())->getTable();
        foreach ($entries as $entry) {
            DB::table($tableName)->insert($entry);
        }
    }
}
