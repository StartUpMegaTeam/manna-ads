<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{

    public function run()
    {

        //todo создать сидер админа

        $user = User::create(['email' => 'admin@admin.ru', 'password' => Hash::make('trial')]);

        $user->update([
            'country_code' => 'RU',
            'language_code' => null,
            'user_type_id' => 1,
            'gender_id' => 1,
            'name' => 'Admin',
            'photo' => null,
            'about' => 'Administrator',
            'phone' => null,
            'phone_hidden' => false,
            'username' => 'admin',
            'email_verified_at' => null,
            'remember_token' => null,
            'is_admin' => true,
            'can_be_impersonated' => true,
            'disable_comments' => false,
            'ip_addr' => null,
            'provider' => null,
            'provider_id' => null,
            'email_token' => null,
            'phone_token' => null,
            'verified_email' => true,
            'verified_phone' => true,
            'accept_terms' => false,
            'accept_marketing_offers' => false,
            'time_zone' => null,
            'blocked' => false,
            'closed' => false,
            'last_activity' => null,
            'last_login_at' => Carbon::now(),
            'deleted_at' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('model_has_roles')->insert([
            'role_id' => Role::whereName('super-admin')->first()->id,
            'model_type' => 'App\Models\User',
            'model_id' => $user->id,
        ]);
    }
}