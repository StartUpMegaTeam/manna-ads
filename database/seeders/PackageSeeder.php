<?php

namespace Database\Seeders;

use App\Models\Package;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackageSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// Packages' "active" column value
		$activeValue = isDemoDomain(env('APP_URL')) ? '1' : '0';
		
		$entries = [
			[
				'name'                  => [
					'ru' => 'Обычный список',
				],
				'short_name'            => [
					'ru' => 'Стандарт',
				],
				'ribbon'                => 'red',
				'has_badge'             => '1',
				'price'                 => '0.00',
				'currency_code'         => 'USD',
				'promo_duration'        => null,
				'duration'              => null,
				'pictures_limit'        => null,
				'description'           => null,
				'facebook_ads_duration' => '0',
				'google_ads_duration'   => '0',
				'twitter_ads_duration'  => '0',
				'linkedin_ads_duration' => '0',
				'recommended'           => '0',
				'parent_id'             => null,
				'lft'                   => '2',
				'rgt'                   => '3',
				'depth'                 => '0',
				'active'                => $activeValue,
			],
			[
				'name'                  => [
					'ru' => 'Объявление вверху страницы',
				],
				'short_name'            => [
					'ru' => 'Премиум',
				],
				'ribbon'                => 'orange',
				'has_badge'             => '1',
				'price'                 => '7.50',
				'currency_code'         => 'USD',
				'promo_duration'        => '7',
				'duration'              => '60',
				'pictures_limit'        => '10',
				'description'           => [
					'ru' => "Показано на главной странице\nВ категории",
				],
				'facebook_ads_duration' => '0',
				'google_ads_duration'   => '0',
				'twitter_ads_duration'  => '0',
				'linkedin_ads_duration' => '0',
				'recommended'           => '1',
				'parent_id'             => null,
				'lft'                   => '4',
				'rgt'                   => '5',
				'depth'                 => '0',
				'active'                => $activeValue,
			],
			[
				'name'                  => [
					'ru' => 'Объявление вверху страницы+',
				],
				'short_name'            => [
					'ru' => 'Премиум+',
				],
				'ribbon'                => 'green',
				'has_badge'             => '1',
				'price'                 => '9.00',
				'currency_code'         => 'USD',
				'promo_duration'        => '30',
				'duration'              => '120',
				'pictures_limit'        => '15',
				'description'           => [
					'ru' => "Показано на главной странице\nВ категории",
				],
				'facebook_ads_duration' => '0',
				'google_ads_duration'   => '0',
				'twitter_ads_duration'  => '0',
				'linkedin_ads_duration' => '0',
				'recommended'           => '0',
				'parent_id'             => null,
				'lft'                   => '6',
				'rgt'                   => '7',
				'depth'                 => '0',
				'active'                => $activeValue,
			],
		];
		
		$tableName = (new Package())->getTable();
		foreach ($entries as $entry) {
			$entry = arrayTranslationsToJson($entry);
			$entryId = DB::table($tableName)->insertGetId($entry);
		}
	}
}
