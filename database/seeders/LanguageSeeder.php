<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $entries = [
            [
                'abbr' => 'ru',
                'locale' => 'ru_RU',
                'name' => 'Russian',
                'native' => 'русский',
                'flag' => null,
                'app_name' => 'russian',
                'script' => 'Cyrl',
                'direction' => 'ltr',
                'russian_pluralization' => '1',
                'date_format' => 'DD MM YYYY',
                'datetime_format' => 'DD MM YYYY [ г.] H:mm',
                'active' => '1',
                'default' => '0',
                'parent_id' => null,
                'lft' => '18',
                'rgt' => '19',
                'depth' => '1',
                'deleted_at' => null,
                'created_at' => now()->format('Y-m-d H:i:s'),
                'updated_at' => now()->format('Y-m-d H:i:s'),
            ],
        ];

        $tableName = (new Language())->getTable();
        foreach ($entries as $entry) {
            DB::table($tableName)->insert($entry);
        }
    }
}
