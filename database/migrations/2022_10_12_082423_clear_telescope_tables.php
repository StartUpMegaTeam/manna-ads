<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ClearTelescopeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET foreign_key_checks=0");
        if (Schema::hasTable('telescope_entries_tags')){
            DB::table('telescope_entries_tags')->truncate();
        }
        if (Schema::hasTable('telescope_entries')){
            DB::table('telescope_entries')->truncate();
        }
        if (Schema::hasTable('telescope_monitoring')){
            DB::table('telescope_monitoring')->truncate();
        }
        DB::statement("SET foreign_key_checks=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
