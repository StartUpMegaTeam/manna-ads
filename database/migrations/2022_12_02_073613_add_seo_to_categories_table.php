<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSeoToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('seo_h1')->nullable();
            $table->string('seo_h1_card')->nullable();
            $table->string('seo_title_card')->nullable();
            $table->string('seo_description_card')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('seo_h1');
            $table->dropColumn('seo_h1_card');
            $table->dropColumn('seo_title_card');
            $table->dropColumn('seo_description_card');
        });
    }
}
