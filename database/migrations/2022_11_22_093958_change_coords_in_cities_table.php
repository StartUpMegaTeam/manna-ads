<?php

use App\Models\City;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCoordsInCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('cities', function (Blueprint $table) {
//            $table->float('longitude', 8, 8)->change();
//            $table->float('latitude', 8, 8)->change();
//        });
//
//        $cityCoords = config('city-coords');
//
//        $citiesList = City::all();
//
//        /** @var City $item */
//        foreach ($citiesList as $item) {
//            $item->latitude = $cityCoords[$item->name]['lat'];
//            $item->longitude = $cityCoords[$item->name]['lon'];
//            $item->save();
//        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table) {
            //
        });
    }
}
