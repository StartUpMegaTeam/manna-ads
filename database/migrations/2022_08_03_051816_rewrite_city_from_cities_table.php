<?php

use App\Models\City;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RewriteCityFromCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        City::query()->delete();

        DB::table('cities')->insert(
            array(
                'country_code' => 'RU',
                'name' => '{"en":"Донецк"}',
                'longitude' => '48.011231',
                'latitude' => '37.803061',
                'feature_class' => 'P',
                'feature_code' => 'PPLC',
                'subadmin1_code'=> null,
                'subadmin2_code' => null,
                'population' => '2244547',
                'time_zone' => 'Europe/Moscow',
                'active' => true
            )
        );
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table) {
            //
        });
    }
}
