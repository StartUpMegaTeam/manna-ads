# Manna Ads

## Local Deployment via Docker

In local terminal:

1. `cp .env.example .env` and add your specs
2. `docker-compose build`
3. `docker-compose up -d`

In docker container terminal:

1. `composer install`
2. `php artisan key:generate`
3. `php artisan storage:link`
4. `php artisan migrate`
5. import database dump
6. copy folder storage/app/public from manna-ads.cyberia.studio

Then go [http://localhost:8000/login](link=http://localhost:8000/login)

## Starting via Docker

- Start: `docker-compose up -d`
- Stop: `docker-compose down`

## Deployment on server

1. `cp .env.example .env`
2. `composer install`
3. `php7.4 artisan key:generate`

## Adminer

Adminer available at `{{ HOST }}/adminer-Kwma4UrPULkK0pGtc2.php`

For
example [http://localhost:8000/adminer-Kwma4UrPULkK0pGtc2.php](link=http://localhost:8000/adminer-Kwma4UrPULkK0pGtc2.php)

**_Since adminer has executable code it's better to mark public directory as 'Excluded' in PHPStorm_**:

1. Right click on 'public' directory
2. Hover option 'Mark Directory as'
3. Choose 'Excluded' in opened tab
