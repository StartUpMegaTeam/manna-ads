<?php

namespace App\Helpers;

use App\Models\Post;
use App\Models\SavedPost;
use App\Models\SavedSearch;
use App\Models\Thread;

class MenuCount
{
    public static function getNotificationCount(): string
    {
        return auth()->user() ? Number::short(Thread::whereHas('post', function ($query) {
            $query->currentCountry()->unarchived();
        })->forUserWithNewMessages(auth()->id())->count()) : '';
    }

    public static function getMyPostsCount()
    {
        return auth()->user() ? Post::whereHas('country')
            ->currentCountry()
            ->where('user_id', auth()->user()->id)
            ->verified()
            ->unarchived()
            ->reviewed()
            ->whereHas('city')
            ->count() : '';
    }

    public static function getSavedPostsCount()
    {
        return auth()->user() ? SavedPost::whereHas('post', function ($query) {
            $query->whereHas('country')
                ->currentCountry();
        })
            ->where('user_id', auth()->user()->id)
            ->whereHas('post.city')
            ->count() : '';
    }

    public static function getSavedSearchCount()
    {
        return auth()->user() ? SavedSearch::whereHas('country')
            ->currentCountry()
            ->where('user_id', auth()->user()->id)
            ->count() : '';
    }

    public static function getArchivedPostsCount()
    {
        return auth()->user() ? Post::whereHas('country')
            ->currentCountry()
            ->whereHas('city')
            ->where('user_id', auth()->user()->id)
            ->archived()->count() : '';
    }
}