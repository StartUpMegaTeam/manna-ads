<?php

namespace App\Helpers;

class Filter
{
    public static function haveQuery()
    {
        return request()->get('q') !== null && request()->get('q') != '';
    }
}