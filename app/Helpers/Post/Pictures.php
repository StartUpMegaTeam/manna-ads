<?php

namespace App\Helpers\Post;

trait Pictures
{
    public function getMainPictureUrl()
    {
        return $this->pictures->count() > 0 ? $this->pictures->get(0)->filename : config('larapen.core.picture.default');
    }
}