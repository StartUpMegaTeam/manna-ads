<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Request;

class AppBackground
{
    public static function coloredBackground()
    {
        $pagesWithColoredBg = [
            '/^\/$/ix',

            '/^login$/ix',
            '/^register$/ix',
            '/^password\/[[:print:]]{0,}$/ix',

            '/^account$/ix',
            '/^account\/[[:print:]]{0,}$/ix',

            '/^search$/ix',
            '/^category\/[[:print:]]{0,}$/ix',
            '/^tag\/[[:print:]]{0,}$/ix',
            '/^users\/[[:print:]]{0,}$/ix',
        ];
        $currentPath = Request::path();

        $recolorWrapper = false;
        foreach ($pagesWithColoredBg as $item) {
            if (preg_match($item, $currentPath)) {
                $recolorWrapper = true;
                break;
            }
        }

        return $recolorWrapper;
    }
}