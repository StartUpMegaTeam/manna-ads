<?php

namespace App\Helpers\CitySelect;

use App\Models\City;
use App\Models\User;

class CitySelectHelper
{
    public static function getCityList(): array
    {
        $cityList = City::query()->orderBy('name', 'ASC')->where('active', '=', '1')->pluck('name', 'id');

        /** @var User $authUser */
        $authUser = auth()->user();

        if ($authUser) {
            $dbCity = $authUser->city_id;
        } elseif(isset($_COOKIE['userLocation'])) {
            $dbCity = $_COOKIE['userLocation'];
        } else {
            $dbCity = null;
        }

        $oldValue = old('l', request()->get('l'));

        if ($dbCity !== null && ($oldValue === '' || $oldValue === null)) {
            $oldValue = $dbCity;
        }

        return [$cityList, $oldValue];
    }
}