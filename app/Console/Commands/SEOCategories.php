<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;

class SEOCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seo:auto-model-title';

    private const SLUG = 'avtomobili';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация SEO Title для моделей марок авто';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $parentCategory = Category::where('slug', '=', self::SLUG)->first();

        if (!$parentCategory) {
            $this->info('Категории со слагом ' . self::SLUG . ' не существует');

            return 0;
        }

        $category = Category::where('slug', '=', self::SLUG)->first();

        $this->output->progressStart($category->children->count());

        foreach ($category->children as $child) {
            foreach ($child->children as $item) {
                $item->title = $child->name . ' ' . $item->name;
                $item->save();
            }
            $this->output->progressAdvance();
        }

        $this->output->progressFinish();

        return 0;
    }
}
