<?php

namespace App\Console\Commands;

use App\Models\FieldOption;
use App\Models\Post;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FixPosts extends Command
{
    protected $signature = 'posts:fix';

    protected $description = 'Command description';

    protected $extraInformation = [
        'тис.км.',
        'тыс.км.',
        'л.',
        'к.с.',
        'комната',
        'комнаты',
        'комнат',
        'м²',
        'с.',
        'км',
        'кг',
        'см³',
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $posts = Post::with(['user' => function (BelongsTo $q) {
            $q->where('is_olx', '=', 1);
        }])->get();

        $this->output->progressStart(count($posts));

        foreach ($posts as $post) {
            $postValues = $post->postValues;

            foreach ($postValues as $postValue) {
                $postValue->value = str_replace($this->extraInformation, '', $postValue->value);
                $postValue->value = trim($postValue->value);

                $postFieldsOptions = $postValue->field_id;

                $fieldsOptions = FieldOption::where('field_id', '=', $postFieldsOptions)->get();

                foreach ($fieldsOptions as $fieldsOption) {
                    if ($postValue->value == $fieldsOption->value && $fieldsOption->parent_id == 0) {
                        $postValue->value = $fieldsOption->id;
                    }
                }

                $postValue->save();
            }
            $this->output->progressAdvance();
        }

        $this->output->progressFinish();

        return 0;
    }
}