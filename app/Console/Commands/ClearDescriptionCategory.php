<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;

class ClearDescriptionCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'category:clear-desc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Очистка SEO описания';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $slug = $this->ask('Введите slug категории, которую нужно заполнить');

        $parentCategory = Category::where('slug', '=', $slug)->first();

        if (!$parentCategory) {
            $this->info('Категории с таким slug не существует');

            return 0;
        }

        /** @var Category $child */
        foreach ($parentCategory->children as $child) {
            $this->clearDesc($child);
        }

        return 0;
    }

    protected function clearDesc($cat) {
        /** @var Category $cat */
        $cat->seo_description = null;
        $cat->save();
        if ($cat->children->count() > 0) {
            foreach ($cat->children as $child) {
                $this->clearDesc($child);
            }
        }
    }
}