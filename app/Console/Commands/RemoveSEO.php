<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;

class RemoveSEO extends Command
{
    protected $signature = 'seo:remove';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $slug = $this->ask('Введите slug родительской категории, в дочерних категориях которой нужно очистить SEO теги');

        $parentCategory = Category::where('slug', '=', $slug)->first();

        if (!$parentCategory) {
            $this->info('Категории с таким slug не существует');

            return 0;
        }

        $this->output->progressStart($parentCategory->children->count());

        $this->removeSeo($parentCategory);

        $this->output->progressFinish();

        return 0;
    }

    public function removeSeo($category)
    {
        /** @var Category $child */
        foreach ($category->children as $child) {
            $child->seo_h1 = null;
            $child->seo_h1_card = null;
            $child->seo_title = null;
            $child->seo_title_card = null;
            $child->seo_description = null;
            $child->seo_description_card = null;
            $child->seo_keywords = null;
            $child->save();

            $this->removeSeo($child);
        }
        $this->output->progressAdvance();
    }
}