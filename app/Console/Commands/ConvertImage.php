<?php

namespace App\Console\Commands;

use App\Models\FieldOption;
use App\Models\Picture;
use App\Models\Post;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ConvertImage extends Command
{
    protected $signature = 'image:convert-resize';

    protected $description = 'Converting images into webp and resize images';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $lastId = 0;
        $limit = 1000;
        $totalCount = Post::where('archived', '=', 0)
            ->where('id', '>', $lastId)
            ->count();

        $progressBar = $this->output->createProgressBar($totalCount);
        $progressBar->start();

        do {
            $posts = Post::where('archived', '=', 0)
                ->where('id', '>', $lastId)
                ->limit($limit)
                ->get();

            /** @var Post $post */
            foreach ($posts as $post) {
                /** @var Picture $picture */
                foreach ($post->pictures as $picture) {
                    foreach (['small', 'medium', 'big'] as $size) {
                        $srcWebp = imgUrl($picture->filename, $size, (bool)config('settings.optimization.webp_format'));
                    }
                }

                $lastId = $post->id;
                $progressBar->advance();
            }
        } while (count($posts) > 0);

        die;

        $progressBar->finish();

        return 0;
    }
}