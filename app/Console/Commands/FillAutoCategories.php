<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;

class FillAutoCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories:fill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $slug = $this->ask('Введите slug категории, которую нужно заполнить');

        $parentCategory = Category::where('slug', '=', $slug)->first();

        if (!$parentCategory) {
            $this->info('Категории с таким slug не существует');

            return 0;
        }

        $filePath = $this->ask('Введите путь к csv файлу с данными. Путь к файлу указывается относительно "storage/app/public"');
        $fullPath = 'storage/' . $filePath;

        $type = $this->choice("Выберите тип категории
        - classified: стандартный тип.
        - rent: аренда.
        - job-offer: Связанные с объявлениями о вакансиях. Поле цены отображается с меткой «Зарплата».
        - job-search: Относится к объявлениям о поиске работы. Поле цены отображается с меткой «Зарплата». Тип объявления «Профессиональный» скрыт.
        - not-salable: Связано с непродаваемой рекламой. Цена не отображается (заменена на '--').",
            ['classified', 'rent', 'job-offer', 'job-search', 'not-salable']);

        //локально 'http://{ip}:8000/' . $fullPath
        $fp = file(asset($fullPath), FILE_SKIP_EMPTY_LINES);
        $this->output->progressStart(count($fp));

        //локально 'http://{ip}:8000/' . $fullPath
        if (($fp = fopen(asset($fullPath), 'rb')) !== false) {
            while (($data = fgetcsv($fp, 0, ';')) !== false) {
                $subCat = Category::where('parent_id', '=', $parentCategory->id)
                    ->where('name', '=', '{"ru":"' . trim($data[0]) . '"}')->first();

                if (!$subCat) {
                    if (trim($data[0]) !== ''){
                        $subCat = $this->createCategory($parentCategory->id, trim($data[0]), $type);
                    }
                }

                if (isset($data[1]) && isset($subCat)) {
                    $model = Category::where('parent_id', '=', $subCat->id)
                        ->where('name', '=', '{"ru":"' . trim($data[1]) . '"}')->first();

                    if (!$model) {
                        if (trim($data[1]) !== ''){
                            $this->createCategory($subCat->id, trim($data[1]), $type);
                        }
                    }
                }

                $this->output->progressAdvance();
            }

            fclose($fp);
        }

        $this->output->progressFinish();

        return 0;
    }

    public function createCategory($parent_id, $name, $type): Category
    {
        return Category::create([
            'parent_id' => $parent_id,
            'name' => $name,
            'description' => $name,
            'seo_title' => $name,
            'seo_description' => $name,
            'seo_keywords' => $name,
            'type' => $type,
            'is_for_permanent' => 0,
            'active' => 1,
        ]);
    }
}
