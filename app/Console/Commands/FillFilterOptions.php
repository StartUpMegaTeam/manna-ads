<?php

namespace App\Console\Commands;

use App\Models\Field;
use App\Models\FieldOption;
use Illuminate\Console\Command;

class FillFilterOptions extends Command
{
    protected $signature = 'options:fill';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    protected $iterator = 1;

    public function handle()
    {
        $fieldID = $this->ask('Введите ID поля, которое нужно заполнить');

        $parentField = Field::where('id', '=', $fieldID)->first();

        if (!$parentField) {
            $this->info('Поля с таким ID не существует');

            return 0;
        }

        $filePath = $this->ask('Введите путь к csv файлу с данными. Путь к файлу указывается относительно "storage/app/public"');
        $fullPath = 'storage/' . $filePath;

        $fp = file(asset($fullPath), FILE_SKIP_EMPTY_LINES);
        $this->output->progressStart(count($fp));

        if (($fp = fopen(asset($fullPath), 'rb')) !== false) {
            while (($data = fgetcsv($fp, 0, ';')) !== false) {

                if (!FieldOption::where('value', '=', '{"ru":"' . $data[0] . '"}')->first()) {
                    if (!$this->createOption($parentField->id, $data[0])) {
                        $this->info('Не удалось создать option с именем: ' . $data[0]);
                    }
                }

                $this->output->progressAdvance();
            }
            fclose($fp);
        }

        $this->output->progressFinish();

        return 0;
    }

    public function createOption($parent_id, $name): FieldOption
    {
        $this->iterator++;

        return FieldOption::create([
            'field_id' => $parent_id,
            'value' => $name,
            'parent_id' => 0,
            'lft' => $this->iterator,
            'rgt' => ++$this->iterator,
            'depth' => null,
        ]);
    }
}