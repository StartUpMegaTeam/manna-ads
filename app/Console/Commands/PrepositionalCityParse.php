<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\City;
use Illuminate\Console\Command;

class PrepositionalCityParse extends Command
{
    protected $signature = 'cities:prep';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $filePath = $this->ask('Введите путь к csv файлу с данными. Путь к файлу указывается относительно "storage/app/public"');
        $fullPath = 'storage/' . $filePath;

        //локально 'http://{ip}:8000/' . $fullPath
        $fp = file(asset($fullPath), FILE_SKIP_EMPTY_LINES);
        $this->output->progressStart(count($fp));

        //локально 'http://{ip}:8000/' . $fullPath
        if (($fp = fopen(asset($fullPath), 'rb')) !== false) {
            while (($data = fgetcsv($fp, 0, ',')) !== false) {

                /** @var City $city */
                if($city = City::where('name', '=', '{"ru":"' . trim($data[0]) . '"}')->first()){
                    $city->name_prepositional = $data[1];
                    $city->save();
                }

                $this->output->progressAdvance();
            }

            fclose($fp);
        }

        $this->output->progressFinish();

        return 0;
    }
}