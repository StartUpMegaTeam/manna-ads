<?php

namespace App\Console\Commands;

use App\Enums\CitiesOlxSlagEnum;
use App\Service\ParserDataService;
use App\Service\ParserWorkCategoryDataService;
use Illuminate\Console\Command;

class ParserData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:run {starPage=1 }{endPage=25}{category=0}{city=0}{chromedriver=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parser run';
    /**
     * @var ParserDataService
     */
    protected $parserDataService;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ParserDataService $parserDataService)
    {
        $this->parserDataService = $parserDataService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $starPage = $this->argument('starPage');
        $endPage = $this->argument('endPage');
        $categorySearch = $this->argument('category');
        $citySearch = $this->argument('city');
        $chromedriver = $this->argument('chromedriver');

        $this->comment('Парсим категорию ' . $categorySearch . ' в ' . CitiesOlxSlagEnum::CITY[$citySearch] . ' c ' . $starPage . ' по ' . $endPage . ' страницы');

        $this->parserDataService->startParser($starPage, $endPage, $categorySearch, $citySearch, $chromedriver);
        $this->comment('Парсинг закончен');
        return 0;
    }
}
