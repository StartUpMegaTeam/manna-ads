<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ChangePostsView extends Component
{

    public function __construct()
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $display = config('settings.listing.display_mode');

        if ($display == 'make-list') {
            $link = qsUrl(request()->url(), array_merge(request()->except('display'), ['display'=>'grid']), null, false);

        } else if ($display == 'make-grid') {
            $link = qsUrl(request()->url(), array_merge(request()->except('display'), ['display'=>'compact']), null, false);

        } else if ($display == 'make-compact') {
            $link = qsUrl(request()->url(), array_merge(request()->except('display'), ['display'=>'list']), null, false);

        } else {
            $link = qsUrl(request()->url(), array_merge(request()->except('display'), ['display'=>'grid']), null, false);
        }

        return view('components.change-posts-view')
            ->with('link', $link ?? null)
            ->with('currDisplay', $display);
    }
}
