<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CookiesBar extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        if (isset($_COOKIE['isAcceptCookie']))
        {
            return null;
        }

        $cookiesUrl = getUrlPageBySlug('cookies');

        return view('components.cookies-bar')
            ->with('cookiesUrl', $cookiesUrl);
    }
}
