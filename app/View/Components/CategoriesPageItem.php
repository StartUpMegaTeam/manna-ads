<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CategoriesPageItem extends Component
{
    protected $cat;
    protected $subCats;

    public function __construct($cat, $subCats)
    {
        $this->cat = $cat;
        $this->subCats = $subCats;
    }

    public function render()
    {
        $randomId = '-' . substr(uniqid(rand(), true), 5, 5);

        return view('components.categories-page-item')
            ->with('randomId', $randomId)
            ->with('cat', $this->cat)
            ->with('subCats', $this->subCats);
    }
}
