<?php

namespace App\View\Components;

use App\Models\City;
use Illuminate\View\Component;

class LocationModal extends Component
{
    public function __construct()
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        if (isset($_COOKIE['dontShowCityModal']) && isset($_COOKIE['userLocation'])) {
            return null;
        }

        $cityList = City::query()->orderBy('name', 'ASC')->where('active', '=', '1')->pluck('name', 'id');

        return view('components.location-modal')->with('cityList', $cityList);
    }
}
