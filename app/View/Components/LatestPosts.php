<?php

namespace App\View\Components;

use App\Helpers\Search\PostQueries;
use Illuminate\View\Component;

class LatestPosts extends Component
{
    protected $posts;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($posts)
    {
        $this->posts = $posts;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.latest-posts')
            ->with('postsCount', $this->posts->toArray()['total'])
            ->with('posts', $this->posts);
    }
}
