<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PromoButton extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        if (!auth()->check()) {
            $addListingUrl = '#quickLogin';
            $addListingAttr = ' data-bs-toggle="modal"';
        } else {
//            $addListingUrl = \App\Helpers\UrlGen::pricing(); //ссылка на создание премиум объявления
            $addListingUrl = \App\Helpers\UrlGen::addPost(); //ссылка на создание объявления
            $addListingAttr = '';
        }

        return view('components.promo-button')
            ->with('addListingUrl', $addListingUrl ?? '')
            ->with('addListingAttr', $addListingAttr ?? '');
    }
}
