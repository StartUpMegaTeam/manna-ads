<?php

namespace App\View\Components;

use App\Helpers\CitySelect\CitySelectHelper;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class CityFilter extends Component
{

    public function __construct()
    {
    }

    public function render(): View
    {
        [$cityList, $oldValue] = CitySelectHelper::getCityList();

        return view('components.city-filter')
            ->with('oldValue', $oldValue ?? '')
            ->with('options', $cityList);
    }
}
