<?php

namespace App\View\Components;

use App\Helpers\CitySelect\CitySelectHelper;
use Illuminate\View\Component;

class Search extends Component
{

    protected $showSaveSearch;
    protected $count;
    protected $isPostDetails;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($showSaveSearch, $count = 0, $isPostDetails = false)
    {
        $this->showSaveSearch = $showSaveSearch;
        $this->count = $count;
        $this->isPostDetails = $isPostDetails;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        [$cityList, $oldValue] = CitySelectHelper::getCityList();

        if ($this->isPostDetails) {
            $searchUrl = '/search';
        } else {
            $searchUrl = request()->url() == env('APP_URL') ? '/search' : request()->url();
        }
        return view('components.search')
            ->with('cityList', $cityList)
            ->with('oldValue', $oldValue)
            ->with('count', $this->count)
            ->with('searchUrl', $searchUrl)
            ->with('showSaveSearch', $this->showSaveSearch);
    }
}
