<?php

namespace App\View\Components;

use App\Helpers\UrlGen;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class NotFoundPage extends Component
{
    protected $fromPage;

    public function __construct($fromPage)
    {
        $this->fromPage = $fromPage;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View
     */
    public function render(): View
    {
        switch ($this->fromPage) {
            case 'my-posts':
            {
                $data = [
                    'imageUrl' => asset('icon/not-found-icons/my-posts.svg'),
                    'titleText' => t('my_posts_not_found'),
                    'linkText' => t('go_to_create_post_page'),
                    'linkUrl' => UrlGen::addPost(),
                ];
                break;
            }
            case 'archived':
            {
                $data = [
                    'imageUrl' => asset('icon/not-found-icons/archived.svg'),
                    'titleText' => t('archived_posts_not_found'),
                    'linkText' => t('go_to_my_posts'),
                    'linkUrl' => url('account/my-posts'),
                ];
                break;
            }
            case 'favourite':
            {
                $data = [
                    'imageUrl' => asset('icon/not-found-icons/saved-posts.svg'),
                    'titleText' => t('saved_posts_not_found'),
                    'linkText' => t('go_to_posts'),
                    'linkUrl' => url('search'),
                ];
                break;
            }
            case 'saved-search':
            {
                $data = [
                    'imageUrl' => asset('icon/not-found-icons/saved-search.svg'),
                    'titleText' => t('saved_search_not_found'),
                    'linkText' => t('go_to_posts'),
                    'linkUrl' => url('search'),
                ];
                break;
            }
            case 'messages':
            {
                $data = [
                    'imageUrl' => asset('icon/not-found-icons/messages.svg'),
                    'titleText' => t('messages_not_found'),
                    'linkText' => t('go_to_posts'),
                    'linkUrl' => url('search'),
                ];
                break;
            }
            default:
            {
                $data = [
                    'imageUrl' => '',
                    'titleText' => '',
                    'linkText' => '',
                    'linkUrl' => '',
                ];
                break;
            }
        }

        return view('components.account.not-found-page', $data);
    }
}
