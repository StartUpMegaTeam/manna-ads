<?php

namespace App\View\Components;

use App\Helpers\MenuCount;
use App\Helpers\Number;
use App\Models\Category;
use App\Models\Post;
use App\Models\SavedPost;
use App\Models\SavedSearch;
use App\Models\Thread;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Header extends Component
{
    protected $count;
    protected $isPostDetails;
    protected $showSearch;

    public function __construct($count = 0, $isPostDetails = false, $showSearch = true)
    {
        $this->count = $count;
        $this->isPostDetails = $isPostDetails;
        $this->showSearch = $showSearch;
    }

    public function render(): View
    {
        $queryString = (request()->getQueryString() ? ('?' . request()->getQueryString()) : '');

        if (!auth()->check()) {
            return view('components.header.header')
                ->with('categories', Category::whereNull('parent_id')->get())
                ->with('count', $this->count)
                ->with('showSearch', $this->showSearch)
                ->with('isPostDetails', $this->isPostDetails)
                ->with('queryString', $queryString);
        }
        return view('components.header.header')
            ->with('categories', Category::whereNull('parent_id')->get())
            ->with('queryString', $queryString)
            ->with('count', $this->count)
            ->with('showSearch', $this->showSearch)
            ->with('isPostDetails', $this->isPostDetails)
            ->with('countMyPosts', MenuCount::getMyPostsCount())
            ->with('savedPosts', MenuCount::getSavedPostsCount())
            ->with('savedSearch', MenuCount::getSavedSearchCount())
            ->with('archivedPosts', MenuCount::getArchivedPostsCount())
            ->with('notificationCount', MenuCount::getNotificationCount());
    }

}
