<?php

namespace App\View\Components;

use App\Helpers\CitySelect\CitySelectHelper;
use App\Helpers\MenuCount;
use App\Helpers\Number;
use App\Models\Thread;
use Illuminate\Support\Facades\Request;
use Illuminate\View\Component;

class MobileSearch extends Component
{
    protected $count;
    protected $isPostDetails;
    protected $showSearch;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($count = 0, $isPostDetails = false, $showSearch = true)
    {
        $this->count = $count;
        $this->isPostDetails = $isPostDetails;
        $this->showSearch = $showSearch;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        [$cityList, $oldValue] = CitySelectHelper::getCityList();

        $isHomePage = Request::path() == '/';

        if ($this->isPostDetails) {
            $searchUrl = '/search';
        } else {
            $searchUrl = request()->url() == env('APP_URL') ? '/search' : request()->url();
        }

        $withoutSearchUrls =
            [
                '/^login$/ix',
                '/^register$/ix',
                '/^password\/[[:print:]]{0,}$/ix',
                '/^account\/[[:print:]]{0,}$/ix',
                '/^posts\/create[[:print:]]{0,}$/ix',
            ];
        $currentPath = Request::path();

        $showSearch = $this->showSearch;

        if (!$this->showSearch) {
            foreach ($withoutSearchUrls as $item) {
                if (preg_match($item, $currentPath)) {
                    $showSearch = false;
                    break;
                }
            }
        }

        $showComponent = !preg_match('/^account\/messages\/[[:print:]]{0,}$/ix', $currentPath);

        return view('components.mobile-search')
            ->with('cityList', $cityList)
            ->with('oldValue', (int)$oldValue)
            ->with('count', $this->count)
            ->with('showComponent', $showComponent)
            ->with('searchUrl', $searchUrl)
            ->with('showSearch', $showSearch)
            ->with('isHomePage', $isHomePage)
            ->with('countMyPosts', MenuCount::getMyPostsCount())
            ->with('savedPosts', MenuCount::getSavedPostsCount())
            ->with('savedSearch', MenuCount::getSavedSearchCount())
            ->with('archivedPosts', MenuCount::getArchivedPostsCount())
            ->with('notificationCount', MenuCount::getNotificationCount());
    }
}
