<?php

namespace App\View\Components;

use App\Helpers\ArrayHelper;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class SearchFilter extends Component
{

    public function __construct()
    {
        //
    }

    public function render()
    {
        $oldValue = request()->get('orderBy') ?? null;
        $options = [
            [
                'value' => 'priceAsc',
                'label' => t('price_low_to_high'),
            ],
            [
                'value' => 'priceDesc',
                'label' => t('price_high_to_low'),
            ],
            [
                'value' => 'date',
                'label' => t('Date'),
            ],
        ];

        $key = false;

        if (!is_null($oldValue)){
            $key = ArrayHelper::findKeyByValue($oldValue, $options, 'value');
        }

        $label = $key !== false ? mb_strimwidth($options[$key]['label'], 0, 15, '...') : t('Sort');

        return view('components.search-filter')
            ->with('options', $options)
            ->with('key', $key ?? false)
            ->with('oldValue', $oldValue)
            ->with('label', $label);
    }
}
