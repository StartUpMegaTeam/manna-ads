<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ErrorPage extends Component
{
    protected $errorCode;
    protected $errorMessage;
    protected $exception;

    public function __construct($errorCode, $errorMessage, $exception)
    {
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
        $this->exception = $exception;
    }

    public function render()
    {

        if (env('APP_ENV') == 'local' && isset($this->exception)) {
            if (is_string($this->exception)) {
                $this->errorMessage = $this->exception;
            } else {
                $this->errorMessage = ($this->exception->getMessage()) ? $this->exception->getMessage() : $this->errorMessage;
            }
        }

        return view('components.error-page')
            ->with('errorCode', $this->errorCode)
            ->with('errorMessage', $this->errorMessage);
    }
}
