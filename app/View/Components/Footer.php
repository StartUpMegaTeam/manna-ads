<?php

namespace App\View\Components;

use App\Models\Page;
use Illuminate\Support\Facades\Request;
use Illuminate\View\Component;

class Footer extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $withAddPostBtnUrls =
            [
                '/^\/$/ix',
                '/^search/ix',
                '/^search\/[[:print:]]{0,}/ix',
                '/^tag\/[[:print:]]{0,}/ix',
            ];
        $currentPath = Request::path();

        $showAddPostBtn = false;

        foreach ($withAddPostBtnUrls as $item) {
            if (preg_match($item, $currentPath)) {
                $showAddPostBtn = true;
                break;
            }
        }

        $withoutMobileFooterUrls =
            [
                '/^account\/messages\/[[:print:]]{0,}$/ix',
            ];

        $hideFooterOnMobile = false;

        foreach ($withoutMobileFooterUrls as $item) {
            if (preg_match($item, $currentPath)) {
                $hideFooterOnMobile = true;
                break;
            }
        }

        return view('components.footer.footer')
            ->with('pages', Page::where('active', '=', '1')->get())
            ->with('showAddPostBtn', $showAddPostBtn)
            ->with('hideFooterOnMobile', $hideFooterOnMobile);
    }
}
