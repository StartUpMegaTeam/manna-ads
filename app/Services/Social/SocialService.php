<?php

namespace App\Services\Social;

use App\Helpers\Ip;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class SocialService
{


    public function __construct()
    {
    }

    public function socialAuth($name, $email, $network)
    {
        if ($user = User::where('email', '=', $email)->first()) {

            return $this->login($user, $network);
        }

        $randomPassword = getRandomPassword(8);

        $countryCode = config('country.code', config('ipCountry.code'));

        // Register the User (As New User)
        $userInfo = [
            'country_code'   => $countryCode,
            'language_code'  => config('app.locale'),
            'name'           => $name,
            'email'          => $email,
            'password'       => Hash::make($randomPassword),
            'ip_addr'        => Ip::get(),
            'verified_email' => 1,
            'verified_phone' => 1,
            'provider'       => null,
            'provider_id'    => null,
            'created_at'     => date('Y-m-d H:i:s'),
        ];
        $user = new User($userInfo);
        $user->save();

        return $this->login($user, $network);
    }

    public function login($user, $network)
    {
        if (auth()->loginUsingId($user->id)) {
            $user->tokens()->delete();

            $deviceName = ucfirst($network);
            $token = $user->createToken($deviceName);

            session()->put('authToken', $token->plainTextToken);

            return $user;
        } else {
            return null;
        }
    }
}