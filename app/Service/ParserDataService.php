<?php

namespace App\Service;

use App\Enums\CategoryOlxUrlEnum;
use App\Enums\ChromeDriverlEnum;
use App\Enums\CitiesOlxSlagEnum;
use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;
use Facebook\WebDriver\Exception\UnknownErrorException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Throwable;

class ParserDataService
{

    protected const WB_POST = 'https://www.olx.ua/d/obyavlenie/';
    protected const SORT_BY = '/?currency=UAH&search%5Border%5D=created_at%3Adesc&page=';

    /**
     * @throws UnknownErrorException
     * @throws NoSuchElementException
     * @throws TimeoutException
     */
    public function startParser($starPage, $endPage, $categorySearch, $citySearch, $chromedriver)
    {
        putenv(ChromeDriverlEnum::URL[$chromedriver]);

        $chromeOptions = new ChromeOptions();
//
//        $url = '127.0.0.1';
//        $portsProxy = [
//            '8080',
//            '8081',
//        ];
//
//        $portsProxyCount = count($portsProxy);
//        $chromeOptions->addArguments(["--proxy-server=http://" . $url . ":" . $portsProxy[0]]);
        $capabilities = DesiredCapabilities::chrome();
        $capabilities->setCapability(ChromeOptions::CAPABILITY_W3C, $chromeOptions);
        $driver = ChromeDriver::start($capabilities);

        $countPostVisit = null;
        $countPostVisitSum = null;
        $indexProxyPort = 0;
        sleep(3);

        foreach (CategoryOlxUrlEnum::URL as $categoryUrl) {

            foreach (CitiesOlxSlagEnum::CITY as $cityUrl) {

                for ($page = 0; $page <= 2; $page++) {
                    $elementsPath = [];

                    $driver->get(CategoryOlxUrlEnum::URL[11] . CitiesOlxSlagEnum::CITY[2] . self::SORT_BY . $page);
                    sleep(3);

                    $elements = $driver->findElements(WebDriverBy::className('css-rc5s2u'));

                    foreach ($elements as $element) {

                        $elementsPath[] = $element->getAttribute('href');

                    }

                    foreach ($elementsPath as $elementPath) {
                        $detailsText = [];
                        $categoriesText = [];
                        $photoUrl = [];

                        $countPostVisit++;
                        $countPostVisitSum++;

                        if ($countPostVisit >= 10) {
                            $countPostVisit = 0;
                            $indexProxyPort++;

//                            if ($indexProxyPort >= $portsProxyCount) {
//                                $indexProxyPort = 0;
//                                sleep(10);
//                            }

                            $driver->quit();

                            $chromeOptions = new ChromeOptions();
                            putenv(ChromeDriverlEnum::URL[$chromedriver]);

//                            $proxy = $portsProxy[$indexProxyPort];
//                            $chromeOptions->addArguments(["--proxy-server=http://" . $url . ":" . $proxy]);
                            $capabilities = DesiredCapabilities::chrome();
                            $capabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

                            $driver = ChromeDriver::start($capabilities);
                        }

                        $driver->get(self::WB_POST . $elementPath);
                        sleep(10);

                        try {

                            $driver->findElement(WebDriverBy::className('css-65ydbw-BaseStyles'))->click();
                            sleep(10);

                            $name = $driver->findElement(WebDriverBy::className('css-1n149dy-TextStyled'))->getText();
                            $phone = $driver->findElement(WebDriverBy::className('css-v1ndtc'))->getText();
                            $categories = $driver->findElements(WebDriverBy::className('css-7dfllt'));
                            $title = $driver->findElement(WebDriverBy::className('css-swd4zc-TextStyled'))->getText();
                            $descriptions = $driver->findElement(WebDriverBy::className('css-12l22jb-TextStyled'))->getText();

//                            $price = $driver->findElement(WebDriverBy::className('css-okktvh-Text'))->getText();
                            $city = $driver->findElement(WebDriverBy::className('css-1g8wrcn-TextStyled'))->getText();
                            $details = $driver->findElements(WebDriverBy::className('css-65jx20-TextStyled'));

                            $photos = $driver->findElements(WebDriverBy::className('css-1bmvjcs'));

                            for ($i = 0; $i <= count($photos) && $i < 4; $i++) {
                                $driver->findElement(WebDriverBy::className('swiper-button-next'))->click();
                                sleep(1);
                            }

                            foreach ($photos as $photo) {
                                $photoUrl[] = $photo->getAttribute('src');
                            }

                            foreach ($categories as $category) {
                                $categoriesText[] = $category->getText();
                            }


                            foreach ($categories as $category) {
                                $ca = $category->getText();
                                var_dump($ca);
                            }

                            foreach ($details as $detail) {
                                $detailsText[] = $detail->getText();
                            }

//
                            $res = Http::asForm()->post("http://manna-ads.lcl/api/parser", [
                                    "name" => $name,
                                    "phone" => $phone,
                                    "categories" => $categoriesText,
                                    "title" => $title,
                                    "descriptions" => $descriptions,
                                    "price" => '',
                                    "city" => $city,
                                    "details" => $detailsText,
                                    "photoUrl" => $photoUrl,
                                    "isPhotoUrl" => true,

                                ]
                            );

//                            $res = Http::withHeaders([
//                                'X-AppApiToken' => 'F3iRjcS6ep2HEnA7d7mPD1WCRNI1glWtL7QRD4ga',
//                            ])->post("https://manna-board.ru/api/parser", [
//                                    "name" => $name,
//                                    "phone" => $phone,
//                                    "categories" => $categoriesText,
//                                    "title" => $title,
//                                    "descriptions" => $descriptions,
//                                    "price" => '',
//                                    "city" => $city,
//                                    "details" => $detailsText,
////                                    "photoUrl" => $photoUrl,
//                                    "isPhotoUrl" => false,
//
//                                ]
//                            );

                            var_dump(json_decode($res->body()));
                        } catch (\Exception|Throwable $exception) {
                            DB::rollBack();
                        }
                    }
                }
            }
        }

        $driver->quit();
        $this->startParser($starPage, $endPage, $categorySearch, $citySearch, $chromedriver);
    }


}
