<?php

namespace App\Http\Resources\Response;

use Illuminate\Contracts\Support\Arrayable;


class JsonResponse implements Arrayable
{

    private $data;
    private $message;

    public function __construct(string $message, array $data = [])
    {
        $this->data = $data;
        $this->message = $message;
    }

    public function toArray(): array
    {
        return array_merge(
            [
                'message' => $this->message,
            ],
            $this->data ? ['data' => $this->data] : [],
        );
    }


}