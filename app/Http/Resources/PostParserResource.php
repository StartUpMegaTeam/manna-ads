<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostParserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            "contact_name" => $this->resource->contact_name,
            "user_id" => $this->resource->user_id,
            "phone" => $this->resource->phone,
            'title' => $this->resource->title,
            'description' => $this->resource->description,
        ];

    }
}
