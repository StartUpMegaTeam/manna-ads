<?php

namespace App\Http\Requests;

class UserLocationRequest extends Request
{
    public function rules()
    {
        return [
            'latitude' => ['required', 'numeric', 'between:-90,90',],
            'longitude' => ['required', 'numeric', 'between:-180,180',],
            'distance' => ['required', 'numeric', 'min:1']
        ];
    }

    public function getLatitude()
    {
        return $this->input('latitude');
    }

    public function getLongitude()
    {
        return $this->input('longitude');
    }

    public function getDistance()
    {
        return $this->input('distance');
    }
}