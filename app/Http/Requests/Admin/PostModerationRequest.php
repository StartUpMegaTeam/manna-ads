<?php

namespace App\Http\Requests\Admin;

use App\Enums\PostStatusEnum;
use Illuminate\Validation\Rule;

class PostModerationRequest extends Request
{
    public function prepareForValidation()
    {
        $input = $this->all();

        if ($this->input('status') == 0) {
            $input['status'] = PostStatusEnum::PUBLISHED;
        } else {
            $input['status'] = PostStatusEnum::REJECTED;
            $input['reject_id'] = (int)$this->input('status');
        }

        request()->merge($input);
        $this->merge($input);
    }

    public function rules()
    {
        return [
            'status' => ['required', Rule::in(PostStatusEnum::STATUS_LIST)],
        ];
    }
}