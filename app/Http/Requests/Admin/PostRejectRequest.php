<?php

namespace App\Http\Requests\Admin;

class PostRejectRequest extends Request
{
    public function rules()
    {
        return [
            'title' => ['required', 'min:1', 'max:255'],
            'description' => ['required', 'min:1', 'max:255'],
        ];
    }
}