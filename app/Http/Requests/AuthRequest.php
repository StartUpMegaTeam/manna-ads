<?php
/**
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Http\Requests;

use App\Rules\EmailRule;

class AuthRequest extends Request
{
	/**
	 * Prepare the data for validation.
	 *
	 * @return void
	 */
	protected function prepareForValidation()
	{
		// Don't apply this to the Admin Panel
		if (isFromAdminPanel()) {
			return;
		}

        $input = $this->all();

        // login (phone)
        if ($this->filled('login')) {
            $loginField = getLoginField($this->input('login'));
            if ($loginField == 'phone') {
                $input['login'] = $this->input('login');
            }
        }

        request()->merge($input); // Required!
        $this->merge($input);
	}
	
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'login' => ['required','email', new EmailRule()],
        ];

        // CAPTCHA
		$rules = $this->captchaRules($rules);
        
        return $rules;
    }
}
