<?php

namespace App\Http\Requests;

class SetUserLocationRequest extends Request
{
    public function rules()
    {
        return [
            'city_id' => ['required', 'exists:cities,id'],
            'user_id' => ['required', 'exists:users,id'],
        ];
    }

    public function getCityId(): int
    {
        return (int)$this->input('city_id');
    }

    public function getUserId(): int
    {
        return (int)$this->input('user_id');
    }
}