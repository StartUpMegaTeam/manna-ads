<?php

namespace App\Http\Requests;

class ForgotPasswordRequest extends Request
{
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        // Don't apply this to the Admin Panel
        if (isFromAdminPanel()) {
            return;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'login' => ['required', 'regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',],
        ];

        // CAPTCHA
        $rules = $this->captchaRules($rules);

        return $rules;
    }
}
