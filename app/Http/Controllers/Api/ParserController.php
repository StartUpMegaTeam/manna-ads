<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Files\Storage\StorageDisk;
use App\Http\Resources\PostParserResource;
use App\Http\Resources\Response\JsonResponse;
use App\Models\Category;
use App\Models\City;
use App\Models\Field;
use App\Models\FieldOption;
use App\Models\Picture;
use App\Models\Post;
use App\Models\PostValue;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Throwable;


class ParserController extends BaseController
{
    const CURRENCY_GRV = 1.63;
    const CURRENCY_DOLLAR = 60.42;

    protected $field = [
        //Авто
        'Год выпуска' => 'Год выпуска',
        'Тип автомобиля' => 'Тип автомобиля',
        'Коробка передач' => 'Коробка передач',
        'Тип кузова' => 'Тип кузова',
        'Вид топлива' => 'Тип двигателя',
        'Пробег' => 'Пробег, км',
        'Объем двигателя' => 'Объем двигателя, л',
        'Тип привода' => 'Привод',
        'Мощность' => 'Мощность, л.с.',

        //Недвижемость
        'Этаж' => 'Этаж',
        'Общая площадь' => 'Общая площадь, м²',
        'Количество комнат' => 'Количество комнат',
        'Ремонт' => 'Ремонт ',
        'Площадь кухни' => 'Площадь кухни, м²',
        'Санузел' => 'Санузел',
        'Этажность' => 'Этажей в доме',
        'Бытовая техника' => 'Техника',

    ];

    protected $fieldOptionsArray = [
        //Авто
        'Газ / бензин' => 'Бензин',
        'Внедорожник / Кроссовер' => 'Внедорожник',
        'Ручная / Механика' => 'Механика',

        //Недвижемость
        'Изолированный' => 'Раздельный',
        'Смежный' => 'Совмещенный',
    ];

    protected $extraInformation = [
        'тис.км.',
        'тыс.км.',
        'л.',
        'к.с.',
        'комнаты',
        'комната',
        'комнат',
        'м²',
        'с.',
        'км',
        'кг',
        'см³'
    ];

    protected $categoryArray = [
        //Недвижемость
        'Посуточная аренда жилья' => 2211,
        'Долгосрочная аренда квартир' => 2210,
        'Квартиры посуточно, почасово' => 2211,
        'Продажа квартир' => 172,
        'Аренда коммерческой недвижимости' => 188,
        'Продажа коммерческой недвижимости' => 187,
        'Долгосрочная аренда домов' => 182,
        'Продажа домов' => 181,
        'Продажа гаражей, парковок' => 185,
        'Аренда гаражей, парковок' => 186,
        'Продажа земли' => 183,
        'Продажа комнат' => 179,

        //Авто
        'Грузовые автомобили' => 2250,

        //Грузовики и спец.техника
        'Навесное и дополнительное оборудование' => 2253,

        //Водный транспорт
        'Гребная лодка' => 2215,

        //'Катер' => 'Катера и яхты',

        //Одежда и обувь
        'Цепочки' => 2060,
        'Мужское белье и плавки' => 1678,
        'Макияж' => 2044,

        //Работа
        'Работа' => 148,
        'Колл-центры / Телекоммуникации' => 1553,
        'IT / компьютеры' => 1553,
        'СТО / автомойки' => 1554,
        'Административный персонал / HR / Секретариат' => 1555,
        'Бухгалтерия' => 1558,
        'Домашний персонал' => 1561,
        'Клининг Домашний персонал' => 1561,
        'Клининг' => 1561,
        'Клининг домашний персонал' => 1561,
        'Посудомойщица' => 1561,
        'Производство' => 2035,
        'Логистика' => 2039,
        'Водитель' => 2039,
        'Логистика / Склад / Доставка' => 2039,
        'Кладовщик' => 2039,
        'Розничная торговля / продажи / закупки' => 2032,
        'Супервайзер' => 2032,
        'Производство / рабочие специальности' => 2035,
        'Производство рабочие' => 2035,
        'Строительство / отделочные работы' => 2037,
        'Отельно-ресторанный бизнес / Туризм' => 2040,
        'Реклама / дизайн / PR' => 2031,
        'Образование / перевод' => 2033,
        'Красота / фитнес / спорт' => 2042,

        //Электроника
        'Акустические системы' => 2133,
        'Техника для кухни' => 1789,
        'Холодильники' => 1948,
        'Телевизоры' => 2143,
        'Мобильные телефоны смартфоны' => 2120,
        'Плиты печи' => 1946,
        'Планшеты эл. книги и аксессуары' => 2118,
        'Медиа проигрыватели' => 2135,
        'Ноутбуки и аксессуары' => 2115,
        'Телефоны и аксессуары' => 2120,
        'Компьютеры и комплектующие' => 2116,

        //Детский мир
        'Одежда для мальчиков' => 2070,
        'Одежда для девочек' => 2069,
//        'Прочие детские товары'=>,

        //Животные
        'Зоотовары' => 2102,

        //Услуги
        'Бизнес и услуги' => 136,
        'Строительство / ремонт / уборка' => 136,
        'Окна / двери / балконы' => 1607,
        'Сантехника / коммуникации' => 1609,
        'Сантехника' => 1609,
        'Cтроительные услуги' => 136,
        'Отделка / ремонт' => 1607,
        'Ремонт и обслуживание техники' => 1582,
        'Климатическая техника' => 1582,
        'Бытовая техника' => 1582,
        'Оборудование' => 1584,
        'Красота / здоровье' => 1581,
        'Маникюр / наращивание ногтей' => 1623,
        'Образование / Спорт' => 1585,
        'Юридические услуги' => 1586,
        'Развлечения / Искусство / Фото / Видео' => 1593,
        'Реклама / полиграфия / маркетинг / интернет' => 1588,
        'Перевозки / транспортные услуги' => 1598,
        'Грузоперевозки' => 1598,
        'Аренда спец.техники' => 1605,
        'Аренда самосвалов' => 1605,
    ];

    protected $categoryArrayAuto = [

        //ВАЗ
        '2101' => 1471,
        '2102' => 1471,
        '2103' => 1471,
        '2104' => 1471,
        '2105' => 1471,
        '2106' => 1471,
        '2107' => 1471,
        '2108' => 1472,
        '2109' => 1472,
        '21099' => 1472,
        '2110' => 1473,
        '2111' => 1473,
        '2112' => 1473,

        '2113' => 1474,
        '2114' => 1474,
        '2115' => 1474,

        '1119 Kalina' => 1488,

        //Lexus
        'nx' => 949,
        'RX серія' => 952,

        //Toyta
        'RAV 4' => 1455,

        //Volkswagen
        'Polo' => 1515,
        'Jetta' => 1507,
        'Passat Sedan' => 1511,

        //Ford
        'Focus' => 656,
        'Festiva' => 653,

        //BMW
        '1 серия' => 304,
        '2 серия' => 305,
        '3 серия' => 306,
        '4 серия' => 307,
        '5 серия' => 308,
        '6 серия' => 309,
        '7 серия' => 310,
        '8 серия' => 311,

        //Ауди
        'Q3' => 259,

        //Nissan
        'Almera Classic' => 1137,
        'X-Terra' => 'XTerra',

        //Scoda
        'Kodiaq' => 1350,

        //Geely

        //Opel

        //Renault
        'Scenic II' => 1285,

        //Chevrolet
        'Tracker' => 454,

    ];

    protected $categoryModaArray = [
        'Пуховики и зимние куртки' => 'Зимние куртки и пуховики',
        'Блузы и рубашки' => 'Рубашки и блузки',
        'Рубашки' => 'Рубашки',
        'Платья' => 'Платья и юбки',
        'Юбки' => 'Платья и юбки',
        'Бюстгальтеры' => 'Нижнее бельё',
        'Угги' => 'Угги, валенки, дутики',
        'Пальто' => 'Пальто',
        'Брюки' => 'Брюки',
        'Сумки' => 'Сумки',
        'Джинсы' => 'Джинсы',
        'Cапоги' => 'Сапоги',
        'Шубы и дубленки' => 'Дублёнки и шубы',
        'Шубы, полушубки' => 'Дублёнки и шубы',
        'Костюмы и пиджаки' => 'Пиджаки и костюмы',
        'Полусапожки' => 'Полусапоги',
        'Сапоги, полусапожки' => 'Сапоги и полусапоги',
        'Ботинки и полуботинки' => 'Ботинки и полуботинки',
        'Кроссовки' => 'Кроссовки и кеды',
        'Босоножки' => 'Босоножки',
        'Туфли' => 'Туфли',
        'Ботинки' => 'Ботинки и полуботинки',
        'Шорты' => 'Другое',
        'Женское белье и купальники' => 'Купальники',
        'Купальники' => 'Купальники',
    ];

    protected $categoryModaParentArray = [
        'Женская одежда' => 1676,
        'Женская обувь' => 1677,
        'Мужская одежда' => 1678,
        'Мужская обувь' => 1679,
    ];

    protected $categoryIgnore = [
        'Другое',
        'другое',
        'Аксессуары',
        'аксессуары',
        'Бытовая техника',
    ];

    protected $detailIgnore = [
        '1 успешная доставка',
        '2 успешных доставки',
        '3 успешных доставки',
        '4 успешных доставки',
        '5+ успешных доставок',
        '10+ успешных доставок',
        '100+ успешных доставок',
        'успешная доставка',
        'успешных доставки',
        'Что такое рейтинг?',
        'Все объявления автора',
        'Донецкая область',
        'Луганская область',
        'Бизнес',
        'Частное лицо',
        'Укрпошта - бесплатно',
        'Нова Пошта - от 70 грн.',
        'Meest - бесплатно',

        //Авто

    ];

    protected function createPost(Request $request)
    {
        if (env('PARSE_ENABLE') == true) {
            return response()->json(new JsonResponse(
                'Доступ закрыт',
            ));
        }

        $data = $request->request->all();
        $faker = Faker::create();

        unset($data['categories'][0]);

        try {
            DB::beginTransaction();

            if ($this->postExists($data, $data['details'])) {
                return response()->json(new JsonResponse(
                    'ОбЪявление не добавлено. Объявление уже существует ',
                ));
            }

            $post = new Post();
            $post->country_code = 'RU';
            $post->user_id = $this->createUser($data);
            $post->category_id = $this->getCategory($data['categories'], $data['details']);
            $post->post_type_id = NULL;
            $post->title = $data['title'];
            $post->description = $data['descriptions'];
            $post->tags = $data['categories'];
            $post->price = $this->getPrice($data['price']);
            $post->contact_name = $data['name'];
            $post->email = 'OLX' . $faker->email();
            $post->phone = $this->getPhone($data['phone']);
            $post->city_id = $this->getCity($data['city']);
            $post->verified_email = 1;
            $post->save();
            $this->postValue($post, $data['details']);
            $i = null;

            if ($data['isPhotoUrl'] == true) {
                foreach ($data['photoUrl'] as $url) {
                    if ($url != null && $i <= 4) {
                        $i++;
                        $picture = new Picture();
                        $picture->post_id = $post->id;
                        $this->photo($picture, $url, $post);
                        $picture->position = $i;
                        $picture->save();
                    }
                }
            }

            DB::commit();

            return response()->json(new JsonResponse(
                'ОбЪявление добавлено',
                (new PostParserResource($post))->toArray($request)
            ));

        } catch (Throwable $ex) {
            DB::rollBack();
            return response()->json(new JsonResponse(
                'ОбЪявление не добавлено : ' . $ex->getMessage(),
            ));
        }
    }

    protected function photo(Picture $picture, $url, Post $post): Picture
    {
        $file = file_get_contents($url);
        $destination_path = 'files/' . strtolower($post->country_code) . '/' . $post->id . '/' . md5($file . time()) . '.jpg';

        StorageDisk::getDisk()->put($destination_path, $file);

        $picture->filename = $destination_path;
        $picture->mime_type = 'image/jpg';

        return $picture;
    }

    protected function createUser($userData): int
    {
        $facer = Faker::create();

        if ($user = User::where('phone', $userData['phone'])->where('is_olx', true)->first()) {
            return $user->id;
        }

        $user = new User();
        $user->country_code = "RU";
        $user->language_code = NULL;
        $user->user_type_id = NULL;
        $user->gender_id = 3;
        $user->name = $userData['name'];
        $user->photo = NULL;
        $user->phone = $this->getPhone($userData['phone']);
        $user->email = "OLX" . $facer->email();
        $user->is_olx = true;
        $user->password = Hash::make($facer->password(10, 20));
        $user->verified_email = 1;

        $user->save();

        return $user->id;
    }

    protected function getCity($city)
    {
        $cityName = explode(',', $city);
        $cityModel = City::whereName('{"ru":"' . $cityName[0] . '"}')->first();

        if ($cityName) {
            return $cityModel->id;
        }

        return NULL;
    }

    protected function getCategory($categories, $details)
    {
        foreach ($details as $detail) {
            $detailArr = explode(':', $detail);

            if ($detailArr[0] == 'Модель') {
                $detailArr[1] = trim($detailArr[1]);

                if (array_key_exists($detailArr[1], $this->categoryArrayAuto)) {
                    return $this->categoryArrayAuto[$detailArr[1]];
                } else if ($category = Category::whereName('{"ru":"' . $detailArr[1] . '"}')->first()) {
                    return $category->id;
                }
            }
        }

        $categoriesCount = count($categories);

        for ($count = $categoriesCount - 1; $count >= 0; $count--) {

            if (array_key_exists($categories[$count], $this->categoryModaArray)) {

                $moda = $this->categoryModaArray[$categories[$count]];

                for ($countModa = $categoriesCount - 1; $countModa >= 0; $countModa--) {

                    if (array_key_exists($categories[$countModa], $this->categoryModaParentArray)) {
                        $modaParent = $this->categoryModaParentArray[$categories[$countModa]];

                        if (!in_array($moda, $this->categoryIgnore)) {
                            if ($category = Category::whereName('{"ru":"' . $moda . '"}')->where('parent_id', $modaParent)->first()) {
                                return $category->id;
                            }
                        }
                    }
                }

            } else if (array_key_exists($categories[$count], $this->categoryArray)) {
                return $this->categoryArray[$categories[$count]];

            } else if ($category = Category::whereName('{"ru":"' . $categories[$count] . '"}')->first()) {

                if (!in_array($category->name, $this->categoryIgnore)) {
                    $categoryName = $categories[$count];

                    return Category::whereName('{"ru":"' . $categoryName . '"}')->first()->id;
                }
            }
        }

        return false;
    }

    protected function postExists($data, $details)
    {
        $descDetail = '';

        foreach ($details as $detail) {
            $detailArr = explode(':', $detail);

            if (array_key_exists($detailArr[0], $this->field)) {
                $detailArr[0] = $this->field[$detailArr[0]];
                $detailArr[1] = str_replace($this->extraInformation, '', $detailArr[1]);
                $detailArr[1] = trim($detailArr[1]);
            }

            if (Field::whereName('{"ru":"' . $detailArr[0] . '"}')->first()) {
                $a = '';
            } else {
                if (!in_array($detail, $this->detailIgnore)) {
                    $descDetail .= "\r\n" . $detail;
                }
            }
        }

        if ($descDetail != '') {
            $descriptions = $data['descriptions'] . "\r\n" . $descDetail;
        } else {
            $descriptions = $data['descriptions'];
        }

        $post = Post::where('description', $descriptions)->first();

        return !($post == '');
    }

    protected function getPrice($price)
    {
        $priceArr = explode(' ', $price);
        $priceCountElement = count($priceArr) - 1;

        if ($priceArr[$priceCountElement] == 'грн.') {
            unset($priceArr[$priceCountElement]);
            $price = implode($priceArr);

            return ceil($price * self::CURRENCY_GRV);

        } else if ($priceArr[$priceCountElement] == '$') {
            unset($priceArr[$priceCountElement]);
            $price = implode($priceArr);

            return ceil($price * self::CURRENCY_DOLLAR);

        } else {
            unset($priceArr[$priceCountElement]);
            $price = implode($priceArr);

            return ceil($price);
        }

    }

    protected function postValue(Post $post, $details): bool
    {
        $descDetail = '';

        foreach ($details as $detail) {
            $detailArr = explode(':', $detail);

            if (array_key_exists($detailArr[0], $this->field)) {
                $detailArr[0] = $this->field[$detailArr[0]];
                $detailArr[1] = str_replace($this->extraInformation, '', $detailArr[1]);
                $detailArr[1] = trim($detailArr[1]);
            }

            if ($field = Field::whereName('{"ru":"' . $detailArr[0] . '"}')->first()) {

                if (!PostValue::wherePostId($post->id)->where('field_id', $field->id)->first()) {
                    $postValue = new PostValue();
                    $postValue->post_id = $post->id;
                    $postValue->field_id = $field->id;

                    if (array_key_exists($detailArr[1], $this->fieldOptionsArray)) {
                        $detailArr[1] = $this->fieldOptionsArray[$detailArr[1]];
                    }

                    if ($fieldOptions = FieldOption::whereValue('{"ru":"' . $detailArr[1] . '"}')->first()) {
                        $postValue->option_id = $fieldOptions->id;
                        $postValue->value = $fieldOptions->id;

                    } else {
                        $postValue->value = $detailArr[1];
                    }

                    $postValue->save();
                }
            } else {
                if (!in_array($detail, $this->detailIgnore)) {
                    $descDetail .= "\r\n" . $detail;
                }
            }
        }

        if ($descDetail != '') {
            $post->description = $post->description . "\r\n" . $descDetail;
            $post->save();
        }

        return true;
    }

    protected function getPhone($phone)
    {
        $phone = str_replace([' '], '', $phone);
        $phone = str_replace(['-'], '', $phone);

        if (iconv_strlen($phone) == 10) {
            return '+38' . $phone;
        }
        return $phone;
    }
}