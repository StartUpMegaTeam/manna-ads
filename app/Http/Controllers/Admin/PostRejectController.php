<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PostRejectRequest as StoreRequest;
use App\Http\Requests\Admin\PostRejectRequest as UpdateRequest;
use Larapen\Admin\app\Http\Controllers\PanelController;

class PostRejectController extends PanelController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->xPanel->setModel('App\Models\PostReject');
        $this->xPanel->setRoute(admin_uri('post-rejects'));
        $this->xPanel->setEntityNameStrings('Причину', 'Причины');

        $this->xPanel->addButtonFromModelFunction('top', 'bulk_delete_btn', 'bulkDeleteBtn', 'end');

        // Filters
        // -----------------------
        $this->xPanel->disableSearchBar();

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
        */
        // COLUMNS
        $this->xPanel->addColumn([
            'name' => 'id',
            'label' => '',
            'type' => 'checkbox',
            'orderable' => false,
        ]);
        $this->xPanel->addColumn([
            'name' => 'title',
            'label' => 'Заголовок',
        ]);
        $this->xPanel->addColumn([
            'name' => 'description',
            'label' => 'Описание',
        ]);

        // FIELDS
        $this->xPanel->addField([
            'name' => 'title',
            'label' => 'Заголовок',
            'type' => 'text',
            'attributes' => [
                'placeholder' => 'Заголовок',
            ],
        ]);
        $this->xPanel->addField([
            'name' => 'description',
            'label' => 'Описание',
            'type' => 'text',
            'attributes' => [
                'placeholder' => 'Описание',
            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}