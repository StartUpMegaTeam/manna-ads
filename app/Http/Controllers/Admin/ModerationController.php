<?php

namespace App\Http\Controllers\Admin;

use App\Enums\PostStatusEnum;
use App\Http\Requests\Admin\PostModerationRequest as UpdateRequest;
use App\Models\Post;
use App\Models\PostReject;
use Illuminate\Support\Facades\Cache;
use Larapen\Admin\app\Http\Controllers\PanelController;

class ModerationController extends PanelController
{
    private $cacheExpiration = 3600;

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->xPanel->setModel('App\Models\Post');
        $this->xPanel->with(['pictures', 'user', 'city']);
        $this->xPanel->addClause('where', 'status', '=', PostStatusEnum::MODERATION);

        $this->xPanel->setRoute(admin_uri('posts-moderation'));

        $this->xPanel->setEntityNameStrings('Объявление', 'Объявления на модерации');

        $this->xPanel->orderBy('created_at', 'DESC');

        $this->xPanel->disableSearchBar();
        /*
		|--------------------------------------------------------------------------
		| COLUMNS AND FIELDS
		|--------------------------------------------------------------------------
		*/
        $this->xPanel->addColumn([
            'name' => 'id',
            'label' => '',
            'type' => 'checkbox',
            'orderable' => false,
        ]);
        $this->xPanel->addColumn([
            'name'  => 'created_at',
            'label' => trans('admin.Date'),
            'type'  => 'datetime',
        ]);
        $this->xPanel->addColumn([
            'name'          => 'title',
            'label'         => mb_ucfirst(trans('admin.title')),
            'type'          => 'model_function',
            'function_name' => 'getTitleHtml',
        ]);
        $this->xPanel->addColumn([
            'name'          => 'price',
            'label'         => trans('admin.Main Picture'),
            'type'          => 'model_function',
            'function_name' => 'getPictureHtml',
        ]);
        $this->xPanel->addColumn([
            'name'          => 'contact_name',
            'label'         => trans('admin.User Name'),
            'type'          => 'model_function',
            'function_name' => 'getUserNameHtml',
        ]);
        $this->xPanel->addColumn([
            'name'          => 'city_id',
            'label'         => mb_ucfirst(trans('admin.city')),
            'type'          => 'model_function',
            'function_name' => 'getCityHtml',
        ]);

        // FIELDS
        $this->xPanel->addField([
            'name'       => 'title',
            'label'      => mb_ucfirst(trans('admin.title')),
            'type'       => 'text',
            'attributes' => [
                'placeholder' => mb_ucfirst(trans('admin.title')),
            ],
            'disable'    => true,
        ]);
        $this->xPanel->addField([
            'name'       => 'description',
            'label'      => trans('admin.Description'),
            'type'       => 'textarea',
            'attributes' => [
                'placeholder' => trans('admin.Description'),
                'id'          => 'description',
                'rows'        => 10,
            ],
            'disable'    => true,
        ]);
        $this->xPanel->addField([
            'label'     => mb_ucfirst(trans('admin.pictures')),
            'name'      => 'pictures', // Entity method
            'entity'    => 'pictures', // Entity method
            'attribute' => 'filename',
            'type'      => 'read_images',
            'disk'      => 'public',
            'disable'    => true,
        ]);
        $this->xPanel->addField([
            'name'              => 'contact_name',
            'label'             => trans('admin.User Name'),
            'type'              => 'text',
            'attributes'        => [
                'placeholder' => trans('admin.User Name'),
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-6',
            ],
            'disable'    => true,
        ]);
        $entity = $this->xPanel->getModel()->find(request()->segment(3));
        $tags = (!empty($entity) && isset($entity->tags)) ? (array)$entity->tags : [];
        $this->xPanel->addField([
            'name'              => 'tags',
            'label'             => trans('admin.Tags'),
            'type'              => 'select2_tagging_from_array',
            'options'           => $tags,
            'allows_multiple'   => true,
            'hint'              => t('tags_hint', [
                'limit' => (int)config('settings.single.tags_limit', 15),
                'min'   => (int)config('settings.single.tags_min_length', 2),
                'max'   => (int)config('settings.single.tags_max_length', 30)
            ]),
            'wrapperAttributes' => [
                'class' => 'col-md-6',
            ],
            'disable'    => true,
        ]);

        $rejects = ['Не нарушает правила'];
        $postRejects = PostReject::pluck('title', 'id')->toArray();

        $rejects = array_merge($rejects, $postRejects);
        $this->xPanel->addField([
            'name'              => 'status',
            'label'             => 'Нарушает правила',
            'type'              => 'select2_from_array',
            'options'           => $rejects,
            'allows_null'       => false,
            'value'             => $this->parentId,
            'wrapperAttributes' => [
                'class' => 'col-md-12',
            ],
        ]);
    }

    public function update(UpdateRequest $request)
    {
        $post = Post::find($request->input('id'));

        $post->status = $request->input('status');

        if ($request->input('status') == PostStatusEnum::REJECTED) {
            $post->reject_id = $request->input('reject_id');
        }

        $post->save();

        $postId = $post->id;
        $cacheId = 'post.with.city.pictures.' . $postId . '.' . config('app.locale');
        Cache::remember($cacheId, $this->cacheExpiration, function () use ($postId) {
            return Post::withCountryFix()
                ->unarchived()
                ->where('id', $postId)
                ->with([
                    'category'      => function ($builder) { $builder->with(['parent']); },
                    'postType',
                    'city',
                    'pictures',
                    'latestPayment' => function ($builder) { $builder->with(['package']); },
                    'savedByLoggedUser',
                    'postReject',
                ])
                ->first();
        });

        return redirect()->to($this->xPanel->route);
    }
}