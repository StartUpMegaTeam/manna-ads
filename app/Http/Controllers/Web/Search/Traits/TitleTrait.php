<?php
/**
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Http\Controllers\Web\Search\Traits;

use App\Helpers\Number;
use App\Helpers\Search\PostQueries;
use App\Helpers\UrlGen;
use App\Http\Controllers\Web\Post\Traits\CatBreadcrumbTrait;
use App\Models\Post;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

trait TitleTrait
{
    use CatBreadcrumbTrait;

    protected $posts = null;

    /**
     * Get Search Meta Tags
     *
     * @return string[]
     */
    public function getMetaTag()
    {
        $metaTag = ['h1' => '', 'title' => '', 'description' => '', 'keywords' => ''];

        [$h1, $title, $description, $keywords] = getMetaTag('search');

        $fallbackTitle = '';

        // Init.
        $fallbackTitle .= t('classified_ads');

        // Keyword
        if (request()->filled('q')) {
            $fallbackTitle .= ' ' . t('for') . ' ';
            $fallbackTitle .= '"' . rawurldecode(request()->get('q')) . '"';
        }

        // Category
        if (isset($this->cat) && !empty($this->cat)) {
            [$h1, $title, $description, $keywords] = getMetaTag('searchCategory', $this->cat);

            // SubCategory
            if (isset($this->subCat) && !empty($this->subCat)) {
                $this->getDefaultPostsBuilder($this->subCat);

                $h1 = str_replace('{category.name}', $this->subCat->title == null ? $this->subCat->name : $this->subCat->title, $h1);
                $h1 = str_replace('{category.title}', $this->subCat->seo_title, $h1);
                $title = str_replace('{category.name}', $this->subCat->title == null ? $this->subCat->name : $this->subCat->title, $title);
                $title = str_replace('{category.title}', $this->subCat->seo_title, $title);
                $description = str_replace('{category.name}', $this->subCat->title == null ? $this->subCat->name : $this->subCat->title, $description);
                $description = str_replace('{category.description}', $this->subCat->seo_description, $description);
                $keywords = str_replace('{category.name}', mb_strtolower($this->subCat->title == null ? $this->subCat->name : $this->subCat->title), $keywords);
                $keywords = str_replace('{category.keywords}', mb_strtolower($this->subCat->seo_keywords), $keywords);

                $fallbackTitle .= ' ' . $this->subCat->name . ',';
                if (!empty($this->subCat->seo_description)) {
                    $fallbackDescription = $this->subCat->seo_description . ', ' . config('country.name');
                }
            } else {
                $this->getDefaultPostsBuilder($this->cat);

                $h1 = str_replace('{category.name}', $this->cat->title == null ? $this->cat->name : $this->cat->title, $h1);
                $h1 = str_replace('{category.title}', $this->cat->seo_title, $h1);
                $title = str_replace('{category.name}', $this->cat->title == null ? $this->cat->name : $this->cat->title, $title);
                $title = str_replace('{category.title}', $this->cat->seo_title, $title);
                $description = str_replace('{category.name}', $this->cat->title == null ? $this->cat->name : $this->cat->title, $description);
                $description = str_replace('{category.description}', $this->cat->seo_description, $description);
                $keywords = str_replace('{category.name}', mb_strtolower($this->cat->title == null ? $this->cat->name : $this->cat->title), $keywords);
                $keywords = str_replace('{category.keywords}', mb_strtolower($this->cat->seo_keywords), $keywords);

                $fallbackTitle .= ' ' . $this->cat->name;
                if (!empty($this->cat->seo_description)) {
                    $fallbackDescription = $this->cat->seo_description . ', ' . config('country.name');
                }
            }

            $minPrice = $this->getCategoryMinPrice();

            if ($minPrice != null) {
                $h1 = str_replace('{category.minPrice}', $minPrice, $h1);
                $title = str_replace('{category.minPrice}', $minPrice, $title);
                $description = str_replace('{category.minPrice}', $minPrice, $description);
                $keywords = str_replace('{category.minPrice}', $minPrice, $keywords);
            }

            $postsCount = $this->getCategoryPostsCount();

            if ($postsCount != null) {
                $h1 = str_replace('{category.count}', $postsCount, $h1);
                $title = str_replace('{category.count}', $postsCount, $title);
                $description = str_replace('{category.count}', $postsCount, $description);
                $keywords = str_replace('{category.count}', $postsCount, $keywords);
            }
        }

        // User
        if (isset($this->sUser) && !empty($this->sUser)) {
            [$h1, $title, $description, $keywords] = getMetaTag('searchProfile');
            $h1 = str_replace('{profile.name}', $this->sUser->name, $h1);
            $title = str_replace('{profile.name}', $this->sUser->name, $title);
            $description = str_replace('{profile.name}', $this->sUser->name, $description);
            $keywords = str_replace('{profile.name}', mb_strtolower($this->sUser->name), $keywords);

            $fallbackTitle .= ' ' . t('of') . ' ';
            $fallbackTitle .= $this->sUser->name;
        }

        // Tag
        if (isset($this->tag) && !empty($this->tag)) {
            [$h1, $title, $description, $keywords] = getMetaTag('searchTag');
            $h1 = str_replace('{tag}', $this->tag, $h1);
            $title = str_replace('{tag}', $this->tag, $title);
            $description = str_replace('{tag}', $this->tag, $description);
            $keywords = str_replace('{tag}', mb_strtolower($this->tag), $keywords);

            $fallbackTitle .= ' ' . t('for') . ' ';
            $fallbackTitle .= $this->tag . ' (' . t('Tag') . ')';
        }

        // Location
        if (request()->filled('r') && !request()->filled('l')) {
            // Administrative Division
            if (isset($this->admin) && !empty($this->admin)) {
                $h1 = str_replace('{location.name}', $this->admin->name, $h1);
                $title = str_replace('{location.name}', $this->admin->name, $title);
                $description = str_replace('{location.name}', $this->admin->name, $description);
                $keywords = str_replace('{location.name}', mb_strtolower($this->admin->name), $keywords);

                $fallbackTitle .= ' ' . t('in') . ' ';
                $fallbackTitle .= $this->admin->name;
                $fallbackDescription = t('ads_in_location', ['location' => $this->admin->name])
                    . ', ' . config('country.name')
                    . '. ' . t('looking_for_product_or_service')
                    . ' - ' . $this->admin->name
                    . ', ' . config('country.name');
            }
        } else {
            // City
            if (isset($this->city) && !empty($this->city)) {
                //Именительный
                $h1 = str_replace('{location.name}', 'в городе ' . $this->city->name, $h1);
                $title = str_replace('{location.name}', 'в городе ' . $this->city->name, $title);
                $description = str_replace('{location.name}', 'в городе ' . $this->city->name, $description);
                $keywords = str_replace('{location.name}', 'в городе ' . mb_strtolower($this->city->name), $keywords);

                //Предложный
                $h1 = str_replace('{location.name.prepositional}', $this->city->name_prepositional, $h1);
                $title = str_replace('{location.name.prepositional}', $this->city->name_prepositional, $title);
                $h1 = str_replace('{location.name.prepositional}', $this->city->name_prepositional, $h1);
                $description = str_replace('{location.name.prepositional}', $this->city->name_prepositional, $description);

                $fallbackTitle .= ' ' . t('in') . ' ';
                $fallbackTitle .= $this->city->name;
                $fallbackDescription = t('ads_in_location', ['location' => $this->city->name])
                    . ', ' . config('country.name')
                    . '. ' . t('looking_for_product_or_service')
                    . ' - ' . $this->city->name
                    . ', ' . config('country.name');
            } else {
                //Именительный
                $nominativeDefault = 'в Донецкой, Луганской, Запорожской и Херсонской областях';
                $h1 = str_replace('{location.name}', $nominativeDefault, $h1);
                $title = str_replace('{location.name}', $nominativeDefault, $title);
                $description = str_replace('{location.name}', $nominativeDefault, $description);
                $keywords = str_replace('{location.name}', $nominativeDefault, $keywords);

                //Предложный
                $prepositionalDefault = 'Донецкой, Луганской, Запорожской и Херсонской областях';
                $h1 = str_replace('{location.name.prepositional}', $prepositionalDefault, $h1);
                $title = str_replace('{location.name.prepositional}', $prepositionalDefault, $title);
                $h1 = str_replace('{location.name.prepositional}', $prepositionalDefault, $h1);
                $description = str_replace('{location.name.prepositional}', $prepositionalDefault, $description);

                $fallbackTitle .= ' ' . t('in') . ' ';
                $fallbackTitle .= $nominativeDefault;
                $fallbackDescription = t('ads_in_location', ['location' => $nominativeDefault])
                    . ', ' . config('country.name')
                    . '. ' . t('looking_for_product_or_service')
                    . ' - ' . $nominativeDefault
                    . ', ' . config('country.name');
            }
        }

        // Country
        $fallbackTitle .= ', ' . config('country.name');

        // view()->share('title', $fallbackTitle);

        $h1 = replaceGlobalPatterns($h1);
        $title = replaceGlobalPatterns($title);
        $description = replaceGlobalPatterns($description);
        $keywords = mb_strtolower(replaceGlobalPatterns($keywords));

        $metaTag['h1'] = $h1;
        $metaTag['title'] = !empty($title) ? $title : $fallbackTitle;
        $metaTag['description'] = !empty($description) ? $description : ($fallbackDescription ?? $fallbackTitle);
        $metaTag['keywords'] = $keywords;

        return array_values($metaTag);
    }

    protected function getDefaultPostsBuilder($category)
    {
        $catChildrenIds = $this->getCategoryChildrenIds($category, $category->id);

        if (!empty($catChildrenIds)) {
            $this->posts = Post::where('archived', '=', 0)
                ->where('archived_manually', '=', 0)
                ->whereIn('category_id', $catChildrenIds);
        }
    }

    /**
     * @return string|null
     */
    public function getCategoryMinPrice(): ?string
    {
        if ($this->posts !== null) {
            $post = $this->posts->whereNotNull('price')
                ->where('price', '>', 0)
                ->orderBy('price')->first();

            if ($post) {

                return Number::money($post->price, ' ');
            }

            return null;
        }

        return null;
    }

    public function getCategoryPostsCount(): ?string
    {
        if ($this->posts !== null) {
            $postsCount = $this->posts->count();

            if ($postsCount > 0) {
                return $postsCount;
            }

            return null;
        }

        return null;
    }

    private function getCategoryChildrenIds($cat, $catId = null, &$idsArr = [])
    {
        if (!empty($catId)) {
            $idsArr[] = $catId;
        }

        if (isset($cat->children) && $cat->children->count() > 0) {
            $subIdsArr = [];
            foreach ($cat->children as $subCat) {
                if ($subCat->active != 1) {
                    continue;
                }

                $idsArr[] = $subCat->id;

                if (isset($subCat->children) && $subCat->children->count() > 0) {
                    $subIdsArr = $this->getCategoryChildrenIds($subCat, null, $subIdsArr);
                }
            }
            $idsArr = array_merge($idsArr, $subIdsArr);
        }

        return $idsArr;
    }

    /**
     * Get Search HTML Title
     *
     * @return string
     */
    public function getHtmlTitle()
    {
        // Title
        $htmlTitle = '';

        // Init.
        $attr = ['countryCode' => config('country.icode')];
        $htmlTitle .= '<a href="' . UrlGen::search() . '" class="current">';
        $htmlTitle .= '<span>' . t('All ads') . '</span>';
        $htmlTitle .= '</a>';

        // Location
        $searchUrl = UrlGen::search([], ['l', 'r', 'location', 'distance']);

        if (request()->filled('r') && !request()->filled('l')) {
            // Administrative Division
            if (isset($this->admin) && !empty($this->admin)) {
                $htmlTitle .= ' ' . t('in') . ' ';
                $htmlTitle .= '<a rel="nofollow" class="jobs-s-tag" href="' . $searchUrl . '">';
                $htmlTitle .= $this->admin->name;
                $htmlTitle .= '</a>';
            }
        } else {
            // City
            if (isset($this->city) && !empty($this->city)) {
                if (config('settings.listing.cities_extended_searches')) {
                    $htmlTitle .= ' ' . t('within') . ' ';
                    $htmlTitle .= '<a rel="nofollow" class="jobs-s-tag" href="' . $searchUrl . '">';
                    $htmlTitle .= t('x_distance_around_city', [
                        'distance' => (PostQueries::$distance == 1) ? 0 : PostQueries::$distance,
                        'unit' => getDistanceUnit(config('country.code')),
                        'city' => $this->city->name,
                    ]);
                    $htmlTitle .= '</a>';
                } else {
                    $htmlTitle .= ' ' . t('in') . ' ';
                    $htmlTitle .= '<a rel="nofollow" class="jobs-s-tag" href="' . $searchUrl . '">';
                    $htmlTitle .= $this->city->name;
                    $htmlTitle .= '</a>';
                }
            }
        }

        // Category
        if (isset($this->cat) && !empty($this->cat)) {
            // Get the parent of parent category URL
            $exceptArr = ['c', 'sc', 'cf', 'minPrice', 'maxPrice'];
            $searchUrl = UrlGen::getCatParentUrl($this->cat->parent->parent ?? null, $this->city ?? null, $exceptArr);

            if (isset($this->cat->parent) && !empty($this->cat->parent)) {
                $htmlTitle .= ' ' . t('in') . ' ';
                $htmlTitle .= '<a rel="nofollow" class="jobs-s-tag" href="' . $searchUrl . '">';
                $htmlTitle .= $this->cat->parent->name;
                $htmlTitle .= '</a>';

                // Get the parent category URL
                $exceptArr = ['sc', 'cf', 'minPrice', 'maxPrice'];
                $searchUrl = UrlGen::getCatParentUrl($this->cat->parent ?? null, $this->city ?? null, $exceptArr);
            }

            $htmlTitle .= ' ' . t('in') . ' ';
            $htmlTitle .= '<a rel="nofollow" class="jobs-s-tag" href="' . $searchUrl . '">';
            $htmlTitle .= $this->cat->name;
            $htmlTitle .= '</a>';
        }

        // Tag
        if (isset($this->tag) && !empty($this->tag)) {
            $htmlTitle .= ' ' . t('for') . ' ';
            $attr = ['countryCode' => config('country.icode')];
            $htmlTitle .= '<a rel="nofollow" class="jobs-s-tag" href="' . UrlGen::search() . '">';
            $htmlTitle .= $this->tag;
            $htmlTitle .= '</a>';
        }

        // Date
        if (request()->filled('postedDate') && isset($this->dates) && isset($this->dates->{request()->get('postedDate')})) {
            $exceptArr = ['postedDate'];
            $searchUrl = UrlGen::search([], $exceptArr);

            $htmlTitle .= t('last');
            $htmlTitle .= '<a rel="nofollow" class="jobs-s-tag" href="' . $searchUrl . '">';
            $htmlTitle .= $this->dates->{request()->get('postedDate')};
            $htmlTitle .= '</a>';
        }

        // Condition
        if (request()->filled('new') && isset($this->conditions) && isset($this->conditions->{request()->get('new')})) {
            $exceptArr = ['new'];
            $searchUrl = UrlGen::search([], $exceptArr);

            $htmlTitle .= '<a rel="nofollow" class="jobs-s-tag" href="' . $searchUrl . '">';
            $htmlTitle .= $this->conditions->{request()->get('new')};
            $htmlTitle .= '</a>';
        }

        view()->share('htmlTitle', $htmlTitle);

        return $htmlTitle;
    }

    /**
     * Get Breadcrumbs Tabs
     *
     * @return array
     */
    public function getBreadcrumb()
    {
        $bcTab = [];

        // City
        if (isset($this->city) && !empty($this->city)) {
            $title = $this->city->name;

            $bcTab[] = collect([
                'name' => $this->city->name,
                'url' => '/search?l=' . $this->city->id,
                'position' => (isset($this->cat) ? 3 : 1),
                'location' => true,
            ]);
        }

        // Admin
        if (isset($this->admin) && !empty($this->admin)) {
            $queryArr = [
                'd' => config('country.icode'),
                'r' => $this->admin->name,
            ];
            $exceptArr = ['l', 'location', 'distance'];
            $searchUrl = UrlGen::search($queryArr, $exceptArr);

            $title = $this->admin->name;

            $attr = ['countryCode' => config('country.icode')];
            $bcTab[] = collect([
                'name' => (isset($this->cat) ? t('All ads') . ' ' . $title : $this->admin->name),
                'url' => $searchUrl,
                'position' => (isset($this->cat) ? 5 : 3),
                'location' => true,
            ]);
        }

        // Category
        $catBreadcrumb = $this->getCatBreadcrumb($this->cat, 4);
        $bcTab = array_merge($bcTab, $catBreadcrumb);

        // Sort by Position
        $bcTab = array_values(Arr::sort($bcTab, function ($value) {
            return $value->get('position');
        }));

        view()->share('bcTab', $bcTab);

        return $bcTab;
    }
}
