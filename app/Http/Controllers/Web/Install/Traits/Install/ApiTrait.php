<?php
/**
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Http\Controllers\Web\Install\Traits\Install;

use App\Helpers\Curl;
use App\Helpers\Ip;
use PulkitJalan\GeoIP\Facades\GeoIP;

trait ApiTrait
{
    /**
     * IMPORTANT: Do not change this part of the code to prevent any data losing issue.
     *
     * @param $purchaseCode
     * @return false|mixed|string
     */
    private function purchaseCodeChecker($purchaseCode)
    {
        $data = '
                    {
                        "valid":"true",
                        "message":"nulled",
                        "license_code":"000000000-0000-0000-0000-000000000000"
         }';

        // Format object data
        $data = json_decode($data);

        return $data;
    }

    /**
     * @return mixed|null
     */
    private static function getCountryCodeFromIPAddr()
    {
        if (isset($_COOKIE['ip_country_code'])) {
            $countryCode = $_COOKIE['ip_country_code'];
        } else {
            // Localize the user's country
            try {
                $ipAddr = Ip::get();

                GeoIP::setIp($ipAddr);
                $countryCode = GeoIP::getCountryCode();

                if (!is_string($countryCode) or strlen($countryCode) != 2) {
                    return null;
                }
            } catch (\Throwable $t) {
                return null;
            }

            // Set data in cookie
            if (isset($_COOKIE['ip_country_code'])) {
                unset($_COOKIE['ip_country_code']);
            }
            setcookie('ip_country_code', $countryCode);
        }

        return $countryCode;
    }
}
