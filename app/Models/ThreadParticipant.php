<?php
/**
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Models;

use App\Helpers\Date;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Larapen\Admin\app\Models\Traits\Crud;

/**
 * App\Models\ThreadParticipant
 *
 * @property int $id
 * @property int|null $thread_id
 * @property int|null $user_id
 * @property string|null $last_read
 * @property int|null $is_important
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read mixed $created_at_formatted
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Thread|null $thread
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant newQuery()
 * @method static \Illuminate\Database\Query\Builder|ThreadParticipant onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant query()
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant whereIsImportant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant whereLastRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant whereThreadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThreadParticipant whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|ThreadParticipant withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ThreadParticipant withoutTrashed()
 * @mixin \Eloquent
 */
class ThreadParticipant extends BaseModel
{
	use SoftDeletes, Crud, Notifiable, HasFactory;
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'threads_participants';
	
	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	// protected $primaryKey = 'id';
	protected $appends = ['created_at_formatted'];
	
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var boolean
	 */
	// public $timestamps = false;
	
	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'thread_id',
		'user_id',
		'last_read',
		'is_important',
	];
	
	/**
	 * The attributes that should be hidden for arrays
	 *
	 * @var array
	 */
	// protected $hidden = [];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/
	protected static function boot()
	{
		parent::boot();
	}
	
	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
	/**
	 * Thread relationship.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 *
	 * @codeCoverageIgnore
	 */
	public function thread()
	{
		return $this->belongsTo(Thread::class, 'thread_id', 'id');
	}
	
	/**
	 * User relationship.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 *
	 * @codeCoverageIgnore
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
	
	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/
	
	/*
	|--------------------------------------------------------------------------
	| ACCESSORS
	|--------------------------------------------------------------------------
	*/
	public function getCreatedAtFormattedAttribute($value)
	{
		$value = new Carbon($this->attributes['created_at']);
		$value->timezone(Date::getAppTimeZone());
		
		$value = Date::formatFormNow($value);
		
		return $value;
	}
	
	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
