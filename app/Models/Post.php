<?php
/**
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Models;

use App\Helpers\Date;
use App\Helpers\Post\Pictures;
use App\Helpers\RemoveFromString;
use App\Helpers\UrlGen;
use App\Models\Post\LatestOrPremium;
use App\Models\Post\SimilarByCategory;
use App\Models\Post\SimilarByLocation;
use App\Models\Scopes\LocalizedScope;
use App\Models\Scopes\VerifiedScope;
use App\Models\Scopes\ReviewedScope;
use App\Models\Traits\CountryTrait;
use App\Observers\PostObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Larapen\Admin\app\Models\Traits\Crud;
use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;

/**
 * App\Models\Post
 *
 * @property int $id
 * @property string|null $country_code
 * @property int|null $user_id
 * @property int|null $category_id
 * @property int|null $post_type_id
 * @property string $title
 * @property string $description
 * @property string|null $tags
 * @property string|null $price
 * @property int|null $negotiable
 * @property string|null $contact_name
 * @property string|null $email
 * @property string|null $phone
 * @property int|null $phone_hidden
 * @property string|null $address
 * @property int|null $city_id
 * @property float|null $lon longitude in decimal degrees (wgs84)
 * @property float|null $lat latitude in decimal degrees (wgs84)
 * @property string|null $ip_addr
 * @property int|null $visits
 * @property string|null $email_token
 * @property string|null $phone_token
 * @property string|null $tmp_token
 * @property int|null $verified_email
 * @property int|null $verified_phone
 * @property int|null $accept_terms
 * @property int|null $accept_marketing_offers
 * @property int|null $is_permanent
 * @property int|null $reviewed
 * @property int|null $featured
 * @property int|null $archived
 * @property string|null $archived_at
 * @property int|null $archived_manually
 * @property string|null $deletion_mail_sent_at
 * @property string|null $fb_profile
 * @property string|null $partner
 * @property integer $status
 * @property integer|null $reject_id
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read \App\Models\Category|null $category
 * @property-read \App\Models\City|null $city
 * @property-read \App\Models\Country|null $country
 * @property-read mixed $created_at_formatted
 * @property-read mixed $slug
 * @property-read mixed $user_photo_url
 * @property-read \App\Models\Payment|null $latestPayment
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Payment[] $payments
 * @property-read int|null $payments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Picture[] $pictures
 * @property-read int|null $pictures_count
 * @property-read \App\Models\PostType|null $postType
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PostValue[] $postValues
 * @property-read int|null $post_values_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SavedPost[] $savedByLoggedUser
 * @property-read int|null $saved_by_logged_user_count
 * @property-read \App\Models\User|null $user
 * @property-read \App\Models\PostReject $postReject
 * @method static \Illuminate\Database\Eloquent\Builder|Post archived()
 * @method static \Illuminate\Database\Eloquent\Builder|Post countryOf($countryCode)
 * @method static \Illuminate\Database\Eloquent\Builder|Post currentCountry()
 * @method static \Illuminate\Database\Eloquent\Builder|Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|Post reviewed()
 * @method static \Illuminate\Database\Eloquent\Builder|Post unarchived()
 * @method static \Illuminate\Database\Eloquent\Builder|Post unreviewed()
 * @method static \Illuminate\Database\Eloquent\Builder|Post unverified()
 * @method static \Illuminate\Database\Eloquent\Builder|Post verified()
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereAcceptMarketingOffers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereAcceptTerms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereArchived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereArchivedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereArchivedManually($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereContactName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDeletionMailSentAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereEmailToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereFbProfile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereFeatured($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereIpAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereIsPermanent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereLon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereNegotiable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post wherePartner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post wherePhoneHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post wherePhoneToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post wherePostTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereReviewed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTmpToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereVerifiedEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereVerifiedPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereVisits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post withCountryFix()
 * @mixin \Eloquent
 */
class Post extends BaseModel implements Feedable
{
	use Crud, CountryTrait, Notifiable, HasFactory, LatestOrPremium, SimilarByCategory, SimilarByLocation, Pictures;
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'posts';
	
	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	protected $appends = ['slug', 'created_at_formatted', 'user_photo_url'];
	
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var boolean
	 */
	public $timestamps = true;
	
	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'country_code',
		'user_id',
		'category_id',
		'post_type_id',
		'title',
		'description',
		'tags',
		'price',
		'negotiable',
		'contact_name',
		'email',
		'phone',
		'phone_hidden',
		'address',
		'city_id',
		'lat',
		'lon',
		'ip_addr',
		'visits',
		'tmp_token',
		'email_token',
		'phone_token',
		'verified_email',
		'verified_phone',
		'accept_terms',
		'accept_marketing_offers',
		'is_permanent',
		'reviewed',
		'featured',
		'archived',
		'archived_at',
		'deletion_mail_sent_at',
		'fb_profile',
		'partner',
		'created_at',
		'updated_at',
        'ads_type',
	];
	
	/**
	 * The attributes that should be hidden for arrays
	 *
	 * @var array
	 */
	// protected $hidden = [];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/
	protected static function boot()
	{
		parent::boot();
		
		Post::observe(PostObserver::class);
		
		static::addGlobalScope(new VerifiedScope());
		static::addGlobalScope(new ReviewedScope());
		static::addGlobalScope(new LocalizedScope());
	}
	
	public function routeNotificationForMail()
	{
		return $this->email;
	}
	
	public function routeNotificationForNexmo()
	{
		$phone = $this->phone;
        $phone = setPhoneSign($phone, 'nexmo');
		
		return $phone;
	}
	
	public function routeNotificationForTwilio()
	{
        $phone = $this->phone;
        $phone = setPhoneSign($phone, 'twilio');
		
		return $phone;
	}
	
	public static function getFeedItems()
	{
		$postsPerPage = (int)config('settings.listing.items_per_page', 50);
		
		$posts = Post::reviewed()->unarchived();
		
		if (request()->has('d') || config('plugins.domainmapping.installed')) {
			$countryCode = config('country.code');
			if (!config('plugins.domainmapping.installed')) {
				if (request()->has('d')) {
					$countryCode = request()->input('d');
				}
			}
			$posts = $posts->where('country_code', $countryCode);
		}
		
		$posts = $posts->take($postsPerPage)->orderByDesc('id')->get();
		
		return $posts;
	}
	
	public function toFeedItem()
	{
		$title = $this->title;
		$title .= (isset($this->city) && !empty($this->city)) ? ' - ' . $this->city->name : '';
		$title .= (isset($this->country) && !empty($this->country)) ? ', ' . $this->country->name : '';
		// $summary = str_limit(str_strip(strip_tags($this->description)), 5000);
		$summary = transformDescription($this->description);
		$link = UrlGen::postUri($this, true);
		
		return FeedItem::create()
			->id($link)
			->title($title)
			->summary($summary)
			->category((!empty($this->category)) ? $this->category->name : '')
			->updated($this->updated_at)
			->link($link)
			->author($this->contact_name);
	}
	
	public function getTitleHtml()
	{
		$out = '';
		
		// $post = self::find($this->id);
		$out .= getPostUrl($this);
		$out .= '<br>';
		$out .= '<small>';
		$out .= $this->pictures->count() . ' ' . trans('admin.pictures');
		$out .= '</small>';
		if (isset($this->archived) && $this->archived == 1) {
			$out .= '<br>';
			$out .= '<span class="badge badge-secondary">';
			$out .= trans('admin.Archived');
			$out .= '</span>';
		}
		
		return $out;
	}
	
	public function getPictureHtml()
	{
		// Get ad URL
		$url = url(UrlGen::postUri($this));
		
		$style = ' style="width:auto; max-height:90px;"';
		// Get first picture
		if ($this->pictures->count() > 0) {
			foreach ($this->pictures as $picture) {
				$url = dmUrl($picture->post->country_code, UrlGen::postPath($this));
				$out = '<img src="' . imgUrl($picture->filename, 'small') . '" data-bs-toggle="tooltip" title="' . $this->title . '"' . $style . ' class="img-rounded">';
				break;
			}
		} else {
			// Default picture
			$out = '<img src="' . imgUrl(config('larapen.core.picture.default'), 'small') . '" data-bs-toggle="tooltip" title="' . $this->title . '"' . $style . ' class="img-rounded">';
		}
		
		// Add link to the Ad
		$out = '<a href="' . $url . '" target="_blank">' . $out . '</a>';
		
		return $out;
	}
	
	public function getUserNameHtml()
	{
		if (isset($this->user) and !empty($this->user)) {
			$url = admin_url('users/' . $this->user->getKey() . '/edit');
			$tooltip = ' data-bs-toggle="tooltip" title="' . $this->user->name . '"';
			
			return '<a href="' . $url . '"' . $tooltip . '>' . $this->contact_name . '</a>';
		} else {
			return $this->contact_name;
		}
	}
	
	public function getCityHtml()
	{
		$out = $this->getCountryHtml();
		$out .= ' - ';
		if (isset($this->city) and !empty($this->city)) {
			$out .= '<a href="' . UrlGen::city($this->city) . '" target="_blank">' . $this->city->name . '</a>';
		} else {
			$out .= $this->city_id;
		}
		
		return $out;
	}
	
	public function getReviewedHtml()
	{
		return ajaxCheckboxDisplay($this->{$this->primaryKey}, $this->getTable(), 'reviewed', $this->reviewed);
	}
	
	public function getFeaturedHtml()
	{
		$out = '-';
		if (config('plugins.offlinepayment.installed')) {
			$opTool = '\extras\plugins\offlinepayment\app\Helpers\OpTools';
			if (class_exists($opTool)) {
				$out = $opTool::featuredCheckboxDisplay($this->{$this->primaryKey}, $this->getTable(), 'featured', $this->featured);
			}
		}
		
		return $out;
	}
	
	/*
	|--------------------------------------------------------------------------
	| QUERIES
	|--------------------------------------------------------------------------
	*/
	
	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
	public function postType()
	{
		return $this->belongsTo(PostType::class, 'post_type_id', 'id');
	}
	
	public function category()
	{
		return $this->belongsTo(Category::class, 'category_id', 'id');
	}
	
	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}
	
	public function latestPayment()
	{
		return $this->hasOne(Payment::class, 'post_id')->orderByDesc('id');
	}
	
	public function payments()
	{
		return $this->hasMany(Payment::class, 'post_id');
	}
	
	public function pictures()
	{
		return $this->hasMany(Picture::class, 'post_id')->orderBy('position')->orderByDesc('id');
	}
	
	public function savedByLoggedUser()
	{
		$guard = isFromApi() ? 'sanctum' : null;
		$userId = (auth($guard)->check()) ? auth($guard)->user()->id : '-1';
		
		return $this->hasMany(SavedPost::class, 'post_id')->where('user_id', $userId);
	}
	
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

    public function postReject()
    {
        return $this->belongsTo(PostReject::class, 'reject_id');
    }

	public function postValues()
	{
		return $this->hasMany(PostValue::class, 'post_id');
	}
	
	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/
	public function scopeVerified($builder)
	{
		$builder->where(function ($query) {
			$query->where('verified_email', 1)->where('verified_phone', 1);
		});
		
		if (config('settings.single.posts_review_activation')) {
			$builder->where('reviewed', 1);
		}
		
		return $builder;
	}
	
	public function scopeUnverified($builder)
	{
		$builder->where(function ($query) {
			$query->where('verified_email', 0)->orWhere('verified_phone', 0);
		});
		
		if (config('settings.single.posts_review_activation')) {
			$builder->orWhere('reviewed', 0);
		}
		
		return $builder;
	}
	
	public function scopeArchived($builder)
	{
		return $builder->where('archived', 1);
	}
	
	public function scopeUnarchived($builder)
	{
		return $builder->where('archived', 0);
	}
	
	public function scopeReviewed($builder)
	{
		if (config('settings.single.posts_review_activation')) {
			return $builder->where('reviewed', 1);
		} else {
			return $builder;
		}
	}
	
	public function scopeUnreviewed($builder)
	{
		if (config('settings.single.posts_review_activation')) {
			return $builder->where('reviewed', 0);
		} else {
			return $builder;
		}
	}
	
	public function scopeWithCountryFix($builder)
	{
		// Check the Domain Mapping Plugin
		if (config('plugins.domainmapping.installed')) {
			return $builder->where('country_code', config('country.code'));
		} else {
			return $builder;
		}
	}
	
	/*
	|--------------------------------------------------------------------------
	| ACCESSORS
	|--------------------------------------------------------------------------
	*/
	public function getCreatedAtAttribute($value)
	{
		$value = new Carbon($value);
		$value->timezone(Date::getAppTimeZone());
		
		return $value;
	}
	
	public function getUpdatedAtAttribute($value)
	{
		$value = new Carbon($value);
		$value->timezone(Date::getAppTimeZone());
		
		return $value;
	}
	
	public function getDeletedAtAttribute($value)
	{
		$value = new Carbon($value);
		$value->timezone(Date::getAppTimeZone());
		
		return $value;
	}
	
	public function getCreatedAtFormattedAttribute($value)
	{
		$value = new Carbon($this->attributes['created_at']);
		$value->timezone(Date::getAppTimeZone());
		
		$value = Date::formatFormNow($value);
		
		return $value;
	}
	
	public function getArchivedAtAttribute($value)
	{
		$value = (is_null($value)) ? $this->updated_at : $value;
		
		$value = new Carbon($value);
		$value->timezone(Date::getAppTimeZone());
		
		return $value;
	}
	
	public function getDeletionMailSentAtAttribute($value)
	{
		// $value = (is_null($value)) ? $this->updated_at : $value;
		
		if (!empty($value)) {
			$value = new Carbon($value);
			$value->timezone(Date::getAppTimeZone());
		}
		
		return $value;
	}
	
	public function getUserPhotoUrlAttribute($value)
	{
		// Default Photo
		$defaultPhotoUrl = url('images/user.svg');
		
		// Photo from User's account
		$userPhotoUrl = null;
		if (!empty($this->user) && isset($this->user->photo_url) && !empty($this->user->photo_url)) {
			$userPhotoUrl = $this->user->photo_url;
		}
		
		$value = (!empty($userPhotoUrl) && $userPhotoUrl != $defaultPhotoUrl) ? $userPhotoUrl : $defaultPhotoUrl;
		
		return $value;
	}
	
	public function getEmailAttribute($value)
	{
		if (isFromAdminPanel() || (!isFromAdminPanel() && in_array(request()->method(), ['GET']))) {
			if (
				isDemoDomain()
				&& request()->segment(2) != 'password'
			) {
				if (auth()->check()) {
					if (auth()->user()->id != 1) {
						if (isset($this->phone_token)) {
							if ($this->phone_token == 'demoFaker') {
								return $value;
							}
						}
						$value = hidePartOfEmail($value);
					}
				}
			}
		}
		
		return $value;
	}
	
	public function getPhoneAttribute($value)
	{
		$countryCode = config('country.code');
		if (isset($this->country_code) && !empty($this->country_code)) {
			$countryCode = $this->country_code;
		}
		
		return $value;
	}
	
	public function getTitleAttribute($value)
	{
		$value = mb_ucfirst($value);
		
		if (!isFromAdminPanel()) {
			if (!empty($this->user)) {
				if (!$this->user->hasAllPermissions(Permission::getStaffPermissions())) {
					$value = RemoveFromString::contactInfo($value, false, true);
				}
			} else {
				$value = RemoveFromString::contactInfo($value, false, true);
			}
		}
		
		return $value;
	}
	
	public function getSlugAttribute($value)
	{
		$value = (is_null($value) && isset($this->title)) ? $this->title : $value;
		
		$value = stripNonUtf($value);
		$value = slugify($value);
		
		return $value;
	}
	
	public function getContactNameAttribute($value)
	{
		$value = mb_ucwords($value);
		
		return $value;
	}
	
	public function getDescriptionAttribute($value)
	{
		if (!isFromAdminPanel()) {
			if (!empty($this->user)) {
				if (!$this->user->hasAllPermissions(Permission::getStaffPermissions())) {
					$value = RemoveFromString::contactInfo($value, false, true);
				}
			} else {
				$value = RemoveFromString::contactInfo($value, false, true);
			}
		}
		
		return $value;
	}
	
	public function getTagsAttribute($value)
	{
		return tagCleaner($value, true);
	}
	
	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
	public function setTagsAttribute($value)
	{
		if (is_array($value)) {
			$value = implode(',', $value);
		}
		
		$this->attributes['tags'] = (!empty($value)) ? mb_strtolower($value) : $value;
	}
}
