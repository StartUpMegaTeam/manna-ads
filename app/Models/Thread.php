<?php
/**
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Models;

use App\Helpers\Date;
use App\Models\Thread\ThreadTrait;
use App\Observers\ThreadObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Larapen\Admin\app\Models\Traits\Crud;

/**
 * App\Models\Thread
 *
 * @property int $id
 * @property int|null $post_id
 * @property string|null $subject
 * @property int|null $deleted_by
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read mixed $created_at_formatted
 * @property-read \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null $latest_message
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ThreadMessage[] $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ThreadParticipant[] $participants
 * @property-read int|null $participants_count
 * @property-read \App\Models\Post|null $post
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static Builder|Thread between(array $participants)
 * @method static Builder|Thread forUser($userId)
 * @method static Builder|Thread forUserWithNewMessages($userId)
 * @method static Builder|Thread newModelQuery()
 * @method static Builder|Thread newQuery()
 * @method static Builder|Thread notDeletedByUser($userId)
 * @method static \Illuminate\Database\Query\Builder|Thread onlyTrashed()
 * @method static Builder|Thread query()
 * @method static Builder|Thread whereCreatedAt($value)
 * @method static Builder|Thread whereDeletedAt($value)
 * @method static Builder|Thread whereDeletedBy($value)
 * @method static Builder|Thread whereId($value)
 * @method static Builder|Thread wherePostId($value)
 * @method static Builder|Thread whereSubject($value)
 * @method static Builder|Thread whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Thread withTrashed()
 * @method static Builder|Thread withoutTimestamps()
 * @method static \Illuminate\Database\Query\Builder|Thread withoutTrashed()
 * @mixin \Eloquent
 */
class Thread extends BaseModel
{
	use SoftDeletes, Crud, Notifiable, ThreadTrait, HasFactory;
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'threads';
	
	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	// protected $primaryKey = 'id';
	protected $appends = ['created_at_formatted'];
	
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var boolean
	 */
	// public $timestamps = false;
	
	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'post_id',
		'subject',
	];
	
	/**
	 * The attributes that should be hidden for arrays
	 *
	 * @var array
	 */
	// protected $hidden = [];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/
	protected static function boot()
	{
		parent::boot();
		
		Thread::observe(ThreadObserver::class);
	}
	
	public function routeNotificationForMail()
	{
		// return $this->to_email;
		
		if (auth()->user()->email != $this->from_email) {
			return $this->from_email;
		} else {
			return $this->to_email;
		}
	}
	
	public function routeNotificationForNexmo()
	{
		$phone = $this->to_phone;
        $phone = setPhoneSign($phone, 'nexmo');
		
		return $phone;
	}
	
	public function routeNotificationForTwilio()
	{
        $phone = $this->to_phone;
        $phone = setPhoneSign($phone, 'twilio');
		
		return $phone;
	}
	
	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
	public function post()
	{
		return $this->belongsTo(Post::class, 'post_id');
	}
	
	/**
	 * Messages relationship.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 *
	 * @codeCoverageIgnore
	 */
	public function messages()
	{
		return $this->hasMany(ThreadMessage::class, 'thread_id', 'id')->orderByDesc('id');
	}
	
	/**
	 * Participants relationship.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 *
	 * @codeCoverageIgnore
	 */
	public function participants()
	{
		return $this->hasMany(ThreadParticipant::class, 'thread_id', 'id');
	}
	
	/**
	 * User's relationship.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 *
	 * @codeCoverageIgnore
	 */
	public function users()
	{
		return $this->belongsToMany(User::class, (new ThreadParticipant)->getTable(), 'thread_id', 'user_id');
	}
	
	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/
	public function scopeNotDeletedByUser(Builder $query, $userId)
	{
		return $query->where(function($q) use ($userId) {
			$q->where('deleted_by', '!=', $userId)->orWhereNull('deleted_by');
		});
	}
	
	/**
	 * Returns threads that the user is associated with.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @param int $userId
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeForUser(Builder $query, $userId)
	{
		$participantsTable = (new ThreadParticipant)->getTable();
		$threadsTable = $this->getTable();
		
		return $query
			->join($participantsTable, $this->getQualifiedKeyName(), '=', $participantsTable . '.thread_id')
			->where($participantsTable . '.user_id', $userId)
			->whereNull($participantsTable . '.deleted_at')
			->select($threadsTable . '.*', $participantsTable . '.last_read', $participantsTable . '.is_important');
	}

    public function scopeForUserNotDeleted(Builder $query, $userId)
    {
        return $query->forUser($userId)->notDeletedByUser($userId);
    }
	
	/**
	 * Returns threads with new messages that the user is associated with.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @param int $userId
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeForUserWithNewMessages(Builder $query, $userId)
	{
		$participantsTable = (new ThreadParticipant)->getTable();
		$threadsTable = $this->getTable();
		
		return $query->notDeletedByUser($userId)
			->join($participantsTable, $this->getQualifiedKeyName(), '=', $participantsTable . '.thread_id')
			->where($participantsTable . '.user_id', $userId)
			->whereNull($participantsTable . '.deleted_at')
			->where(function (Builder $query) use ($participantsTable, $threadsTable) {
				$query->where(
					$threadsTable . '.updated_at',
					'>',
					$this->getConnection()->raw($this->getConnection()->getTablePrefix() . $participantsTable . '.last_read')
				)->orWhereNull($participantsTable . '.last_read');
			})
			->select($threadsTable . '.*', $participantsTable . '.last_read', $participantsTable . '.is_important');
	}
	
	public function scopeWithoutTimestamps()
	{
		$this->timestamps = false;
		return $this;
	}
	
	/**
	 * Returns threads between given user ids.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @param array $participants
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeBetween(Builder $query, array $participants)
	{
		return $query->whereHas('participants', function (Builder $q) use ($participants) {
			$q->whereIn('user_id', $participants)
				->select($this->getConnection()->raw('DISTINCT(thread_id)'))
				->groupBy('thread_id')
				->havingRaw('COUNT(thread_id)=' . count($participants));
		});
	}
	
	/*
	|--------------------------------------------------------------------------
	| ACCESSORS
	|--------------------------------------------------------------------------
	*/
	public function getCreatedAtAttribute($value)
	{
		$value = new Carbon($value);
		$value->timezone(Date::getAppTimeZone());
		
		return $value;
	}
	
	public function getCreatedAtFormattedAttribute($value)
	{
        $value = new Carbon($this->attributes['created_at']);
        $value->timezone(Date::getAppTimeZone());

        $value = \Carbon\Carbon::parse($value)->format('d.m.y h:i');

        return $value;
    }

    public function getUnredMessageCountAttribute()
    {
        if ($this->latest_message && $this->latest_message->user_id != auth()->id()) {
            $lastRead = ThreadParticipant::where('thread_id', '=', $this->id)
                ->where('user_id', '=', auth()->id())->first()->last_read;
            if ($lastRead !== null) {
                return ThreadMessage::where('thread_id', '=', $this->id)
                    ->where('user_id', '<>', auth()->id())
                    ->where('created_at', '>=', date($lastRead))->count();
            } else {
                return ThreadMessage::where('thread_id', '=', $this->id)
                    ->where('user_id', '<>', auth()->id())->count();
            }
        } else {
            return null;
        }
    }
	
	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
