<?php
/**
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Models;


use App\Models\Traits\CountryTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Larapen\Admin\app\Models\Traits\Crud;

/**
 * App\Models\SavedSearch
 *
 * @property int $id
 * @property string|null $country_code
 * @property int|null $user_id
 * @property string|null $keyword To show
 * @property string|null $query
 * @property int|null $count
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Country|null $country
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch countryOf($countryCode)
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch currentCountry()
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch query()
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch whereKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch whereQuery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SavedSearch whereUserId($value)
 * @mixin \Eloquent
 */
class SavedSearch extends BaseModel
{
    use Crud, CountryTrait, HasFactory;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'saved_search';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    // protected $primaryKey = 'id';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    // public $timestamps = false;
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['country_code', 'user_id', 'keyword', 'query', 'count'];
    
    /**
     * The attributes that should be hidden for arrays
     *
     * @var array
     */
    // protected $hidden = [];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = [];
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
	protected static function boot()
	{
		parent::boot();
	}
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
