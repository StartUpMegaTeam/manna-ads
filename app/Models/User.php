<?php
/**
 * LaraClassifier - Classified Ads Web Application
 * Copyright (c) BeDigit. All Rights Reserved
 *
 * Website: https://laraclassifier.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Models;

use App\Exceptions\BusinessLogicException;
use App\Helpers\Auth\NewAccessToken;
use App\Helpers\Date;
use App\Helpers\FileHelper;
use App\Helpers\Files\Storage\StorageDisk;
use App\Models\Scopes\LocalizedScope;
use App\Models\Traits\CountryTrait;
use App\Notifications\ResetPasswordNotification;
use App\Observers\UserObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Larapen\Admin\app\Models\Traits\Crud;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string|null $country_code
 * @property string|null $language_code
 * @property int|null $user_type_id
 * @property int|null $gender_id
 * @property string $name
 * @property string|null $photo
 * @property string|null $about
 * @property string|null $phone
 * @property int|null $phone_hidden
 * @property string|null $username
 * @property string|null $email
 * @property Carbon|null $email_verified_at
 * @property string|null $password
 * @property string|null $remember_token
 * @property int|null $is_admin
 * @property int|null $can_be_impersonated
 * @property int|null $disable_comments
 * @property boolean $is_olx
 * @property string|null $ip_addr
 * @property string|null $provider facebook, google, twitter, linkedin, ...
 * @property string|null $provider_id Provider User ID
 * @property string|null $email_token
 * @property string|null $phone_token
 * @property int|null $verified_email
 * @property int|null $verified_phone
 * @property int|null $accept_terms
 * @property int|null $accept_marketing_offers
 * @property int|null $city_id
 * @property string|null $time_zone
 * @property int|null $blocked
 * @property int|null $closed
 * @property string|null $last_activity
 * @property Carbon|null $last_login_at
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read \App\Models\Country|null $country
 * @property-read \App\Models\Gender|null $gender
 * @property-read mixed $created_at_formatted
 * @property-read mixed $photo_url
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Thread[] $receivedThreads
 * @property-read int|null $received_threads_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $savedPosts
 * @property-read int|null $saved_posts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SavedSearch[] $savedSearch
 * @property-read int|null $saved_search_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Thread[] $threads
 * @property-read int|null $threads_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @property-read \App\Models\UserType|null $userType
 * @method static \Illuminate\Database\Eloquent\Builder|User countryOf($countryCode)
 * @method static \Illuminate\Database\Eloquent\Builder|User currentCountry()
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User unverified()
 * @method static \Illuminate\Database\Eloquent\Builder|User verified()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAcceptMarketingOffers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAcceptTerms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBlocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCanBeImpersonated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereClosed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDisableComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIpAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLanguageCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastActivity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTimeZone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereVerifiedEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereVerifiedPhone($value)
 * @mixin \Eloquent
 */
class User extends BaseUser
{
    use Crud, HasRoles, CountryTrait, HasApiTokens, Notifiable, HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    // protected $primaryKey = 'id';
    protected $appends = ['created_at_formatted', 'photo_url'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_code',
        'language_code',
        'user_type_id',
        'gender_id',
        'name',
        'photo',
        'about',
        'phone',
        'phone_hidden',
        'email',
        'username',
        'password',
        'remember_token',
        'is_admin',
        'can_be_impersonate',
        'disable_comments',
        'ip_addr',
        'provider',
        'provider_id',
        'email_token',
        'phone_token',
        'verified_email',
        'verified_phone',
        'accept_terms',
        'accept_marketing_offers',
        'time_zone',
        'blocked',
        'closed',
        'last_activity',
        'city_id',
        'is_olx',
    ];

    /**
     * The attributes that should be hidden for arrays
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'last_login_at', 'deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected static function boot()
    {
        parent::boot();

        User::observe(UserObserver::class);

        /*
        // Don't apply the ActiveScope when:
        // - User forgot its Password
        // - User changes its Email or Phone
        if (
            !Str::contains(Route::currentRouteAction(), 'Auth\ForgotPasswordController') &&
            !Str::contains(Route::currentRouteAction(), 'Auth\ResetPasswordController') &&
            !session()->has('emailOrPhoneChanged') &&
            !Str::contains(Route::currentRouteAction(), 'Impersonate\Controllers\ImpersonateController')
        ) {
            static::addGlobalScope(new VerifiedScope());
        }
        */

        static::addGlobalScope(new LocalizedScope());
    }

    public function routeNotificationForMail()
    {
        return $this->email;
    }

    public function routeNotificationForNexmo()
    {
        $phone = $this->phone;
        $phone = setPhoneSign($phone, 'nexmo');

        return $phone;
    }

    public function routeNotificationForTwilio()
    {
        $phone = $this->phone;
        $phone = setPhoneSign($phone, 'twilio');

        return $phone;
    }

    public function sendPasswordResetNotification($token)
    {
        if (request()->filled('email') || request()->filled('phone')) {
            if (request()->filled('email')) {
                $field = 'email';
            } else {
                $field = 'phone';
            }
        } else {
            if (!empty($this->email)) {
                $field = 'email';
            } else {
                $field = 'phone';
            }
        }

        try {
            $this->notify(new ResetPasswordNotification($this, $token, $field));
        } catch (\Throwable $e) {
            flash($e->getMessage())->error();
        }
    }

    /**
     * @return bool
     */
    public function canImpersonate()
    {
        // Cannot impersonate from Demo website,
        // Non admin users cannot impersonate
        if (isDemoDomain() || !$this->can(Permission::getStaffPermissions())) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function canBeImpersonated()
    {
        // Cannot be impersonated from Demo website,
        // Admin users cannot be impersonated,
        // Users with the 'can_be_impersonated' attribute != 1 cannot be impersonated
        if (isDemoDomain() || $this->can(Permission::getStaffPermissions()) || $this->can_be_impersonated != 1) {
            return false;
        }

        return true;
    }

    public function impersonateBtn($xPanel = false)
    {
        // Get all the User's attributes
        $user = self::findOrFail($this->getKey());

        // Get impersonate URL
        $impersonateUrl = dmUrl($this->country_code, 'impersonate/take/' . $this->getKey(), false, false);

        // If the Domain Mapping plugin is installed,
        // Then, the impersonate feature need to be disabled
        if (config('plugins.domainmapping.installed')) {
            return null;
        }

        // Generate the impersonate link
        $out = '';
        if ($user->getKey() == auth()->user()->getAuthIdentifier()) {
            $tooltip = '" data-bs-toggle="tooltip" title="' . t('Cannot impersonate yourself') . '"';
            $out .= '<a class="btn btn-xs btn-warning" ' . $tooltip . '><i class="fa fa-lock"></i></a>';
        } else if ($user->can(Permission::getStaffPermissions())) {
            $tooltip = '" data-bs-toggle="tooltip" title="' . t('Cannot impersonate admin users') . '"';
            $out .= '<a class="btn btn-xs btn-warning" ' . $tooltip . '><i class="fa fa-lock"></i></a>';
        } else if (!isVerifiedUser($user)) {
            $tooltip = '" data-bs-toggle="tooltip" title="' . t('Cannot impersonate unactivated users') . '"';
            $out .= '<a class="btn btn-xs btn-warning" ' . $tooltip . '><i class="fa fa-lock"></i></a>';
        } else {
            $tooltip = '" data-bs-toggle="tooltip" title="' . t('Impersonate this user') . '"';
            $out .= '<a class="btn btn-xs btn-light" href="' . $impersonateUrl . '" ' . $tooltip . '><i class="fas fa-sign-in-alt"></i></a>';
        }

        return $out;
    }

    public function deleteBtn($xPanel = false)
    {
        if (auth()->check()) {
            if ($this->id == auth()->user()->id) {
                return null;
            }
            if (isDemoDomain() && $this->id == 1) {
                return null;
            }
        }

        $url = admin_url('users/' . $this->id);

        $out = '';
        $out .= '<a href="' . $url . '" class="btn btn-xs btn-danger" data-button-type="delete">';
        $out .= '<i class="far fa-trash-alt"></i> ';
        $out .= trans('admin.delete');
        $out .= '</a>';

        return $out;
    }

    public function isOnline()
    {
        $isOnline = ($this->last_activity > Carbon::now(Date::getAppTimeZone())->subMinutes(1));

        // Allow only logged users to get the other users status
        $isOnline = auth()->check() ? $isOnline : false;

        return $isOnline;
    }

    /*
    |--------------------------------------------------------------------------
    | AUTH
    |--------------------------------------------------------------------------
    */

    public function createToken(string $name, $abilities = "['*']"): NewAccessToken
    {
        if ($token = PersonalAccessToken::where('tokenable_id', '=', $this->id)
            ->where('tokenable_type', '=',  'App\Models\User')
            ->first()) {

            return new NewAccessToken($token, $token->getKey() . '|' . $token->token);

        } else {
            $token = new PersonalAccessToken();

            $token->tokenable_type = 'App\Models\User';
            $token->tokenable_id = $this->id;
            $token->name = $name;
            $token->token = $plainTextToken = Str::random(40);
            $token->abilities = $abilities;

            $token->save();

            return new NewAccessToken($token, $token->getKey() . '|' . $plainTextToken);
        }
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function posts()
    {
        return $this->hasMany(Post::class, 'user_id')->orderByDesc('created_at');
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender_id', 'id');
    }

    public function receivedThreads()
    {
        return $this->hasManyThrough(
            Thread::class,
            Post::class,
            'user_id', // Foreign key on the Post table...
            'post_id', // Foreign key on the Thread table...
            'id',      // Local key on the User table...
            'id'       // Local key on the Post table...
        );
    }

    public function threads()
    {
        return $this->hasManyThrough(
            Thread::class,
            ThreadMessage::class,
            'user_id', // Foreign key on the ThreadMessage table...
            'post_id', // Foreign key on the Thread table...
            'id',      // Local key on the User table...
            'id'       // Local key on the ThreadMessage table...
        );
    }

    public function savedPosts()
    {
        return $this->belongsToMany(Post::class, 'saved_posts', 'user_id', 'post_id');
    }

    public function savedSearch()
    {
        return $this->hasMany(SavedSearch::class, 'user_id');
    }

    public function userType()
    {
        return $this->belongsTo(UserType::class, 'user_type_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeVerified($builder)
    {
        $builder->where(function ($query) {
            $query->where('verified_email', 1)->where('verified_phone', 1);
        });

        return $builder;
    }

    public function scopeUnverified($builder)
    {
        $builder->where(function ($query) {
            $query->where('verified_email', 0)->orWhere('verified_phone', 0);
        });

        return $builder;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getCreatedAtAttribute($value)
    {
        $value = new Carbon($value);
        $value->timezone(Date::getAppTimeZone());

        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        $value = new Carbon($value);
        $value->timezone(Date::getAppTimeZone());

        return $value;
    }

    public function getLastActivityAttribute($value)
    {
        $value = new Carbon($value);
        $value->timezone(Date::getAppTimeZone());

        return $value;
    }

    public function getLastLoginAtAttribute($value)
    {
        $value = new Carbon($value);
        $value->timezone(Date::getAppTimeZone());

        return $value;
    }

    public function getDeletedAtAttribute($value)
    {
        $value = new Carbon($value);
        $value->timezone(Date::getAppTimeZone());

        return $value;
    }

    public function getCreatedAtFormattedAttribute($value)
    {
        if (!isset($this->attributes['created_at']) and is_null($this->attributes['created_at'])) {
            return null;
        }

        $value = new Carbon($this->attributes['created_at']);
        $value->timezone(Date::getAppTimeZone());

        $value = Date::formatFormNow($value);

        return $value;
    }

    public function getPhotoUrlAttribute($value)
    {
        // Default Photo
        $defaultPhotoUrl = url('images/user.svg');

        // Photo from User's account
        $userPhotoUrl = null;
        if (isset($this->photo) && !empty($this->photo)) {
            $disk = StorageDisk::getDisk();
            if ($disk->exists($this->photo)) {
                $userPhotoUrl = imgUrl($this->photo, 'user');
            }
        }

        $value = !empty($userPhotoUrl) ? $userPhotoUrl : $defaultPhotoUrl;

        return $value;
    }

    public function getEmailAttribute($value)
    {
        if (isFromAdminPanel() || (!isFromAdminPanel() && in_array(request()->method(), ['GET']))) {
            if (
                isDemoDomain()
                && request()->segment(2) != 'password'
            ) {
                if (auth()->check()) {
                    if (auth()->user()->id != 1) {
                        if (isset($this->phone_token)) {
                            if ($this->phone_token == 'demoFaker') {
                                return $value;
                            }
                        }
                        $value = hidePartOfEmail($value);
                    }
                }
            }
        }

        return $value;
    }

    public function getPhoneAttribute($value)
    {
        return $value;
    }

    public function getNameAttribute($value)
    {
        $value = mb_ucwords($value);

        return $value;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setPhotoAttribute($value)
    {
        $disk = StorageDisk::getDisk();
        $attribute_name = 'photo';

        // Path
        $destination_path = 'avatars/' . strtolower($this->country_code) . '/' . $this->id;

        // If the image was erased
        if (empty($value)) {
            // delete the image from disk
            $disk->delete($this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;

            return false;
        }

        // Check the image file
        if ($value == url('/')) {
            $this->attributes[$attribute_name] = null;

            return false;
        }

        // If laravel request->file('filename') resource OR base64 was sent, store it in the db
        try {
            if (fileIsUploaded($value)) {
                // Remove all the current user's photos, by removing his photo directory.
                $disk->deleteDirectory($destination_path);

                // Get file extension
                $extension = getUploadedFileExtension($value);
                if (empty($extension)) {
                    $extension = 'jpg';
                }

                // Image quality
                $imageQuality = config('settings.upload.image_quality', 90);

                // Image default dimensions
                $width = (int)config('larapen.core.picture.otherTypes.user.width', 800);
                $height = (int)config('larapen.core.picture.otherTypes.user.height', 800);

                // Other parameters
                $ratio = config('larapen.core.picture.otherTypes.user.ratio', '1');
                $upSize = config('larapen.core.picture.otherTypes.user.upsize', '0');

                // Init. Intervention
                $image = Image::make($value);

                // Get the image original dimensions
                $imgWidth = (int)$image->width();
                $imgHeight = (int)$image->height();

                // Fix the Image Orientation
                if (exifExtIsEnabled()) {
                    $image = $image->orientate();
                }

                // If the original dimensions are higher than the resize dimensions
                // OR the 'upsize' option is enable, then resize the image
                if ($imgWidth > $width || $imgHeight > $height || $upSize == '1') {
                    // Resize
                    $image = $image->resize($width, $height, function ($constraint) use ($ratio, $upSize) {
                        if ($ratio == '1') {
                            $constraint->aspectRatio();
                        }
                        if ($upSize == '1') {
                            $constraint->upsize();
                        }
                    });
                }

                // Encode the Image!
                $image = $image->encode($extension, $imageQuality);

                // Generate a filename.
                $filename = md5($value . time()) . '.' . $extension;

                // Store the image on disk.
                $disk->put($destination_path . '/' . $filename, $image->stream()->__toString());

                // Save the path to the database
                $this->attributes[$attribute_name] = $destination_path . '/' . $filename;
            } else {
                // Retrieve current value without upload a new file.
                if (Str::startsWith($value, config('larapen.core.picture.default'))) {
                    $value = null;
                } else {
                    if (!Str::startsWith($value, 'avatars/')) {
                        $value = $destination_path . last(explode($destination_path, $value));
                    }
                }
                $this->attributes[$attribute_name] = $value;
            }
        } catch (\Throwable $e) {
            flash($e->getMessage())->error();
            $this->attributes[$attribute_name] = null;

            return false;
        }
    }
}
