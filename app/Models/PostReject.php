<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Larapen\Admin\app\Models\Traits\Crud;

class PostReject extends BaseModel
{
    use Crud, HasFactory;

    protected $fillable = [
        'title',
        'description',
    ];
}
