<?php

namespace App\Enums;

class PostStatusEnum
{
    const MODERATION = 1;
    const REJECTED = 2;
    const PUBLISHED = 3;

    const VALUES = [
        self::REJECTED => 'Отклонить',
        self::PUBLISHED => 'Опубликовать',
    ];

    const STATUS_TITLE = [
        self::MODERATION => 'Объявление на обработке',
        self::REJECTED => 'Объявление отклонено',
        self::PUBLISHED => 'Объявление опубликовано'
    ];

    const STATUS_LIST = [ self::MODERATION, self::REJECTED, self::PUBLISHED ];
}