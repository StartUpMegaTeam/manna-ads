<?php

namespace App\Enums;

class AdsTypesEnum
{
    const WITH_PRICE = 1;
    const FREE = 2;

    public static function adsTypes()
    {
        return [
            self::WITH_PRICE => t('ads_with_price'),
            self::FREE => t('free_ads'),
        ];
    }
}