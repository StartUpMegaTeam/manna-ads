<?php

namespace App\Enums;

class CitiesOlxSlagEnum
{
    const berdyansk = 1;
    const mariupol = 2;
    const melitopol = 3;
    const severodonetsk = 4;
    const kakhovka = 5;

    const CITY = [
        self::berdyansk => 'berdyansk',
        self::mariupol => 'mariupol',
        self::melitopol => 'melitopol',
        self::severodonetsk => 'severodonetsk',
        self::kakhovka => 'kakhovka',

    ];

}
