<?php

namespace App\Enums;

class CategoryOlxUrlEnum
{
    const AUTO = 1;           // Авто 11 359
    const MODA = 2;           // Мода и стиль 49 922
    const ElECTRONIKA = 3;    // Электроника 35 995
    const HOME = 4;           // Дом и сад 35 569
    const CHILDREN_WORLD = 5; // Детский мир 52 861
    const SPORT = 6;          // Хобби, отдых и спорт 26 982
    const SPARES = 7;         // Запчасти для транспорта 24 191
    const NEDVIGEMOST = 8;    // Запчасти для транспорта 24 191
    const ANIMALS = 9;
    const WORK = 10;
    const USLUGi = 11;

    const URL = [
        self::AUTO => 'https://www.olx.ua/d/transport/',
        self::MODA => 'https://www.olx.ua/d/moda-i-stil/',
        self::ElECTRONIKA => 'https://www.olx.ua/d/elektronika/',
        self::HOME => 'https://www.olx.ua/d/moda-i-stil/',
        self::CHILDREN_WORLD => 'https://www.olx.ua/d/detskiy-mir/',
        self::SPORT => 'https://www.olx.ua/d/hobbi-otdyh-i-sport/',
        self::SPARES => 'https://www.olx.ua/d/zapchasti-dlya-transporta/',
        self::NEDVIGEMOST => 'https://www.olx.ua/d/nedvizhimost/',
        self::ANIMALS => 'https://www.olx.ua/d/zhivotnye/',
        self::WORK => 'https://www.olx.ua/d/rabota/',
        self::USLUGi => 'https://www.olx.ua/d/uslugi/',
    ];

}
