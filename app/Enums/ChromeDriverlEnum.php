<?php

namespace App\Enums;

class ChromeDriverlEnum
{
    const FIRST = 0;
    const SECOND = 1;
    const THIRD = 2;
    const FOURTH = 3;
    const FIFTH = 4;

    const URL = [
        self::FIRST => 'WEBDRIVER_CHROME_DRIVER=C:\Users\Rubillex\Desktop\chromedriver.exe',
        self::SECOND => 'WEBDRIVER_CHROME_DRIVER=C:\Users\Rubillex\Desktop\chromedrive.exe',
        self::THIRD => 'WEBDRIVER_CHROME_DRIVER=C:\Users\Rubillex\Desktop\chromedriv.exe',
        self::FOURTH => 'WEBDRIVER_CHROME_DRIVER=C:\Users\Rubillex\Desktop\chromedr.exe',
        self::FIFTH => 'WEBDRIVER_CHROME_DRIVER=C:\Users\Rubillex\Desktop\chromed.exe',
    ];

}
